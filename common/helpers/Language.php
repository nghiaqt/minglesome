<?php

namespace common\helpers;

/**
 *
 * @author Tuong Tran <tuong.tran@outlook.com>
 */
class Language {

  public static $languageCodes = [
    'da' => 'Danish',
    'de' => 'German',
    'en' => 'English',
    'es' => 'Spanish',
    'fr' => 'French',
    'it' => 'Italian',
    'nb' => 'Norwegian',
    'nl' => 'Dutch',
    'pt' => 'Portuguese',
    'sv' => 'Swedish'
  ];
  public static $currency = [
    'USD' => 'USD',
    'CAD' => 'CAD',
    'GBP' => 'GBP',
    'EUR' => 'EUR',
    'AUD' => 'AUD',
    'EUR' => 'EUR',
    'MXN' => 'MXN',
    'ARS' => 'ARS',
    'BRL' => 'BRL',
    'CHF' => 'CHF',
    'DKK' => 'DKK',
    'NOK' => 'NOK',
    'SEK' => 'SEK'
  ];
  public static $countryCurrency = [
    'USD' => 'USA',
    'CAD' => 'Canada',
    'GBP' => 'UK',
    'EUR' => 'Ireland',
    'AUD' => 'Australia',
    'EUR' => 'España, Portugal, Deutschland, Österreich, France, Italia, Nederland, België',
    'MXN' => 'Mexico',
    'ARS' => 'Argentina',
    'BRL' => 'Brazil',
    'CHF' => 'Schweiz',
    'DKK' => 'Danmark',
    'NOK' => 'Norge',
    'SEK' => 'Sverige',
  ];

}
