<?php

namespace common\helpers;

/**
 * manage date helper
 * 
 */
class DateHelper {

  /**
   * 
   * @param \MongoDate $date
   * @param type $format
   * @return date
   */
  public static function formatDate($date, $format = 'Y-m-d') {
    if ($date instanceof \MongoDate) {
      return date($format, $date->sec);
    } elseif (preg_match('/^(?:20|19)[0-9]{2}\-(?:0[1-9]|1[012])\-(?:0[1-9]|[12][0-9]|3[01])$/', $date)) {
      return date($format, strtotime($date));
    } else {
      return date($format);
    }
  }

  public static function convertTime($time) {
    if ($time > (60 * 60)) {
      $hour = floor($time / (60 * 60));
      $min = ($time / 60) % 60;
      $sec = $time % 60;
      return $hour . 'h ' . $min . 'm ' . $sec . 's';
    } elseif ($time > 60) {
      $min = ($time / 60) % 60;
      $sec = $time % 60;
      return $min . 'm ' . $sec . 's';
    } else {
      return $time . 's';
    }
  }

}
