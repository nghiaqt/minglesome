<?php

namespace common\helpers;

use \Yii;
use yii\web\Request;

/**
 * manage functions fot location like get coordinates ...
 */
class GeoLocation {

  /**
   * 
   * @return type
   */
  public static function getCountryByIp() {
    $request = new Request;
    $ip = $request->getUserIP();
    //check if server is localhost
    if ($ip !== '::1' && $ip !== '127.0.0.1') {
      $locationInfo = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
      if ($locationInfo) {
        $country = \common\models\Country::findOne([
              'countryCode' => strtolower($locationInfo->country)
        ]);
        if ($country) {
          return $country->countryCode;
        }
      }
      return 'us';
    } else {
      return 'us';
    }
  }

  /**
   * get coordinate based on ip address
   * @return array
   */
  public static function getCoordiatesByIp() {

    $request = new Request;
    $ip = $request->getUserIP();

    //check if server is localhost
    if ($ip !== '::1' && $ip !== '127.0.0.1') {
      $locationInfo = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
      $coordinates = explode(',', $locationInfo->loc);
      return [
          floatval($coordinates[1]), //longitude
          floatval($coordinates[0])//latitude			
      ];
    } else {
      return [
          106.637663,
          10.812493
      ];
    }
  }

  /**
   * get coordinates from google
   * @param type $address
   * @return mixed Array|Boolean
   */
  public static function getCoordiatesByAddress($address) {
    $address = urlencode($address);
    $url = "http://maps.googleapis.com/maps/api/geocode/json?address=$address";
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $curlData = curl_exec($curl);
    curl_close($curl);
    $geoInfo = json_decode($curlData);
    if ($geoInfo && $geoInfo->status === 'OK') {
      return [
          floatval($geoInfo->results[0]->geometry->location->lng),
          floatval($geoInfo->results[0]->geometry->location->lat)
      ];
    }
    return false;
  }

}
