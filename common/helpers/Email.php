<?php

namespace common\helpers;

use \Yii;
use common\models\EmailTemplate;
use common\models\Invite;
use common\models\User;
use common\models\Setting;
use common\models\Country;

/**
 * make the Send Mail function
 */
class Email {

  /**
   * send email
   * 
   * @param string $subject
   * @param string $body
   * @param string $receiver
   * @param string $sender
   */
  public static function send($subject, $body, $receiver, $sender = '') {
    if (!$receiver) {
      return false;
    }
    $setting = Setting::get('site');
    if (!$sender) {
      if (isset($setting['adminEmail']) && $setting['adminEmail'])
        $sender = $setting['adminEmail'];
      else
        $sender = 'admin@gmail.com';
    }
    $subject = str_replace(['[SITE_NAME]', '[URL]'], [$setting['title'], FRONTSITE_URL], $subject);
    $body = str_replace(['[SITE_NAME]', '[URL]'], [$setting['title'], FRONTSITE_URL], $body);
    return Yii::$app->mailer->compose()
        ->setFrom($sender)
        ->setTo($receiver)
        ->setSubject($subject)
        ->setHtmlBody($body)
        ->send();
  }

  /**
   * Active account Email Template
   * 
   * @param string $username
   * @param string $password
   * @param string $email
   * @param string $activeKey
   */
  public static function sendActiveKey($user,$country, $ios = false) {
    if ($ios) {
      $email = EmailTemplate::findOne(['code' => EmailTemplate::REGISTER_IOS]);
    } else {
      $email = EmailTemplate::findOne(['code' => EmailTemplate::REGISTER]);
    }
    if ($email) {
      $email->loadDataForCountry(Yii::$app->session['language']);

      $activeLink = 'http://' . $country . '.' . BASE_URL . '#/active-account/' . $user->activeKey;
      $subject = str_replace(['[NAME]'], [$user->username], $email->subject);
      $body = str_replace([
        '[NAME]',
        '[EMAIL]',
        '[LINK]',
        '[/LINK]',
        '[ACTIVE_LINK]'
        ], [
        $user->username,
        $user->email,
        '<a href="' . $activeLink . '">',
        '</a>',
        $activeLink
        ], $email->body);

      //check invite
      if (Yii::$app->session['invite']) {
        //add invite discount info to email
        $currency = Yii::$app->session['currency'];
        $invite = Invite::findOne(Yii::$app->session['invite']);
        if ($invite) {
          $owner = User::findOne($invite->owner);
          $invite->loadDataForCountry($currency);
          $invite->formatDate();
          $body = str_replace([
            '[DISCOUNT]',
            '[/DISCOUNT]',
            '[INVITE_CODE]',
            '[PRICE]',
            '[START_DATE]',
            '[END_DATE]'
            ], [
            '',
            '',
            $invite->code . $owner->userId,
            $currency . ' ' .
            $invite->amount,
            $invite->startDate,
            $invite->endDate
            ], $body);
        } else {
          $body = preg_replace('/(\[DISCOUNT\])(.*?)(\[\/DISCOUNT\])/ism', '', $body);
        }
      } else {
        //remove info discount
        $body = preg_replace('/(\[DISCOUNT\])(.*?)(\[\/DISCOUNT\])/ism', '', $body);
      }
      static::send($subject, $body, $user->email);
    }
  }

  /**
   * 
   * @param type $user
   * @param type $invoice
   * @param type $invite
   */
  public static function sendConfirmUpgrade($user, $invoice, $invite) {
    $email = EmailTemplate::findOne(['code' => EmailTemplate::UPGRADE]);
    if ($email) {
      $email->loadDataForCountry(Yii::$app->session['language']);
      $subject = str_replace(['[NAME]'], [$user->username], $email->subject);
      $body = str_replace([
        '[NAME]',
        '[EMAIL]',
        '[PAYMENT_PRICE]',
        '[EXPIRED_PREMIUM]'
        ], [
        $user->username,
        $user->email,
        $invoice->currency . ' ' . number_format($invoice->total, 2),
        date('Y-m-d', $user->expiredPremium->sec)
        ], $email->body);

      //check invite
      if ($invite) {
        //add invite discount info to email
        $currency = Yii::$app->session['currency'];
        $invite->loadDataForCountry($currency);
        $invite->formatDate();
        $body = str_replace([
          '[DISCOUNT]',
          '[/DISCOUNT]',
          '[INVITE_CODE]',
          '[PRICE]',
          '[START_DATE]',
          '[END_DATE]'
          ], [
          '',
          '',
          $user->inviteCode,
          $currency . ' ' . $invite->amount,
          $invite->startDate,
          $invite->endDate
          ], $body);
      } else {
        $body = preg_replace('/(\[DISCOUNT\])(.*?)(\[\/DISCOUNT\])/ism', '', $body);
      }
      static::send($subject, $body, $user->email);
    }
  }

  /**
   * 
   * @param type $user
   * @param type $invoice
   * @param type $invite
   */
  public static function sendConfirmFreeUpgrade($user, $invoice) {
    $email = EmailTemplate::findOne(['code' => EmailTemplate::UPGRADE_FREE]);
    if ($email) {
      $email->loadDataForCountry(Yii::$app->session['language']);
      $subject = str_replace(['[NAME]'], [$user->username], $email->subject);
      $body = str_replace([
        '[NAME]',
        '[EMAIL]',
        '[PAYMENT_PRICE]',
        '[EXPIRED_PREMIUM]'
        ], [
        $user->username,
        $user->email,
        $invoice->currency . ' ' . number_format($invoice->total, 2),
        date('Y-m-d', $user->expiredPremium->sec)
        ], $email->body);

      static::send($subject, $body, $user->email);
    }
  }

  /**
   * Send new password to user for Forgot Password function
   * 
   * @param string $username
   * @param string $password
   * @param string $email	 
   */
  public static function sendNewPassword($username, $password, $emailUser) {
    $email = EmailTemplate::findOne(['code' => EmailTemplate::FORGOT]);
    if ($email) {
      $email->loadDataForCountry(Yii::$app->session['language']);
      $subject = str_replace(['[NAME]'], [$username], $email->subject);
      $body = str_replace(
        array('[NAME]', '[NEWPASS]'), array($username, $password), $email->body);

      self::send($subject, $body, $emailUser);
    }
  }

  /**
   * Send email notify change new password to user
   * if email address was also changed. also send it to old email
   * @param object $user
   * @param string $password
   * @param string $email This value is pre email
   */
  public static function sendNotifyChangeNewPassword($user, $password, $oldEmail = '') {
    $email = EmailTemplate::findOne(['code' => EmailTemplate::CHANGE_PASSWORD]);
    if ($email) {
      $email->loadDataForCountry(Yii::$app->session['language']);
      $subject = str_replace(['[NAME]'], [$user->username], $email->subject);
      $body = str_replace([
        '[NAME]', '[NEWPASS]'
        ], [
        $user->username, $password
        ], $email->body);

      //send notify to user email
      self::send($subject, $body, $user->email);

      //send notify to the pre email
      if ($oldEmail) {
        self::send($subject, $body, $oldEmail);
      }
    }
  }

  /**
   * Send email notify change new email to both new & pre email
   * @param object $user
   * @param string $preEmail
   */
  public static function sendNotifyChangeEmail($user, $preEmail) {
    $email = EmailTemplate::findOne(['code' => EmailTemplate::CHANGE_EMAIL]);
    if ($email) {
      $email->loadDataForCountry(Yii::$app->session['language']);
      $subject = str_replace(['[NAME]'], [$user->username], $email->subject);
      $body = str_replace(
        [
        '[NAME]', '[OLD_EMAIL]', '[NEW_EMAIL]'
        ], [
        $user->username, $preEmail, $user->email
        ], $email->body);

      //send notify to user email
      self::send($subject, $body, $user->email);

      //send notify to the pre email
      self::send($subject, $body, $preEmail);
    }
  }

  /**
   * Send email active restore membership to email user
   * @param object $user
   */
  public static function sendEmailActiveRestoreMembership($user) {
    $email = EmailTemplate::findOne(['code' => EmailTemplate::RESTORE_ACCOUNT]);
    if ($email) {
      $email->loadDataForCountry(Yii::$app->session['language']);

      $subject = str_replace(['[NAME]'], [$user->username], $email->subject);
      $link = FRONTSITE_URL . '#/active-restore/' . $user->activeKey;
      $body = str_replace([
        '[NAME]', '[EMAIL]', '[LINK]', '[/LINK]', '[ACTIVE_LINK]'
        ], [
        $user->username, $user->email, '<a href="' . $link . '">', '</a>', $link
        ], $email->body);
      self::send($subject, $body, $user->email);
    }
  }

  /**
   * Send email notify membership has been cancelled
   * @param type $user
   */
  public static function sendNotifyCancelMembership($user) {
    $email = EmailTemplate::findOne(['code' => EmailTemplate::CANCEL_MEMBERSHIP]);
    if ($email) {
      $email->loadDataForCountry(Yii::$app->session['language']);
      $subject = str_replace(['[NAME]'], [$user->username], $email->subject);
      $body = str_replace([
        '[NAME]'
        ], [
        $user->username
        ], $email->body);
      self::send($subject, $body, $user->email);
    }
  }

  /**
   * Send new password to user for Forgot Password function
   * 
   * @param string $username
   * @param string $password
   * @param string $email	 
   */
  public static function sendWinnerEmail($user) {
    $language = 'en';
    if ($user->country) {
      $country = Country::findOne(['countryCode' => $user->country]);
      if ($country) {
        $language = $country->languageCode;
      }
    }
    $email = EmailTemplate::findOne(['code' => EmailTemplate::WINNER]);
    if ($email) {
      $email->loadDataForCountry($language);
      $subject = str_replace(['[NAME]'], [$user->username], $email->subject);
      $body = str_replace(
        ['[NAME]'], [$user->username], $email->body);

      self::send($subject, $body, $user->email);
    }
  }

  /**
   * Send email happy birthday to user
   * @param type $user
   */
  public static function sendBirthdayEmail($user) {
    $lang = 'en';
    $country = Country::findOne([
        'countryCode' => $user['country']
    ]);
    if ($country) {
      $lang = $country->languageCode;
    }
    $email = EmailTemplate::findOne(['code' => EmailTemplate::BIRTHDAY_EMAIL]);
    if ($email) {
      $email->loadDataForCountry($lang);

      $subject = str_replace(['[NAME]'], [$user['username']], $email->subject);
      $body = str_replace([
        '[NAME]', '[EMAIL]', '[AGE]'
        ], [
        $user['username'], $user['email'], $user['age']
        ], $email->body);
      self::send($subject, $body, $user['email']);
    }
  }

  /**
   * Send email notify user end 3 day trial
   * @param type $user
   */
  public static function sendNotifyEndTrial($user) {
    $lang = 'en';
    $country = Country::findOne([
        'countryCode' => $user['country']
    ]);
    if ($country) {
      $lang = $country->languageCode;
    }
    $email = EmailTemplate::findOne(['code' => EmailTemplate::END_TRIAL]);
    if ($email) {
      $email->loadDataForCountry($lang);

      $subject = str_replace(['[NAME]'], [$user['username']], $email->subject);
      $body = str_replace([
        '[NAME]'
        ], [
        $user['username']
        ], $email->body);
      self::send($subject, $body, $user['email']);
    }
  }

}
