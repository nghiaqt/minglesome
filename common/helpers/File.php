<?php

namespace common\helpers;

use \yii\web\UploadedFile;

/**
 * manage file helper
 *
 * @author Tuong Tran <tuong.tran@outlook.com>
 */
class File {

  /**
   * magazine upload file
   * @param type $file
   * @return type
   */
  public static function upload($file, $path, $required = false) {
    if (!$file) {
      return [
          'status' => false,
          'message' => $required ? 'File is required.' : ''
      ];
    }

    $ext = $file->getExtension();
    $fileName = $file->getBaseName();
    $image = ['jpg', 'jpeg', 'gif', 'png'];
    $video = ['mp4'];
    $whitelist = array_merge($image, $video);
    if (!in_array($ext, $whitelist)) {
      return [
          'status' => false,
          'message' => 'File type does not allow'
      ];
    }
    if (in_array($ext, $image)) {
      $type = 'image';
    } else {
      $type = 'video';
    }
    //TODO - set thumb
    $newName = md5(microtime()) . '.' . $ext;
    if ($file->saveAs($path . $newName)) {
      return [
          'status' => true,
          'name' => $newName,
          'fileName' => $fileName,
          'type' => $type,
          'mimeType' => $file->type,
          'size' => $file->size
      ];
    } else {
      return [
          'status' => false,
          'message' => 'Unknown error!'
      ];
    }
  }

  /**
   * magazine upload file
   * @param type $file
   * @return type
   */
  public static function uploadMagazine($file) {
    if (!$file) {
      return [
          'status' => false,
          'message' => ''
      ];
    }

    $ext = $file->getExtension();
    $fileName = $file->getBaseName();
    $image = ['jpg', 'jpeg', 'gif', 'png'];
    $video = ['mp4'];
    $whitelist = array_merge($image, $video);
    if (!in_array($ext, $whitelist)) {
      return [
          'status' => false,
          'message' => 'File type does not allow'
      ];
    }
    if (in_array($ext, $image)) {
      $type = 'image';
    } else {
      $type = 'video';
    }
    //TODO - set thumb
    $newName = md5(microtime()) . '.' . $ext;
    if ($file->saveAs(MAGAZINE_PATH . $newName)) {
      return [
          'status' => true,
          'name' => $newName,
          'fileName' => $fileName,
          'type' => $type,
          'mimeType' => $file->type,
          'size' => $file->size
      ];
    } else {
      return [
          'status' => false,
          'message' => 'Unknown error!'
      ];
    }
  }

  /**
   *
   * @param \yii\web\UploadedFile $instance
   */
  public static function uploadPhoto($fileField = 'file') {
    $file = UploadedFile::getInstanceByName($fileField);

    if (!$file) {
      return [
          'status' => false,
          'message' => 'File is required.'
      ];
    }

    $ext = $file->getExtension();
    $whitelist = ['jpg', 'jpeg', 'gif', 'png'];

    if (!in_array($ext, $whitelist)) {
      return [
          'status' => false,
          'message' => 'File type does not allow'
      ];
    }

    //TODO - set thumb
    $newName = md5(microtime()) . '.' . $ext;
    if ($file->saveAs(PHOTO_UPLOAD_PATH . $newName)) {
      return [
          'status' => true,
          'name' => $newName,
          'thumbnail' => $newName,
          'mimeType' => $file->type,
          'size' => $file->size
      ];
    } else {
      return [
          'status' => false,
          'message' => 'Unknown error!'
      ];
    }
  }

  /**
   * get file url from model
   *
   * @param common\models\Photo $model
   */
  public static function getPhotoUrl($model) {
    if (is_array($model)) {
      $source = $model['source'];
      $path = $model['path'];
    } elseif (is_object($model)) {
      $source = $model->source;
      $path = $model->path;
    }

    switch ($source) {
      case 'directly':
        return PHOTO_UPLOAD_URL . $path;
      default :
        //not support yet
        return null;
    }
  }

  public static function savePhotoFile($base64String) {
    $fileName = md5(microtime()) . '.png';
    $data = explode(',', $base64String);
    if (isset($data[1])) {
//      $ifp = fopen(PHOTO_UPLOAD_PATH . $fileName, "wb");
//      fwrite($ifp, base64_decode($data[1]));
//      fclose($ifp);
      file_put_contents(PHOTO_UPLOAD_PATH . $fileName, base64_decode($data[1]));
      return $fileName;
    }else{
      file_put_contents(PHOTO_UPLOAD_PATH . $fileName, base64_decode($base64String));
      return $fileName;
    }
  }

}