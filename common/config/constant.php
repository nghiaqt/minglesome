<?php

$baseUrl = isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] !== '127.0.0.1' ? 'minglesome.com' : 'minglesome.dev';
$host = 'http://api.' . $baseUrl . '/';
$frontSite = 'http://' . $baseUrl . '/';
$backSite = 'http://backend.' . $baseUrl . '/';
$nodeServer = 'http://' . $baseUrl . ':4000/';

define('PHOTO_UPLOAD_PATH', dirname(dirname(__DIR__)) . '/api/web/photos/');
define('PHOTO_UPLOAD_URL', $host . 'photos/');
define('API_URL', $host);
define('BASE_URL', $baseUrl);
define('FRONTSITE_URL', $frontSite);
define('BACKSITE_URL', $backSite);
define('NODE_SERVER_URL', $nodeServer);
define('ADMIN_CONTACT', '53d9ea6eddca15c81700003b');

define('MAGAZINE_PATH', dirname(dirname(__DIR__)) . '/api/web/magazine/');
define('MAGAZINE_URL', $host . 'magazine/');

define('BANNER_PATH', dirname(dirname(__DIR__)) . '/api/web/banner/');
define('BANNER_URL', $host . 'banner/');

//define status settings
define('STATUS_PENDING', 0);
define('STATUS_CONFIRMED', 1);
define('STATUS_CANCELLED', 2);

define('PAYMENT_STATUS_PENDING', 0);
define('PAYMENT_STATUS_CONFIRMED', 1);

if (!defined('ACCOUNT_KEY')) {
  define('ACCOUNT_KEY', 'Vs74raiT/lk1VfVeruSOsd0zXEdPxwegnpXYi9D0NBQ');
}
if (!defined('CACHE_DIRECTORY')) {
  define('CACHE_DIRECTORY', 'http://localhost/translate/cache/');
}
if (!defined('LANG_CACHE_FILE')) {
  define('LANG_CACHE_FILE', 'http://localhost/lang.cache');
}
if (!defined('ENABLE_CACHE')) {
  define('ENABLE_CACHE', true);
}
if (!defined('UNEXPECTED_ERROR')) {
  define('UNEXPECTED_ERROR', 'There is some un expected error . please check the code');
}
if (!defined('MISSING_ERROR')) {
  define('MISSING_ERROR', 'Missing Required Parameters ( Language or Text) in Request');
}
