<?php

namespace common\components;

/**
 * AccessRule.php
 *
 * @author phuc.nghiemtuc <phuc.nghiemtuc@gmail.com>
 * @copyright 2014 hoanvusolutions http://hoanvusolutions.com
 * @package Access Rule
 * @version 1.0
 */
class AccessRule extends \yii\filters\AccessRule {
  /*
   * self define role
   * overwrite matchRole
   * add rules to this role
   */

  protected function matchRole($user) {
    if (empty($this->roles)) {
      return true;
    }

    foreach ($this->roles as $role) {
      if ($role === '?' && $user->getIsGuest()) {
        return true;
      } elseif ($role === '@' && !$user->getIsGuest()) {
        return true;
      } elseif (!$user->getIsGuest()) {
        // user is not guest, let's check his role (or do something else)
        if ($role === $user->identity->level) {
          return true;
        }
      }
    }
    return false;
  }
}
