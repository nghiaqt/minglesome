<?php
namespace common\validators\types;

use yii\validators\Validator;

class IsArray extends Validator{
  public function validateAttribute($object, $attribute) {
    $attr = $object->{$attribute};
    if (!is_array($attr)) {      
      $this->addError($object, $attribute, 'should be an array');
    }
  }
}