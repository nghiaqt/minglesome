<?php

namespace common\validators;

use common\helpers\Language;
use yii\validators\Validator;

/**
 * validate language input
 * language input must be array
 * [
 *  'en' => 'this is text of english',
 *  'fr' => 'this is text of france'
 * ]
 *
 * @author Tuong Tran <tuong.tran@outlook.com>
 * @version 0.1
 */
class LanguagePack extends Validator {

  public $scenario;
  public $model;

  public function validateAttribute($object, $attribute) {
    $attr = $object->{$attribute};
    
    if (is_array($attr)) {
      $languages = Language::$languageCodes;
      foreach ($attr as $key => $value) {
        if (is_array($value)) {
          foreach ($value as $keyChild => $valueChild) {
            if (!array_key_exists($keyChild, $languages)) {
              $this->addError($object, $attribute, 'Language code incorrect');
            }
            //validate string
            if (!is_string($valueChild)) {
              $this->addError($object, $attribute, 'Content must be string');
            }
            break;
          }
        }
      }
    } else {
      $this->addError($object, $attribute, 'should be an array');
    }
  }

}
