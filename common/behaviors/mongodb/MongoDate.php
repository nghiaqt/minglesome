<?php

namespace common\behaviors\mongodb;

use yii\mongodb\ActiveRecord;

class MongoDate extends \yii\base\Behavior{

  public $attributes;

  public function events() {
    return [
      ActiveRecord::EVENT_BEFORE_INSERT => 'processDate',
      ActiveRecord::EVENT_BEFORE_UPDATE => 'processDate'
    ];
  }

  public function processDate($event) {
    if(!empty($this->attributes[$event->name])){
      foreach($this->attributes[$event->name] as $atribute){
        $this->owner->$atribute = new \MongoDate();
      }
    }
  }
}
