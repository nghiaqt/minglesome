<?php

namespace common\behaviors\mongodb;

use yii\mongodb\ActiveRecord;

/**
 * convert string to mongoId
 * @author Tuong Tran <tuong.tran@outlook.com>
 */
class MongoId extends \yii\base\Behavior {

  public $attributes;

  public function events() {
    return [
        ActiveRecord::EVENT_BEFORE_INSERT => 'processId',
        ActiveRecord::EVENT_BEFORE_UPDATE => 'processId'
    ];
  }

  public function processId($event) {
    if (!empty($this->attributes[$event->name])) {
      foreach ($this->attributes[$event->name] as $atribute) {
        if (!empty($this->owner->$atribute)) {
          if (is_array($this->owner->$atribute)) {
            $dataAttribute=[];
            foreach ($this->owner->$atribute as $key => $value) {
              if (is_string($value)) {
                $dataAttribute[$key] = new \MongoId($value);
              }else{
                $dataAttribute[$key]=$value;
              }
            }
            $this->owner->$atribute=$dataAttribute;
          } else if (is_string($this->owner->$atribute)) {
            $this->owner->$atribute = new \MongoId($this->owner->$atribute);
          }
        }
      }
    }
  }

}
