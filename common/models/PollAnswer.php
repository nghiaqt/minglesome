<?php

namespace common\models;
use yii\mongodb\ActiveRecord;

/**
 * PollAnswer model
 * @property \MongoId $_id
 * @property \MongoId $pollId
 * @property string $answer
 * @property \MongoId $createdBy
 * @property \MongoDate $createdAt
 * 
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class PollAnswer extends ActiveRecord{
  
  /**
   * @return string the name of the index associated with this ActiveRecord class.
   */
  public static function collectionName() {
    return 'pollAnswers';
  }

  /**
   * @return array list of attribute names.
   */
  public function attributes() {
    return [
        '_id', 'pollId', 'answer', 'createdBy','createdAt'
    ];
  }

  /**
   * define rule for poll answer
   * @return type
   */
  public function rules() {
    return [
        ['answer', 'string'],
        [['pollId', 'createdBy', 'createdAt'], 'safe']
    ];
  }

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'mongoDate' => [
            'class' => '\common\behaviors\mongodb\MongoDate',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt']
            ],
        ],
        'mongoId' => [
            'class' => '\common\behaviors\mongodb\MongoId',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdBy','pollId']
            ]
        ]
    ];
  }
}