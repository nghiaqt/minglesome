<?php

namespace common\models;

use yii\mongodb\ActiveRecord;

/**
 * Manager Conversation
 * @property \MongoId $_id
 * @property boolean $isPrivate
 * @property array $recipients
 * @property string $lastMessage
 * @property array $unreadUsers
 * @property array $deletedUsers List user has deleted conversation
 * @property \MongoId $createdBy
 * @property \MongoId $updatedBy
 * @property boolean $isConversation
 * @property string $subject is used for contact page
 * @property boolean $isReplied is used for contact page( to determine if admin replied this)
 * @property string $email is used for contact page, is the email of guest
 * @property boolean $isContact to determine if this conversation is contact
 * @property \MongoDate $createdAt
 * @property \MongoDate $updatedAt
 * @property boolean $isDeleted
 * 
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class Conversation extends ActiveRecord {

  public $__v;

  /**
   * @return string the name of the index associated with this ActiveRecord class.
   */
  public static function collectionName() {
    return 'conversation';
  }

  /**
   * ovewride parent scope to get available ...
   */
  public static function find() {
    return parent::find()->andWhere(['isDeleted'=>false]);
  }

  /**
   * @return array list of attribute names.
   */
  public function attributes() {
    return [
        '_id', 'recipients', 'createdBy', 'updatedBy', 'unreadUsers', 'deletedUsers',
        'isPrivate', 'isConversation', 'subject', 'isReplied', 'email', 'isContact',
        'lastMessage','isDeleted',
        'updatedAt', 'createdAt'
    ];
  }

  /**
   * define rule for invoice
   * @return type
   */
  public function rules() {
    return [
        [['recipients'], 'required'],
        [['isPrivate'], 'default', 'value' => true],
        [['isConversation', 'isReplied', 'isContact', 'isDeleted'], 'default', 'value' => false],
        [['isPrivate', 'isConversation', 'isContact', 'isDeleted'], 'in', 'range' => [true, false]],
        [['lastMessage', 'subject'], 'string'],
        ['email', 'email'],
        ['email', 'default', 'value' => ''],
        [['deletedUsers','unreadUsers'], 'default', 'value' => []],
        [['unreadUsers', 'deletedUsers', 'createdBy', 'updatedBy', 'createdAt', 'updatedAt'], 'safe']
    ];
  }

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'mongoDate' => [
            'class' => '\common\behaviors\mongodb\MongoDate',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt', 'updatedAt'],
                ActiveRecord::EVENT_BEFORE_UPDATE => ['updatedAt']
            ],
        ],
        'mongoId' => [
            'class' => '\common\behaviors\mongodb\MongoId',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['recipients', 'createdBy', 'updatedBy', 'unreadUsers'],
                ActiveRecord::EVENT_BEFORE_UPDATE => ['recipients', 'updatedBy', 'unreadUsers', 'deletedUsers'],
            ]
        ]
    ];
  }

}