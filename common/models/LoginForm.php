<?php
namespace common\models;

use yii\base\Model;

/**
 * Manage all activities for event
 * 
 * @property \MongoId $_id
 * @property string $name 
 * @property MongoDate $createdAt
 * @property MongoDate $updatedAt 
 * @property \MongoId $createdBy 
 */
class LoginForm  extends Model{
	//put your code here
	 public $username;
   public $password;
	 /**
		* @return array the validation rules.
		*/
	 public function rules(){
			 return [
					 // username and password are both required
					 [['username', 'password'], 'required'],
					 // password is validated by validatePassword()
					 ['password', 'validatePassword']					 
			 ];
	 }
	 /**
     * Validates the password.
     * This method serves as the inline validation for password.
     */
    public function validatePassword(){
        $user = User::findByUsername($this->username, true);
        if (!$user || !$user->validatePassword($this->password)) {
            $this->addError('password', 'Incorrect username or password.');
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login(){
        if ($this->validate()) {
            $user = User::findByUsername($this->username);
						Yii::$app->user->login($user, 0);												
            return true;
        } else {
            return false;
        }
    }
}

