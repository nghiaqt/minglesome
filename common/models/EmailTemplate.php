<?php

namespace common\models;

use yii\mongodb\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * Manager 
 * @property \MongoId $_id
 * @property string $code Define email
 * @property string $name
 * @property string $subject
 * @property string $body
 * @property \MongoId $createdBy
 * @property \MongoDate $createdAt
 * 
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class EmailTemplate extends ActiveRecord {

  const REGISTER = 'register';
  const REGISTER_IOS = 'iosregister';
  const UPGRADE = 'upgrade';
  const FORGOT='forgot';
  const CHANGE_PASSWORD='password';
  const CHANGE_EMAIL='email';
  const RESTORE_ACCOUNT='restore';
  const BIRTHDAY_EMAIL='birthday';
  const END_TRIAL='endtrial';
  const CANCEL_MEMBERSHIP='cancel';
  const WINNER='winner';
  const UPGRADE_FREE='upgrade_free';

  /**
   * @return string the name of the index associated with this ActiveRecord class.
   */
  public static function collectionName() {
    return 'emailTemplate';
  }

  /**
   * @return array list of attribute names.
   */
  public function attributes() {
    return [
        '_id', 'code', 'name', 'subject', 'body', 'createdBy', 'createdAt',
    ];
  }

  /**
   * define rule for invoice
   * @return type
   */
  public function rules() {
    return [
        [['code', 'name'], 'string'],
        [['subject', 'body'], 'common\validators\LanguagePack'],
        [['createdBy', 'createdAt'], 'safe']
    ];
  }

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'mongoDate' => [
            'class' => '\common\behaviors\mongodb\MongoDate',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt']
            ],
        ],
        'mongoId' => [
            'class' => '\common\behaviors\mongodb\MongoId',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdBy']
            ]
        ]
    ];
  }

  public function search($params) {
    $query = static::find();
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => array('pageSize' => 20)
    ]);

    if (!($this->load($params) && $this->validate())) {
      return $dataProvider;
    }

    $where = [];
    foreach ($this->getAttributes() as $key => $value) {
      if ($value) {
        if (is_bool($value)) {
          $where[$key] = (bool) $value;
        } elseif (is_string($value)) {
          $where[$key] = new \MongoRegex('/' . $value . '/i');
        }
      }
    }
    $query->where($where);

    return $dataProvider;
  }

  public function loadDataForCountry($lang = 'en') {
    if (!isset($this->subject[$lang]) || !$this->subject[$lang]) {
      $lang = 'en';
    }
    $this->subject = $this->subject[$lang];
    $this->body = $this->body[$lang];
  }

}