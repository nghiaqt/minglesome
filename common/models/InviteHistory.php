<?php

namespace common\models;

use yii\mongodb\ActiveRecord;

/**
 * Invite history
 *
 * @property \MongoId $_id
 * @property \MongoId $inviteId
 * @property string $inviteCode
 * @property int $premiums Qty user upgrade
 * @property \MongoId $owner
 * @property MongoDate $createdAt
 * 
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class InviteHistory extends ActiveRecord {

  /**
   * @return string the name of the index associated with this ActiveRecord class.
   */
  public static function collectionName() {
    return 'inviteHistory';
  }

  /**
   * @return array list of attribute names.
   */
  public function attributes() {
    return [
        '_id', 'inviteId', 'inviteCode', 'inviteCodeWithoutMemberId', 'premiums', 'createdAt', 'owner'
    ];
  }

  /**
   * define rule for invoice
   * @return type
   */
  public function rules() {
    return [
        ['premiums', 'number', 'integerOnly' => true],
        ['premiums', 'default', 'value' => 0],
        [['inviteCode'], 'string'],
        [['inviteId', 'owner', 'createdAt'], 'safe']
    ];
  }

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'mongoDate' => [
            'class' => '\common\behaviors\mongodb\MongoDate',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt']
            ],
        ],
        'mongoId' => [
            'class' => '\common\behaviors\mongodb\MongoId',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['owner', 'inviteId']
            ]
        ]
    ];
  }

}