<?php

namespace common\models;

use yii\mongodb\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * Manager 
 * @property \MongoId $_id
 * @property string $type
 * @property string $title
 * @property string $content
 * @property Boolean $status
 * @property \MongoId $createdBy
 * @property \MongoDate $createdAt
 * 
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class Page extends ActiveRecord {

  public $__v;

  /**
   * @return string the name of the index associated with this ActiveRecord class.
   */
  public static function collectionName() {
    return 'page';
  }

  /**
   * @return array list of attribute names.
   */
  public function attributes() {
    return [
        '_id', 'type', 'title', 'content', 'status', 'createdBy', 'createdAt',
    ];
  }

  /**
   * define rule for invoice
   * @return type
   */
  public function rules() {
    return [
        [['type'], 'string'],
        [['title', 'content', 'createdBy', 'createdAt'], 'safe']
    ];
  }

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'mongoDate' => [
            'class' => '\common\behaviors\mongodb\MongoDate',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt']
            ],
        ],
        'mongoId' => [
            'class' => '\common\behaviors\mongodb\MongoId',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdBy']
            ]
        ]
    ];
  }

  /**
   * 
   * @param type $params
   * @return \yii\data\ActiveDataProvider
   */
  public function search($params) {
    $query = static::find();
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => array('pageSize' => 20)
    ]);

    if (!($this->load($params) && $this->validate())) {
      return $dataProvider;
    }

    $where = [];
    foreach ($this->getAttributes() as $key => $value) {
      if ($value) {
        if (is_bool($value)) {
          $where[$key] = (bool) $value;
        } elseif (is_string($value)) {
          $where[$key] = new \MongoRegex('/' . $value . '/i');
        }
      }
    }
    $query->where($where);

    return $dataProvider;
  }

  public static function getPageByType($type, $lang = 'en') {
    $model = static::find()->where(['type' => $type])->asArray()->one();
    if ($model) {
      if (isset($model['title'][$lang])) {
        $model['title'] = $model['title'][$lang];
        $model['content'] = $model['content'][$lang];
      } else {
        $model['title'] = $model['title']['en'];
        $model['content'] = $model['content']['en'];
      }
    }
    return $model;
  }

}