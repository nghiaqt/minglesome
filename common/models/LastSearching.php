<?php

namespace common\models;

use yii\mongodb\ActiveRecord;

/**
 * manage last search of user
 * 
 * @property \MongoId $_id
 * @property \MongoId $createdBy
 * @property string	$username
 * @property array	$genders
 * @property array	$statuses
 * @property array	$orientations
 * @property int $ageFrom
 * @property int $ageTo
 * @property array $upfors
 * @property array $others
 * @property array $ethnicities
 * @property array $religions
 * @property array $figures
 * @property float $heightFrom
 * @property float $heightTo
 * @property string	$country
 * @property string	$state
 * @property string	$city
 * @property int $distance get user within this radius
 * @property array $speakings
 * @property array $styles
 * @property string	$keywords
 * @property string	$sortMode
 * @property boolean $offline
 * @property boolean $premium
 * @property boolean $picture
 * @property boolean $interesting
 * @property boolean $interested
 * @property boolean $dates
 * @property boolean $events
 * @property boolean $filter
 */
class LastSearching extends ActiveRecord {

  public static function collectionName() {
    return 'lastSearchings';
  }

  public function attributes() {
    return [
        '_id', 'createdBy', 'username', 'genders', 'statuses', 'orientations', 'ageFrom', 'ageTo',
        'upfors', 'ethnicities', 'religions', 'looks', 'heightFrom', 'heightTo', 'country', 'state',
        'city', 'distance', 'speakings', 'styles', 'keywords', 'sortMode', 'others', 'figures',
        'offline', 'premium', 'picture', 'interesting', 'interested', 'dates', 'events', 'filter'
    ];
  }

  public function rules() {
    return [
        ['createdBy', 'safe'],
        [['username', 'keywords', 'sortMode', 'country', 'state', 'city'], 'string'],
        [['ageFrom', 'ageTo', 'distance'], 'number', 'integerOnly' => true],
        [['offline', 'premium', 'picture', 'interesting', 'interested', 'dates', 'events', 'filter'],
            'in', 'range' => [true, false]],
        [['premium', 'picture', 'dates', 'events', 'offline', 'interesting', 'interested', 'filter'],
            'default', 'value' => false],
        [['genders', 'statuses', 'orientations', 'upfors', 'ethnicities', 'religions', 'looks',
        'speakings', 'styles', 'others', 'figures'], 'default', 'value' => []],
        [['heightFrom', 'heightTo'], 'default', 'value' => []],
        ['sortMode', 'default', 'value' => 'distance'],
        [['ageFrom', 'ageTo', 'country','state', 'city'], 'default', 'value' => ''],
        [['genders', 'statuses', 'orientations', 'upfors', 'ethnicities', 'religions', 'looks',
        'speakings', 'styles', 'others', 'figures', 'heightFrom', 'heightTo'],
            'common\validators\types\IsArray']
    ];
  }

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'mongoId' => [
            'class' => '\common\behaviors\mongodb\MongoId',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdBy']
            ]
        ]
    ];
  }
  
  public function loadDataAsArray($height, $distance) {
    $data = $this->toArray();
    if (isset($data['heightFrom']) && isset($data['heightFrom'][$height])) {
      $data['heightFrom'] = $data['heightFrom'][$height];
    } else {
      $data['heightFrom'] = '';
    }
    if (isset($data['heightTo']) && isset($data['heightTo'][$height])) {
      $data['heightTo'] = $data['heightTo'][$height];
    } else {
      $data['heightTo'] = '';
    }
    unset($data['_id']);
    unset($data['createdBy']);
    return $data;
  }

}