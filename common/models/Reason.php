<?php

namespace common\models;

use yii\mongodb\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * Manager Reason
 * @property \MongoId $_id
 * @property array $name
 * @property MongoDate $createdAt
 * @property MongoDate $updatedAt 
 * @property \MongoId $createdBy
 * @property string $type Enum('subscription','contact') default 'subscription'
 *
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class Reason extends ActiveRecord {

  /**
   * @return string the name of the index associated with this ActiveRecord class.
   */
  public static function collectionName() {
    return 'reasons';
  }

  /**
   * @return array list of attribute names.
   */
  public function attributes() {
    return [
        '_id', 'name', 'createdAt', 'updatedAt', 'createdBy', 'type'
    ];
  }

  /**
   * define rule for reason
   * @return type
   */
  public function rules() {
    return [
        ['name', 'common\validators\LanguagePack'],
        [['createdBy', 'createdAt', 'updatedAt'], 'safe'],
        ['type', 'string'],
        ['type', 'default', 'value' => 'subscription']
    ];
  }

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'mongoId' => [
            'class' => '\common\behaviors\mongodb\MongoId',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdBy', 'packageId']
            ]
        ],
        'mongoDate' => [
            'class' => '\common\behaviors\mongodb\MongoDate',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt', 'updatedAt'],
                ActiveRecord::EVENT_BEFORE_UPDATE => ['updatedAt']
            ]
        ]
    ];
  }

  /**
   * search data provider
   * @param type $params
   * @return \common\models\ActiveDataProvider
   */
  public function search($params) {
    $query = static::find();
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => array('pageSize' => 20)
    ]);

    $this->load($params);

    $where = [];
    foreach ($this->getAttributes() as $key => $value) {
      if ($value) {
        if ($key == 'name') {
          $key = 'name.en';
          $value = new \MongoRegex('/' . $value . '/i');
        } elseif (is_numeric($value)) {
          $value = (int) $value;
        } elseif (is_string($value)) {
          if ($value == 'true' || $value == 'false') {
            $value = $value == 'true' ? true : false;
          } else {
            $value = new \MongoRegex('/' . $value . '/i');
          }
        }
        $where[$key] = $value;
      }
    }
    $query->where($where);

    return $dataProvider;
  }

  /**
   * Get all data by language code
   * @param type $lang This value is language code
   * @return array [id=>name]
   */
  public static function getAllAsArray($lang = 'en', $type = 'subscription') {
    $return = [];
    $models = static::find()->where(['type' => $type])->all();
    foreach ($models as $model) {
      if (isset($model->name[$lang])) {
        $return[(string) $model->_id] = $model->name[$lang];
      }
    }
    return $return;
  }

}