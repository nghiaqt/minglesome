<?php

namespace common\models;

use yii\mongodb\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * Manage all translation text
 * @property \MongoId $_id
 * @property string $key to determine what words to translate
 * @property string $value is text translation 
 * 
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class TranslationForCountry extends ActiveRecord {

  /**
   * @return string the name of the index associated with this ActiveRecord class.
   */
  public static function collectionName() {
    return 'translationsForCountry';
  }

  /**
   * @return array list of attribute names.
   */
  public function attributes() {
    return [
      '_id', 'key', 'value'
    ];
  }

  /**
   * define rule for translation
   * @return type
   */
  public function rules() {
    return [
      [['key'], 'string'],
      ['value', 'safe']
    ];
  }

  /**
   * search data provider
   * @param type $params
   * @return \common\models\ActiveDataProvider
   */
  public function search($params) {
    $query = static::find();
    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => array('pageSize' => 20)
    ]);

    $this->load($params);

    $where = [];
    foreach ($this->getAttributes() as $key => $value) {
      if ($value) {
        if ($key == 'value') {
          $key = 'value.us';
          $value = new \MongoRegex('/' . $value . '/i');
        } else {
          $value = new \MongoRegex('/' . $value . '/i');
        }
        $where[$key] = $value;
      }
    }
    $query->where($where);

    return $dataProvider;
  }

  /**
   * Get all data by language code
   * @param type $country This value is language code
   * @return array [id=>name]
   */
  public static function getAllAsArray($country = 'us') {
    $models = static::find()->asArray()->all();
    $data = [];
    foreach ($models as $model) {
      if (isset($model['value'][$country])) {
        $data[$model['key']] = $model['value'][$country];
      } else {
        $data[$model['key']] = $model['value']['us'];
      }
    }
    return $data;
  }

}
