<?php

namespace common\models;

use yii\mongodb\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * Country model
 * 
 * @property \MongoId $_id
 * @property array $countryName
 * @property string $countryCode 
 * @property MongoDate $createdAt
 * @property MongoDate $updatedAt
 * @property string $languageCode This value is language for country
 * @property string $currency This value is currency for country
 * @property string $formatCurrency
 * @property string $height
 * @property string $distance
 * @property \MongoId $createdBy 
 * 
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class Country extends ActiveRecord {

  /**
   * @return string the name of the index associated with this ActiveRecord class.
   */
  public static function collectionName() {
    return 'countries';
  }

  /**
   * @return array list of attribute names.
   */
  public function attributes() {
    return [
        '_id', 'countryName', 'countryCode', 'createdAt', 'updatedAt', 'createdBy', 'languageCode',
        'currency', 'formatCurrency', 'height', 'distance'
    ];
  }

  /**
   * define rule for country
   * @return type
   */
  public function rules() {
    return [
        [['createdAt', 'updatedAt', 'createdBy'], 'safe'],
        //validate attributes    
        [['countryCode', 'languageCode', 'formatCurrency', 'currency', 'height', 'distance'],
            'string', 'min' => 2, 'max' => 255],
        ['countryName', 'common\validators\LanguagePack'],
        ['height', 'in', 'range' => ['cm', 'feet']],
        ['distance', 'in', 'range' => ['km', 'miles']],
        //add default value
        ['height', 'default', 'value' => 'cm'],
        ['distance', 'default', 'value' => 'km'],
    ];
  }

  public function attributeLabels() {
    return [
        'languageCode' => 'Language'
    ];
  }

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'mongoDate' => [
            'class' => '\common\behaviors\mongodb\MongoDate',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt', 'updatedAt'],
                ActiveRecord::EVENT_BEFORE_UPDATE => ['updatedAt'],
            ],
        ],
    ];
  }

  /**
   * search data provider
   * @param type $params
   * @return \common\models\ActiveDataProvider
   */
  public function search($params) {
    $query = static::find();
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => array('pageSize' => 20)
    ]);

    $this->load($params);

    $where = [];
    foreach ($this->getAttributes() as $key => $value) {
      if ($value) {
        if ($key == 'countryName') {
          $key = 'countryName.en';
          $value = new \MongoRegex('/' . $value . '/i');
        } elseif (is_numeric($value)) {
          $value = (int) $value;
        } elseif (is_string($value)) {
          if ($value == 'true' || $value == 'false') {
            $value = $value == 'true' ? true : false;
          } else {
            $value = new \MongoRegex('/' . $value . '/i');
          }
        }
        $where[$key] = $value;
      }
    }
    $query->where($where);

    return $dataProvider;
  }

  /**
   * Get all data by language code
   * @param type $lang This value is language code
   * @return array [countrycode=>country name]
   */
  public static function getAllAsArray($lang = 'en') {
    $return = [];
    $models = static::find()->all();
    foreach ($models as $model) {
      if (isset($model->countryName[$lang])) {
        $return[$model->countryCode] = $model->countryName[$lang];
      }
    }
    return $return;
  }

  public static function getHeight(){
    return ['cm'=>'cm','feet'=>'feet'];
  }
  
  public static function getDistance(){
    return ['km'=>'km','miles'=>'miles'];
  }
}
