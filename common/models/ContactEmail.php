<?php

namespace common\models;
use yii\mongodb\ActiveRecord;
/**
 * Manage all contact emails
 * 
 * @property \MongoId $_id
 * @property string $email
 * @property \MongoDate $createdAt
 * @property \MongoDate $updatedAt
 */
class ContactEmail extends ActiveRecord {
  /**
   * @return string the name of the index associated with this ActiveRecord class.
   */
  public static function collectionName() {
    return 'contactEmail';
  }
	/**
   * @return array list of attribute names.
   */
  public function attributes() {
    return [
        '_id', 'email', 'createdAt', 'updatedAt'
    ];
  }
	/**
   * define rule for invoice
   * @return type
   */
  public function rules() {
    return [        
        ['email', 'email'],
        [['createdAt', 'updatedAt'], 'safe']
    ];
  }
	 /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'mongoDate' => [
            'class' => '\common\behaviors\mongodb\MongoDate',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt', 'updatedAt'],
                ActiveRecord::EVENT_BEFORE_UPDATE => ['updatedAt']
            ],
        ]
    ];
  }
}

