<?php

namespace common\models;

use yii\mongodb\ActiveRecord;

/**
 * Ethnicity model
 * 
 * @property \MongoId $_id   
 * @property \MongoId $createdBy 
 * @property string $fileName 
 * @property boolean $isAvatar
 * @property string $description description for picture
 * @property string $comment comment for picture
 */
class Picture extends ActiveRecord {

  public static function collectionName() {
    return 'pictures';
  }

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'mongoDate' => [
            'class' => '\common\behaviors\mongodb\MongoDate',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt']
            ],
        ],
        'mongoId' => [
            'class' => '\common\behaviors\mongodb\MongoId',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdBy']
            ]
        ]
    ];
  }

  public function attributes() {
    return [
        '_id', 'createdBy', 'fileName', 'isAvatar', 'description', 'comment', 'createdAt'
    ];
  }

  public function rules() {
    return [
        ['isAvatar', 'default', 'value' => false],
        ['isAvatar', 'in', 'range' => [true, false]],
        [['fileName', 'description', 'comment'], 'string'],
        [['createdBy', 'createdAt'], 'safe']
    ];
  }

}

