<?php

namespace common\models;

use yii\mongodb\ActiveRecord;

/**
 * Manage all Reply of event 
 *
 * @property \MongoId $_id
 * @property \MongoId $createdBy
 * @property \MongoId $dateEventId determine what event will be replied
 * @property string $text
 * @property boolean $unread
 * @property MongoDate $createdAt
 * @property \MongoDate $onDate
 * @property \MongoId $authorEvent
 * @property boolean $isDeleted
 */
class DateEventReply extends ActiveRecord {

  public $__v;

  public static function collectionName() {
    return 'dateEventReplies';
  }

  /**
   * ovewride parent scope to get available ...
   */
  public static function find() {
    return parent::find()->andWhere(['isDeleted'=>false]);
  }

  public function attributes() {
    return [
      '_id', 'createdBy', 'dateEventId', 'text', 'unread', 'createdAt',
      'updatedAt', 'onDate', 'authorEvent', 'isDeleted'
    ];
  }

  public function rules() {
    return [
      [['unread', 'isDeleted'], 'in', 'range' => [true, false]],
      ['unread', 'default', 'value' => true],
      ['isDeleted', 'default', 'value' => false],
      ['text', 'string', 'min' => 2, 'max' => 255],
      [['createdAt', 'updatedAt', 'createdBy', 'dateEventId', 'onDate', 'authorEvent'], 'safe']
    ];
  }

  public function behaviors() {
    return [
      'mongoDate' => [
        'class' => '\common\behaviors\mongodb\MongoDate',
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt', 'updatedAt'],
          ActiveRecord::EVENT_BEFORE_UPDATE => ['updatedAt'],
        ],
      ],
      'mongoId' => [
        'class' => '\common\behaviors\mongodb\MongoId',
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['createdBy', 'dateEventId', 'authorEvent'],
          ActiveRecord::EVENT_BEFORE_UPDATE => ['dateEventId', 'authorEvent']
        ]
      ]
    ];
  }

  /**
   * Get all date event of user
   * @param \MongoId $userId
   * @param string $lang
   * @return array
   */
  public static function getAllAsArray($dateEventId, $ownerReply, $lang = 'en') {
    $query = DateEventReply::find()->where([
      'dateEventId' => new \MongoId($dateEventId)
    ]);
    if ($ownerReply) {
      $query->andWhere(['createdBy' => $ownerReply]);
    }
    $dateReplies = $query->asArray()->all();
    foreach ($dateReplies as &$dateReply) {
      $user = User::findOne([
            '_id' => $dateReply['createdBy']
      ]);
      $dateReply['replierName'] = $user->username;
    }
    return $dateReplies;
  }

}
