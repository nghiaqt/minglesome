<?php

namespace common\models;

use \Yii;
use yii\base\NotSupportedException;
use yii\mongodb\ActiveRecord;
use yii\web\IdentityInterface;
use yii\data\ActiveDataProvider;
use common\helpers\Email;

/**
 * Model is used for manage all attributes of user
 *
 * @property \MongoId $_id
 * @property int $userId Auto increment
 * @property string $username
 * @property string $password write-only password
 * @property string $passwordResetToken
 * @property string $authKey
 * @property string $email
 * @property string $emailVerified
 * @property MongoDate $createdAt
 * @property MongoDate $updatedAt
 * @property array $coordinates contain longitude and latitude
 * @property array $coordinatesByAddress Description
 * @property MongoDate $birthdate
 * @property int $age
 * @property string $country
 * @property array $countryName
 * @property string $state
 * @property string $stateName Description
 * @property string $city
 * @property string $gender 
 * @property float $height
 * @property \MongoId $orientation
 * @property array $styles contain object id
 * @property \MongoId $figure
 * @property \MongoId $ethnicity
 * @property \MongoId $religion
 * @property array $upfors contain object id
 * @property array $others contain object id
 * @property array $speakings contain object id
 * @property array $keywords
 * @property array $prompts
 * @property string $baggage
 * @property string $inMyOwnWords 
 * @property array $statusMarried
 * @property string $preferThings things which user prefer in admirerers
 * @property string $notlikeThings things which users don't like in admirerers
 * @property string $statusComment like status of yahoo or skype
 * @property string $avatar
 * @property string $avatarUrl
 * @property \MongoDate $expiredPremium
 * @property \MongoDate $expiredDelayCancel
 * @property int $activeToken token is used for finding user to active account
 * @property \MogoId $questionId is question in the Edit Profile
 * @property string $answer is answer in the Edit Profile
 * @property string $activeKey
 * @property boolean $isLogged determine if user logged in  
 * @property \MongoDate $lastSignedOn to determine 
 * @property \MongoDate $lastActivity to determine 
 * @property boolean $isCurrentAdmin to determine what is current admin( for contact purpose )
 * @property int $status enum(1: new, 2:activated, 3:compensated, 4: warned, 5:banned, 6: deleted)
 * @property int $level enum(1: free, 2:trial, 3:premium, 99:supper admin)
 * @property string $inviteCode Create by admin (code + userId)
 * @property boolean $isAffiliate Create by admin. add when user click affiliate link
 * @property int $leads Qty of visitors this member has generated with his invite code (number of visitors invited by this member)
 * @property int $premiums Number of premium upgrades invited by this member. (we will have awards/contest for who invited most people that upgraded to premium)
 * @property int $invitedBy Member ID of the member who invited this member to the site. (if a member invited this member, if not, then leave blank)
 * @property int $reported Qty of times this member has been reported by other members
 * @property int $blocked Qty of times this member has been blocked
 * @property int $messages Qty of messages sent by this member
 * @property int $events Qty of events/dates created by this member
 * @property int $streaming Qty of time this member has streamed his cam
 * @property int $photos Qty of photos uploaded in this member’s profile
 * @property int $wins Qty of times this member has won a contest on our site
 * @property int $polls Qty of polls this member has answered on our site
 * @property int $comments Qty of comments this member has posted in articles on our magazine page
 */
class User extends ActiveRecord implements IdentityInterface {

  /** const level user */
  const LEVEL_FREE = 1;
  const LEVEL_TRIAL = 2;
  const LEVEL_PREMIUM = 3;
  const LEVEL_ADMIN = 99;

  /** const status user */
  const STATUS_NEW = 1;
  const STATUS_ACTIVATED = 2;
  const STATUS_COMPENSATED = 3;
  const STATUS_WARNED = 4;
  const STATUS_BANNED = 5;
  const STATUS_DELETED = 6;
  const NOT_CURRENT_ADMIN = false;
  const IS_CURRENT_ADMIN = true;

  /** define flag */
  public $newPassword = '';
  public $isNewAddress = false;
  public $curStatus;

  public static function collectionName() {
    return 'users';
  }

  public static function create($attributes) {
    /** @var User $user */
    $user = new static();
    $user->setAttributes($attributes);
    $user->setPassword($attributes['password']);
    $user->generateAuthKey();

    if ($user->save()) {
      return $user;
    } else {
      return null;
    }
  }

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
      'mongoDate' => [
        'class' => '\common\behaviors\mongodb\MongoDate',
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => [
            'createdAt', 'updatedAt', 'expiredPremium', 'lastActivity'],
          ActiveRecord::EVENT_BEFORE_UPDATE => ['updatedAt', 'lastActivity'],
        ],
      ],
      'mongoId' => [
        'class' => '\common\behaviors\mongodb\MongoId',
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_UPDATE => [
            'avatar', 'ethnicity', 'figure', 'orientation', 'questionId',
            'religion', 'others', 'speakings', 'styles', 'upfors'
          ]
        ]
      ]
    ];
  }

  //put your code here
  public static function findIdentity($id) {
    return static::findOne($id);
  }

  /**
   * @inheritdoc
   */
  public static function findIdentityByAccessToken($token, $type = null) {
    throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
  }

  /**
   * Finds user by password reset token
   *
   * @param  string      $token password reset token
   * @return static|null
   */
  public static function findByPasswordResetToken($token) {
    $expire = \Yii::$app->params['user.passwordResetTokenExpire'];
    $parts = explode('_', $token);
    $timestamp = (int) end($parts);
    if ($timestamp + $expire < time()) {
      // token expired
      return null;
    }

    return static::findOne([
        'password_reset_token' => $token,
        'emailVerified' => true,
    ]);
  }

  /**
   * Finds user by username
   *
   * @param  string      $username
   * @return static|null
   */
  public static function findByUsername($username, $isAdmin = false) {
    $where = ['username' => $username];
    if ($isAdmin) {
      $where = array_merge($where, ['level' => self::LEVEL_ADMIN]);
    }
    return static::findOne($where);
  }

  /**
   * Finds user by username
   *
   * @param  string      $username
   * @return static|null
   */
  public static function findByEmail($email) {
    return static::findOne(['email' => $email]);
  }

  public function getId() {
    return (string) $this->_id;
  }

  public function getAuthKey() {
    return $this->authKey;
  }

  public function validateAuthKey($authKey) {
    return $this->getAuthKey() === $authKey;
  }

  /**
   * Validates password
   *
   * @param  string  $password password to validate
   * @return boolean if password provided is valid for current user
   */
  public function validatePassword($password) {
    return Yii::$app->getSecurity()->validatePassword($password, $this->password);
  }

  /**
   * Generates password hash from password and sets it to the model
   *
   * @param string $password
   */
  public function setPassword($password) {
    $this->password = Yii::$app->getSecurity()->generatePasswordHash($password);
  }

  /**
   * Generates "remember me" authentication key
   */
  public function generateAuthKey() {
    $this->authKey = Yii::$app->getSecurity()->generateRandomString() . '_' . time();
  }

  /**
   * Generates new password reset token
   */
  public function generatePasswordResetToken() {
    $this->passwordResetToken = Yii::$app->getSecurity()->generateRandomString() . '_' . time();
  }

  /**
   * Removes password reset token
   */
  public function removePasswordResetToken() {
    $this->passwordResetToken = null;
  }

  /**
   * @return array list of attribute names.
   */
  public function attributes() {
    return [
      '_id', 'userId', 'username', 'email', 'emailVerified', 'status', 'level',
      'password', 'passwordResetToken', 'authKey', 'createdAt', 'updatedAt',
      'coordinates', 'coordinatesByAddress', 'country', 'countryName', 'state',
      'city', 'stateName', 'religion', 'upfors', 'prompts',
      'gender', 'height', 'orientation', 'styles', 'figure', 'ethnicity',
      'others', 'speakings', 'birthdate', 'age', 'keywords', 'baggage',
      'inMyOwnWords', 'avatar', 'avatarUrl',
      'statusMarried', 'preferThings', 'notlikeThings', 'statusComment',
      'activeToken', 'lastActivity',
      'questionId', 'answer', 'activeKey', 'isLogged', 'lastSignedOn',
      'expiredPremium', 'expiredDelayCancel', 'isCurrentAdmin', 'isAffiliate',
      'inviteCode', 'invitedBy', 'leads', 'premiums', 'reported', 'blocked',
      'messages', 'events', 'streaming', 'photos', 'wins', 'polls', 'comments',
      'isNewAddress'
    ];
  }

  public function rules() {
    return [
      //require field
      [['username', 'email'], 'required'],
      [['password', 'birthdate', 'gender'], 'required', 'on' => 'insert'],
      [['username', 'email'], 'filter', 'filter' => 'trim'],
      //validators fields
      ['email', 'email'],
      [['email', 'userId'], 'unique'],
      [['username', 'inviteCode', 'preferThings', 'notlikeThings', 'statusComment',
        'passwordResetToken', 'authKey', 'country', 'state', 'city', 'activeToken',
        'newPassword', 'statusMarried', 'gender', 'stateName'],
        'string', 'min' => 2, 'max' => 255],
      ['password', 'string', 'min' => 6],
      [['baggage', 'inMyOwnWords', 'avatarUrl', 'answer', 'activeKey'], 'string'],
      //integer
      [['age', 'level', 'status', 'leads', 'premiums', 'reported', 'blocked',
        'messages', 'events', 'streaming', 'photos', 'wins', 'polls', 'comments',
        'invitedBy', 'userId'], 'number', 'integerOnly' => true],
      //float
      [['age', 'level', 'status', 'leads', 'premiums', 'reported', 'blocked',
        'messages', 'events', 'streaming', 'photos', 'wins', 'polls', 'comments',
        'invitedBy', 'userId', 'height'], 'converNumber'],
      [['isLogged', 'isCurrentAdmin', 'emailVerified', 'isAffiliate'],
        'in', 'range' => [true, false]],
      [['avatar', 'createdAt', 'updatedAt', 'orientation', 'figure', 'religion', 
        'ethnicity', 'birthdate', 'questionId', 'countryName', 'lastSignedOn', 
        'expiredPremium', 'expiredDelayCancel', 'lastActivity'], 'safe'],
      [['styles', 'others', 'speakings', 'upfors', 'coordinates', 
        'coordinatesByAddress', 'keywords', 'prompts'],
        'common\validators\types\IsArray'],
      [['expiredPremium', 'expiredDelayCancel', 'birthdate'], 'dateValidator'],
      //set default value
      [['username', 'inviteCode', 'preferThings', 'notlikeThings', 'statusComment',
        'passwordResetToken', 'authKey', 'country', 'state', 'city', 'activeToken',
        'newPassword', 'statusMarried', 'gender', 'stateName',
        'baggage', 'inMyOwnWords', 'avatarUrl', 'answer', 'activeKey'],
        'default', 'value' => ''],
      [['styles', 'others', 'speakings', 'upfors', 'coordinates',
        'coordinatesByAddress', 'keywords','prompts'],
        'default', 'value' => []],
      [['isLogged', 'isCurrentAdmin', 'emailVerified', 'isAffiliate'],
        'default', 'value' => false],
      ['level', 'default', 'value' => static::LEVEL_TRIAL],
      ['status', 'default', 'value' => static::STATUS_NEW],
      [['age', 'level', 'status', 'leads', 'premiums', 'reported', 'blocked',
        'messages', 'events', 'streaming', 'photos', 'wins', 'polls', 'comments',
        'invitedBy', 'userId'], 'default', 'value' => 0],
    ];
  }

  /**
   * Convert string to int for mongodb
   * @param type $attr
   * @param type $params
   */
  public function converNumber($attr, $params) {
    if ($this->{$attr}) {
      if (is_array($this->{$attr})) {
        $data = [];
        foreach ($this->{$attr} as $k => $v) {
          $data[$k] = (string) $v;
        }
        $this->{$attr} = $data;
      } else {
        $this->{$attr} = (int) $this->{$attr};
      }
    }
  }

  /**
   * validate date format
   * convert date to mongo date
   * @param type $attr
   */
  public function dateValidator($attr) {
    if (!$this->getErrors() && !$this->{$attr} instanceof \MongoDate) {
      if (!preg_match('/^(?:20|19)[0-9]{2}\-(?:0[1-9]|1[012])\-(?:0[1-9]|[12][0-9]|3[01])$/', $this->{$attr})) {
        $this->addError($attr, 'Date format must be yyyy-mm-dd');
      } else {
        $this->{$attr} = new \MongoDate(strtotime($this->{$attr}));
      }
    }
  }

  /**
   * Edit attribute label
   * @return type
   */
  public function attributeLabels() {
    return [
      'userId' => '#',
      'createdAt' => 'Joined',
      'username' => 'User'
    ];
  }

  /**
   * Set auto increment id for user
   * @param type $insert
   * @return boolean
   */
  public function beforeSave($insert) {
    if (parent::beforeSave($insert)) {
      if ($this->isNewRecord) {
        $user = static::find()
          ->orderBy(['userId' => SORT_DESC])
          ->one();
        if ($user) {
          $this->userId = $user->userId + 1;
        } else {
          $this->userId = 1;
        }
      }
      if ($this->birthdate) {
        $this->age = date('Y') - date('Y', $this->birthdate->sec);
      }
      if ($this->isNewAddress) {
        $state = State::findOne($this->state);
        if ($state) {
          $this->stateName = $state->name;
        }
        $address = $this->city . ' ' . $this->state . ' ' . $this->country;
        if (($coordinates = \common\helpers\GeoLocation::getCoordiatesByAddress($address))) {
          $this->coordinatesByAddress = [
            'type' => 'Point',
            'coordinates' => $coordinates
          ];
        } else {
          $this->coordinatesByAddress = $this->coordinates;
        }
      }
      if ($this->curStatus !== $this->status) {
        if ($this->status === static::STATUS_DELETED || $this->status === static::STATUS_BANNED) {
          $this->cancelMemberShip(true);
        } elseif ($this->curStatus === static::STATUS_DELETED || $this->curStatus === static::STATUS_BANNED) {
          $this->cancelMemberShip(false);
        }
      }
      return true;
    }
    return false;
  }

  /**
   * 
   * @param type $cancel
   */
  private function cancelMemberShip($cancel) {
    //delete all conversation & chat conversation
    Conversation::updateAll(['isDeleted' => $cancel], [
      'isPrivate' => true,
      'recipients' => ['$in' => [$this->_id]]
    ]);
    DateEventReply::updateAll(['isDeleted' => $cancel], [
      'createdBy' => $this->_id
    ]);
    //delete all date event & reply event
    DateEvent::updateAll(['isDeleted' => $cancel], ['createdBy' => $this->_id]);
    //delete all interest
    Interest::updateAll(['isDeleted' => $cancel], ['$or' => [
        ['createdBy' => $this->_id],
        ['interestedUser' => $this->_id]
    ]]);
    Visitor::updateAll(['isDeleted' => $cancel], ['$or' => [
        ['createdBy' => $this->_id],
        ['viewed' => $this->_id]
    ]]);
  }

  /**
   * delete all data of user
   * @return boolean
   */
  public function beforeDelete() {
    if (parent::beforeDelete()) {
      //delete all conversation & chat conversation
      $conversations = Conversation::findAll([
          'isPrivate' => true,
          'recipients' => ['$in' => [$this->_id]]
      ]);
      foreach ($conversations as $conversation) {
        ChatConversation::deleteAll(['conversationId' => $conversation->_id]);
        $conversation->delete();
      }
      //delete all date event & reply event
      $events = DateEvent::findAll(['createdBy' => $this->_id]);
      foreach ($events as $event) {
        DateEventReply::deleteAll(['dateEventId' => $event->_id]);
        $event->delete();
      }
      //delete all interest
      Interest::deleteAll(['$or' => [
          ['createdBy' => $this->_id],
          ['interestedUser' => $this->_id]
      ]]);
      Visitor::deleteAll(['$or' => [
          ['createdBy' => $this->_id],
          ['viewed' => $this->_id]
      ]]);
      BlockUser::deleteAll(['$or' => [
          ['blocker' => $this->_id],
          ['blockedUser' => $this->_id]
      ]]);
      ReportUser::deleteAll(['$or' => [
          ['createdBy' => $this->_id],
          ['reportedUser' => $this->_id]
      ]]);
      return true;
    }
    return false;
  }

  public function afterFind() {
    $this->curStatus = $this->status;
    parent::afterFind();
  }

  /**
   * 
   * @param type $params
   * @return \yii\data\ActiveDataProvider
   */
  public function search($params) {
//    var_dump($params);die();
    $query = static::find();
    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort' => ['defaultOrder' => ['createdAt' => SORT_DESC]],
      'pagination' => array('pageSize' => 20)
    ]);

    if (!($this->load($params))) {
      return $dataProvider;
    }

    $where = [];
    foreach ($this->getAttributes() as $key => $value) {
      if ($value != '') {
        if (is_numeric($value)) {
          $value = (int) $value;
        } elseif (is_string($value)) {
          if ($value == 'true' || $value == 'false') {
            $value = $value == 'true' ? true : false;
          } else {
            $value = new \MongoRegex('/' . $value . '/i');
          }
        }
        $where[$key] = $value;
      }
    }
    $query->where($where);

    return $dataProvider;
  }

  /**
   * define status
   */
  public static function getStatus() {
    return [
      static::STATUS_NEW => 'new',
      static::STATUS_ACTIVATED => 'checked',
      static::STATUS_COMPENSATED => 'compensated',
      static::STATUS_WARNED => 'warned',
      static::STATUS_BANNED => 'banned',
      static::STATUS_DELETED => 'deleted',
    ];
  }

  public function getStatusText() {
    switch ($this->status) {
      case static::STATUS_NEW:
        return 'new';
      case static::STATUS_ACTIVATED:
        return 'checked';
      case static::STATUS_COMPENSATED:
        return 'compensated';
      case static::STATUS_WARNED:
        return 'warned';
      case static::STATUS_BANNED:
        return 'banned';
      case static::STATUS_DELETED:
        return 'deleted';
      default :
        return 'undefine';
    }
  }

  /**
   * define level
   */
  public static function getLevel() {
    return [
      static::LEVEL_FREE => 'free',
      static::LEVEL_TRIAL => 'trial',
      static::LEVEL_PREMIUM => 'premium',
      static::LEVEL_ADMIN => 'admin',
    ];
  }

  public function getLevelText() {
    switch ($this->level) {
      case static::LEVEL_FREE:
        return 'free';
      case static::LEVEL_TRIAL:
        return 'trial';
      case static::LEVEL_PREMIUM:
        return 'premium';
      case static::LEVEL_ADMIN:
        return 'admin';
      default :
        return 'undefine';
    }
  }

  /**
   * get all genders
   */
  public static function getGenders() {
    $genderDatas = [
      'GENDER_WOMAN' => 'woman',
      'GENDER_MAN' => 'man',
      'GENDER_COUPLE' => 'couple',
      'GENDER_GROUP' => 'group'
    ];
    return $genderDatas;
  }

  public static function getGendersCode() {
    return [
      'GENDER_WOMAN' => 'GENDER_WOMAN',
      'GENDER_MAN' => 'GENDER_MAN',
      'GENDER_COUPLE' => 'GENDER_COUPLE',
      'GENDER_GROUP' => 'GENDER_GROUP'
    ];
  }

  public function getGenderText() {
    switch ($this->gender) {
      case 'GENDER_WOMAN':
        return 'woman';
      case 'GENDER_MAN':
        return 'man';
      case 'GENDER_COUPLE':
        return 'couple';
      case 'GENDER_GROUP':
        return 'group';
      default :
        return 'undefine';
    }
  }

  /**
   * get all status married
   */
  public static function getStatusMarried() {
    $statusMarriedDatas = [
      'STATUSMARRIED_SINGLE' => 'single',
      'STATUSMARRIED_NOT_SINGLE' => 'not single',
      'STATUSMARRIED_MIXED' => 'mixed'
    ];
    return $statusMarriedDatas;
  }

  public static function getStatusMarriedCode() {
    return [
      'STATUSMARRIED_SINGLE' => 'STATUSMARRIED_SINGLE',
      'STATUSMARRIED_NOT_SINGLE' => 'STATUSMARRIED_NOT_SINGLE',
      'STATUSMARRIED_MIXED' => 'STATUSMARRIED_MIXED'
    ];
  }

  /**
   * get avatar url
   * @return type
   */
  public function getAvatarUrl() {
    if ($this->avatarUrl) {
      $return = PHOTO_UPLOAD_URL . $this->avatarUrl;
    } else {
      if ($this->gender == 'GENDER_WOMAN') {
        $return = PHOTO_UPLOAD_URL . 'default_female.png';
      } else {
        $return = PHOTO_UPLOAD_URL . 'default_male.png';
      }
    }
    return $return;
  }

  /**
   * convert time streaming to hour min secon
   * @return type
   */
  public function getStreaming() {
    if ($this->streaming) {
      $time = floor($this->streaming / 1000);
      return \common\helpers\DateHelper::convertTime($time);
    }
  }

  /**
   * get all data profile of user
   * @param type $lang
   * @return string
   */
  public function getFullPublicData($lang = 'en', $heightType = 'feet') {

    $userData = $this->getAttributes();
    foreach ($userData as $attrName => $attrValue) {
      if (is_object($attrValue)) {
        if ($attrValue instanceof \MongoId) {
          $userData[$attrName] = (string) $attrValue;
        } elseif ($attrValue instanceof \MongoDate) {
          $userData[$attrName] = $attrValue->sec * 1000;
        }
      } else if (is_array($attrValue)) {
        $mongoIds = $attrValue;
        foreach ($mongoIds as $key => $value) {
          if ($value instanceof \MongoId) {
            $userData[$attrName][$key] = (string) $value;
          } elseif ($value instanceof \MongoDate) {
            $userData[$attrName][$key] = $value->sec * 1000;
          }
        }
      }
    }

    //get user's country
    if ($userData['country']) {
      $country = Country::findOne(['countryCode' => $userData['country']]);
      if ($country) {
        if (isset($country->countryName[$lang])) {
          $userData['countryName'] = $country->countryName[$lang];
        } else {
          $userData['countryName'] = $country->countryName['en'];
        }
      }
    }

    //get orientation
    if ($userData['orientation']) {
      $orientation = ProfileContent::findOne(['_id' => new \MongoId($userData['orientation'])]);
      if ($orientation) {
        if (isset($orientation->name[$lang])) {
          $userData['orientationName'] = $orientation->name[$lang];
        } else {
          $userData['orientationName'] = $orientation->name['en'];
        }
      }
    }

    //get styles
    if ($userData['styles']) {
      $userData['styleNames'] = [];
      foreach ($userData['styles'] as $styleItem) {
        $style = ProfileContent::findOne(['_id' => new \MongoId($styleItem)]);
        if ($style) {
          if (isset($style->name[$lang])) {
            $userData['styleNames'][] = $style->name[$lang];
          } else {
            $userData['styleNames'][] = $style->name['en'];
          }
        }
      }
    }

    //get figure		
    if ($userData['figure']) {
      $figure = ProfileContent::findOne(['_id' => new \MongoId($userData['figure'])]);
      if ($figure) {
        if (isset($figure->name[$lang])) {
          $userData['figureName'] = $figure->name[$lang];
        } else {
          $userData['figureName'] = $figure->name['en'];
        }
      }
    }

    //get ethnicity		
    if ($userData['ethnicity']) {
      $ethnicity = ProfileContent::findOne(['_id' => new \MongoId($userData['ethnicity'])]);
      if ($ethnicity) {
        if (isset($ethnicity->name[$lang])) {
          $userData['ethnicityName'] = $ethnicity->name[$lang];
        } else {
          $userData['ethnicityName'] = $ethnicity->name['en'];
        }
      }
    }

    //get religion		
    if ($userData['religion']) {
      $ethnicity = ProfileContent::findOne(['_id' => new \MongoId($userData['religion'])]);
      if ($ethnicity) {
        if (isset($ethnicity->name[$lang])) {
          $userData['religionName'] = $ethnicity->name[$lang];
        } else {
          $userData['religionName'] = $ethnicity->name['en'];
        }
      }
    }

    //get upfors
    if ($userData['upfors']) {
      $userData['upforNames'] = [];
      foreach ($userData['upfors'] as $upforItem) {
        $upfor = ProfileContent::findOne(['_id' => new \MongoId($upforItem)]);
        if ($upfor) {
          if (isset($upfor->name[$lang])) {
            $userData['upforNames'][] = $upfor->name[$lang];
          } else {
            $userData['upforNames'][] = $upfor->name['en'];
          }
        }
      }
    }

    //get others
    if ($userData['others']) {
      $userData['otherNames'] = [];
      foreach ($userData['others'] as $otherItem) {
        $other = ProfileContent::findOne(['_id' => new \MongoId($otherItem)]);
        if ($other) {
          if (isset($other->name[$lang])) {
            $userData['otherNames'][] = $other->name[$lang];
          } else {
            $userData['otherNames'][] = $other->name['en'];
          }
        }
      }
    }

    //get speakings
    if ($userData['speakings']) {
      $userData['speakingNames'] = [];
      foreach ($userData['speakings'] as $speakingItem) {
        $speaking = ProfileContent::findOne(['_id' => new \MongoId($speakingItem)]);
        if ($speaking) {
          if (isset($speaking->name[$lang])) {
            $userData['speakingNames'][] = $speaking->name[$lang];
          } else {
            $userData['speakingNames'][] = $speaking->name['en'];
          }
        }
      }
    }

    //get religion		
    if ($userData['questionId']) {
      $question = ProfileContent::findOne(['_id' => new \MongoId($userData['questionId'])]);
      if ($question) {
        if (isset($question->name[$lang])) {
          $userData['questionName'] = $question->name[$lang];
        } else {
          $userData['questionName'] = $question->name['en'];
        }
      }
    }

    if ($userData['height'] && isset($userData['height'][$heightType])) {
      $userData['height'] = $userData['height'][$heightType];
    } else {
      $userData['height'] = '';
    }

    if (isset($userData['avatarUrl']) && $userData['avatarUrl']) {
      $userData['avatarUrl'] = PHOTO_UPLOAD_URL . $userData['avatarUrl'];
    } else {
      if ($userData['gender'] == 'GENDER_WOMAN') {
        $userData['avatarUrl'] = PHOTO_UPLOAD_URL . 'default_female.png';
      } else {
        $userData['avatarUrl'] = PHOTO_UPLOAD_URL . 'default_male.png';
      }
    }

    if (!isset($userData['keywords'])) {
      $userData['keywords'] = [];
    }

    $private = [
      'password', 'passwordResetToken', 'authKey', 'activeToken',
      'emailVerified', 'expiredPremium', 'expiredDelayCancel', 'inviteCode',
      'isCurrentAdmin',
      'createdAt', 'updatedAt'
    ];
    foreach ($private as $item) {
      unset($userData[$item]);
    }

    return $userData;
  }

  /**
   * get public data of current user such as username, email...
   * @return array
   */
  public function getPublicData($lang = 'en', $viewer = '') {
    $userData = $this->getAttributes();

    //get user's country
    if ($userData['country']) {
      $country = Country::findOne(['countryCode' => $userData['country']]);
      if ($country) {
        if (isset($country->countryName[$lang])) {
          $userData['countryName'] = $country->countryName[$lang];
        } else {
          $userData['countryName'] = $country->countryName['en'];
        }
      }
    }

    if (isset($userData['avatarUrl']) && $userData['avatarUrl']) {
      $userData['avatarUrl'] = PHOTO_UPLOAD_URL . $userData['avatarUrl'];
    } else {
      if ($userData['gender'] == 'GENDER_WOMAN') {
        $userData['avatarUrl'] = PHOTO_UPLOAD_URL . 'default_female.png';
      } else {
        $userData['avatarUrl'] = PHOTO_UPLOAD_URL . 'default_male.png';
      }
    }

    $publics = [
      '_id', 'username', 'avatarUrl', 'country', 'countryName', 'email', 'level', 'city', 'age',
      'preferThings', 'notlikeThings', 'statusComment', 'isLogged', 'lastActivity'
    ];

    $return = [];
    foreach ($publics as $item) {
      $return[$item] = $userData[$item];
    }

    if ($viewer) {
      $key = KeyUser::findOne(['keyUser' => $viewer->_id, 'createdBy' => $this->_id]);
      //check privacy filter
      if (!$key) {
        $privacyFilter = PrivacyFilter::findOne([
            'createdBy' => $this->_id
        ]);
        //check if user match with Privacy filter
        if ($privacyFilter) {
          $return['isMatchPrivacyFilter'] = $privacyFilter->isMatchPrivacyFilter($viewer);
        }
      } else {
        $return['isMatchPrivacyFilter'] = false;
      }
    } else {
      $return['isMatchPrivacyFilter'] = true;
    }

    return $return;
  }

  /**
   * 
   * @return type
   */
  public static function getAllAsArray() {
    $users = static::find()->all();
    $return = [];
    foreach ($users as $user) {
      $return[(string) $user->_id] = $user->username;
    }
    return $return;
  }

  public function setWinner() {
    $this->wins+=1;
    Email::sendWinnerEmail($this);
  }

}
