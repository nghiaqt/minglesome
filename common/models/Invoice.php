<?php

namespace common\models;

use yii\mongodb\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * Manager all invoices
 * @property \MongoId $_id
 * @property \MongoId $packageId
 * @property \MongoId $createdBy
 * @property string $email User email
 * @property string $packageName
 * @property integer $packageMonth
 * @property string $currency
 * @property string $amount
 * @property string $inviteCode Invite code user enter
 * @property string $inviteId Invite form affiliate url
 * @property integer $total
 * @property string $token
 * @property string $params Payment params of paypal
 * @property integer $isPayment
 * @property boolean $isRecurring
 * @property integer $createAt
 * @property integer $updateAt
 * 
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class Invoice extends ActiveRecord {

  /**
   * @return string the name of the index associated with this ActiveRecord class.
   */
  public static function collectionName() {
    return 'invoices';
  }

  /**
   * @return array list of attribute names.
   */
  public function attributes() {
    return [
        '_id', 'email', 'packageId','packageName', 'packageMonth',
        'currency', 'amount', 'inviteCode', 'total', 'isRecurring', 'inviteId',
        'token', 'params', 'isPayment', 'createdAt', 'updatedAt', 'createdBy'
    ];
  }

  /**
   * define rule for invoice
   * @return type
   */
  public function rules() {
    return [
        [['packageId','currency','total','createdBy'], 'required'],
        [['isPayment', 'isRecurring'], 'default', 'value' => false],
        [['isPayment', 'isRecurring'], 'in', 'range' => [true,false]],
        [['packageMonth'], 'number', 'integerOnly' => true],
        ['amount','converNumber'],
        [['email','packageName', 'currency', 'inviteCode', 'token'],'string'],
        [['amount', 'params', 'total', 'packageId', 'inviteId', 'createdBy', 
          'createdAt', 'updatedAt'], 'safe']
    ];
  }
  
  
  /**
   * Convert string to int for mongodb
   * @param type $attr
   * @param type $params
   */
  public function converNumber($attr, $params) {
    if ($this->{$attr}) {
      $this->{$attr} = (float) $this->{$attr};
    }
  }
  
  public function attributeLabels() {
    return [
        'amount'=>'Package Amount'
    ];
  }

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'mongoDate' => [
            'class' => '\common\behaviors\mongodb\MongoDate',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt', 'updatedAt'],
                ActiveRecord::EVENT_BEFORE_UPDATE => ['updatedAt'],
            ],
        ],
        'mongoId' => [
            'class' => '\common\behaviors\mongodb\MongoId',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['packageId', 'inviteId', 'createdBy']
            ]
        ]
    ];
  }

  /**
   * search data provider
   * @param type $params
   * @return \common\models\ActiveDataProvider
   */
  public function search($params) {
    $query = static::find();
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => array('pageSize' => 20)
    ]);

    $this->load($params);
    $where = [];
    foreach ($this->getAttributes() as $key => $value) {
      if ($value!='') {
        if (is_numeric($value)) {
          $value = (int) $value;
        } elseif (is_string($value)) {
          if ($value == 'true' || $value == 'false') {
            $value = $value == 'true' ? true : false;
          } else {
            $value = new \MongoRegex('/' . $value . '/i');
          }
        }
        $where[$key] = $value;
      }
    }
    $query->where($where);

    return $dataProvider;
  }

}