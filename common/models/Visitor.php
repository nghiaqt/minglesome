<?php

namespace common\models;

use \Yii;
use yii\mongodb\ActiveRecord;

/**
 * Model is used for manage all user visitor
 * @property \MongoId $_id
 * @property \MongoId $viewed
 * @property \MongoId $createdBy
 * @property \MongoDate $createdAt
 * @property \MongoDate $updatedAt
 * @property string $lastMessage
 * @property boolean $unread
 * @property boolean $isDeleted
 * 
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class Visitor extends ActiveRecord {

  public $__v;

  /**
   * @return string the name of the index associated with this ActiveRecord class.
   */
  public static function collectionName() {
    return 'visitor';
  }

  /**
   * ovewride parent scope to get available ...
   */
  public static function find() {
    return parent::find()->andWhere(['isDeleted'=>false]);
  }

  /**
   * @return array list of attribute names.
   */
  public function attributes() {
    return [
        '_id', 'viewed','lastMessage', 'unread',
        'createdAt', 'updatedAt', 'createdBy', 'isDeleted'
    ];
  }

  /**
   * define rule for invoice
   * @return type
   */
  public function rules() {
    return [
        [['unread','isDeleted'],'in','range'=>[true,false]],
        ['unread','default','value'=>true],
        ['isDeleted','default','value'=>false],
        [['createdBy', 'viewed'], 'required'],
        [['lastMessage'], 'string'],
        [['createdBy', 'viewed', 'createdAt', 'updatedAt'], 'safe']
    ];
  }

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'mongoDate' => [
            'class' => '\common\behaviors\mongodb\MongoDate',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt','updatedAt'],
                ActiveRecord::EVENT_BEFORE_UPDATE => ['updatedAt']
            ],
        ],
        'mongoId' => [
            'class' => '\common\behaviors\mongodb\MongoId',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['viewed', 'createdBy']
            ]
        ]
    ];
  }

}