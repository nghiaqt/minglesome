<?php

namespace common\models;

use yii\mongodb\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * Manager states
 * @property \MongoId $_id
 * @property string $countryCode
 * @property string $name
 * @property \MongoId $createdBy
 * @property \MongoDate $createdAt
 *
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class State extends ActiveRecord {

  /**
   * @return string the name of the index associated with this ActiveRecord class.
   */
  public static function collectionName() {
    return 'states';
  }

  /**
   * @return array list of attribute names.
   */
  public function attributes() {
    return [
        '_id', 'countryCode', 'name', 'createdBy', 'createdAt'
    ];
  }

  /**
   * define rule for invoice
   * @return type
   */
  public function rules() {
    return [
        [['countryCode', 'name'], 'string'],
        [['createdBy', 'createdAt'], 'safe']
    ];
  }

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'mongoDate' => [
            'class' => '\common\behaviors\mongodb\MongoDate',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt']
            ],
        ],
        'mongoId' => [
            'class' => '\common\behaviors\mongodb\MongoId',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdBy']
            ]
        ]
    ];
  }

  public function search($params) {
    $query = static::find();
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => array('pageSize' => 20)
    ]);

    if (!($this->load($params) && $this->validate())) {
      return $dataProvider;
    }

    $where = [];
    foreach ($this->getAttributes() as $key => $value) {
      if ($value) {
        if (is_bool($value)) {
          $where[$key] = (bool) $value;
        } elseif (is_string($value)) {
          $where[$key] = new \MongoRegex('/' . $value . '/i');
        }
      }
    }
    $query->where($where);

    return $dataProvider;
  }

  /**
   * Get all data by language code
   * @param type $lang This value is language code
   * @return array [countrycode=>country name]
   */
  public static function getAllAsArray($countryCode) {
    $return=[];
    if(!$countryCode){
      return $return;
    }
    $models = static::find()->where(['countryCode' => $countryCode])->all();
    foreach ($models as $model){
      $return[(string)$model->_id]=$model->name;
    }
    return $return;
  }

}