<?php

namespace common\models;

use yii\mongodb\ActiveRecord;

/**
 * Description of Interest
 *
 * @property \MongoId $_id
 * @property \MongoId $createdBy
 * @property \MongoId $interestedUser
 * @property \MongoDate $createdAt
 * @property boolean $unread
 * @property boolean $isInterest
 * @property boolean $isDeleted
 * @author tuancasi
 */
class Interest extends ActiveRecord {

  public static function collectionName() {
    return 'interests';
  }

  /**
   * ovewride parent scope to get available ...
   */
  public static function find() {
    return parent::find()->andWhere(['isDeleted'=>false]);
  }

  public function attributes() {
    return [
        '_id', 'createdBy', 'interestedUser', 'createdAt', 'unread', 'isInterest', 'isDeleted'
    ];
  }

  public function rules() {
    return [
        [['unread', 'isInterest', 'isDeleted'], 'in', 'range' => [true, false]],
        [['unread', 'isInterest'], 'default', 'value' => true],
        ['isDeleted', 'default', 'value' => false],
        [['interestedUser', 'createdAt', 'createdBy'], 'safe']
    ];
  }
  
  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'mongoDate' => [
            'class' => '\common\behaviors\mongodb\MongoDate',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt'],
                ActiveRecord::EVENT_BEFORE_UPDATE => ['createdAt']
            ],
        ],
        'mongoId' => [
            'class' => '\common\behaviors\mongodb\MongoId',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdBy', 'interestedUser']
            ]
        ]
    ];
  }

}
