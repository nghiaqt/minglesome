<?php

namespace common\models;

use yii\mongodb\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * Manager User Feedback
 * @property \MongoId $_id
 * @property array $reasons
 * @property string $feedback
 * @property string $email
 * @property string $type
 * @property MongoDate $createdAt
 * @property MongoDate $updatedAt 
 * @property \MongoId $createdBy
 * 
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class UserFeedback extends ActiveRecord {

  const TYPE_SUB = 'subscription';
  const TYPE_MEM = 'membership';

  /**
   * @return string the name of the index associated with this ActiveRecord class.
   */
  public static function collectionName() {
    return 'userFeedback';
  }

  /**
   * @return array list of attribute names.
   */
  public function attributes() {
    return [
        '_id', 'reasons', 'feedback', 'email', 'type', 'createdAt', 'updatedAt', 'createdBy'
    ];
  }

  /**
   * define rule for invoice
   * @return type
   */
  public function rules() {
    return [
        ['feedback', 'string'],
        ['email', 'email'],
        ['type', 'in', 'range' => ['subscription', 'membership']],
        ['reasons', 'common\validators\types\IsArray'],
        [['createdBy', 'createdAt', 'updatedAt'], 'safe']
    ];
  }

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'mongoId' => [
            'class' => '\common\behaviors\mongodb\MongoId',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdBy', 'reasons']
            ]
        ],
        'mongoDate' => [
            'class' => '\common\behaviors\mongodb\MongoDate',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt', 'updatedAt'],
                ActiveRecord::EVENT_BEFORE_UPDATE => ['updatedAt']
            ]
        ]
    ];
  }

  /**
   * search data provider
   * @param type $params
   * @return \common\models\ActiveDataProvider
   */
  public function search($params) {
    $query = static::find();
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => array('pageSize' => 20)
    ]);

    $this->load($params);
    $where = [];
    foreach ($this->getAttributes() as $key => $value) {
      if ($value != '') {
        if (is_numeric($value)) {
          $value = (int) $value;
        } elseif (is_string($value)) {
          if ($value == 'true' || $value == 'false') {
            $value = $value == 'true' ? true : false;
          } else {
            $value = new \MongoRegex('/' . $value . '/i');
          }
        }
        $where[$key] = $value;
      }
    }
    $query->where($where);

    return $dataProvider;
  }

  public function getReasons() {
    if ($this->reasons) {
      $reasons = Reason::findAll(['_id' => ['$in' => $this->reasons]]);
      return $reasons;
    }
  }

  /**
   * Get type define
   * @return array
   */
  public static function getType() {
    return [
        static::TYPE_MEM => 'cancel membership',
        static::TYPE_SUB => 'stop subscription'
    ];
  }

}