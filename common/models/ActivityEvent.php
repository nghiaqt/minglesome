<?php

namespace common\models;

use yii\mongodb\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * Manage all activities for event
 * 
 * @property \MongoId $_id
 * @property string $name 
 * @property MongoDate $createdAt
 * @property MongoDate $updatedAt 
 * @property \MongoId $createdBy 
 */
class ActivityEvent extends ActiveRecord {

  public static function collectionName() {
    return 'activityEvents';
  }

  public function attributes() {
    return [
        '_id', 'name', 'createdAt', 'updatedAt', 'createdBy'
    ];
  }

  public function rules() {
    return [
        ['name', 'common\validators\LanguagePack'],
        [['createdAt', 'updatedAt', 'createdBy'], 'safe']
    ];
  }

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'mongoDate' => [
            'class' => '\common\behaviors\mongodb\MongoDate',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt', 'updatedAt'],
                ActiveRecord::EVENT_BEFORE_UPDATE => ['updatedAt'],
            ],
        ],
    ];
  }

  /**
   * search data provider
   * @param type $params
   * @return \common\models\ActiveDataProvider
   */
  public function search($params) {
    $query = static::find();
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => array('pageSize' => 20)
    ]);

    $this->load($params);

    $where = [];
    foreach ($this->getAttributes() as $key => $value) {
      if ($value) {
        if ($key == 'name') {
          $key = 'name.en';
          $value = new \MongoRegex('/' . $value . '/i');
        } elseif (is_numeric($value)) {
          $value = (int) $value;
        } elseif (is_string($value)) {
          if ($value == 'true' || $value == 'false') {
            $value = $value == 'true' ? true : false;
          } else {
            $value = new \MongoRegex('/' . $value . '/i');
          }
        }
        $where[$key] = $value;
      }
    }
    $query->where($where);

    return $dataProvider;
  }

  /**
   * Get all data by language code
   * @param type $lang This value is language code
   * @return array [id=>name]
   */
  public static function getAllAsArray($lang = 'en') {
    $return = [];
    $models = static::find()->all();
    foreach ($models as $model) {
      if (isset($model->name[$lang])) {
        $return[(string) $model->_id] = $model->name[$lang];
      }
    }
    return $return;
  }

  /**
   * Get activity event name by primary key
   * @param type $id
   * @param type $lang
   */
  public static function getNameById($id, $lang = 'en') {
    $name = '';
    $model = static::findOne($id);
    if ($model) {
      if (isset($model->name[$lang])) {
        $name = $model->name[$lang];
      } elseif (isset($model->name['en'])) {
        $name = $model->name['en'];
      }
    }
    return $name;
  }

}
