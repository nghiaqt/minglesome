<?php

namespace common\models;

use yii\mongodb\ActiveRecord;

/**
 * Manage blocking users
 *
 * @property \MongoId $_id
 * @property \MongoId $blocker is user block the others
 * @property \MongoId $blockedUser is users are blocked by the others
 * @property \MongoDate $createdAt
 */
class BlockUser extends ActiveRecord {

  //put your code here
  public static function collectionName() {
    return 'blockings';
  }

  public function attributes() {
    return [
        '_id', 'blocker', 'blockedUser', 'createdAt'
    ];
  }

  public function rules() {
    return [
        [['blocker', 'blockedUser', 'createdAt'], 'safe']
    ];
  }

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'mongoDate' => [
            'class' => '\common\behaviors\mongodb\MongoDate',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt'],
            ],
        ],
    ];
  }

}
