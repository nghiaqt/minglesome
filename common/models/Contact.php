<?php

namespace common\models;

use yii\mongodb\ActiveRecord;
use yii\data\ActiveDataProvider;
/**
 * Manage all contact of users
 *
 * @property \MongoId $_id
 * @property \MongoId $createdBy
 * @property string $subject 
 * @property string $content
 * @property string $email 
 * @property \MongoDate $createdAt
 * @property \MongoDate $updatedAt
 * @property boolean $unread 
 */
class Contact extends ActiveRecord {

  public static function collectionName() {
    return 'contacts';
  }

  public function attributes() {
    return [
        '_id', 'subject', 'content', 'email', 'createdAt', 'updatedAt', 'unread', 'country', 'createdBy'
    ];
  }

  public function rules() {
    return [
        ['unread', 'default', 'value' => true],
        ['unread', 'in', 'range' => [true, false]],
        [['subject', 'content', 'email'], 'required'],
        ['email', 'email'],
        [['content', 'email', 'country'], 'string'],
        [['subject', 'createdAt', 'updatedAt'], 'safe']
    ];
  }

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'mongoDate' => [
            'class' => '\common\behaviors\mongodb\MongoDate',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt', 'updatedAt'],
                ActiveRecord::EVENT_BEFORE_UPDATE => ['updatedAt'],
            ],
        ],
        'mongoId' => [
            'class' => '\common\behaviors\mongodb\MongoId',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdBy','subject']
            ]
        ]
    ];
  }
  
  public function getReason(){
    return $this->hasOne(Reason::className(), ['_id'=>'subject']);
  }

  /**
   * search data provider
   * @param type $params
   * @return \common\models\ActiveDataProvider
   */
  public function search($params) {
    $query = static::find();
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => array('pageSize' => 20)
    ]);

    $this->load($params);
    $where = [];
    foreach ($this->getAttributes() as $key => $value) {
      if ($value!='') {
        if($key=='subject'){
          $value=new \MongoId($value);
        }elseif (is_numeric($value)) {
          $value = (int) $value;
        } elseif (is_string($value)) {
          if ($value == 'true' || $value == 'false') {
            $value = $value == 'true' ? true : false;
          } else {
            $value = new \MongoRegex('/' . $value . '/i');
          }
        }
        $where[$key] = $value;
      }
    }
    $query->where($where);

    return $dataProvider;
  }

}