<?php

namespace common\models;

use yii\mongodb\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * Banner model
 * @property \MongoId $_id
 * @property \MongoId $createdBy
 * @property string $country Country code
 * @property array $file
 * @property string $url
 * @property \MongoDate $startDate
 * @property \MongoDate $endDate
 * @property \MongoDate $createAt
 * @property int $status
 * 
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class Banner extends ActiveRecord {

  /**
   * @return string the name of the index associated with this ActiveRecord class.
   */
  public static function collectionName() {
    return 'banners';
  }

  /**
   * @return array list of attribute names.
   */
  public function attributes() {
    return [
        '_id', 'country', 'file', 'url', 'startDate', 'endDate', 'createdBy', 'createdAt'
    ];
  }

  /**
   * define rule for invoice
   * @return type
   */
  public function rules() {
    return [
        [['url', 'startDate', 'endDate'], 'required'],
        [['country', 'url'], 'string'],
        [['startDate', 'endDate'], 'dateValidator'],
        [['file', 'createdBy', 'createdAt', 'startDate', 'endDate'], 'safe']
    ];
  }

  /**
   * Convert string to int for mongodb
   * @param type $attr
   * @param type $params
   */
  public function converNumber($attr, $params) {
    if (!$this->getErrors() && $this->{$attr}) {
      $this->{$attr} = (int) $this->{$attr};
    }
  }

  /**
   * validator date format
   * convert date to mongo date
   * @param type $attr
   */
  public function dateValidator($attr) {
    if (!$this->getErrors() && !$this->{$attr} instanceof \MongoDate) {
      if (!preg_match('/^(?:20|19)[0-9]{2}\-(?:0[1-9]|1[012])\-(?:0[1-9]|[12][0-9]|3[01])$/', $this->{$attr})) {
        $this->addError($attr, 'Date format must be yyyy-mm-dd');
      } else {
        $this->{$attr} = new \MongoDate(strtotime($this->{$attr}));
      }
    }
  }

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'mongoDate' => [
            'class' => '\common\behaviors\mongodb\MongoDate',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt'],
            ],
        ],
        'mongoId' => [
            'class' => '\common\behaviors\mongodb\MongoId',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdBy'],
            ]
        ]
    ];
  }

  /**
   * search data provider
   * @param type $params
   * @return \common\models\ActiveDataProvider
   */
  public function search($params) {
    $query = static::find();
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => array('pageSize' => 20)
    ]);

    $this->load($params);

    $where = [];
    foreach ($this->getAttributes() as $key => $value) {
      if ($value != '') {
        if (is_numeric($value)) {
          $value = (int) $value;
        } elseif (is_string($value)) {
          if ($value == 'true' || $value == 'false') {
            $value = $value == 'true' ? true : false;
          } else {
            $value = new \MongoRegex('/' . $value . '/i');
          }
        }
        $where[$key] = $value;
      }
    }
    $query->where($where);

    return $dataProvider;
  }

  /**
   * conver date format
   * @param type $format
   */
  public function formatDate($format = 'Y-m-d') {
    if (isset($this->startDate->sec)) {
      $this->startDate = date($format, $this->startDate->sec);
    }
    if (isset($this->endDate->sec)) {
      $this->endDate = date($format, $this->endDate->sec);
    }
  }
}