<?php

namespace common\models;

use yii\mongodb\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * Manage Invite code
 *
 * @property \MongoId $_id
 * @property string $code
 * @property string $description
 * @property int $month valid month for package
 * @property int $amount
 * @property \MongoId $owner
 * @property \MongoId $packageId
 * @property \MongoDate $startDate
 * @property \MongoDate $endDate
 * @property MongoDate $createdAt
 * @property MongoDate $updatedAt 
 * @property \MongoId $createdBy 
 * 
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class Invite extends ActiveRecord {

  /**
   * @return string the name of the index associated with this ActiveRecord class.
   */
  public static function collectionName() {
    return 'invites';
  }

  /**
   * @return array list of attribute names.
   */
  public function attributes() {
    return [
      '_id', 'code', 'description', 'amount', 'month', 'startDate', 'endDate',
      'createdAt', 'updatedAt', 'createdBy', 'owner', 'packageId'
    ];
  }

  /**
   * define rule for Invite
   * @return type
   */
  public function rules() {
    return [
      [['packageId', 'owner', 'code', 'startDate', 'endDate'], 'required'],
      ['month', 'number', 'integerOnly' => true],
      [['description'], 'string'],
      ['code', 'string', 'min' => 8, 'max' => 8],
      ['code', 'unique'],
      [['amount', 'startDate', 'endDate', 'createdBy', 'owner', 'packageId',
        'createdAt', 'updatedAt'], 'safe'],
      [['startDate', 'endDate'], 'dateValidator'],
      ['month', 'converNumber']
    ];
  }

  public function attributeLabels() {
    return [
      'packageId' => 'Affiliate product'
    ];
  }

  /**
   * Convert string to int for mongodb
   * @param type $attr
   * @param type $params
   */
  public function converNumber($attr, $params) {
    if (!$this->getErrors() && $this->{$attr}) {
      $this->{$attr} = (int) $this->{$attr};
    }
  }

  /**
   * validator date format
   * convert date to mongo date
   * @param type $attr
   */
  public function dateValidator($attr) {
    if (!$this->getErrors() && !$this->{$attr} instanceof \MongoDate) {
      if (!preg_match('/^(?:20|19)[0-9]{2}\-(?:0[1-9]|1[012])\-(?:0[1-9]|[12][0-9]|3[01])$/', $this->{$attr})) {
        $this->addError($attr, 'Date format must be yyyy-mm-dd');
      } else {
        $this->{$attr} = new \MongoDate(strtotime($this->{$attr}));
      }
    }
  }

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
      'mongoDate' => [
        'class' => '\common\behaviors\mongodb\MongoDate',
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt', 'updatedAt'],
          ActiveRecord::EVENT_BEFORE_UPDATE => ['updatedAt'],
        ],
      ],
      'mongoId' => [
        'class' => '\common\behaviors\mongodb\MongoId',
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['createdBy', 'owner', 'packageId'],
          ActiveRecord::EVENT_BEFORE_UPDATE => ['owner', 'packageId']
        ]
      ]
    ];
  }

  public function afterSave($insert, $changedAttributes) {
    if ($insert) {
      //update invite code & reset qty invited user
      $user = User::findOne($this->owner);
      //add invite history
      $history = new InviteHistory();
      $history->inviteId = $this->_id;
      $history->owner = $this->owner;
      $history->inviteCode = $user->inviteCode = $this->code . $user->userId;
      $history->inviteCodeWithoutMemberId = $this->code;
      if ($history->save()) {
        $user->premiums = 0;
        $user->save();
      }
    }
  }

  /**
   * search data provider
   * @param type $params
   * @return \common\models\ActiveDataProvider
   */
  public function search($params) {
    $query = static::find();
    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => array('pageSize' => 20)
    ]);

    $this->load($params);

    $where = [];
    foreach ($this->getAttributes() as $key => $value) {
      if ($value != '') {
        if (is_numeric($value)) {
          $value = (int) $value;
        } elseif (is_string($value)) {
          if ($value == 'true' || $value == 'false') {
            $value = $value == 'true' ? true : false;
          } else {
            $value = new \MongoRegex('/' . $value . '/i');
          }
        }
        $where[$key] = $value;
      }
    }
    $query->where($where);

    return $dataProvider;
  }

  /**
   * get owner user
   * @return object
   */
  public function getUserOwner() {
    return $this->hasOne(User::className(),['_id'=>'owner']);
  }
  
  public function getPackage(){
    return $this->hasOne(Package::className(), ['_id'=>'packageId']);
  }

  /**
   * conver date format
   * @param type $format
   */
  public function formatDate($format = 'Y-m-d') {
    if (isset($this->startDate->sec)) {
      $this->startDate = date($format, $this->startDate->sec);
    }
    if (isset($this->endDate->sec)) {
      $this->endDate = date($format, $this->endDate->sec);
    }
  }

  /**
   * convert data return for country
   * @param type $lang
   */
  public function loadDataForCountry($currency = 'USD') {
    if (isset($this->amount[$currency]) && $this->amount[$currency] !== '') {
      $this->amount = number_format($this->amount[$currency], 2);
    } else {
      $this->amount = number_format($this->amount['USD'], 2);
    }
  }

}
