<?php

namespace common\models;

use yii\mongodb\ActiveRecord;

/**
 * Setting model
 * 
 * @property string $category
 * @property array @content
 * 
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class Setting extends ActiveRecord {

  /**
   * @return string the name of the index associated with this ActiveRecord class.
   */
  public static function collectionName() {
    return 'settings';
  }

  /**
   * @return array list of attribute names.
   */
  public function attributes() {
    return [
        '_id', 'category', 'content'
    ];
  }

  /**
   * define rule for setting
   * @return type
   */
  public function rules() {
    return [
        ['category','required'],
        ['category', 'string'],
        [['content'], 'safe']
    ];
  }

  /**
   * Get setting data
   * @param type $category
   * @param type $key
   * @return string
   */
  public static function get($category, $key = '') {
    $model = static::findOne(['category' => $category]);
    if ($model) {
      if ($key) {
        if(isset($model->content[$key])){
          return $model->content[$key];
        }else{
          return '';
        }
      } else {
        return $model->content;
      }
    }
  }

}