<?php

namespace common\models;

use yii\mongodb\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * Manage all translation text
 * @property \MongoId $_id
 * @property string $key to determine what words to translate
 * @property string $value is text translation 
 * 
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class Translation extends ActiveRecord {

  /**
   * @return string the name of the index associated with this ActiveRecord class.
   */
  public static function collectionName() {
    return 'translations';
  }

  /**
   * @return array list of attribute names.
   */
  public function attributes() {
    return [
        '_id', 'key', 'value'
    ];
  }

  /**
   * define rule for translation
   * @return type
   */
  public function rules() {
    return [
        [['key'], 'string'],
        ['value', 'common\validators\LanguagePack']
    ];
  }

  /**
   * search data provider
   * @param type $params
   * @return \common\models\ActiveDataProvider
   */
  public function search($params) {
    $query = static::find();
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => array('pageSize' => 20)
    ]);

    $this->load($params);

    $where = [];
    foreach ($this->getAttributes() as $key => $value) {
      if ($value) {
        if ($key == 'value') {
          $key = 'value.en';
          $value = new \MongoRegex('/' . $value . '/i');
        } elseif (is_numeric($value)) {
          $value = (int) $value;
        } elseif (is_string($value)) {
          if ($value == 'true' || $value == 'false') {
            $value = $value == 'true' ? true : false;
          } else {
            $value = new \MongoRegex('/' . $value . '/i');
          }
        }
        $where[$key] = $value;
      }
    }
    $query->where($where);

    return $dataProvider;
  }

  /**
   * Get all data by language code
   * @param type $lang This value is language code
   * @return array [id=>name]
   */
  public static function getAllAsArray($lang = 'en') {
    $models = static::find()->asArray()->all();
    $data=[];
    foreach ($models as $model) {
      if (isset($model['value'][$lang])) {
        $data[$model['key']] = $model['value'][$lang];
      }else{
        $data[$model['key']] = $model['value']['en'];
      }
    }
    return $data;
  }

}
