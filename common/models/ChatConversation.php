<?php

namespace common\models;

use yii\mongodb\ActiveRecord;

/**
 * Chat Conversation
 * @property \MongoId $_id
 * @property \MongoId $conversationId
 * @property string $text
 * @property boolean $isContact determine this message for contact
 * @property array $deletedUsers List user has deleted chat conversation
 * @property \MongoId $createdBy
 * @property \MongoDate $createdAt
 * 
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class ChatConversation extends ActiveRecord {

  public $__v;

  /**
   * @return string the name of the index associated with this ActiveRecord class.
   */
  public static function collectionName() {
    return 'chatConversation';
  }

  /**
   * @return array list of attribute names.
   */
  public function attributes() {
    return [
        '_id', 'conversationId', 'text', 'isContact',
        'createdAt', 'createdBy', 'deletedUsers'
    ];
  }

  /**
   * define rule for chat conversation
   * @return type
   */
  public function rules() {
    return [
        [['text', 'createdBy'], 'required'],
        [['text'], 'string'],
        [['conversationId', 'createdBy', 'createdAt'], 'safe'],
        ['isContact', 'default', 'value' => false],
        ['isContact', 'in', 'range' => [true, false]],
        ['deletedUsers', 'default', 'value' => []],
    ];
  }

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'mongoDate' => [
            'class' => '\common\behaviors\mongodb\MongoDate',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt']
            ],
        ],
        'mongoId' => [
            'class' => '\common\behaviors\mongodb\MongoId',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['conversationId', 'createdBy'],
                ActiveRecord::EVENT_BEFORE_UPDATE => ['conversationId', 'deletedUsers'],
            ]
        ]
    ];
  }

}