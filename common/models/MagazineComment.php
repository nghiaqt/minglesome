<?php

namespace common\models;

use yii\mongodb\ActiveRecord;

/**
 * Manage Magazine comment
 * 
 * @property \MongoId $_id
 * @property \MongoId $magazineId
 * @property string $content
 * @property \MongoId $createdBy
 * @property \MongoDate $createdAt
 * 
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class MagazineComment extends ActiveRecord {

  /**
   * @return string the name of the index associated with this ActiveRecord class.
   */
  public static function collectionName() {
    return 'magazineComment';
  }

  /**
   * @return array list of attribute names.
   */
  public function attributes() {
    return [
        '_id', 'magazineId', 'createdBy', 'content', 'createdAt'
    ];
  }

  /**
   * define rule for magazine comment
   * @return type
   */
  public function rules() {
    return [
        [['content', 'createdBy'], 'required'],
        ['content', 'string'],
        [['magazineId', 'createdAt', 'createdBy'], 'safe']
    ];
  }

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'mongoDate' => [
            'class' => '\common\behaviors\mongodb\MongoDate',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt']
            ],
        ],
        'mongoId' => [
            'class' => '\common\behaviors\mongodb\MongoId',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdBy', 'magazineId']
            ]
        ]
    ];
  }

}
