<?php

namespace common\models;

use yii\mongodb\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * Polls model
 * @property \MongoId $_id
 * @property \MongoId $createdBy
 * @property string $country Country code
 * @property string $question
 * @property array $answers
 * @property int $status
 * @property \MongoDate $createAt
 * 
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class Polls extends ActiveRecord {

  /**
   * @return string the name of the index associated with this ActiveRecord class.
   */
  public static function collectionName() {
    return 'polls';
  }

  /**
   * @return array list of attribute names.
   */
  public function attributes() {
    return [
        '_id', 'country', 'question', 'answers', 'createdBy', 'createdAt', 'status'
    ];
  }

  public function attributeLabels() {
    return [
        'status' => 'Active'
    ];
  }

  /**
   * define rule for invoice
   * @return type
   */
  public function rules() {
    return [
        ['status', 'converNumber'],
        [['country', 'question'], 'string'],
        [['createdBy', 'createdAt', 'answers'], 'safe']
    ];
  }

  /**
   * Convert string to int for mongodb
   * @param type $attr
   * @param type $params
   */
  public function converNumber($attr, $params) {
    if (!$this->getErrors() && $this->{$attr}) {
      $this->{$attr} = (int) $this->{$attr};
    }
  }

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'mongoDate' => [
            'class' => '\common\behaviors\mongodb\MongoDate',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt'],
            ],
        ],
        'mongoId' => [
            'class' => '\common\behaviors\mongodb\MongoId',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdBy'],
            ]
        ]
    ];
  }

  /**
   * search data provider
   * @param type $params
   * @return \common\models\ActiveDataProvider
   */
  public function search($params) {
    $query = static::find();
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => array('pageSize' => 20)
    ]);

    $this->load($params);

    $where = [];
    foreach ($this->getAttributes() as $key => $value) {
      if ($value != '') {
        if (is_numeric($value)) {
          $value = (int) $value;
        } elseif (is_string($value)) {
          if ($value == 'true' || $value == 'false') {
            $value = $value == 'true' ? true : false;
          } else {
            $value = new \MongoRegex('/' . $value . '/i');
          }
        }
        $where[$key] = $value;
      }
    }
    $query->where($where);

    return $dataProvider;
  }

  public function getAnswers() {
    $return = '';
    foreach ($this->answers as $answer) {
      $total = PollAnswer::find()
              ->where([
                  'pollId' => $this->_id,
                  'answer' => $answer
              ])
              ->count();
      $return.='<p>' . $answer . ': '.$total . '</p>';
    }
    return $return;
  }

}