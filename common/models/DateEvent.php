<?php

namespace common\models;

use yii\mongodb\ActiveRecord;
use common\models\DateEventReply;

/**
 * Manage all events of user
 *
 * @property \MongoId $_id
 * @property MongoDate $onDate event will begin at this date
 * @property string $name event name
 * @property string $description event name
 * @property string $country
 * @property string $city
 * @property boolean $unread
 * @property MongoDate $createdAt
 * @property MongoDate $updatedAt
 * @property \MongoId $activityEventId activity for event like dinner, ... are manage in admin 
 * @property string $activityEvent
 * @property \MongoId $createdBy 
 * @property \MongoId $updatedBy 
 * @property boolean $isGroup to determine if a event is on group or one on one
 * @property boolean $isDeleted
 */
class DateEvent extends ActiveRecord {

  public static function collectionName() {
    return 'dateEvents';
  }

  /**
   * ovewride parent scope to get available ...
   */
  public static function find() {
    return parent::find()->andWhere(['isDeleted'=>false]);
  }

  public function attributes() {
    return [
        '_id', 'onDate', 'name', 'description', 'country', 'city', 'createdAt', 'updatedAt', 'createdBy',
        'updatedBy', 'activityEventId', 'isGroup', 'activityEvent', 'unread', 'isDeleted'
    ];
  }

  public function rules() {
    return [
        [['isGroup', 'unread', 'isDeleted'], 'default', 'value' => false],
        [['isGroup', 'unread', 'isDeleted'], 'in', 'range' => [true, false]],
        [['name', 'country', 'city', 'activityEvent'], 'string', 'min' => 2, 'max' => 255],
        [['description'], 'string'],
        [['createdAt', 'updatedAt', 'onDate', 'createdBy', 'updatedBy', 'activityEventId'], 'safe']
    ];
  }

  public function behaviors() {
    return [
        'mongoDate' => [
            'class' => '\common\behaviors\mongodb\MongoDate',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt', 'updatedAt'],
                ActiveRecord::EVENT_BEFORE_UPDATE => ['updatedAt'],
            ],
        ],
        'mongoId' => [
            'class' => '\common\behaviors\mongodb\MongoId',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdBy', 'activityEventId', 'updatedBy'],
                ActiveRecord::EVENT_BEFORE_UPDATE => ['activityEventId', 'updatedBy']
            ]
        ]
    ];
  }

  public function beforeSave($insert) {
    if (parent::beforeSave($insert)) {
      if (!($this->onDate instanceof \MongoDate)) {
        $this->onDate = new \MongoDate(strtotime($this->onDate));
      }

      return true;
    }
    return false;
  }

  /**
   * Get all date event of user
   * @param \MongoId $userId
   * @param string $lang
   * @return array
   */
  public static function getAllAsArray($userId, $ownerReply, $lang = 'en') {
    if (is_string($userId)) {
      $userId = new \MongoId($userId);
    }
    $dates = static::find()->where([
                        'createdBy' => $userId,
                        'onDate' => ['$gt' => new \MongoDate(mktime(0, 0, 0))]
                    ])
                    ->orderBy(['onDate' => SORT_ASC])
                    ->asArray()->all();
    foreach ($dates as &$date) {
      $activityName = '';
      if ($date['activityEventId']) {
        $activityName = ActivityEvent::getNameById($date['activityEventId'], $lang);
      }
      $date['activityEvent'] = $activityName;
      $createdBy = $ownerReply != $date['createdBy'] ? $ownerReply : '';
      $date['replies'] = DateEventReply::getAllAsArray($date['_id'], $createdBy, $lang);
    }
    return $dates;
  }

  public function loadDataForCountry($lang = 'en') {
    $activityName = '';
    if ($this->activityEventId) {
      $activity = ActivityEvent::findOne($this->activityEventId);
      if ($activity) {
        if (isset($activity->name[$lang])) {
          $activityName = $activity->name[$lang];
        } elseif (isset($activity->name['en'])) {
          $activityName = $activity->name['en'];
        }
      }
    }
    $this->activityEvent = $activityName;
  }

}
