<?php

namespace common\models;

use yii\mongodb\ActiveRecord;

/**
 * Manage key users
 *
 * @property \MongoId $_id
 * @property \MongoId $keyUser is user key
 * @property \MongoId $createdBy
 * @property \MongoDate $createdAt
 */
class KeyUser extends ActiveRecord {

  /**
   * @return string the name of the index associated with this ActiveRecord class.
   */
  public static function collectionName() {
    return 'keys';
  }

  /**
   * @return array list of attribute names.
   */
  public function attributes() {
    return [
        '_id', 'keyUser', 'createdBy', 'createdAt'
    ];
  }

  /**
   * define rule for invoice
   * @return type
   */
  public function rules() {
    return [
        [['keyUser', 'createdBy'], 'required'],
        [['createdAt'], 'safe']
    ];
  }

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'mongoDate' => [
            'class' => '\common\behaviors\mongodb\MongoDate',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt'],
            ],
        ],
        'mongoId' => [
            'class' => '\common\behaviors\mongodb\MongoId',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdBy', 'keyUser']
            ]
        ]
    ];
  }

}
