<?php

namespace common\models;

use yii\mongodb\ActiveRecord;

/**
 * Manage Profile Filter information 
 * 
 * @property \MongoId $_id
 * @property array $genders
 * @property array $statuses
 * @property array $orientations
 * @property int $ageFrom
 * @property int $ageTo
 * @property array $upfors
 * @property array $others
 * @property array $ethnicities
 * @property array $religions
 * @property array $figures
 * @property float $heightFrom
 * @property float $heightTo
 * @property array $countries
 * @property array $speakings
 * @property array $styles
 * @property boolean $offline
 * @property boolean $premium
 * @property boolean $picture
 * @property boolean $interesting
 * @property \MongoId $createdBy
 * 
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class PrivacyFilter extends ActiveRecord {

  /**
   * @return string the name of the index associated with this ActiveRecord class.
   */
  public static function collectionName() {
    return 'privacyFilters';
  }

  /**
   * @return array list of attribute names.
   */
  public function attributes() {
    return [
        '_id', 'genders', 'statuses', 'orientations', 'ageFrom', 'ageTo', 'upfors', 'others',
        'ethnicities', 'religions', 'figures', 'heightFrom', 'heightTo', 'countries',
        'speakings', 'styles', 'createdBy', 'offline', 'premium', 'picture', 'interesting'
    ];
  }

  /**
   * define rule for privacy filter
   * @return type
   */
  public function rules() {
    return [
        [['genders', 'statuses', 'countries', 'orientations', 'upfors', 'others', 'ethnicities',
        'religions', 'figures', 'speakings', 'styles'], 'common\validators\types\IsArray'],
        ['createdBy', 'safe'],
        [['offline', 'premium', 'picture', 'interesting'], 'in', 'range' => [true, false]],
        [['offline', 'premium', 'picture', 'interesting'], 'default', 'value' => false],
        [['ageFrom', 'ageTo'], 'number', 'integerOnly' => true],
        [['heightFrom', 'heightTo'], 'number'],
        [['genders', 'statuses', 'orientations', 'upfors', 'others', 'ethnicities', 'religions',
        'figures', 'speakings', 'styles', 'countries'], 'default', 'value' => []]
    ];
  }

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'mongoId' => [
            'class' => '\common\behaviors\mongodb\MongoId',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => [
                    'createdBy', 'orientations', 'upfors', 'others', 'ethnicities', 'religions',
                    'figures', 'speakings', 'styles'
                ],
                ActiveRecord::EVENT_BEFORE_UPDATE => [
                    'createdBy', 'orientations', 'upfors', 'others', 'ethnicities', 'religions',
                    'figures', 'speakings', 'styles'
                ],
            ]
        ]
    ];
  }

  /**
   * check if user matches with Privacy Filter settings
   * 
   * @param array $user is user need to be check
   * @param array $privacyFilter is privacy setting data
   * @return boolean True is match privacy filter. otherwise
   */
  public function isMatchPrivacyFilter($user) {
    if ($user) {
      if (is_object($user)) {
        $user = $user->toArray();
      }
      //check when login
      if ($this->offline) {
        $author = User::findOne($this->createdBy);
        if (!$author->isLogged) {
          return true;
        }
      }
      //check non premium
      if ($this->premium && $user['level'] < User::LEVEL_PREMIUM) {
        return true;
      }
      //check no picture
      if ($this->picture && !$user['photos']) {
        return true;
      }
      //check not interesting
      if ($this->interesting) {
        $interesting = Interest::findOne([
                    'createdBy' => $this->createdBy,
                    'interestedUser' => $user['_id'],
                    'isInterest' => false
        ]);
        if ($interesting) {
          return true;
        }
      }
      //check gender
      foreach ($this->genders as $gender) {
        if ($user['gender'] === $gender) {
          return TRUE;
        }
      }
      //check status married
      foreach ($this->statuses as $status) {
        if ($user['statusMarried'] === $status) {
          return TRUE;
        }
      }
      //check orientation
      foreach ($this->orientations as $orientation) {
        if ($user['orientation'] === $orientation) {
          return TRUE;
        }
      }
      //check upfor
      foreach ($this->upfors as $upfor) {
        if (in_array($upfor, $user['upfors'])) {
          return TRUE;
        }
      }
      //check other
      foreach ($this->others as $other) {
        if (in_array($other, $user['others'])) {
          return TRUE;
        }
      }
      //check ethnicity
      foreach ($this->ethnicities as $ethnicity) {
        if ($user['ethnicity'] === $ethnicity) {
          return TRUE;
        }
      }
      //check figures
      foreach ($this->figures as $figure) {
        if ($user['figure'] === $figure) {
          return TRUE;
        }
      }
      //check speaking
      foreach ($this->speakings as $speaking) {
        if (in_array($speaking, $user['speakings'])) {
          return TRUE;
        }
      }
      //check style
      foreach ($this->styles as $style) {
        if (in_array($style, $user['styles'])) {
          return TRUE;
        }
      }
      //check country
      foreach ($this->countries as $countryCode) {
        if ($user['country'] === $countryCode) {
          return TRUE;
        }
      }
      if ($this->ageFrom && (int) $user['age'] < (int) $this->ageFrom) {
        return TRUE;
      }
      if ($this->ageTo && (int) $user['age'] > (int) $this->ageTo) {
        return TRUE;
      }
      if ($this->heightFrom && (float) $user['height'] < (float) $this->heightFrom) {
        return TRUE;
      }
      if ($this->heightTo && (float) $user['height'] > (float) $this->heightTo) {
        return TRUE;
      }
    }
    return false;
  }

}
