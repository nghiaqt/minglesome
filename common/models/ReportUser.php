<?php

namespace common\models;

use yii\mongodb\ActiveRecord;
use yii\data\ActiveDataProvider;
use common\models\User;

/**
 * Manager report
 * @property \MongoId $_id
 * @property \MongoId $reportedUser
 * @property string $message
 * @property \MongoId $createdBy
 * @property \MongoDate $createdAt
 * 
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class ReportUser extends ActiveRecord {

  public $__v;

  /**
   * @return string the name of the index associated with this ActiveRecord class.
   */
  public static function collectionName() {
    return 'reportUser';
  }

  /**
   * @return array list of attribute names.
   */
  public function attributes() {
    return [
        '_id', 'message', 'createdBy', 'reportedUser', 'createdAt',
    ];
  }

  /**
   * define rule for report user
   * @return type
   */
  public function rules() {
    return [
        [['message','reportedUser','createdBy'], 'required'],
        [['message'], 'string'],
        [['createdBy', 'createdAt'], 'safe']
    ];
  }

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'mongoDate' => [
            'class' => '\common\behaviors\mongodb\MongoDate',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt']
            ],
        ],
        'mongoId' => [
            'class' => '\common\behaviors\mongodb\MongoId',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['reportedUser', 'createdBy']
            ]
        ]
    ];
  }

  /**
   * search data provider
   * @param type $params
   * @return \common\models\ActiveDataProvider
   */
  public function search($params) {
    $query = static::find();
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => array('pageSize' => 20)
    ]);

    $this->load($params);
    $where = [];
    foreach ($this->getAttributes() as $key => $value) {
      if ($value != '') {
        if($key=='createdBy' || $key=='reportedUser'){
          $value=new \MongoId($value);
        }elseif (is_numeric($value)) {
          $value = (int) $value;
        } elseif (is_string($value)) {
          if ($value == 'true' || $value == 'false') {
            $value = $value == 'true' ? true : false;
          } else {
            $value = new \MongoRegex('/' . $value . '/i');
          }
        }
        $where[$key] = $value;
      }
    }
    $query->where($where);

    return $dataProvider;
  }

}