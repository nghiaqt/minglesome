<?php

namespace common\models;

use yii\mongodb\ActiveRecord;
use yii\data\ActiveDataProvider;
/**
 * Manage magazines
 * 
 * @property \MongoId $_id
 * @property \MongoId $createdBy
 * @property \MongoId $owner
 * @property string $country Country code
 * @property string $title
 * @property string $content
 * @property string $file
 * @property \MongoDate $createdAt
 * @property \MongoDate $updatedAt 
 */
class Magazine extends ActiveRecord {

  public static function collectionName() {
    return 'magazine';
  }

  public function attributes() {
    return [
        '_id', 'createdBy', 'owner', 'country', 'title', 'content', 'file', 'createdAt', 'updatedAt'
    ];
  }

  public function rules() {
    return [
        [['country', 'title', 'content'], 'string'],
        [['file','createdAt', 'updatedAt', 'createdBy', 'owner'], 'safe']
    ];
  }
  
  public function attributeLabels() {
    return [
    ];
  }

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'mongoDate' => [
            'class' => '\common\behaviors\mongodb\MongoDate',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt', 'updatedAt'],
                ActiveRecord::EVENT_BEFORE_UPDATE => ['updatedAt'],
            ],
        ],
        'mongoId' => [
            'class' => '\common\behaviors\mongodb\MongoId',
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['createdBy', 'owner'],
                ActiveRecord::EVENT_BEFORE_UPDATE => ['owner']
            ]
        ]
    ];
  }

  
  /**
   * search data provider
   * @param type $params
   * @return \common\models\ActiveDataProvider
   */
  public function search($params) {
    $query = static::find();
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => array('pageSize' => 20)
    ]);

    $this->load($params);

    $where = [];
    foreach ($this->getAttributes() as $key => $value) {
      if ($value != '') {
        if (is_numeric($value)) {
          $value = (int) $value;
        } elseif (is_string($value)) {
          if ($value == 'true' || $value == 'false') {
            $value = $value == 'true' ? true : false;
          } else {
            $value = new \MongoRegex('/' . $value . '/i');
          }
        }
        $where[$key] = $value;
      }
    }
    $query->where($where);

    return $dataProvider;
  }

  /**
   * get owner user
   * @return object
   */
  public function getOwner() {
    return User::findOne(['_id' => $this->owner]);
  }
}
