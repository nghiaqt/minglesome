<?php

namespace common\models;

use yii\mongodb\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * Manager all invoices
 * @property \MongoId $_id
 * @property \MongoId $packageId
 * @property \MongoId $createdBy
 * @property string $packageName
 * @property integer $packageMonth
 * @property string $currency
 * @property string $amount
 * @property string $profileId
 * @property integer $profileStatus
 * @property array $params
 * @property array $cancelParams
 * @property integer $createAt
 * @property integer $updateAt
 * 
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class RecurringPaymentsProfile extends ActiveRecord {

  /**
   * @return string the name of the index associated with this ActiveRecord class.
   */
  public static function collectionName() {
    return 'recurringPaymentsProfiles';
  }

  /**
   * @return array list of attribute names.
   */
  public function attributes() {
    return [
      '_id', 'packageId', 'packageName', 'packageMonth',
      'currency', 'amount', 'profileId', 'profileStatus',
      'params', 'cancelParams','createdAt', 'updatedAt', 'createdBy'
    ];
  }

  /**
   * define rule for invoice
   * @return type
   */
  public function rules() {
    return [
      [['packageMonth'], 'number', 'integerOnly' => true],
      ['amount', 'converNumber'],
      [['packageName', 'currency', 'profileId', 'profileStatus'], 'string'],
      [['params', 'cancelParams','amount', 'packageId', 'createdBy', 
        'createdAt', 'updatedAt'], 'safe']
    ];
  }

  /**
   * Convert string to int for mongodb
   * @param type $attr
   * @param type $params
   */
  public function converNumber($attr, $params) {
    if ($this->{$attr}) {
      $this->{$attr} = (float) $this->{$attr};
    }
  }

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
      'mongoDate' => [
        'class' => '\common\behaviors\mongodb\MongoDate',
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt', 'updatedAt'],
          ActiveRecord::EVENT_BEFORE_UPDATE => ['updatedAt'],
        ],
      ],
    ];
  }

}
