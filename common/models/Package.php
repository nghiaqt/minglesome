<?php

namespace common\models;

use yii\mongodb\ActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * Manage membership packages( only prenium package)
 * @property \MongoId $_id
 * @property string $type Enum('web','ios')
 * @property string $appleProductId Apple product
 * @property boolean $isRecurring (the customer change requirement 2/2015)
 * @property boolean $isAffiliate (the customer change requirement 4/2015)
 * @property array $name 
 * @property string $description
 * @property int $month valid month for package
 * @property int $amount
 * @property int $order
 * @property MongoDate $createdAt
 * @property MongoDate $updatedAt 
 * @property \MongoId $createdBy 
 * 
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class Package extends ActiveRecord {

  /**
   * @return string the name of the index associated with this ActiveRecord class.
   */
  public static function collectionName() {
    return 'package';
  }

  /**
   * @return array list of attribute names.
   */
  public function attributes() {
    return [
      '_id', 'appleProductId', 'type', 'isRecurring', 'isAffiliate', 'name',
      'description', 'month', 'amount', 'createdAt', 'updatedAt', 'createdBy',
      'order'
    ];
  }

  /**
   * define rule for package
   * @return type
   */
  public function rules() {
    return [
      [['month', 'order'], 'number', 'integerOnly' => true],
      [['name', 'description'], 'common\validators\LanguagePack'],
      [['isRecurring', 'isAffiliate'], 'default', 'value' => '0'],
      [['isRecurring', 'isAffiliate'], 'in', 'range' => ['1', '0']],
      [['appleProductId', 'type', 'amount', 'createdBy', 'createdAt', 'updatedAt'], 'safe']
    ];
  }

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
      'mongoDate' => [
        'class' => '\common\behaviors\mongodb\MongoDate',
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt', 'updatedAt'],
          ActiveRecord::EVENT_BEFORE_UPDATE => ['updatedAt'],
        ],
      ],
      'mongoId' => [
        'class' => '\common\behaviors\mongodb\MongoId',
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['createdBy']
        ]
      ]
    ];
  }

  /**
   * search data provider
   * @param type $params
   * @return \common\models\ActiveDataProvider
   */
  public function search($params) {
    $query = static::find();
    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => array('pageSize' => 20)
    ]);

    $this->load($params);

    $where = [];
    foreach ($this->getAttributes() as $key => $value) {
      if ($value) {
        if (is_numeric($value)) {
          $value = (int) $value;
        } elseif (is_string($value)) {
          if ($value == 'true' || $value == 'false') {
            $value = $value == 'true' ? true : false;
          } else {
            if ($key == 'name') {
              $key = 'name.en';
              $value = new \MongoRegex('/' . $value . '/i');
            } else {
              $value = new \MongoRegex('/' . $value . '/i');
            }
          }
        }
        $where[$key] = $value;
      }
    }
    $query->where($where);

    return $dataProvider;
  }

  /**
   * define type
   * @return array
   */
  public static function getType() {
    return [
      'web' => 'Web app',
      'ios' => 'Ios app'
    ];
  }

  public function getTypeName() {
    switch ($this->type) {
      case 'web':
        $return = 'Web app';
        break;
      case 'ios':
        $return = 'Ios app';
        break;
      default:
        $return = 'Undefine';
        break;
    }
    return $return;
  }

  /**
   * Get all data by language code
   * @param type $lang This value is language code
   * @return array [id=>name]
   */
  public static function getAllAsArray($isAffiliate = false, $lang = 'us', $currency = 'USD', $ios = false) {
    $where = ['type' => $ios ? 'ios' : 'web'];
    if (!$isAffiliate) {
      $where['isAffiliate'] = '0';
    }
    $models = static::find()
      ->where($where)
      ->orderBy('order')
      ->asArray()
      ->all();
    foreach ($models as $key => $model) {
      $name = '';
      $description = '';
      $amount = 0;

      if (isset($model['name'][$lang])) {
        $name = $model['name'][$lang];
      }
      if (isset($model['description'][$lang])) {
        $description = $model['description'][$lang];
      }
      if (isset($model['amount'][$currency])) {
        $amount = $model['amount'][$currency];
      }

      $models[$key]['name'] = $name;
      $models[$key]['description'] = $description;
      $models[$key]['amount'] = $amount;
    }
    return $models;
  }
  
  /**
   * get all affiliate for dropdown
   * @return array [id=>name]
   */
  public static function getAllAffiliateAsArray(){
    $models = static::find()
      ->where(['isAffiliate'=>'1'])
      ->orderBy('order')
      ->all();
    $return=[];
    foreach ($models as $model){
      $return[(string)$model->_id]=$model->name['us'];
    }
    return $return;
  }

  public static function getById($id, $lang = 'us', $currency = 'USD',$ios = false) {
    $where = ['type' => $ios ? 'ios' : 'web'];
    if ($id) {
      $where['_id'] = $id;
    }
    $models = static::find()
      ->where($where)
      ->orderBy('order')
      ->asArray()
      ->all();
    foreach ($models as $key => $model) {
      $name = '';
      $description = '';
      $amount = 0;

      if (isset($model['name'][$lang])) {
        $name = $model['name'][$lang];
      }
      if (isset($model['description'][$lang])) {
        $description = $model['description'][$lang];
      }
      if (isset($model['amount'][$currency])) {
        $amount = $model['amount'][$currency];
      }

      $models[$key]['name'] = $name;
      $models[$key]['description'] = $description;
      $models[$key]['amount'] = $amount;
    }
    return $models;
  }
  

  /**
   * Load data translate
   * @param string $lang
   * @param string $currency
   */
  public function loadDataForCountry($lang = 'us', $currency = 'USD') {
    $name = '';
    $description = '';
    $amount = 0;
    if (isset($this->name[$lang])) {
      $name = $this->name[$lang];
    }
    if (isset($this->description[$lang])) {
      $description = $this->description[$lang];
    }
    if (isset($this->amount[$currency])) {
      $amount = $this->amount[$currency];
    }
    $this->name = $name;
    $this->description = $description;
    $this->amount = $amount;
  }

}
