<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Package;

$this->params['breadcrumbs'][] = ['label' => 'Products'];
$this->params['breadcrumbs'][] = ['label' => '<i class="fa fa-pencil"></i> Add New', 'url' => ['package/create']];
?>
<div class="row">
  <div class="col-lg-12">
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $model,
        'layout' => "<div class='responsive-table'>{items}</div>\n
          <div class='row'>
            <div class='col-xs-6'>{summary}</div>
            <div class='col-xs-6'>
              <div class='dataTables_paginate paging_simple_numbers'>
              {pager}
              </div>
            </div>
          </div>",
        'tableOptions' => ['class' => 'table table-striped table-bordered sortableTable responsive-table dataTable'],
        'columns' => [
            [
                'header' => 'Action',
                'options' => ['style' => 'width:50px'],
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
            ],
          [
            'attribute'=>'type',
            'value'=>'typeName',
            'filter'=>  Package::getType()
          ],
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function($model) {
                  if (is_array($model->name)) {
                    foreach ($model->name as $key => $value) {
                      if ($key == 'us') {
                        return $value;
                      }
                    }
                  }
                }
            ],
            'month',
            [
                'attribute' => 'amount',
                'format' => 'raw',
                'value' => function($model) {
                  $return = '';
                  if (is_array($model->amount)) {
                    foreach ($model->amount as $key => $value) {
                      if ($value && is_numeric($value)) {
                        $return .= '<div>(' . $key . ') ' . number_format($value, 2) . '</div>';
                      }
                    }
                  }
                  return $return;
                }
            ],
              [
                'attribute'=>'order',
                'filter'=>false
              ],
            [
                'attribute' => 'createdAt',
                'filter' => false,
                'value' => function($model) {
                  if (isset($model->createdAt->sec)) {
                    return $model->createdAt->sec;
                  } else {
                    return '';
                  }
                },
                'format' => 'date'
            ]
        ],
    ]);
    ?>
  </div>
</div>
