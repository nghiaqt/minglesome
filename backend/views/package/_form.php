<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\helpers\Language;
use dosamigos\ckeditor\CKEditor;
use common\models\Package;
use common\models\Country;
?>

<?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>
<div class="padding10">
  <?= $form->field($model,'type')->dropDownList(Package::getType());?>
  <?= $form->field($model, 'month'); ?>
  <?= $form->field($model, 'appleProductId'); ?>
  <?= $form->field($model, 'order'); ?>
  <?= $form->field($model, 'isRecurring')->checkbox(['uncheck' => false, 'checked' => true]); ?>
  <?= $form->field($model, 'isAffiliate')->checkbox(['uncheck' => false, 'checked' => true]); ?>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="box">
      <header class="dark">
        <div class="icons">
          <i class="fa fa-check"></i>
        </div>
        <h5>Amount</h5>
        <!-- .toolbar -->
        <div class="toolbar">
          <nav style="padding: 8px;">
            <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
              <i class="fa fa-minus"></i>
            </a>
          </nav>
        </div><!-- /.toolbar -->
      </header>
      <div class="body">
        <?php
        foreach (Language::$currency as $code => $name) :
          ?>
          <?= $form->field($model, 'amount[' . $code . ']')->textInput()->label($name); ?>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
</div>
<?php
foreach (Country::getAllAsArray() as $code => $name) :
  ?>
  <div class="row">
    <div class="col-lg-12">
      <div class="box">
        <header class="dark">
          <div class="icons">
            <i class="fa fa-check"></i>
          </div>
          <h5><?= $name; ?></h5>
          <!-- .toolbar -->
          <div class="toolbar">
            <nav style="padding: 8px;">
              <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                <i class="fa fa-minus"></i>
              </a>
            </nav>
          </div><!-- /.toolbar -->
        </header>
        <div class="body">
          <?= $form->field($model, 'name[' . $code . ']')->textInput(); ?>
          <?=
          $form->field($model, 'description[' . $code . ']')->widget(CKEditor::className(), [
              'options' => ['rows' => 3],
              'preset' => 'standard'
          ]);
          ?>
        </div>
      </div>
    </div>
  </div>
  <?php
endforeach;
?>
<div class="form-actions no-margin-bottom">
  <input type="submit" value="Save" class="btn btn-primary">
</div>
<?php ActiveForm::end(); ?>