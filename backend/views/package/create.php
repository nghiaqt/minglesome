<?php

$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['package/index']];
$this->params['breadcrumbs'][] = ['label' => 'Create'];
?>

<?= $this->render('_form', compact('model')); ?>
 