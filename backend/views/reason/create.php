<?php

$this->params['breadcrumbs'][] = ['label' => 'Reason', 'url' => ['reason/index','type'=>$model->type]];
$this->params['breadcrumbs'][] = ['label' => 'Create'];
?>

<?= $this->render('_form', compact('model')); ?>
 