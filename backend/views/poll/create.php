<?php

$this->params['breadcrumbs'][] = ['label' => 'Polls', 'url' => ['poll/index']];
$this->params['breadcrumbs'][] = ['label' => 'Create'];
?>

<?= $this->render('_form', compact('model')); ?>
 