<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\Country;
?>

<?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>
<div class="row">
  <div class="col-lg-12">
    <div class="box">
      <header class="dark">
        <div class="icons">
          <i class="fa fa-check"></i>
        </div>
        <h5>Poll Info</h5>
        <!-- .toolbar -->
        <div class="toolbar">
          <nav style="padding: 8px;">
            <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
              <i class="fa fa-minus"></i>
            </a>
          </nav>
        </div><!-- /.toolbar -->
      </header>
      <div class="body">
        <?= $form->field($model, 'country')->dropDownList(Country::getAllAsArray()); ?>
        <?= $form->field($model, 'question'); ?>
        <div class="form-group">
          <label class="control-label col-sm-3">Answer</label>
          <div class="col-sm-6" id="answers">
            <div class="answer-fields">
              <?php
              if (!empty($model->answers)) {
                $count = 0;
                foreach ($model->answers as $key=>$value) {
                  if ($count < 2) {
                    echo '<p>';
                    echo Html::input('text', 'Polls[answers][]', $value, ['class' => 'form-control']);
                    echo '</p>';
                  } else {
                    echo '<div class="input-group">';
                    echo Html::input('text', 'Polls[answers][]', $value, ['class' => 'form-control']);
                    echo '<span class="input-group-addon">
                      <a href="#" title="Delete Field" class="del-answer-field glyphicon glyphicon-remove"></a>
                    </span>
                    </div>';
                  }
                  $count++;
                }
              } else {
                echo '<p>';
                echo Html::input('text', 'Polls[answers][]', '', ['class' => 'form-control']);
                echo '</p><p>';
                echo Html::input('text', 'Polls[answers][]', '', ['class' => 'form-control']);
                echo '</p>';
              }
              ?>
            </div>
            <a class="btn btn-warning add-answer-field">Add Field</a>
          </div>
        </div>
        <?= $form->field($model, 'status')->checkbox(); ?>
        <div class="form-actions no-margin-bottom">
          <input type="submit" value="Save" class="btn btn-primary">
        </div>
      </div>
    </div>
  </div>
</div>

<?php ActiveForm::end(); ?>

