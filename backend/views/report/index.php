<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\ReportUser;
use common\models\User;

$this->params['breadcrumbs'][] = ['label' => 'Report'];

$users=User::getAllAsArray();
?>
<div class="row">
  <div class="col-lg-12">
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $model,
        'layout' => "<div class='responsive-table'>{items}</div>\n
          <div class='row'>
            <div class='col-xs-6'>{summary}</div>
            <div class='col-xs-6'>
              <div class='dataTables_paginate paging_simple_numbers'>
              {pager}
              </div>
            </div>
          </div>",
        'tableOptions' => ['class' => 'table table-striped table-bordered sortableTable responsive-table dataTable'],
        'columns' => [
            [
                'attribute' => 'createdBy',
                'filter' => $users,
                'format'=>'raw',
                'value' => function($model) {
                  $user = User::findOne($model->createdBy);
                  if ($user) {
                    return Html::a($user->username,FRONTSITE_URL . '#/view-profile/' . $model->_id,['target'=>'_blank']);
                  } else {
                    return 'deleted';
                  }
                }
            ],
            [
                'attribute' => 'reportedUser',
                'filter' => $users,
                'format'=>'raw',
                'value' => function($model) {
                  $user = User::findOne($model->reportedUser);
                  if ($user) {
                    return Html::a($user->username,FRONTSITE_URL . '#/view-profile/' . $model->_id,['target'=>'_blank']);
                  } else {
                    return 'deleted';
                  }
                }
            ],
            'message',
            [
                'attribute' => 'createdAt',
                'filter' => false,
                'value' => function($model) {
                  if (isset($model->createdAt->sec)) {
                    return $model->createdAt->sec;
                  } else {
                    return '';
                  }
                },
                'format' => 'date'
            ]
        ],
    ]);
    ?>
  </div>
</div>
