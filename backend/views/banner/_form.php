<?php

//helper
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
//widget
use kartik\widgets\DatePicker;
//model
use common\models\User;
use common\models\Country;
?>

<?php
$form = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data'],
            'layout' => 'horizontal'
        ]);
?>
<div class="row">
  <div class="col-lg-12">
    <div class="box">
      <header class="dark">
        <div class="icons">
          <i class="fa fa-check"></i>
        </div>
        <h5>Banner Info</h5>
        <!-- .toolbar -->
        <div class="toolbar">
          <nav style="padding: 8px;">
            <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
              <i class="fa fa-minus"></i>
            </a>
          </nav>
        </div><!-- /.toolbar -->
      </header>
      <div class="body">

        <?php
        echo $form->field($model, 'country')->dropDownList(Country::getAllAsArray());
        echo $form->field($model, 'url');
        echo $form->field($model, 'startDate')->widget(DatePicker::classname(), [
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ]
        ]);
        echo $form->field($model, 'endDate')->widget(DatePicker::classname(), [
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ]
        ]);
        ?>
        <div class="form-group">
          <label class="control-label col-sm-3">File</label>
          <div class="col-sm-6">
            <?php
            if ($model->file):
              if ($model->file['type'] == 'image'):
                ?>
                <img src="<?= BANNER_URL . $model->file['name']; ?>" height="100"/>
                <?php
              endif;
            endif;
            ?>
            <?= Html::fileInput('file', '', ['accept' => 'image/*']); ?>
            <?= Html::error($model, 'file') ?>
          </div>
        </div>

        <div class="form-actions no-margin-bottom">
          <input type="submit" value="Save" class="btn btn-primary">
        </div>
      </div>
    </div>
  </div>
</div>

<?php ActiveForm::end(); ?>