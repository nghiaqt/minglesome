<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Country;

$this->params['breadcrumbs'][] = ['label' => 'Banners'];
$this->params['breadcrumbs'][] = ['label' => '<i class="fa fa-pencil"></i> Add New', 'url' => ['banner/create']];
?>
<div class="row">
  <div class="col-lg-12">
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $model,
        'layout' => "<div class='responsive-table'>{items}</div>\n
          <div class='row'>
            <div class='col-xs-6'>{summary}</div>
            <div class='col-xs-6'>
              <div class='dataTables_paginate paging_simple_numbers'>
              {pager}
              </div>
            </div>
          </div>",
        'tableOptions' => ['class' => 'table table-striped table-bordered sortableTable responsive-table dataTable'],
        'columns' => [
            [
                'header' => 'Action',
                'options' => ['style' => 'width:50px'],
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
            ],
            [
              'header'=>'File',
                'format'=>'raw',
              'value'=>function($model){
                if($model->file){
                  if($model->file['type']=='image'){
                    return Html::img(BANNER_URL.$model->file['name'],['width'=>100]);
                  }
                }
              }
            ],
            [
                'attribute' => 'country',
                'filter' => Country::getAllAsArray()
            ],
            'url',
            [
                'attribute' => 'startDate',
                'filter' => false,
                'value' => function($model) {
                  if (isset($model->startDate->sec)) {
                    return $model->startDate->sec;
                  } else {
                    return '';
                  }
                },
                'format' => ['date', 'Y-m-d']
            ],
            [
                'attribute' => 'endDate',
                'filter' => false,
                'value' => function($model) {
                  if (isset($model->endDate->sec)) {
                    return $model->endDate->sec;
                  } else {
                    return '';
                  }
                },
                'format' => ['date', 'Y-m-d']
            ],
            [
                'attribute' => 'createdAt',
                'filter' => false,
                'value' => function($model) {
                  if (isset($model->createdAt->sec)) {
                    return $model->createdAt->sec;
                  } else {
                    return '';
                  }
                },
                'format' => 'date'
            ]
        ],
    ]);
    ?>
  </div>
</div>
