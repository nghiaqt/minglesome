<?php
$this->params['breadcrumbs'][] = ['label' => 'Dashboard'];
?>

<div class="row stats_box">

  <div class="col-sm-6 col-md-4 col-lg-3">
    <div class="statbox widget box box-shadow">
      <a href="<?= Yii::$app->urlManager->createUrl(['user/index', 'User[status]' => 1]); ?>">
        <div class="widget-content">
          <div class="visual black">
            <i class="fa fa-signal"></i></div>
          <div class="title">MEMBERS</div>
          <div class="value"><?= number_format($statistic['members']); ?></div>
        </div>
      </a>
    </div>
  </div>
  <div class="col-sm-6 col-md-4 col-lg-3">
    <div class="statbox widget box box-shadow">
      <a href="<?= Yii::$app->urlManager->createUrl(['payment/index']); ?>">
        <div class="widget-content">
          <div class="visual gray">
            <i class="fa fa-signal"></i></div>
          <div class="title">UPGRADES</div>
          <div class="value"><?= number_format($statistic['upgrades']); ?></div>
        </div>
      </a>
    </div> 
  </div> 
  <div class="col-sm-6 col-md-4 col-lg-3">
    <div class="statbox widget box box-shadow">
      <div class="widget-content">
        <div class="visual red">
          <i class="fa fa-signal"></i></div>
        <div class="title">INVITES</div>
        <div class="value"><?= number_format($statistic['invites']); ?></div>
      </div>
    </div> 
  </div>

  <div class="col-sm-6 col-md-4 col-lg-3">
    <div class="statbox widget box box-shadow">
      <a href="<?= Yii::$app->urlManager->createUrl(['contact/index']); ?>">
        <div class="widget-content">
          <div class="visual blue">
            <i class="fa fa-signal"></i></div>
          <div class="title">INBOX</div>
          <div class="value"><?= number_format($statistic['inbox']); ?></div>
        </div>
      </a>
    </div> 
  </div> 
  <div class="col-sm-6 col-md-4 col-lg-3">
    <div class="statbox widget box box-shadow">
      <a href="<?= Yii::$app->urlManager->createUrl(['report/index']); ?>">
        <div class="widget-content">
          <div class="visual green"><i class="fa fa-signal"></i></div>
          <div class="title">REPORTS</div>
          <div class="value"><?= number_format($statistic['reports']); ?></div>
        </div>
      </a>
    </div> 
  </div>
  <div class="col-sm-6 col-md-4 col-lg-3">
    <div class="statbox widget box box-shadow">
      <a href="<?= Yii::$app->urlManager->createUrl(['user/index']); ?>">
        <div class="widget-content">
          <div class="visual yellow">
            <i class="fa fa-signal"></i></div>
          <div class="title">BLOCKS</div>
          <div class="value"><?= number_format($statistic['blocks']); ?></div>
        </div>
      </a>
    </div>
  </div>
  <div class="col-sm-6 col-md-4 col-lg-3">
    <div class="statbox widget box box-shadow">
      <a href="<?= Yii::$app->urlManager->createUrl(['user/index']); ?>">
        <div class="widget-content">
          <div class="visual violet">
            <i class="fa fa-signal"></i></div>
          <div class="title">PHOTOS</div>
          <div class="value"><?= number_format($statistic['photos']); ?></div>
        </div>
      </a>
    </div>
  </div>
  <div class="col-sm-6 col-md-4 col-lg-3">
    <div class="statbox widget box box-shadow">
      <a href="<?= Yii::$app->urlManager->createUrl(['user/index']); ?>">
        <div class="widget-content">
          <div class="visual teal">
            <i class="fa fa-signal"></i></div>
          <div class="title">MESSAGES</div>
          <div class="value"><?= number_format($statistic['messages']); ?></div>
        </div>
      </a>
    </div>
  </div>
  <div class="col-sm-6 col-md-4 col-lg-3">
    <div class="statbox widget box box-shadow">
      <a href="<?= Yii::$app->urlManager->createUrl(['user/index']); ?>">
        <div class="widget-content">
          <div class="visual pink">
            <i class="fa fa-signal"></i></div>
          <div class="title">DATE EVENT ADS</div>
          <div class="value"><?= number_format($statistic['events']); ?></div>
        </div>
      </a>
    </div>
  </div>
  <div class="col-sm-6 col-md-4 col-lg-3">
    <div class="statbox widget box box-shadow">
      <a href="<?= Yii::$app->urlManager->createUrl(['user/index']); ?>">
        <div class="widget-content">
          <div class="visual peru">
            <i class="fa fa-signal"></i></div>
          <div class="title">STREAMING TIME</div>
          <div class="value">
            <?=
            $statistic['streamings'] ?
                    \common\helpers\DateHelper::convertTime($statistic['streamings'] / 1000) : 0;
            ?>
          </div>
        </div>
      </a>
    </div>
  </div>
  <div class="col-sm-6 col-md-4 col-lg-3">
    <div class="statbox widget box box-shadow">
      <div class="widget-content">
        <div class="visual tomato">
          <i class="fa fa-signal"></i></div>
        <div class="title">POLLS</div>
        <div class="value"><?= number_format($statistic['polls']); ?></div>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-md-4 col-lg-3">
    <div class="statbox widget box box-shadow">
      <a href="<?= Yii::$app->urlManager->createUrl(['magazine/index']); ?>">
        <div class="widget-content">
          <div class="visual wheat">
            <i class="fa fa-signal"></i></div>
          <div class="title">COMMENTS</div>
          <div class="value"><?= number_format($statistic['comments']); ?></div>
        </div>
      </a>
    </div>
  </div>
  <div class="col-sm-6 col-md-4 col-lg-3">
    <div class="statbox widget box box-shadow">
      <div class="widget-content">
        <div class="visual skyblue">
          <i class="fa fa-signal"></i></div>
        <div class="title">SUBSCRIPTIONS STOPPED</div>
        <div class="value"><?= number_format($statistic['subscriptions']); ?></div>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-md-4 col-lg-3">
    <div class="statbox widget box box-shadow">
      <div class="widget-content">
        <div class="visual olive">
          <i class="fa fa-signal"></i></div>
        <div class="title">PROFILES DELETED</div>
        <div class="value"><?= number_format($statistic['memberships']); ?></div>
      </div>
    </div>
  </div>
</div>
<hr>
<div class="row">
  <div class="col-lg-12">
    <div class="box">
      <div class="body">
        <table class="table table-condensed table-hovered sortableTable">
          <thead>
            <tr>
              <th>Currency
                <i class="fa sort"></i>
              </th>
              <th>Total
                <i class="fa sort"></i>
              </th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($revenue as $key => $value): ?>
              <tr>
                <td><?= $key; ?></td>
                <td><?= number_format($value, 2); ?></td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>