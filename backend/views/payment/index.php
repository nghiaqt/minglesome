<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->params['breadcrumbs'][] = ['label' => 'Payment'];
?>
<div class="row">
  <div class="col-lg-12">
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $model,
        'layout' => "<div class='responsive-table'>{items}</div>\n
          <div class='row'>
            <div class='col-xs-6'>{summary}</div>
            <div class='col-xs-6'>
              <div class='dataTables_paginate paging_simple_numbers'>
              {pager}
              </div>
            </div>
          </div>",
        'tableOptions' => ['class' => 'table table-striped table-bordered sortableTable responsive-table dataTable'],
        'columns' => [
            'email',
            'packageName',
            'packageMonth',
            [
                'attribute' => 'amount',
                'value' => function($model) {
                  return '(' . $model->currency . ') ' . number_format($model->amount, 2);
                }
            ],
            'inviteCode',
            [
                'attribute' => 'total',
                'value' => function($model) {
                  return '(' . $model->currency . ') ' . $model->total;
                }
            ],
            [
                'attribute' => 'createdAt',
                'filter' => false,
                'value' => function($model) {
                  if (isset($model->createdAt->sec)) {
                    return $model->createdAt->sec;
                  } else {
                    return '';
                  }
                },
                'format' => 'date'
            ]
        ],
    ]);
    ?>
  </div>
</div>
