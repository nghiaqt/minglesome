<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use backend\assets\AppAsset;
use backend\widgets\Alert;

/**
 * @var \yii\web\View $this
 * @var string $content
 */
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
  <head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode(Yii::$app->id) ?></title>		
    <base href="<?= Yii::getAlias('@web') ?>/">
    <?php $this->head() ?>
  </head>
  <body class="bg-dark dk">
    <?php $this->beginBody() ?>
    <div id="wrap">

      <?= $this->render('top'); ?>
      <?= $this->render('left'); ?>

      <div id="content">
        <div class="outer">
          <div class="inner bg-light lter">
            <?= Alert::widget() ?>	
            <?= $content ?>
          </div>
          <!-- /.inner -->           
        </div>
      </div>
    </div>	

    <footer class="Footer bg-dark dker">
      <p>2014 &copy; Metis Bootstrap Admin Template</p>
    </footer>
    <!-- /#footer -->

    <script>
      window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>');
    </script>
    <?php $this->endBody() ?>
  </body>
</html>
<?php $this->endPage() ?>
