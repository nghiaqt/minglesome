<?php

use yii\widgets\Menu;

$user = Yii::$app->user->getIdentity();
?>

<div id="left">

  <!-- BEGIN MAIN NAVIGATION -->
  <div class="media user-media bg-dark dker">
    <div class="user-media-toggleHover">
      <span class="fa fa-user"></span> 
    </div>
    <div class="user-wrapper bg-dark">
      <a class="user-link" href="">
        <img class="media-object img-thumbnail user-img" alt="User Picture" src="<?= $user->getAvatarUrl(); ?>">
        <span class="label label-danger user-label"></span> 
      </a> 
      <div class="media-body">
        <h5 class="media-heading">Archie</h5>
        <ul class="list-unstyled user-info">
          <li><?= $user->username; ?></li>
          <li>Last Access :
            <br>
            <small>
              <i class="fa fa-calendar"></i>&nbsp;<?= date('d M H:i', $user->lastSignedOn->sec); ?></small> 
          </li>
        </ul>
      </div>
    </div>
  </div>
  <!-- #menu -->
  <?=
  Menu::widget([
      'options' => [
          'id' => 'menu',
          'class' => 'bg-dark dker'
      ],
      'encodeLabels' => false,
      'items' => [
          // Important: you need to specify url as 'controller/action',
          // not just as 'controller' even if default action is used.
          ['label' => '<i class="fa fa-dashboard"></i><span class="link-title">&nbsp;DASHBOARD</span>',
              'url' => ['dashboard/index']],
          ['label' => '<i class="fa fa-comments "></i><span class="link-title">&nbsp;SUPPORT/INBOX</span>',
              'url' => ['contact/index']],
          ['label' => '<i class="fa fa-briefcase"></i><span class="link-title">&nbsp;INVITES</span>',
              'url' => ['invite/index']],
          ['label' => '<i class="fa fa-credit-card"></i><span class="link-title">&nbsp;PAYMENTS</span>',
              'url' => ['payment/index']],
          ['label' => '<i class="fa fa-user"></i><span class="link-title">&nbsp;MEMBERS</span>',
              'url' => ['user/index']],
          ['label' => '<i class="fa fa-user"></i><span class="link-title">&nbsp;FEEDBACK</span>',
              'url' => ['feedback/index']],
          ['label' => '<i class="fa fa-user"></i><span class="link-title">&nbsp;REPORT</span>',
              'url' => ['report/index']],
          ['label' => '<i class="fa fa-comment"></i><span class="link-title">&nbsp;POLLS</span>',
              'url' => ['poll/index']],
          ['label' => '<i class="fa fa-image"></i><span class="link-title">&nbsp;BANNERS</span>',
              'url' => ['banner/index']],
//           ['label' => '<i class="fa fa-group"></i><span class="link-title">&nbsp;MAGAZINE</span>',
//               'url' => ['magazine/index']],
          ['label' => '<i class="fa fa-money"></i><span class="link-title">&nbsp;PRODUCTS</span>',
              'url' => ['package/index']],
          ['label' => '<i class="fa fa-file"></i><span class="link-title">&nbsp;PAGES</span>',
              'url' => ['page/index']],
          ['label' => '<i class="fa fa-file"></i><span class="link-title">&nbsp;EMAIL TEMPLATES</span>',
              'url' => ['email-template/index']],
          ['label' => '<i class="fa fa-file"></i><span class="link-title">&nbsp;COUNTRIES</span>',
              'url' => ['country/index']],
          ['label' => '<i class="fa fa-file"></i><span class="link-title">&nbsp;STATES</span>',
              'url' => ['state/index']],
          ['label' => '<i class="fa fa-file"></i><span class="link-title">&nbsp;CITIES</span>',
              'url' => ['city/index']],
          ['label' => '<i class="fa fa-file"></i><span class="link-title">&nbsp;PROFILE CONTENT</span><span class="fa arrow"></span>',
              'url' => ['post/index'],
              'items' => [
                  ['label' => '<i class="fa fa-angle-right"></i>&nbsp; ORIENTATION', 'url' => ['post/index', 'type' => 'orientation']],
                  ['label' => '<i class="fa fa-angle-right"></i>&nbsp; STYLE', 'url' => ['post/index', 'type' => 'style']],
                  ['label' => '<i class="fa fa-angle-right"></i>&nbsp; FIGURE', 'url' => ['post/index', 'type' => 'figure']],
                  ['label' => '<i class="fa fa-angle-right"></i>&nbsp; ETHNICITY', 'url' => ['post/index', 'type' => 'ethnicity']],
                  ['label' => '<i class="fa fa-angle-right"></i>&nbsp; OTHER', 'url' => ['post/index', 'type' => 'other']],
                  ['label' => '<i class="fa fa-angle-right"></i>&nbsp; RELIGION', 'url' => ['post/index', 'type' => 'religion']],
                  ['label' => '<i class="fa fa-angle-right"></i>&nbsp; UPFOR', 'url' => ['post/index', 'type' => 'upfor']],
                  ['label' => '<i class="fa fa-angle-right"></i>&nbsp; SPEAKING', 'url' => ['post/index', 'type' => 'speaking']],
                  ['label' => '<i class="fa fa-angle-right"></i>&nbsp; QUESTION', 'url' => ['post/index', 'type' => 'question']],
              ]],
          ['label' => '<i class="fa fa-file"></i><span class="link-title">&nbsp;ACTIVITY EVENT</span>',
              'url' => ['activity-event/index']],
          ['label' => '<i class="fa fa-file"></i><span class="link-title">&nbsp;REASON</span><span class="fa arrow"></span>',
              'url' => ['reason/index'],
              'items'=>[
                  ['label' => '<i class="fa fa-angle-right"></i>&nbsp; REASON CANCEL', 'url' => ['reason/index', 'type' => 'subscription']],
                  ['label' => '<i class="fa fa-angle-right"></i>&nbsp; REASON CONTACT', 'url' => ['reason/index', 'type' => 'contact']],
              ]],
          ['label' => '<i class="fa fa-file"></i><span class="link-title">&nbsp;TRANSLATIONS</span>',
              'url' => ['translate/index']],
          ['label' => '<i class="fa fa-file"></i><span class="link-title">&nbsp;TRANSLATIONS FOR COUNTRY</span>',
              'url' => ['translate-for-country/index']],
          ['label' => '<i class="fa fa-file"></i><span class="link-title">&nbsp;SETTINGS</span><span class="fa arrow"></span>',
              'url' => ['setting/index'],
              'items' => [
                  ['label' => '<i class="fa fa-angle-right"></i>&nbsp; SITE', 'url' => ['setting/index', 'type' => 'site']],
                  ['label' => '<i class="fa fa-angle-right"></i>&nbsp; PAYPAL', 'url' => ['setting/index', 'type' => 'paypal']],
//                  ['label' => '<i class="fa fa-angle-right"></i>&nbsp; EMAIL', 'url' => ['setting/index', 'type' => 'email']],
//                   ['label' => '<i class="fa fa-angle-right"></i>&nbsp; JOIN TRY', 'url' => ['setting/index', 'type' => 'join-try']],
                  ['label' => '<i class="fa fa-angle-right"></i>&nbsp; SOCIAL', 'url' => ['setting/index', 'type' => 'social']],
                  ['label' => '<i class="fa fa-angle-right"></i>&nbsp; MEMBERS SAY', 'url' => ['setting/index', 'type' => 'member-say']],
                  ['label' => '<i class="fa fa-angle-right"></i>&nbsp; WITHIN', 'url' => ['setting/index', 'type' => 'within']],
              ]],
          ['label' => '<i class="fa fa-bar-chart-o"></i><span class="link-title">&nbsp;STATISTICS</span>',
              'url' => ['dashboard/index']],
      ],
  ]);
  ?>
  <!-- /#menu -->
  <!-- END MAIN NAVIGATION -->

</div>