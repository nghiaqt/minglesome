<?php

use yii\helpers\Html;
use backend\assets\AppAsset;
/**
 * @var \yii\web\View $this
 * @var string $content
 */
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" xmlns:ng="http://angularjs.org/" ng-app="app">
  <head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= Html::encode($this->title) ?></title>
    <base href="<?= Yii::getAlias('@web') ?>/">
    <?php $this->head() ?>
  </head>
  <body class="login">
    <?php $this->beginBody() ?>
            
        <?= $content ?>
    
    <?php $this->endBody() ?>
  </body>
</html>
<?php $this->endPage() ?>
