<?php

$this->params['breadcrumbs'][] = ['label' => 'Countries', 'url' => ['country/index']];
$this->params['breadcrumbs'][] = ['label' => 'Create'];
?>

<?= $this->render('_form', compact('model')); ?>
 