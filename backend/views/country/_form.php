<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\helpers\Language;
use common\models\Country;
?>

<?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>
<div class="row">
  <div class="col-lg-12">
    <div class="box">
      <header class="dark">
        <div class="icons">
          <i class="fa fa-check"></i>
        </div>
        <h5>Amount</h5>
        <!-- .toolbar -->
        <div class="toolbar">
          <nav style="padding: 8px;">
            <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
              <i class="fa fa-minus"></i>
            </a>
          </nav>
        </div><!-- /.toolbar -->
      </header>
      <div class="body">
        <?php 
        
        echo $form->field($model, 'countryCode');
        echo $form->field($model, 'languageCode')->dropDownList(Language::$languageCodes);
        echo $form->field($model, 'currency')->dropDownList(Language::$currency);
        echo $form->field($model, 'height')->dropDownList(Country::getHeight());
        echo $form->field($model, 'distance')->dropDownList(Country::getDistance());
        ?>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="box">
      <header class="dark">
        <div class="icons">
          <i class="fa fa-check"></i>
        </div>
        <h5>Country Name</h5>
        <!-- .toolbar -->
        <div class="toolbar">
          <nav style="padding: 8px;">
            <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
              <i class="fa fa-minus"></i>
            </a>
          </nav>
        </div><!-- /.toolbar -->
      </header>
      <div class="body">
        <?php
        foreach (Language::$languageCodes as $code => $name) :
          ?>
          <?= $form->field($model, 'countryName[' . $code . ']')->textInput()->label($name); ?>
          <?php
        endforeach;
        ?>
      </div>
    </div>
  </div>
</div>

<div class="form-actions no-margin-bottom">
  <input type="submit" value="Save" class="btn btn-primary">
</div>
<?php ActiveForm::end(); ?>