<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Country;

$this->params['breadcrumbs'][] = ['label' => 'Countries'];
$this->params['breadcrumbs'][] = ['label' => '<i class="fa fa-pencil"></i> Add New', 'url' => ['country/create']];
?>
<div class="row">
  <div class="col-lg-12">
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $model,
        'layout' => "<div class='responsive-table'>{items}</div>\n
          <div class='row'>
            <div class='col-xs-6'>{summary}</div>
            <div class='col-xs-6'>
              <div class='dataTables_paginate paging_simple_numbers'>
              {pager}
              </div>
            </div>
          </div>",
        'tableOptions' => ['class' => 'table table-striped table-bordered sortableTable responsive-table dataTable'],
        'columns' => [
            [
                'header' => 'Action',
                'options' => ['style' => 'width:50px'],
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
            ],
            'countryCode',
            [
                'attribute' => 'countryName',
                'format' => 'raw',
                'value' => function($model) {
                  if (is_array($model->countryName) && isset($model->countryName['en'])) {
                    return $model->countryName['en'];
                  }
                }
            ],
            'languageCode',
            'currency',
            [
                'attribute'=>'height',
                'filter'=>  Country::getHeight()
            ],
            [
                'attribute'=>'distance',
                'filter'=>  Country::getDistance()
            ],
            [
                'attribute' => 'createdAt',
                'filter' => false,
                'value' => function($model) {
                  if (isset($model->createdAt->sec)) {
                    return $model->createdAt->sec;
                  } else {
                    return '';
                  }
                },
                'format' => 'date'
            ]
        ],
    ]);
    ?>
  </div>
</div>
