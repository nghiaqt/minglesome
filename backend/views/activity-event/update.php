<?php

$this->params['breadcrumbs'][] = ['label' => 'Activity Event', 'url' => ['activity-event/index']];
$this->params['breadcrumbs'][] = ['label' => 'Update'];
?>

<?= $this->render('_form', compact('model')); ?>
 