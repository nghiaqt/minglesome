<div class="row">
  <div class="col-md-6">
    <?php
    echo $form->field($model, 'content[miles][0]')->label('Distance (miles):');
    echo $form->field($model, 'content[miles][1]')->label('Distance (miles):');
    echo $form->field($model, 'content[miles][2]')->label('Distance (miles):');
    echo $form->field($model, 'content[miles][3]')->label('Distance (miles):');
    echo $form->field($model, 'content[miles][4]')->label('Distance (miles):');
    ?>
  </div>
  <div class="col-md-6">
    <?php
    echo $form->field($model, 'content[km][0]')->label('Distance (km):');
    echo $form->field($model, 'content[km][1]')->label('Distance (km):');
    echo $form->field($model, 'content[km][2]')->label('Distance (km):');
    echo $form->field($model, 'content[km][3]')->label('Distance (km):');
    echo $form->field($model, 'content[km][4]')->label('Distance (km):');
    ?>
  </div>
</div>