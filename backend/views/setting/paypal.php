<?php
echo $form->field($model, 'content[live]')->inline()->radioList(['Sandbox', 'Live'])->label('Title');
echo $form->field($model, 'content[username]')->label('Username');
echo $form->field($model, 'content[password]')->label('Password');
echo $form->field($model, 'content[signature]')->label('Signature');
?>    