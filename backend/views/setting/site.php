<?php

echo $form->field($model, 'content[adminEmail]')->label('Admin Email');
echo $form->field($model, 'content[title]')->label('Title');
echo $form->field($model, 'content[description]')->textarea()->label('Description');
echo $form->field($model, 'content[keywords]')->textarea()->label('Keyword');
echo $form->field($model, 'content[script]')->textarea()->label('Script Analytics');
?>    