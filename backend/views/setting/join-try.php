
<?php

use common\helpers\Language;
use dosamigos\ckeditor\CKEditor;
?>

<?php

foreach (Language::$languageCodes as $code => $name) :
echo $form->field($model, 'content['.$code.']')->widget(CKEditor::className(), [
    'options' => ['rows' => 3],
    'preset' => 'full'
])->label($name);
endforeach;
?>