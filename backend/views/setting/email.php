<?php
echo $form->field($model, 'content[smtp]')->inline()->radioList([1=>'Yes', 0=>'No'])->label('Using SMTP');
echo $form->field($model, 'content[smtpHost]')->label('SMTP Host');
echo $form->field($model, 'content[smtpPort]')->label('SMTP Port');
echo $form->field($model, 'content[smtpAccount]')->label('SMTP Mail Account');
echo $form->field($model, 'content[smtpPassword]')->label('SMTP Mail Password');
?>