<?php
echo $form->field($model, 'content[media]')->label('Home Page Media (Youtube Id)');
echo $form->field($model, 'content[instagram]')->label('Instagram');
echo $form->field($model, 'content[facebook]')->label('Facebook');
echo $form->field($model, 'content[twitter]')->label('Twitter');
echo $form->field($model, 'content[google]')->label('Google+');
echo $form->field($model, 'content[youtube]')->label('Youtube');
echo $form->field($model, 'content[vimeo]')->label('Vimeo');
echo $form->field($model, 'content[pinterest]')->label('Pinterest');
echo $form->field($model, 'content[stumbleupon]')->label('Stumble Upon');
?>