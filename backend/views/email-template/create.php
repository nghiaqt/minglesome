<?php

$this->params['breadcrumbs'][] = ['label' => 'Email Template', 'url' => ['email-template/index']];
$this->params['breadcrumbs'][] = ['label' => 'Create'];
?>

<?= $this->render('_form', compact('model')); ?>
 