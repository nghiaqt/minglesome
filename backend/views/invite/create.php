<?php

$this->params['breadcrumbs'][] = ['label' => 'Invites', 'url' => ['invite/index']];
$this->params['breadcrumbs'][] = ['label' => 'Create'];
?>

<?= $this->render('_form', compact('model')); ?>
 