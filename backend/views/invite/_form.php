<?php

//helper
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\helpers\Language;
//widget
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
//model
use common\models\User;
use common\models\Package;
?>

<?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>
<div class="row">
  <div class="col-lg-12">
    <div class="box">
      <header class="dark">
        <div class="icons">
          <i class="fa fa-check"></i>
        </div>
        <h5>Invite Info</h5>
        <!-- .toolbar -->
        <div class="toolbar">
          <nav style="padding: 8px;">
            <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
              <i class="fa fa-minus"></i>
            </a>
          </nav>
        </div><!-- /.toolbar -->
      </header>
      <div class="body">

        <?php
        echo $form->field($model, 'packageId')
          ->checkBoxList(Package::getAllAffiliateAsArray());

        echo $form->field($model, 'owner')->widget(Select2::classname(), [
          'data' => User:: getAllAsArray(),
          'options' => ['disabled' => !$model->isNewRecord]
        ]);

        echo $form->field($model, 'code')->textInput(['readOnly' => !$model->isNewRecord]);

        echo $form->field($model, 'description')->textarea();

        echo $form->field($model, 'startDate')
          ->widget(DatePicker::classname(), [
            'pluginOptions' => [
              'autoclose' => true,
              'format' => 'yyyy-mm-dd'
            ]
        ]);

        echo $form->field($model, 'endDate')
          ->widget(DatePicker::classname(), [
            'pluginOptions' => [
              'autoclose' => true,
              'format' => 'yyyy-mm-dd'
            ]
        ]);
        ?>
      </div>
    </div>
  </div>
</div>
<div class="form-actions no-margin-bottom">
  <input type="submit" value="Save" class="btn btn-primary">
</div>
<?php ActiveForm::end(); ?>