<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var \common\models\LoginForm $model
 */
$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-signin">
  <div class="text-center">
    <img src="images/logo.png" alt="Metis Logo">
  </div>
  <hr>
  <div class="tab-content">
    <div id="login" class="tab-pane active">
      <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
      <p class="text-muted text-center">
        Enter your email and password
      </p>
      <?= $form->field($model, 'email', ['template' => '{input}'])->textInput(['placeholder' => 'Email', 'class' => 'form-control top']) ?>
      <?= $form->field($model, 'password', ['template' => '{input}{hint}{error}'])->passwordInput(['placeholder' => 'Password', 'class' => 'form-control bottom']) ?>

      <?= $form->field($model, 'rememberMe')->checkbox() ?>
      <?= Html::submitButton('Login', ['class' => 'btn btn-lg btn-primary btn-block', 'name' => 'login-button']) ?>
      <?php ActiveForm::end(); ?>
    </div>
    <div id="forgot" class="tab-pane">
      <form action="">
        <p class="text-muted text-center">Enter your valid e-mail</p>
        <input type="email" placeholder="mail@domain.com" class="form-control">
        <br>
        <button class="btn btn-lg btn-danger btn-block" type="submit">Recover Password</button>
      </form>
    </div>
  </div>
  <hr>
  <div class="text-center">
    <ul class="list-inline">
      <li> <a class="text-muted" href="#login" data-toggle="tab">Login</a>  </li>
      <li> <a class="text-muted" href="#forgot" data-toggle="tab">Forgot Password</a>  </li>
    </ul>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
      $('.list-inline li > a').click(function() {
        var activeForm = $(this).attr('href') + ' > form';
        //console.log(activeForm);
        $(activeForm).addClass('animated fadeIn');
        //set timer to 1 seconds, after that, unload the animate animation
        setTimeout(function() {
          $(activeForm).removeClass('animated fadeIn');
        }, 1000);
      });
    });
</script>