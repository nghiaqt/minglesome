<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\Country;
?>

<?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>
<div class="row">
  <div class="col-lg-12">
    <div class="box">
      <header class="dark">
        <div class="icons">
          <i class="fa fa-check"></i>
        </div>
        <h5>State Info</h5>
        <!-- .toolbar -->
        <div class="toolbar">
          <nav style="padding: 8px;">
            <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
              <i class="fa fa-minus"></i>
            </a>
          </nav>
        </div><!-- /.toolbar -->
      </header>
      <div class="body">
        <?php
        echo $form->field($model, 'countryCode')->dropDownList(Country::getAllAsArray(),['prompt'=>'Select a Country']);
        echo $form->field($model, 'name');
        ?>
        <div class="form-actions no-margin-bottom">
          <input type="submit" value="Save" class="btn btn-primary">
        </div>
      </div>
    </div>
  </div>
</div>
<?php ActiveForm::end(); ?>