<?php

$this->params['breadcrumbs'][] = ['label' => 'Magazine', 'url' => ['magazine/index']];
$this->params['breadcrumbs'][] = ['label' => 'Create'];
?>

<?= $this->render('_form', compact('model')); ?>
 