<?php

//helper
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
//widget
use kartik\widgets\Select2;
use dosamigos\ckeditor\CKEditor;
//model
use common\models\User;
use common\models\Country;
?>

<?php
$form = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data'],
            'layout' => 'horizontal'
        ]);
?>
<div class="row">
  <div class="col-lg-12">
    <div class="box">
      <header class="dark">
        <div class="icons">
          <i class="fa fa-check"></i>
        </div>
        <h5>Article Info</h5>
        <!-- .toolbar -->
        <div class="toolbar">
          <nav style="padding: 8px;">
            <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
              <i class="fa fa-minus"></i>
            </a>
          </nav>
        </div><!-- /.toolbar -->
      </header>
      <div class="body">

        <?php
        echo $form->field($model, 'owner')->widget(Select2::classname(), [
            'data' => User:: getAllAsArray()
        ]);
        echo $form->field($model, 'country')->dropDownList(Country::getAllAsArray());
        echo $form->field($model, 'title');

        echo $form->field($model, 'content', [
            'template' => "{label}\n<div class='col-sm-12'>{input}\n{error}</div>"
        ])->widget(CKEditor::className(), [
            'options' => ['rows' => 3],
            'preset' => 'standard'
        ]);
        ?>
        <div class="form-group">
          <label class="control-label col-sm-3">File</label>
          <div class="col-sm-6">
            <?php
            if ($model->file):
              if ($model->file['type'] == 'image'):
                ?>
                <img src="<?= MAGAZINE_URL . $model->file['name']; ?>" height="100"/>
              <?php elseif ($model->file['type'] == 'video'): ?>
                <video width="160" height="120" controls>
                  <source src="<?= MAGAZINE_URL . $model->file['name']?>" type="video/mp4">
                  Your browser does not support the video tag.
                </video>
                <?php
              endif;
            endif;
            ?>
            <?= Html::fileInput('file', '', ['accept' => 'image/*,video/mp4']); ?>
          </div>
        </div>

        <div class="form-actions no-margin-bottom">
          <input type="submit" value="Save" class="btn btn-primary">
        </div>
      </div>
    </div>
  </div>
</div>

<?php ActiveForm::end(); ?>