<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Country;

$this->params['breadcrumbs'][] = ['label' => 'Magazine'];
$this->params['breadcrumbs'][] = ['label' => '<i class="fa fa-pencil"></i> Add New', 'url' => ['magazine/create']];
?>
<div class="row">
  <div class="col-lg-12">
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $model,
        'layout' => "<div class='responsive-table'>{items}</div>\n
          <div class='row'>
            <div class='col-xs-6'>{summary}</div>
            <div class='col-xs-6'>
              <div class='dataTables_paginate paging_simple_numbers'>
              {pager}
              </div>
            </div>
          </div>",
        'tableOptions' => ['class' => 'table table-striped table-bordered sortableTable responsive-table dataTable'],
        'columns' => [
            [
                'header' => 'Action',
                'options' => ['style' => 'width:50px'],
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
            ],
            [
              'header'=>'File',
                'format'=>'raw',
              'value'=>function($model){
                if($model->file){
                  if($model->file['type']=='image'){
                    return Html::img(MAGAZINE_URL.$model->file['name'],['width'=>100]);
                  }else{
                    return '<video width="160" height="120" controls>
                  <source src="'.MAGAZINE_URL.$model->file['name'].'" type="video/mp4">
                  Your browser does not support the video tag.
                </video>';
                  }
                }
              }
            ],
            [
                'header' => 'Owner',
                'value' => function($model) {
                  $owner = $model->getOwner();
                  if ($owner) {
                    return $owner->username;
                  }else{
                    return 'deleted';
                  }
                }
            ],
            [
                'attribute' => 'country',
                'filter' => Country::getAllAsArray()
            ],
            'title',
            [
                'attribute' => 'createdAt',
                'filter' => false,
                'value' => function($model) {
                  if (isset($model->createdAt->sec)) {
                    return $model->createdAt->sec;
                  } else {
                    return '';
                  }
                },
                'format' => 'date'
            ]
        ],
    ]);
    ?>
  </div>
</div>
