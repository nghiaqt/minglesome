<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\UserFeedback;

$this->params['breadcrumbs'][] = ['label' => 'Feedback'];
?>
<div class="row">
  <div class="col-lg-12">
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $model,
        'layout' => "<div class='responsive-table'>{items}</div>\n
          <div class='row'>
            <div class='col-xs-6'>{summary}</div>
            <div class='col-xs-6'>
              <div class='dataTables_paginate paging_simple_numbers'>
              {pager}
              </div>
            </div>
          </div>",
        'tableOptions' => ['class' => 'table table-striped table-bordered sortableTable responsive-table dataTable'],
        'columns' => [
            'email',
            [
                'attribute' => 'type',
                'filter' => UserFeedback::getType()
            ],
            [
                'header' => 'Reasons',
                'filter' => false,
                'format' => 'raw',
                'value' => function($model) {
                  $reasons = $model->getReasons();
                  $return = '';
                  if ($reasons) {
                    foreach ($reasons as $item) {
                      $return.='<p>' . $item->name['en'] . '</p>';
                    }
                  }
                  return $return;
                }
            ],
            'feedback',
            [
                'attribute' => 'createdAt',
                'filter' => false,
                'value' => function($model) {
                  if (isset($model->createdAt->sec)) {
                    return $model->createdAt->sec;
                  } else {
                    return '';
                  }
                },
                'format' => 'date'
            ]
        ],
    ]);
    ?>
  </div>
</div>
