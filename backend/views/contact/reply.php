<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dosamigos\ckeditor\CKEditor;

$this->params['breadcrumbs'][] = ['label' => 'Support/inbox','url'=>'contact/index'];
$this->params['breadcrumbs'][] = ['label' => 'Reply'];
?>

<?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>
<div class="row">
  <div class="col-lg-12">
    <div class="box">
      <header class="dark">
        <div class="icons">
          <i class="fa fa-check"></i>
        </div>
        <h5>Reply Contact</h5>
        <!-- .toolbar -->
        <div class="toolbar">
          <nav style="padding: 8px;">
            <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
              <i class="fa fa-minus"></i>
            </a>
          </nav>
        </div><!-- /.toolbar -->
      </header>
      <div class="body">
        <?php
        echo $form->field($model, 'email');
        echo $form->field($model, 'subject');
        echo $form->field($model, 'content', [
            'template' => "{label}\n<div class='col-sm-12'>{input}\n{error}</div>"
        ])->widget(CKEditor::className(), [
            'options' => ['rows' => 3],
            'preset' => 'standard'
        ]);
        ?>
        <div class="form-actions no-margin-bottom">
          <input type="submit" value="Save" class="btn btn-primary">
        </div>
      </div>
    </div>
  </div>
</div>

<?php ActiveForm::end(); ?>