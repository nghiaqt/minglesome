<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Country;

$this->params['breadcrumbs'][] = ['label' => 'Support/inbox'];
?>
<div class="row">
  <div class="col-lg-12">
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $model,
        'layout' => "<div class='responsive-table'>{items}</div>\n
          <div class='row'>
            <div class='col-xs-6'>{summary}</div>
            <div class='col-xs-6'>
              <div class='dataTables_paginate paging_simple_numbers'>
              {pager}
              </div>
            </div>
          </div>",
        'tableOptions' => ['class' => 'table table-striped table-bordered sortableTable responsive-table dataTable'],
        'columns' => [
            [
                'header' => 'Action',
                'options' => ['style' => 'width:50px'],
                'class' => 'yii\grid\ActionColumn',
                'template' => '{reply}',
                'buttons' => [
                    'reply' => function ($url, $model) {
                      if ($model->createdBy) {
                        $url = FRONTSITE_URL . '#/view-profile/' . $model->createdBy;
                        return Html::a('<i class="fa fa-send"></i>', $url, [
                                    'title' => 'Reply',
                                    'target' => '_blank'
                        ]);
                      } else {
                        $url = Yii::$app->urlManager->createUrl(['contact/reply', 'id' => $model->_id]);
                        return Html::a('<i class="fa fa-send"></i>', $url, [
                                    'title' => 'Reply'
                        ]);
                      }
                    }
                ],
            ],
            [
                'attribute' => 'country',
                'filter' => Country::getAllAsArray()
            ],
            'email',
            [
                'attribute' => 'subject',
                'filter'=>  \common\models\Reason::getAllAsArray('en', 'contact'),
                'value' => function($model) {
                  if ($model->reason)
                    return $model->reason->name['en'];
                }
            ],
            'content',
            [
                'attribute' => 'createdAt',
                'filter' => false,
                'value' => function($model) {
                  if (isset($model->createdAt->sec)) {
                    return $model->createdAt->sec;
                  } else {
                    return '';
                  }
                },
                'format' => 'date'
            ]
        ],
    ]);
    ?>
  </div>
</div>
