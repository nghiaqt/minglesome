<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\helpers\Language;
use dosamigos\ckeditor\CKEditor;
?>

<?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>
<?php
if ($model->isNewRecord) {
  echo $form->field($model, 'type')->textInput();
}
?>
<?php
foreach (Language::$languageCodes as $code => $name) :
  ?>
  <div class="row">
    <div class="col-lg-12">
      <div class="box">
        <header class="dark">
          <div class="icons">
            <i class="fa fa-check"></i>
          </div>
          <h5><?= $name; ?></h5>
          <!-- .toolbar -->
          <div class="toolbar">
            <nav style="padding: 8px;">
              <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                <i class="fa fa-minus"></i>
              </a>
            </nav>
          </div><!-- /.toolbar -->
        </header>
        <div class="body">
          <?= $form->field($model, 'title[' . $code . ']')->textInput(); ?>
          <?=
          $form->field($model, 'content[' . $code . ']',[
              'template' => "{label}\n<div class='col-sm-12'>{input}\n{error}</div>"
              ])->widget(CKEditor::className(), [
              'options' => ['rows' => 3],
              'preset' => 'standard'
          ]);
          ?>
        </div>
      </div>
    </div>
  </div>
  <?php
endforeach;
?>
<div class="form-actions no-margin-bottom">
  <input type="submit" value="Save" class="btn btn-primary">
</div>
<?php ActiveForm::end(); ?>