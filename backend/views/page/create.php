<?php

$this->params['breadcrumbs'][] = ['label' => 'Page', 'url' => 'page/index'];
$this->params['breadcrumbs'][] = ['label' => 'Create'];
?>

<?= $this->render('_form', compact('model')); ?>
 