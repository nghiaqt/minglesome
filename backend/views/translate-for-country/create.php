<?php

$this->params['breadcrumbs'][] = ['label' => 'Translations', 'url' => ['translate-for-country/index']];
$this->params['breadcrumbs'][] = ['label' => 'Create'];
?>

<?= $this->render('_form', compact('model')); ?>
 