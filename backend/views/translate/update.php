<?php

$this->params['breadcrumbs'][] = ['label' => 'Translations', 'url' => ['translate/index']];
$this->params['breadcrumbs'][] = ['label' => 'Update'];
?>

<?= $this->render('_form', compact('model')); ?>
 