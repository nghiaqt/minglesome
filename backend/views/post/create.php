<?php

$this->params['breadcrumbs'][] = ['label' => 'Profile ' . $model->type, 'url' => ['post/index', 'type' => $model->type]];
$this->params['breadcrumbs'][] = ['label' => 'Create'];
?>

<?= $this->render('_form', compact('model')); ?>
 