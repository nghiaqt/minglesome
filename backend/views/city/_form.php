<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use common\models\Country;
use common\models\State;
?>

<?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>
<div class="row">
  <div class="col-lg-12">
    <div class="box">
      <header class="dark">
        <div class="icons">
          <i class="fa fa-check"></i>
        </div>
        <h5>City Info</h5>
        <!-- .toolbar -->
        <div class="toolbar">
          <nav style="padding: 8px;">
            <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
              <i class="fa fa-minus"></i>
            </a>
          </nav>
        </div><!-- /.toolbar -->
      </header>
      <div class="body">
        <?php
        echo $form->field($model, 'countryCode')->dropDownList(Country::getAllAsArray(), ['prompt' => 'Select a Country']);
        echo $form->field($model, 'stateId')->dropDownList(State::getAllAsArray($model->countryCode), ['prompt' => 'Selete a State']);
        echo $form->field($model, 'name');
        ?>
        <div class="form-actions no-margin-bottom">
          <input type="submit" value="Save" class="btn btn-primary">
        </div>
      </div>
    </div>
  </div>
</div>
<?php ActiveForm::end(); ?>
<?php
    $this->registerJs("
      var countrySelect = $('#city-countrycode');
      var stateSelect = $('#city-stateid');
      countrySelect.change(function() {
        $.ajax({
          type: 'POST',
          url: '". Yii::$app->urlManager->createAbsoluteUrl(['state/get-data'])."',
          data: {countryCode: countrySelect.val()}
        }).done(function(data) {
          var options = '';
          data = jQuery.parseJSON(data);
          stateSelect.find('option').remove();
          stateSelect.append($('<option value=\"\">Selete a State</option>'));
          for (var i in data)
          {
            options += '<option value=\"' + i + '\">' + data[i] + '</option>';
          }
          console.log(options);
          stateSelect.append($(options));
        });
      });
    ",View::POS_END);
?>