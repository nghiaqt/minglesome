<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->params['breadcrumbs'][] = ['label' => 'Cities'];
$this->params['breadcrumbs'][] = ['label' => '<i class="fa fa-pencil"></i> Add New', 'url' => ['city/create']];
?>
<div class="row">
  <div class="col-lg-12">
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $model,
        'layout' => "<div class='responsive-table'>{items}</div>\n
          <div class='row'>
            <div class='col-xs-6'>{summary}</div>
            <div class='col-xs-6'>
              <div class='dataTables_paginate paging_simple_numbers'>
              {pager}
              </div>
            </div>
          </div>",
        'tableOptions' => ['class' => 'table table-striped table-bordered sortableTable responsive-table dataTable'],
        'columns' => [
            [
                'header' => 'Action',
                'options' => ['style' => 'width:50px'],
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
            ],
            'countryCode',
            'name',
            [
                'attribute' => 'createdAt',
                'filter' => false,
                'value' => function($model) {
                  if (isset($model->createdAt->sec)) {
                    return $model->createdAt->sec;
                  } else {
                    return '';
                  }
                },
                'format' => 'date'
            ]
        ],
    ]);
    ?>
  </div>
</div>
