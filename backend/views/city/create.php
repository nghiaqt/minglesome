<?php

$this->params['breadcrumbs'][] = ['label' => 'Cities', 'url' => ['city/index']];
$this->params['breadcrumbs'][] = ['label' => 'Create'];
?>

<?= $this->render('_form', compact('model')); ?>
 