<?php
$this->params['breadcrumbs'][] = ['label' => 'User', 'url' => 'user/index'];
$this->params['breadcrumbs'][] = ['label' => 'View'];

use yii\widgets\DetailView;
?>

<?php
echo DetailView::widget([
    'model'=>$model,
    'attributes'=>[
        'username',
        'email',
        'age',
        'country',
        'state',
        'city',
    ]
    ]);
?>