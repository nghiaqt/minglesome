<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use common\models\User;
use common\models\Country;
use common\models\ProfileContent;
?>
<div class="row">
  <div class="col-lg-12">
    <div class="box">
      <header class="dark">
        <div class="icons">
          <i class="fa fa-check"></i>
        </div>
        <h5>User Info</h5>
        <!-- .toolbar -->
        <div class="toolbar">
          <nav style="padding: 8px;">
            <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
              <i class="fa fa-minus"></i>
            </a>
          </nav>
        </div><!-- /.toolbar -->
      </header>
      <div class="body">
        <?php
        $form = ActiveForm::begin(['layout' => 'horizontal']);
        ?>
        <?php
        echo $form->field($model, 'username')->textInput();
        echo $form->field($model, 'email')->textInput();
        echo $form->field($model, 'newPassword')->passwordInput();
        echo $form->field($model, 'birthdate')->widget(DatePicker::classname(), [
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ]
        ]);
        echo $form->field($model, 'gender')->dropDownList(User::getGenders());
        echo $form->field($model, 'country')->dropDownList(Country::getAllAsArray());
        echo $form->field($model, 'state')->textInput();
        echo $form->field($model, 'city')->textInput();
        echo $form->field($model, 'statusMarried')->dropDownList(User::getStatusMarried());
        echo $form->field($model, 'orientation')->dropDownList(ProfileContent::getAllAsArray('orientation'));
        echo $form->field($model, 'styles')->widget(Select2::classname(), [
            'data' => ProfileContent::getAllAsArray('style'),
            'options' => ['multiple' => true]
        ]);
        echo $form->field($model, 'figure')->dropDownList(ProfileContent::getAllAsArray('figure'));
//        echo $form->field($model, 'height')->textInput();
        echo $form->field($model, 'ethnicity')->dropDownList(ProfileContent::getAllAsArray('ethnicity'));
        echo $form->field($model, 'religion')->dropDownList(ProfileContent::getAllAsArray('religion'));
        echo $form->field($model, 'upfors')->widget(Select2::classname(), [
            'data' => ProfileContent::getAllAsArray('upfor'),
            'options' => ['multiple' => true]
        ]);
        echo $form->field($model, 'others')->widget(Select2::classname(), [
            'data' => ProfileContent::getAllAsArray('other'),
            'options' => ['multiple' => true]
        ]);
        echo $form->field($model, 'speakings')->widget(Select2::classname(), [
            'data' => ProfileContent::getAllAsArray('speaking'),
            'options' => ['multiple' => true]
        ]);
        echo $form->field($model, 'baggage')->textarea();
        echo $form->field($model, 'inMyOwnWords')->textarea();
        echo $form->field($model, 'questionId')->dropDownList(ProfileContent::getAllAsArray('question'));
        echo $form->field($model, 'answer')->textarea();
        echo $form->field($model, 'status')->dropDownList(User::getStatus());
        echo $form->field($model, 'expiredDelayCancel')->widget(DatePicker::classname(), [
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ]
        ]);
        echo $form->field($model, 'level')->dropDownList(User::getLevel());
        echo $form->field($model, 'expiredPremium')->widget(DatePicker::classname(), [
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ]
        ]);
        ?>
        <div class="form-actions no-margin-bottom">
          <input type="submit" value="Save" class="btn btn-primary">
        </div>
        <?php
        ActiveForm::end();
        ?>
      </div>
    </div>
  </div>
</div>