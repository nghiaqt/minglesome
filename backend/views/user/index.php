<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;
use kartik\widgets\DatePicker;
use yii\helpers\Url;
use common\models\User;
use common\models\Country;

$this->params['breadcrumbs'][] = ['label' => 'Members'];
?>
<?php $form = ActiveForm::begin(['layout' => 'inline', 'method' => 'get']); ?>
<div class="row show-grid">
  <div class="col-lg-4">
    <div class="form-group field-user-status has-success">
      <?= Html::dropDownList('status', '', User::getStatus(), ['class' => 'form-control']) ?>
    </div>
    <?= Html::submitButton('Update Status', ['class' => 'btn btn-primary', 'name' => 'updateStatus']); ?>
  </div>
  <div class="col-lg-8">
    <?=
    DatePicker::widget([
      'name' => 'expired',
      'pluginOptions' => [
        'autoclose' => true,
        'format' => 'yyyy-mm-dd'
      ]
    ]);
    ?>
    <?= Html::submitButton('Update Premium', ['class' => 'btn btn-primary', 'name' => 'updateExpired']); ?>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <?=
    GridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $model,
      'layout' => "
          <div class='responsive-table'>
          {items}
          </div>
          <div class='row'>
            <div class='col-xs-6'>{summary}</div>
            <div class='col-xs-6'>
              <div class='dataTables_paginate paging_simple_numbers'>
              {pager}
              </div>
            </div>
          </div>",
      'tableOptions' => ['class' => 'table table-striped table-bordered sortableTable dataTable table-user'],
      'columns' => [
        [
          'class' => \yii\grid\CheckboxColumn::className(),
          'header' => ''
        ],
        [
          'header' => 'Action',
//                'options' => ['style' => 'width:50px'],
          'class' => 'yii\grid\ActionColumn',
          'template' => '{view} {update} {send}',
          'buttons' => [
            'view' => function ($url, $model) {
              return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', FRONTSITE_URL . '#/view-profile/' . $model->_id, [
                  'title' => 'View Profile',
                  'target' => '_blank'
              ]);
            },
            'send' => function($url, $model) {
              return Html::a('<span class="glyphicon glyphicon-send"></span>', 
                Url::to(['user/send-winner-email','id'=> (string)$model->_id]), [
                  'title' => 'Send Winner Email'
              ]);
            }
            ]
          ],
          'age',
          [
            'header' => 'Avatar',
            'filter' => false,
            'format' => 'raw',
            'value' => function($model) {
              return Html::img($model->getAvatarUrl(), ['width' => '50']);
            }
            ],
            [
              'attribute' => 'status',
              'filter' => User::getStatus(),
              'value' => function($model) {
                return $model->getStatusText();
              }
            ],
            'username',
            'userId',
            [
              'attribute' => 'level',
              'filter' => User::getLevel(),
              'value' => function($model) {
                return $model->getLevelText();
              }
            ],
            'email',
            [
              'attribute' => 'createdAt',
              'filter' => false,
              'value' => function($model) {
                if (isset($model->createdAt->sec)) {
                  return $model->createdAt->sec;
                } else {
                  return '';
                }
              },
              'format' => 'date'
            ],
            [
              'attribute' => 'country',
              'filter' => Country::getAllAsArray()
            ],
            [
              'attribute' => 'gender',
              'filter' => User::getGenders(),
              'value' => function($model) {
                return $model->getGenderText();
              }
            ],
            'inviteCode',
            'leads',
            'premiums',
            'invitedBy',
            'reported',
            'blocked',
            'messages',
            'events',
            [
              'attribute' => 'streaming',
              'value' => function($model) {
                return $model->getStreaming();
              }
            ],
            'photos',
            'wins',
            'polls',
            'comments',
          ],
        ]);
        ?>
      </div>
    </div>
    <?php ActiveForm::end(); ?>