<?php

namespace backend\controllers;

use \Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use common\helpers\File;
use common\models\Banner;

/**
 * Banner Controller
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class BannerController extends Controller {

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'access' => [
            'class' => AccessControl::className(),
            'ruleConfig' => [
                'class' => '\common\components\AccessRule'
            ],
            'rules' => [
                [
                    'actions' => [
                        'index', 'create', 'update', 'delete'
                    ],
                    'allow' => true,
                    'roles' => [99], //1 free, 2 trial, 3 premium, 99 supper admin
                ],
                [
                    'allow' => false, // Do not have access
                    'roles' => ['?']
                ]
            ]
        ]
    ];
  }

  /**
   * Lists all Banner models.
   * @return mixed
   */
  public function actionIndex() {
    $model = new Banner();
    $dataProvider = $model->search(Yii::$app->request->getQueryParams());
    return $this->render('index', compact('model', 'dataProvider'));
  }

  /**
   * Creates a new Banner model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate() {
    $model = new Banner();
    $model->createdBy = Yii::$app->user->id;
    if ($model->load(Yii::$app->request->post()) && $model->validate()) {
      //save file upload
      $file = File::upload(UploadedFile::getInstanceByName('file'), BANNER_PATH, true); 
      if ($file['status']) {
        $model->file = $file;
      } elseif ($file['message']) {
        $model->addError('file', $file['message']);
      }
      if (!$model->getErrors() && $model->save()) {
        return $this->redirect(['index']);
      }
    }
    return $this->render('create', compact('model'));
  }

  /**
   * Updates an existing Banner model.
   * If update is successful, the browser will be redirected to the 'index' page.
   * @param string $id Page PK
   * @return mixed
   */
  public function actionUpdate($id) {
    $model = $this->findModel($id);
    $model->formatDate();
    if ($model->load(Yii::$app->request->post()) && $model->validate()) {
      //save file upload
      $file = File::upload(UploadedFile::getInstanceByName('file'), BANNER_PATH);      
      if ($file['status']) {
        $model->file = $file;
      } elseif ($file['message']) {
        $model->addError('file', $file['message']);
      }
      if ($model->save()) {
        return $this->redirect(['index']);
      }
    }
    return $this->render('update', compact('model'));
  }

  /**
   * Deletes an existing Banner model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param string $id Page PK
   * @return mixed
   */
  public function actionDelete($id) {
    $this->findModel($id)->delete();
    return $this->redirect(Yii::$app->request->getReferrer() ? Yii::$app->request->getReferrer() : 'index');
  }

  /**
   * Finds the Banner model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @return Banner the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id) {
    if (($model = Banner::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

}