<?php

namespace backend\controllers;

use \Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\State;

/**
 * State Controller
 *
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class StateController extends Controller {

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'access' => [
            'class' => AccessControl::className(),
            'ruleConfig' => [
                'class' => '\common\components\AccessRule'
            ],
            'rules' => [
                [
                    'actions' => [
                        'index', 'create', 'update', 'delete','get-data'
                    ],
                    'allow' => true,
                    'roles' => [99], //1 free, 2 trial, 3 premium, 99 supper admin
                ],
                [
                    'allow' => false, // Do not have access
                    'roles' => ['?']
                ]
            ]
        ]
    ];
  }

  /**
   * Lists all Profile Content models.
   * @return mixed
   */
  public function actionIndex() {
    $model = new State();
    $dataProvider = $model->search(Yii::$app->request->getQueryParams());
    return $this->render('index', compact('model', 'dataProvider'));
  }

  /**
   * Creates a new Profile Content model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate() {
    $model = new State();
    $model->createdBy = Yii::$app->user->id;
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['index']);
    } else {
      return $this->render('create', compact('model'));
    }
  }

  /**
   * Updates an existing Profile Content model.
   * If update is successful, the browser will be redirected to the 'index' page.
   * @param string $id Page PK
   * @return mixed
   */
  public function actionUpdate($id) {
    $model = $this->findModel($id);
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['index']);
    } else {
      return $this->render('update', compact('model'));
    }
  }

  /**
   * Deletes an existing Profile Content model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param string $id Page PK
   * @return mixed
   */
  public function actionDelete($id) {
    $this->findModel($id)->delete();
    return $this->redirect(Yii::$app->request->getReferrer() ? Yii::$app->request->getReferrer() : 'index');
  }

  /**
   * Finds the Profile Content model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @return Profile Content the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id) {
    if (($model = State::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  public function actionGetData(){
    $return =[];
    if(isset($_POST['countryCode']) && $_POST['countryCode']){
      $return= State::getAllAsArray($_POST['countryCode']);
    }
    return json_encode($return);
  }
}