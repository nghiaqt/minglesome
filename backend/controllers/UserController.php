<?php

namespace backend\controllers;

use \Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\User;

/**
 * User Controller
 * 
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class UserController extends Controller {

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'access' => [
            'class' => AccessControl::className(),
            'ruleConfig' => [
                'class' => '\common\components\AccessRule'
            ],
            'rules' => [
                [
                    'actions' => [
                        'index', 'create', 'update', 'delete', 'send-winner-email'
                    ],
                    'allow' => true,
                    'roles' => [99], //1 free, 2 trial, 3 premium, 99 supper admin
                ],
                [
                    'allow' => false, // Do not have access
                    'roles' => ['?']
                ]
            ]
        ]
    ];
  }

  /**
   * Lists all User models.
   * @return mixed
   */
  public function actionIndex() {
    if (($post = Yii::$app->getRequest()->get()) && isset($post['selection']) && $post['selection']) {
      if (isset($post['updateStatus']) && isset($post['status'])) {
        foreach ($post['selection'] as $item) {
          $user = User::findOne($item);
          if ($user) {
            $user->status = $post['status'];
            if($post['status']==User::STATUS_DELETED){
              $user->expiredDelayCancel=date('Y-m-d',strtotime('+1 months'));
            }
            $user->save();
          }
        }
      } else if (isset($post['updateExpired']) && isset($post['expired']) && $post['expired'] > date('Y-m-d')) {
        foreach ($post['selection'] as $item) {
          $user = User::findOne($item);
          if ($user && $user->level <= User::LEVEL_PREMIUM) {
            $user->level = User::LEVEL_PREMIUM;
            $user->expiredPremium = $post['expired'];
            $user->save();
          }
        }
      }
    }
    $model = new User();
    $dataProvider = $model->search(Yii::$app->request->getQueryParams());
    return $this->render('index', compact('model', 'dataProvider'));
  }

  /**
   * Displays a single User model.
   * @param string $id User PK
   * @return mixed
   */
  public function actionView($id) {
    return $this->render('view', [
                'model' => $this->findModel($id),
    ]);
  }

  /**
   * Creates a new User model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate() {
    $model = new User();

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['index']);
    } else {
      return $this->render('create', [
                  'model' => $model,
      ]);
    }
  }

  /**
   * Updates an existing User model.
   * If update is successful, the browser will be redirected to the 'index' page.
   * @param string $id User PK
   * @return mixed
   */
  public function actionUpdate($id) {
    $model = $this->findModel($id);
    $model->birthdate = \common\helpers\DateHelper::formatDate($model->birthdate);
    $model->expiredPremium = \common\helpers\DateHelper::formatDate($model->expiredPremium);
    $model->expiredDelayCancel = \common\helpers\DateHelper::formatDate($model->expiredDelayCancel);
    if ($model->load(Yii::$app->request->post())) {
      if ($model->newPassword) {
        $model->setPassword($model->newPassword);
      }
      if ($model->save()) {
        return $this->redirect(['index']);
      }
    }
    return $this->render('update', [
                'model' => $model,
    ]);
  }
  
  public function actionDelete($id){
    if($this->findModel($id)->delete()){
      return $this->redirect(['index']);
    }
  }
  
  public function actionSendWinnerEmail($id){
    $user=$this->findModel($id);
    $user->setWinner();
    $user->save();
    return $this->redirect(['index']);
  }

    /**
   * Finds the User model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param string $id User PK
   * @return User the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id) {
    if (($model = User::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

}