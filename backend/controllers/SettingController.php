<?php

namespace backend\controllers;

use \Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\Setting;
use yii\web\UploadedFile;
use common\helpers\File;

/**
 * Setting Controller
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class SettingController extends Controller {

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'access' => [
            'class' => AccessControl::className(),
            'ruleConfig' => [
                'class' => '\common\components\AccessRule'
            ],
            'rules' => [
                [
                    'actions' => [
                        'index'
                    ],
                    'allow' => true,
                    'roles' => [99], //1 free, 2 trial, 3 premium, 99 supper admin
                ],
                [
                    'allow' => false, // Do not have access
                    'roles' => ['?']
                ]
            ]
        ]
    ];
  }

  /**
   * 
   * @return type
   */
  public function actionIndex() {
    $type = Yii::$app->getRequest()->get('type');
    if (!$type) {
      $type = 'site';
    }
    $model = Setting::findOne(['category' => $type]);
    if (!$model) {
      $model = new Setting();
      $model->category = $type;
    }
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
        $this->refresh();
    }
    return $this->render('index', compact('model'));
  }

}