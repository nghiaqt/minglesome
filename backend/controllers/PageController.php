<?php

namespace backend\controllers;

use \Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\Page;

/**
 * Page Controller
 *
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class PageController extends Controller {

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'access' => [
            'class' => AccessControl::className(),
            'ruleConfig' => [
                'class' => '\common\components\AccessRule'
            ],
            'rules' => [
                [
                    'actions' => [
                        'index', 'create', 'update', 'delete'
                    ],
                    'allow' => true,
                    'roles' => [99],//1 free, 2 trial, 3 premium, 99 supper admin
                ],
                [
                    'allow' => false, // Do not have access
                    'roles' => ['?']
                ]
            ]
        ]
    ];
  }

  /**
   * Lists all Page models.
   * @return mixed
   */
  public function actionIndex() {
    $model = new Page();
    $dataProvider = $model->search(Yii::$app->request->getQueryParams());
    return $this->render('index', compact('model', 'dataProvider'));
  }
  
  public function actionCreate(){
    $model=new Page();
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['index']);
    } else {
      return $this->render('create', compact('model'));
    }
  }

  /**
   * Updates an existing Page model.
   * If update is successful, the browser will be redirected to the 'index' page.
   * @param string $id Page PK
   * @return mixed
   */
  public function actionUpdate($id) {
    $model = $this->findModel($id);
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['index']);
    } else {
      return $this->render('update', compact('model'));
    }
  }

  /**
   * Finds the Page model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @return Page the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id) {
    if (($model = Page::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

}