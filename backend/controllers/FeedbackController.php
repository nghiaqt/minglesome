<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\UserFeedback;

/**
 * Email Template Controller
 *
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class FeedbackController extends Controller {

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'access' => [
            'class' => AccessControl::className(),
            'ruleConfig' => [
                'class' => '\common\components\AccessRule'
            ],
            'rules' => [
                [
                    'actions' => [
                        'index', 'create', 'update', 'delete'
                    ],
                    'allow' => true,
                    'roles' => [99], //1 free, 2 trial, 3 premium, 99 supper admin
                ],
                [
                    'allow' => false, // Do not have access
                    'roles' => ['?']
                ]
            ]
        ]
    ];
  }

  /**
   * Lists all Profile Content models.
   * @return mixed
   */
  public function actionIndex() {
    $model = new UserFeedback();
    $dataProvider = $model->search(Yii::$app->request->getQueryParams());
    return $this->render('index', compact('model', 'dataProvider'));
  }

}