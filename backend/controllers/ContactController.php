<?php

namespace backend\controllers;

//load helper
use \Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
//load model
use common\models\Contact;
use backend\models\EmailForm;
use common\helpers\Email;

/**
 * Payment Controller
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class ContactController extends Controller {

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'access' => [
            'class' => AccessControl::className(),
            'ruleConfig' => [
                'class' => '\common\components\AccessRule'
            ],
            'rules' => [
                [
                    'actions' => [
                        'index', 'reply'
                    ],
                    'allow' => true,
                    'roles' => [99], //1 free, 2 trial, 3 premium, 99 supper admin
                ],
                [
                    'allow' => false, // Do not have access
                    'roles' => ['?']
                ]
            ]
        ]
    ];
  }

  /**
   * load contact list
   * @return type
   */
  public function actionIndex() {
    $model = new Contact();
    $dataProvider = $model->search(Yii::$app->request->getQueryParams());
    return $this->render('index', compact('model', 'dataProvider'));
  }

  public function actionReply($id) {
    $contact = $this->findModel($id);
    $model = new EmailForm();
    $model->subject = 'Re: ' . $contact->subject;
    $model->email = $contact->email;
    $model->content = $contact->content;
    if ($model->load(Yii::$app->request->post()) && $model->validate()) {
      Email::send($model->subject, $model->content, $model->email);
      return $this->redirect(['contact/index']);
    }
    return $this->render('reply', compact('model'));
  }

  /**
   * Finds the Banner model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @return Banner the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id) {
    if (($model = Contact::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

}