<?php

namespace backend\controllers;

//load helper
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
//load model
use common\models\User;
use common\models\Invoice;
use common\models\InviteHistory;
use common\models\ReportUser;
use common\models\BlockUser;
use common\models\Picture;
use common\models\ChatConversation;
use common\models\DateEvent;
use common\models\MagazineComment;
use common\models\UserFeedback;
use common\helpers\Language;
use common\models\Polls;

/**
 * Dashboard Controller
 * 
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class DashboardController extends Controller {

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'access' => [
            'class' => AccessControl::className(),
            'ruleConfig' => [
                'class' => '\common\components\AccessRule'
            ],
            'rules' => [
                [
                    'actions' => [
                        'index', 'create', 'update', 'delete'
                    ],
                    'allow' => true,
                    'roles' => [99], //1 free, 2 trial, 3 premium, 99 supper admin
                ],
                [
                    'allow' => false, // Do not have access
                    'roles' => ['?']
                ]
            ]
        ]
    ];
  }

  /**
   * Statistic system
   * 
   * @return type
   */
  public function actionIndex() {
    $statistic['members'] = User::find()->count();
    $statistic['upgrades'] = Invoice::find()->where(['isPayment' => true])->count();
    $statistic['invites'] = InviteHistory::find()->count();

    $statistic['inbox'] = InviteHistory::find()->count(); //inbox/support

    $statistic['reports'] = ReportUser::find()->count();
    $statistic['blocks'] = BlockUser::find()->count();
    $statistic['photos'] = Picture::find()->count();
    $statistic['messages'] = ChatConversation::find()->count();
    $statistic['events'] = DateEvent::find()->count();

    $statistic['streamings'] = User::find()->sum('streaming');
    $statistic['polls'] = Polls::find()->count();

    $statistic['comments'] = MagazineComment::find()->count();
    $statistic['subscriptions'] = UserFeedback::find()->where(['type' => UserFeedback::TYPE_SUB])->count();
    $statistic['memberships'] = UserFeedback::find()->where(['type' => UserFeedback::TYPE_MEM])->count();

    $revenue = [];
    foreach (Language::$currency as $key => $value) {
      $revenue[$value] = Invoice::find()
              ->where([
                  'currency' => $key,
                  'isPayment' => STATUS_CONFIRMED
              ])
              ->sum('amount');
    }
    return $this->render('index', compact('statistic', 'revenue'));
  }

}

