<?php

namespace backend\controllers;

use \Yii;
use yii\web\Controller;
use backend\models\LoginForm;
use common\models\User;

/**
 * Site controller
 *
 * @author Tuong Tran <tuong.tran@outlook.com>
 * @version 0.1
 */
class SiteController extends Controller {

  public function actionLogin() {
    if (Yii::$app->user->isGuest) {
      $this->layout = 'login';
      $model = new LoginForm();

      if ($model->load(Yii::$app->request->post()) && $model->login()) {
        return $this->redirect(['dashboard/index']);
      } else {
        return $this->render('login', [
                    'model' => $model,
        ]);
      }
    } else {
      if (Yii::$app->user->identity->level <= User::LEVEL_PREMIUM) {
        Yii::$app->user->logout();
        return $this->refresh();
      } else {
        return $this->redirect(['dashboard/index']);
      }
    }
  }

  public function actionLogout() {
    Yii::$app->user->logout();
    return $this->redirect(['site/login']);
  }

  public function actionError() {
    die('Error');
  }

}
