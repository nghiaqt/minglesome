<?php

namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class EmailForm extends Model {

  public $email;
  public $subject;
  public $content;

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
        // email and password are both required
        [['email', 'subject', 'content'], 'required'],
        ['email', 'email'],
        // rememberMe must be a boolean value
        [['subject', 'content'], 'string'],
    ];
  }

}