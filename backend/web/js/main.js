/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * add/remove answer field for polls action 'poll/index'
 */
$('#answers').on('click', '.add-answer-field', function() {
  $('#answers .answer-fields').append('<div class="input-group"><input type="text" class="form-control" name="Polls[answers][]" value=""><span class="input-group-addon"><a href="#" title="Delete Field" class="del-answer-field glyphicon glyphicon-remove"></a></span></div>');
});
$('#answers').on('click', '.del-answer-field', function() {
  $(this).parent().parent().remove();
  return false;
});
