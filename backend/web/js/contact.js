$(document).ready(function() {
	var socket = io.connect(window.CONFIG.nodeurl);
	$('.delete-contact').click(function() {
		var answer = confirm('Do you want to delete it?');
		var contactId = $(this).attr('contactId');
		if (answer) {
			window.location.assign(window.CONFIG.baseurl + 'contact/delete/' + contactId);
		}
	});
	/**
	 * set status is replied for each message
	 */
	$('.set-reply').click(function(){
		
		var conversationId = $(this).attr('conversation-id');
		var isMember = $(this).attr('is-member');
		var isReplied = $(this).attr('is-replied');
		var userId = $(this).attr('userId');
		var el = $(this);
		//if this message was replied
		if(!isReplied){
			$.post('/contact/set-replied', {conversationId: conversationId}, function(res){				
				el.attr('is-replied', 'yes');
				el.find('.label-status').html('Replied');
			});
		}
		//if this is member
		if(isMember){
			//go to profile viewer
			window.open(window.CONFIG.siteUrl+'#/view-profile/'+userId, '_blank');
		}
	});
});
