<?php

use yii\helpers\Html;
use common\models\Setting;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?= Setting::get('site', 'description') ?>">
    <meta name="keywords" content="<?= Setting::get('site', 'keywords') ?>">
    <meta name="author" content="hoanvusolutions.com">
    <title><?= Html::encode(Setting::get('site', 'title')) ?></title>
    <base href="<?= Yii::getAlias('@web') ?>/">
    <link href="/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/support.css" rel="stylesheet">
    <?php $this->head() ?>
  </head>
  <body>
    <?php $this->beginBody() ?>
    <div class="container">
        <div class="header">
          <div class="logo"><a ui-sref="home"><img src="images/img_05.jpg"></a></div><!--logo-->
        </div><!--header-->
        <?php if($page):?>
        <div class="content">
          <h1 class="title"><?php echo $page['title'];?></h1>
          <div>
            <?php echo $page['content'];?>
          </div>
        </div>
        <?php endif;?>
    </div>
    <?php $this->endBody() ?>			
  </body>
</html>
<?php $this->endPage() ?>