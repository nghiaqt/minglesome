<?php

use yii\helpers\Html;
use yii\web\View;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;
use common\models\User;
use common\models\Setting;
use common\helpers\GeoLocation;

// load app asset
AppAsset::register($this);

//load js
$this->registerJsFile('i18n/angular-locale_' . Yii::$app->session['language'] .
  '.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

// register javascript
if (Yii::$app->user->isGuest) {
  $this->registerJs('window.USER = {}', View::POS_HEAD);
} else {
  $user = str_replace('\\', '', json_encode(Yii::$app->user->identity
        ->getFullPublicData(Yii::$app->session['language'], Yii::$app->session['height']), JSON_UNESCAPED_UNICODE));
  $this->registerJs('window.USER = ' . $user, View::POS_HEAD);
}

$this->registerJs('window.CONFIG = {
          baseurl: "' . Yii::getAlias('@web') . '/",
          nodeurl: "' . NODE_SERVER_URL . '",
          apiUrl: "' . API_URL . '",
          language: "' . Yii::$app->session['language'] . '",
          currency: "' . Yii::$app->session['currency'] . '",
          height: "' . Yii::$app->session['height'] . '",
          distance: "' . Yii::$app->session['distance'] . '",
          country:"' . GeoLocation::getCountryByIp() . '",
          _country:"' . Yii::$app->session['country'] . '"
        };
        window.ROLE = {
          free:' . User::LEVEL_FREE . ',
          trial:' . User::LEVEL_TRIAL . ',
          premium:' . User::LEVEL_PREMIUM . '
        };', View::POS_HEAD);

$this->registerJs('window.flash={};', View::POS_HEAD);
if (Yii::$app->session->hasFlash('success')) {
  $this->registerJs('window.flash.success="' .
    Yii::$app->session->getFlash('success') . '";', View::POS_HEAD);
}
if (Yii::$app->session->hasFlash('error')) {
  $this->registerJs('window.flash.danger="' .
    Yii::$app->session->getFlash('error') . '";', View::POS_HEAD);
}

//load socket
$user_agent = $_SERVER['HTTP_USER_AGENT'];
if (preg_match('/(?i)msie [6-9]/', $_SERVER['HTTP_USER_AGENT']) ||
  preg_match('/Android ([1](?:\.[0-9])?|[4](?:\.[0-3])?)$/', $user_agent) ||
  preg_match('/opera mini/i', $user_agent)) {
  $this->registerJs('window.isNotSupportBrowser = true;', View::POS_HEAD);
  $this->registerJs('window.flash.danger="Your browser doesn\'t seem to support '
    . 'the latest technologies. Some features on our site will not work. '
    . 'Please download/upgrade to: Chrome or IE10......";', View::POS_HEAD);
} else {
  $this->registerJsFile(NODE_SERVER_URL . 'socket.io/socket.io.js');
}
?>

<?php $this->beginPage() ?>
<!DOCTYPE HTML>
<html lang="<?= Yii::$app->language ?>" xmlns:ng="http://angularjs.org" id="ng-app" ng-app="app">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?= Setting::get('site', 'description') ?>">
    <meta name="keywords" content="<?= Setting::get('site', 'keywords') ?>">
    <meta name="author" content="hoanvusolutions.com">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0' >
    <meta name="google-site-verification" content="RkJfAUvimGeMoMfJEXEMSNd-Exwwe1sP0smyajDn_a8" />
    <title><?= Html::encode(Setting::get('site', 'title')) ?></title>
    <base href="<?= Yii::getAlias('@web') ?>/">
    <!---Internet Explorer Compatibility---->
    <!--[if lte IE 7]>
      <script src="app/libs/json3.min.js"></script>
    <![endif]-->
    <!--[if lte IE 8]>
      <script>
        document.createElement('ng-include');
        document.createElement('ng-pluralize');
        document.createElement('ng-view');

        // Optionally these for CSS
        document.createElement('ng:include');
        document.createElement('ng:pluralize');
        document.createElement('ng:view');
      </script>
    <![endif]-->
    <!--[if lt IE 10]>
<script src="bower_components/xhr-xdr-adapter/src/xhr-xdr-adapter.js" type="text/javascript"></script>
<![endif]-->
    <?php $this->head() ?>
  </head>
  <body ng-class="{'firefox':detectedBrowser.browser.firefox}">
    <?php $this->beginBody() ?>

    <!--activity-->
    <div ng-if="authService.isAuthenticated()" class="activity-tab">
      <div pl-activity></div><div pl-camtab></div>
    </div>
    <!--end activity-->

    <!--registration-->
    <div ng-if="!authService.isAuthenticated()" register-form></div>
    <!--end registration-->

    <!--header-->
    <div class="header fixed">
      <div class="logo"><a ui-sref="home"><img ng-src="images/img_05.png"></a></div><!--logo-->
    </div>
    <!--end header-->

    <!--menu-->
    <div pl-menu></div>
    <!--end menu-->

    <!--action-->
    <div pl-action></div>
    <!--end action-->

    <!--wrap-data-->
    <div class='wrap-data' ng-class="{
      'page-profile':authService.isAuthenticated(),
        'page-guest':!authService.isAuthenticated()
    }">
      <!--content-->
      <ui-view></ui-view>
      <!--end content-->
    </div>
    <!--end wrap-data-->

    <?= Setting::get('site', 'script') ?>
    <?php $this->endBody() ?>
  </body>
</html>
<?php $this->endPage() ?>