<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Tuong Tran <tuong.tran@outlook.com>
 */
class AppAsset extends AssetBundle {

  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
      //3rd css
      'bower_components/bootstrap/dist/css/bootstrap.min.css',
      'bower_components/font-awesome/css/font-awesome.min.css',
      'bower_components/angular-loading-bar/build/loading-bar.min.css',
      'bower_components/videogular-themes-default/videogular.min.css',
      //our css
      'css/growl.css',
      'css/toggleswitch.css',
      'css/style.css',
      'css/checkbox.css',
      'css/responsive.css',
  ];
  public $js = [
      //our app
      'bower_components/jquery/dist/jquery.min.js',
      'app/libs/webrtc/adapter.js',
      'app/libs/webrtc/stream.js',
      'app/libs/TweenMax.min.js',
      'app/libs/jquery.slimscroll.js',
      'bower_components/moment/min/moment.min.js',
      'bower_components/jquery-ui/jquery-ui.min.js',
      'bower_components/lodash/dist/lodash.min.js',
      'bower_components/bootstrap/dist/js/bootstrap.min.js',
      'bower_components/ng-file-upload/angular-file-upload-shim.min.js',
      'bower_components/angular/angular.min.js',
      'bower_components/ng-file-upload/angular-file-upload.min.js',
      'bower_components/angular-ui-router/release/angular-ui-router.min.js',
      'bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
      'bower_components/restangular/dist/restangular.min.js',
      'bower_components/angular-cookies/angular-cookies.min.js',
      'bower_components/angular-sanitize/angular-sanitize.min.js',
      'bower_components/angular-animate/angular-animate.min.js',
      'bower_components/angular-loading-bar/build/loading-bar.min.js',
      'bower_components/angular-translate/angular-translate.min.js',
      'bower_components/angular-translate-loader-url/angular-translate-loader-url.min.js',
      'bower_components/checklist-model/checklist-model.js',
      'bower_components/angular-timer/dist/angular-timer.min.js',
      'bower_components/angular-scroll/angular-scroll.js',
      'bower_components/checklist-model/checklist-model.js',
      'bower_components/videogular/videogular.min.js',
      'bower_components/videogular-controls/controls.min.js',
      'bower_components/videogular-buffering/buffering.min.js',
      'bower_components/videogular-overlay-play/overlay-play.min.js',
      'bower_components/videogular-poster/poster.min.js',
      'bower_components/angular-youtube-mb/dist/angular-youtube-embed.min.js',
      //app states provider
      'app/config/main.js',
      'app/states/states.js',
      'app/services/services.js',
      'app/storage/sessionStorage.js',
      'app/flash/main.js',
      'app/directives/directives.js',
      'app/directives/dirPagination.js',
      //utils
      'app/utils/main.js',
      'app/utils/multiselect.js',
      'app/utils/growl.js',
      'app/utils/webcam.js',
      'app/utils/angular-canvas-ext/exif.min.js',
      'app/utils/angular-canvas-ext/megapix-image.min.js',
      'app/utils/angular-canvas-ext/angular-canvas-ext.js',
      'app/utils/angular-slimscroll.js',
      //auth
      'app/auth/main.js',
      'app/auth/services/auth.js',
      'app/auth/services/guard.js',
      'app/auth/directives.js',
      'app/auth/states.js',
      'app/auth/controllers.js',
      //chat
      'app/chat/main.js',
      'app/chat/services.js',
      //home page
      'app/home/main.js',
      'app/home/states.js',
      'app/home/controllers.js',
      'app/home/services.js',
      //profile page
      'app/profile/main.js',
      'app/profile/states.js',
      'app/profile/controllers.js',
      'app/profile/services.js',
      'app/profile/directives.js',
      //account
      'app/account/main.js',
      'app/account/states.js',
      'app/account/controllers.js',
      'app/account/services.js',
      //activity
      'app/activity/main.js',
      'app/activity/states.js',
      'app/activity/controllers.js',
      'app/activity/services.js',
      'app/activity/directives.js',
      //magazine
      'app/magazine/main.js',
      'app/magazine/states.js',
      'app/magazine/controllers.js',
      'app/magazine/directives.js',
      'app/magazine/services.js',
      //search
      'app/search/main.js',
      'app/search/states.js',
      'app/search/controllers.js',
      'app/search/directives.js',
      //polls
      'app/poll/main.js',
      'app/poll/states.js',
      'app/poll/controllers.js',
      'app/poll/services.js',
      //app
      'app/app.js'
  ];
  public $depends = [];

  public static function getCssFiles() {
    $class = new AppAsset();
    return $class->css;
  }

  public static function getJsFiles() {
    $class = new AppAsset();
    return $class->js;
  }

}
