<?php

namespace frontend\controllers;

use \Yii;
use yii\web\Controller;
use common\models\Setting;
use common\helpers\Email;
use common\models\Invoice;
use common\models\User;
use common\models\Invite;
use common\models\InviteHistory;
use common\models\RecurringPaymentsProfile;

/**
 * Payment Controller
 *
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class PaymentController extends Controller {

  public $enableCsrfValidation = false;

  /**
   * Check token from paypal
   * Update is payment for invoice
   * Update expired primium for user
   * @param string $token Token from paypal
   */
  public function actionReturn() {
    if (isset($_GET['token'])) {
      $token = trim($_GET['token']);
      $invoice = Invoice::findOne(['token' => $token, 'isPayment' => false]);
      if ($invoice) {
        $error = '';

        $paypal = Yii::$app->Paypal;
        $setting = Setting::get('paypal');
        if ($setting) {
          $paypal->apiLive = $setting['live'] ? true : false;
          $paypal->apiUsername = $setting['username'];
          $paypal->apiPassword = $setting['password'];
          $paypal->apiSignature = $setting['signature'];
        }
        $paypal->SetPaypalUrl();
        $paypal->isRecurring = $invoice->isRecurring;
        $result = $paypal->GetExpressCheckoutDetails($token);

        $result['ORDER'] = [
          'TOTAL' => $invoice->total,
          'DESC' => $invoice->packageName,
          'CURRENCY' => $invoice->currency,
          'FREQUENCY' => $invoice->packageMonth
        ];
        //Detect errors
        if (!$paypal->isCallSucceeded($result)) {
          if ($paypal->apiLive == true) {
            //Live mode basic error message
            //$error = 'ERROR_PAYMENT';
            $error = $result['L_LONGMESSAGE0'];
          } else {
            //Sandbox output the actual error message to dive in.
            $error = $result['L_LONGMESSAGE0'];
          }
        } else {
          if ($invoice->isRecurring) {
            $paymentResult = $paypal->CreateRecurringPaymentsProfile($result);
          } else {
            $paymentResult = $paypal->DoExpressCheckoutPayment($result);
          }
          //Detect errors
          if (!$paypal->isCallSucceeded($paymentResult)) {
            if ($paypal->apiLive == true) {
              //Live mode basic error message
              $error = 'ERROR_PAYMENT';
            } else {
              //Sandbox output the actual error message to dive in.
              $error = $paymentResult['L_LONGMESSAGE0'];
            }
          } else {
            //payment was completed successfully
            //update payment status
            $invoice->isPayment = true;
            $invoice->params = $paymentResult;
            if ($invoice->save()) {
              /**
               * recurring payment paypal
               * create recurring paypal profile on paypal
               * create recurring paypal profile on server
               */
              if ($invoice->isRecurring) {
                $recurring = new RecurringPaymentsProfile();
                $recurring->attributes = $invoice->attributes;
                $recurring->profileId = $paymentResult['PROFILEID'];
                $recurring->profileStatus = 'Active'; //$paymentResult['PROFILESTATUS']
                $recurring->params = $paymentResult;
                $recurring->save();
              }
              //default invite info
              $invite = '';
              //update user
              $user = User::findIdentity($invoice->createdBy);
              if ($user) {
                //update for invite user
                if ($invoice->inviteCode) {
                  //get history
                  $history = InviteHistory::findOne(['inviteCode' => $invoice->inviteCode]);
                  if (!$history) {
                    $history = InviteHistory::findOne(['inviteCodeWithoutMemberId' => $invoice->inviteCode]);
                  }
                  if ($history) {
                    //update Number of premium upgrades invited of invite member.
                    $history->premiums+=1;
                    $history->save();
                    
                    //update total invite for user
                    $ownerInvite = User::findOne($history->owner);
                    if ($ownerInvite) {
                      $ownerInvite->premiums+=1;
                      $ownerInvite->save();
                    }
                  }
                }
                if ($invoice->inviteId) {
                  //check invite
                  $invite = Invite::findOne($invoice->inviteId);
                  if ($invite && $user->inviteCode != $invite->code . $user->userId) {
                    //add invite code for this user
                    $userInvite = new InviteHistory();
                    $userInvite->inviteId = $invite->_id;
                    $userInvite->inviteCode = $invite->code . $user->userId;
                    $userInvite->owner = $user->_id;
                    if ($userInvite->save()) {
                      $user->inviteCode = $userInvite->inviteCode;
                      $user->premiums = '';
                    }
                  } else {
                    $invite = '';
                  }
                }

                //update premium for user payment
                if ($user->level < User::LEVEL_PREMIUM) {
                  $user->level = User::LEVEL_PREMIUM;
                }
                if (is_object($user->expiredPremium) && $user->expiredPremium instanceof \MongoDate &&
                  $user->expiredPremium->sec > time()) {
                  $user->expiredPremium = new \MongoDate(strtotime('+' . $invoice->packageMonth . ' months', $user->expiredPremium->sec));
                } else {
                  $user->expiredPremium = new \MongoDate(strtotime('+' . $invoice->packageMonth . ' months'));
                }
                if ($user->save()) {
                  Email::sendConfirmUpgrade($user, $invoice, $invite);
                  Yii::$app->session->setFlash('success', 'SUCCESS_PAYMENT');
                } else {
                  $errors = $user->getErrors();
                  foreach ($errors as $err) {
                    foreach ($err as $er) {
                      $error.=$er . '<br/>';
                    }
                  }
                }
              } else {
                $error = 'ERROR_PAYMENT_NONE_USER';
              }
            } else {
              $error = 'ERROR_PAYMENT_NONE_INVOICE';
            }
          }
        }
        if ($error) {

          Yii::$app->session->setFlash('error', $error);
        }
      }
    }
    $this->redirect(['#/purchase']);
  }

  public function actionIpn() {
    Email::send('Ipn paypal', json_encode($_POST), 'tranthanhhoai1990@gmail.com');
    if (isset($_POST['txn_type']) && $_POST['txn_type'] &&
      isset($_POST['recurring_payment_id']) && $_POST['recurring_payment_id']) {
      $recurring = RecurringPaymentsProfile::findOne([
          'profileId' => $_POST['recurring_payment_id']
      ]);
      if ($recurring) {
        if ($_POST['txn_type'] == 'recurring_payment') {
          if (isset($_POST['payment_status']) && $_POST['payment_status'] == 'Completed') {
            $invoice = new Invoice();
            $invoice->attributes = $recurring->attributes;
            $invoice->isRecurring = true;
            $invoice->isPayment = true;
            $invoice->total = $recurring->amount;
            $invoice->params = $_POST;
            if (!$invoice->save()) {
              Email::send('Ipn paypal', json_encode($invoice->getErrors()), 'tranthanhhoai1990@gmail.com');
            }
            //update user
            $user = User::findIdentity($invoice->createdBy);
            if ($user) {
              //update premium for user payment
              if ($user->level < User::LEVEL_PREMIUM) {
                $user->level = User::LEVEL_PREMIUM;
              }
              if (is_object($user->expiredPremium) && $user->expiredPremium instanceof \MongoDate &&
                $user->expiredPremium->sec > time()) {
                $user->expiredPremium = new \MongoDate(strtotime('+' . $invoice->packageMonth . ' months', $user->expiredPremium->sec));
              } else {
                $user->expiredPremium = new \MongoDate(strtotime('+' . $invoice->packageMonth . ' months'));
              }
              $user->save();
            }
          }
        } else if (isset($_POST['profile_status'])) {
          $recurring->profileStatus = $_POST['profile_status'];
          $recurring->save();
        }
      }
    }
    exit();
  }

  public function actionCancel() {
    if (isset($_GET['token'])) {
      $token = trim($_GET['token']);
      $invoice = Invoice::findOne(['token' => $token, 'isPayment' => false]);
      if ($invoice) {
        $invoice->delete();
      }
    }
    $this->redirect(['#/purchase']);
  }

}
