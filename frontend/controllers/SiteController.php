<?php

namespace frontend\controllers;

use \Yii;
use yii\web\Controller;
use common\models\Country;

/**
 * Site Controller
 *
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class SiteController extends Controller {

  /**
   * config page
   * set session country
   * set session language
   * set session currency
   */
  public function init() {
    $domain = explode('.', $_SERVER['HTTP_HOST']);
    $subdomain = '';
    if (count($domain) > 2) {
      if ($domain[0] !== 'www') {
        $subdomain = $domain[0];
      } else if(count($domain)===4){
        $subdomain = $domain[1];
      }
    }
    /**
     * check session country config setting for domain
     * config language, currency, height, distance for country
     */
      if ($subdomain) {
        $country = Country::findOne(['countryCode' => $subdomain]);
        if ($country) {
          Yii::$app->session['country'] = $country->countryCode;
          Yii::$app->session['language'] = $country->languageCode;
          Yii::$app->session['currency'] = $country->currency;
          Yii::$app->session['height'] = $country->height;
          Yii::$app->session['distance'] = $country->distance;
        } else {
          echo '<h3>Subdomain does not support!</h3>';
          exit;
        }
      } else {
        //set default us country
        $country = Country::findOne(['countryCode' => 'us']);
        if ($country) {
          Yii::$app->session['country'] = $country->countryCode;
          Yii::$app->session['language'] = $country->languageCode;
          Yii::$app->session['currency'] = $country->currency;
          Yii::$app->session['height'] = $country->height;
          Yii::$app->session['distance'] = $country->distance;
        }
      }
    parent::init();
  }

  /**
   * Load page
   * @return type
   */
  public function actionIndex() {
    return $this->render('index');
  }

  public function actionError() {
    return $this->renderPartial('error');
  }

  public function actionSupport() {
    $page = \common\models\Page::getPageByType('support', Yii::$app->session['language']);
    return $this->renderPartial('support', compact('page'));
  }

}
