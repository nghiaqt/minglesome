<?php

namespace frontend\controllers;

//load helper
use \Yii;
use yii\web\Controller;
//load model
use common\models\Invite;
use common\helpers\Email;

/**
 * affiliate controller
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class AffiliateController extends Controller {

  /**
   * set invite data
   * @param type $id
   */
  public function actionIndex() {
    if (Yii::$app->user->isGuest) {
      Yii::$app->session['affiliate'] = true;
    } else {
      $user = Yii::$app->user->identity;
      $user->isAffiliate = true;
      $user->save();
    }
    $this->redirect(['site/index']);
  }

}
