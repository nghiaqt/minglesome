(function(){
  'use strict';

  angular.module('app.chat.services', []);

  angular.module('app.chat', [
    'app.chat.services'
  ]);
})();
