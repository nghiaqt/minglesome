(function() {
  var propertyServices = angular.module('app.chat.services', []);
  propertyServices.factory('chatService', [
    'Restangular',
    function(Restangular) {
      var _rest = Restangular.all('chat');
      return {
        /**
         * get chat conversation
         * @param {type} message
         * @returns {@exp;@exp;_rest@pro;get@call;@call;then|@exp;_rest@pro;get@call;@call;then}
         */
        getMessage: function(id, params) {
          return _rest.one(id).one('message').get(params).then(function(res) {
            return res;
          });
        },
        /**
         * Get conversation data for private chat
         * @param {array} recipients
         * @returns {@exp;_rest@pro;customPOST@call;@call;then|@exp;@exp;_rest@pro;customPOST@call;@call;then}
         */
        private: function(recipients) {
          return _rest.customPOST(recipients, 'private').then(function(res) {
            return res;
          });
        },
        /**
         * Get conversations by date
         * @param {type} date
         * @returns {@exp;_rest@pro;get@call;@call;then|@exp;@exp;_rest@pro;get@call;@call;then}
         */
        conversations: function(params) {
          return _rest.get('activity', params).then(function(res) {
            return res;
          });
        },
        getConversation: function(id) {
          return _rest.one(id).one('get').get().then(function(res) {
            return res;
          });
        },
        /**
         * delete picture
         * @param {type} picture
         * @returns {boolean} True if success, errors if not save
         */
        del: function(conversation) {
          return _rest.customDELETE(conversation.conversationId).then(function(res) {
            return res;
          });
        },
        read: function(id) {
          return _rest.one(id).one('read').get().then(function(res) {
            return res;
          });
        },
        contact: function(recipients) {
          return _rest.customPOST(recipients, 'contact').then(function(res) {
            return res;
          });
        },
        totalMessageToday: function() {
          return _rest.get('total-message-today').then(function(res) {
            return res;
          });
        }
      };
    }
  ]);
})();