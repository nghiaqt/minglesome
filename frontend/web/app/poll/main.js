(function() {
  'use strict';

  angular.module('app.poll.states', ['app.states']);
  angular.module('app.poll.controllers', []);
  angular.module('app.poll.services', []);

  angular.module('app.poll', [
    'app.poll.states',
    'app.poll.controllers',
    'app.poll.services'
  ]);
})();
