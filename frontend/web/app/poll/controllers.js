(function() {
  'use strict';
  var controllers = angular.module('app.poll.controllers', []);
  controllers.controller('PollIndexCtrl', [
    '$scope', 'poll', 'pollService', function($scope, poll, pollService) {

      $scope.poll = poll;
      $scope.select = $scope.poll.userAnswer;

      //select answer
      $scope.answerPoll = function(answer) {
        if (answer != $scope.poll.userAnswer) {
          pollService.answer($scope.poll._id, answer);
          if ($scope.poll.userAnswer.length > 0) {
            angular.forEach($scope.poll.answers, function(value) {
              if (value.answer == answer) {
                value.total += 1;
              }
              if (value.answer == $scope.poll.userAnswer) {
                value.total -= 1;
              }
            });
          } else {
            $scope.poll.totalAnswer += 1;
            angular.forEach($scope.poll.answers, function(value) {
              if (answer == value.answer) {
                value.total += 1;
              }
            });
          }
          $scope.poll.userAnswer = answer;
        }
      };

    }]);
})();