(function() {
  'use strict';
  angular.module('app.poll.states').config([
    'appStatesProvider',
    function(appStatesProvider) {
      appStatesProvider.push([
        {
          name: 'poll',
          url: '/poll/:id',
          templateUrl: '/app/poll/views/index.html',
          controller: 'PollIndexCtrl',
          resolve: {
            poll: ['pollService', '$stateParams', function(pollService, $stateParams) {
                return pollService.view($stateParams.id).then(function(res){
                  return res;
                });
              }]
          }
        }
      ]);
    }
  ]);
})();