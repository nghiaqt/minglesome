(function() {
  var propertyService = angular.module('app.poll.services', []);
  propertyService.factory('pollService', [
    'Restangular', function(Restangular) {
      var _rest = Restangular.all('polls');
      return {
        get: function() {
          return _rest.get('');
        },
        view: function(id) {
          return _rest.get(id);
        },
        answer: function(id, answer) {
          return _rest.one(id).customPOST({answer:answer});
        }
      };
    }
  ]);
})();