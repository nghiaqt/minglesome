angular.module('app.search.directives')
  .directive('plActionSearch', [
    'Restangular', '$rootScope', 'profileService',
    function (Restangular, $rootScope, profileService) {
      'use strict';
      return {
        restrict: 'AE',
        transclude: true,
        scope: {},
        link: function (scope, element, attrs) {
          if ($rootScope.$state.current.name === 'search.index') {
            if ($rootScope.$stateParams.type === 'lounge') {
              scope.page = 'lounge';
            } else {
              scope.page = 'users';
            }
          } else {
            scope.page = 'dates';
          }

          scope.dropdown = [];
          //get neccessary data for searching like gender, status married, orientation
          profileService.getDropdownData().then(function (res) {
            if (res) {
              scope.dropdown = res;
            }
          });
          if (!$rootScope.dataFilter) {
            //get data last searching
            Restangular.one('last-searching').get().then(function (res) {
              scope.dataFilter = $rootScope.dataFilter = res;
              $rootScope.$emit('dataFilter-' + scope.page);
              if (scope.dataFilter.country) {
                profileService.getStates({
                  countryCode: scope.dataFilter.country
                }).then(function (res) {
                  scope.dropdown.state = res.data;
                });
                if (scope.dataFilter.state) {
                  profileService.getCities({
                    countryCode: scope.dataFilter.country,
                    stateId: scope.dataFilter.state
                  }).then(function (res) {
                    scope.dropdown.city = res.data;
                  });
                }
              }
            });
          } else {
            scope.dataFilter = $rootScope.dataFilter;
          }

        },
        controller: [
          '$scope', '$growl', '$document', '$timeout',
          function ($scope, $growl, $document, $timeout) {
            //set view mode
            $scope.viewMode = 'small';
            //config slimscroll
            $scope.slimScrollResize = angular.copy($rootScope.slimScrollActionResize);
            $scope.slimScrollOptions = {
              disableFadeOut: true
            };
            $scope.focusFilter = function (input) {
              if ($rootScope.detectedBrowser.platform.mobile) {
                $scope.slimScrollOptions.scrollTo = input;
              }
            };
            //check search page (search, dates, lounge)
            var unbindStateChange = $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
              if (toState.name === 'search.index') {
                if (toParams.type === 'lounge') {
                  $scope.page = 'lounge';
                } else {
                  $scope.page = 'users';
                }
              } else {
                $scope.page = 'dates';
              }
              if (toParams.mode && toParams.mode === 'large') {
                $scope.viewMode = 'large';
              } else {
                $scope.viewMode = 'small';
              }
            });

            //get amount record
            var unbindAmountRecord = $rootScope.$on('amountRecord', function (event, mass) {
              $scope.amountRecord = mass;
            });

            //destroy $on
            $scope.$on('$destroy', function () {
              unbindStateChange();
              unbindAmountRecord();
            });

            //load state when select country
            $scope.loadState = function () {
              $scope.dataFilter.state = '';
              profileService.getStates({
                countryCode: $scope.dataFilter.country
              }).then(function (res) {
                $scope.dropdown.state = res.data;
                $scope.dropdown.city = [];
                $scope.dataFilter.city = '';
              });
            };
            //load city when select state
            $scope.loadCity = function () {
              $scope.dataFilter.city = '';
              if ($scope.dataFilter.state) {
                profileService.getCities({
                  countryCode: $scope.dataFilter.country,
                  stateId: $scope.dataFilter.state
                }).then(function (res) {
                  $scope.dropdown.city = res.data;
                });
              }
            };

            //set default data
            $scope.error = {
              ageFrom: '',
              ageTo: '',
              heightFrom: '',
              heightTo: ''
            };

            //set distance for search
            $scope.setDistance = function (distance) {
              $scope.dataFilter.distance = distance;
            };

            //set mode for sorting like: alphabet, last logged
            $scope.setSortMode = function (sortMode) {
              $scope.dataFilter.sortMode = sortMode;
            };

            //get/validate data for searching
            $scope.seach = function () {
              var errorMessage = '<ul>';
              var error = false;

              //validations
              if ($scope.dataFilter.ageFrom && isNaN($scope.dataFilter.ageFrom)) {
                errorMessage = errorMessage + '<li><span translate="AGE_FROM"></span>' + '<span translate="MUST_BE_NUMBER"></span></li>';
                error = true;
              }
              if ($scope.dataFilter.ageTo && isNaN($scope.dataFilter.ageTo)) {
                errorMessage = errorMessage + '<li><span translate="AGE_TO"></span>' + '<span translate="MUST_BE_NUMBER"></span></li>';
                error = true;
              }
              if ($scope.dataFilter.heightFrom && isNaN($scope.dataFilter.heightFrom)) {
                errorMessage = errorMessage + '<li><span translate="HEIGHT_FROM"></span>' + '<span translate="MUST_BE_NUMBER"></span></li>';
                error = true;
              }
              if ($scope.dataFilter.heightTo && isNaN($scope.dataFilter.heightTo)) {
                errorMessage = errorMessage + '<li><span translate="HEIGHT_TO"></span>' + '<span translate="MUST_BE_NUMBER"></span></li>';
                error = true;
              }
              errorMessage = errorMessage + '</ul>';

              //if there's no error
              if (error === false) {
                $scope.isOpen = false;
                $rootScope.dataFilter = angular.copy($scope.dataFilter);
                Restangular.one('last-searching').customPOST($rootScope.dataFilter, 'create');
                $rootScope.$broadcast('dataFilter-' + $scope.page, 'search');
              } else {
                $growl.box('', errorMessage, {
                  class: 'danger'
                }).open();
              }
            };
          }],
        templateUrl: '/app/search/views/searchbox.html'
      };
    }])
  .directive('plDates', [
    'Restangular', '$rootScope',
    function (Restangular, $rootScope) {
      'use strict';
      return {
        restrict: 'AE',
        transclude: true,
        scope: {
          day: '=day',
          month: '=month',
          year: '=year'
        },
        templateUrl: '/app/search/views/datesbox.html',
        controller: ['$scope', '$growl', '$document', function ($scope, $growl, $document) {

            $scope.premium = window.ROLE.premium;
            $scope.pageSize = 10;
            $scope.currentPage = 1;
            $scope.isMorePage = false;
            $scope.userList = [];

            //get data for searching
            $scope.loadData = function () {
              var dataFilter = angular.copy($rootScope.dataFilter);
              dataFilter.dayOfMonth = $scope.day;
              dataFilter.month = $scope.month;
              dataFilter.year = $scope.year;
              dataFilter.pageSize = $scope.pageSize;
              dataFilter.currentPage = $scope.currentPage;

              Restangular.one('user').customPOST(dataFilter, 'search').then(function (res) {
                angular.forEach(res.data, function (value) {
                  $scope.userList.push(value);
                });
                $scope.isMorePage = res.isMorePage;
              });
            };

            //search action submit data
            var unbindFilter = $rootScope.$on('dataFilter-dates', function (event, mass) {
              $scope.currentPage = 1;
              $scope.isMorePage = false;
              $scope.userList = [];
              $scope.loadData();
            });
            //destroy $on
            $scope.$on('$destroy', function () {
              unbindFilter();
            });

            //default load data
            if ($rootScope.dataFilter) {
              $scope.loadData();
            }

            $scope.nextPage = function () {
              $scope.currentPage += 1;
              $scope.loadData();
            };
          }]
      };
    }]);
