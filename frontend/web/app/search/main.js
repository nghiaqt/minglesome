(function(){
  'use strict';

  angular.module('app.search.states', ['app.states']);
  angular.module('app.search.controllers', []);
	angular.module('app.search.directives', []);
	
  angular.module('app.search', [
    'app.search.states',
    'app.search.controllers',
		'app.search.directives'
  ]);
})();
