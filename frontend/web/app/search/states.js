(function() {
  'use strict';
  angular.module('app.search.states').config([
    'appStatesProvider',
    function(appStatesProvider) {
      appStatesProvider.push([
        {
          name: 'search',
          templateUrl: '/app/search/views/search.html',
          controller: function() {
          },
          resolve: {
            //get polls & banners
            polls: ['pollService', function(pollService) {
                return pollService.get().then(function(res) {
                  return res;
                });
              }]
          }
        },
        {
          name: 'search.index',
          url: '/search?type&page&mode',
          templateUrl: function($stateParams) {
            if ($stateParams.type == 'lounge') {
              return '/app/search/views/lounge.html';
            } else {
              return '/app/search/views/index.html';
            }
          },
          controllerProvider: function($stateParams) {
            if ($stateParams.type == 'lounge') {
              return 'SearchLoungeCtrl';
            } else {
              return 'SearchIndexCtrl';
            }
          }
        },
        {
          name: 'search.dates',
          url: '/search/dates',
          templateUrl: '/app/search/views/dates.html',
          controller: 'SearchDatesCtrl'
        },
        {
          name: 'search.lounge',
          url: '/search/lounge?mode',
          templateUrl: '/app/search/views/lounge.html',
          controller: 'SearchLoungeCtrl'
        }
      ]);
    }
  ]);
})();