(function () {
  /**
   * search controller
   * @type @exp;angular@call;module
   */
  'use strict';
  var controllers = angular.module('app.search.controllers', []);

  //seach user
  controllers.controller('SearchIndexCtrl', [
    '$scope', '$rootScope', 'Restangular', '$timeout', 'polls', 'pollService', 'apBrowserHelper',
    function ($scope, $rootScope, Restangular, $timeout, polls, pollService, apBrowserHelper) {

      $scope.premium = window.ROLE.premium;
      //use browser helper of angular canvas detect mobile
      $scope.maxSize = apBrowserHelper.platform.mobile ? 5 : 12;
      $scope.pageSize = 18;//number of user on each page
      $scope.totalSize = 0;
      $rootScope.pageSearch = $scope.currentPage = $rootScope.$stateParams.page || 1;// current page

      $scope.userList = [];
      $scope.viewMode = $rootScope.$stateParams.mode || 'small';

      //change view mode in search action (small/lagre)
      var unbindViewMode = $rootScope.$on('viewMode', function (event, mass) {
        $scope.viewMode = mass;
      });

      //get data for searching
      var getData = function () {
        var dataFilter = angular.copy($rootScope.dataFilter);
        dataFilter.pageSize = $scope.pageSize;
        dataFilter.currentPage = $scope.currentPage;
        Restangular.one('user').customPOST(dataFilter, 'search').then(function (res) {
          $scope.userList = res.data;
          $scope.totalSize = res.total;
          $rootScope.$emit('amountRecord', $scope.userList.length);
          if (res.total == 0) {
            if (dataFilter.country && dataFilter.city && dataFilter.distance == 0) {
              if (window.CONFIG.distance == 'miles') {
                $scope.notify = 'SEARCH_NOTIFY_MILIES';
              } else {
                $scope.notify = 'SEARCH_NOTIFY_KM';
              }
            }
          } else {
            $scope.notify = '';
          }
        });
      };

      //search action submit data
      var unbindFilter = $rootScope.$on('dataFilter-users', function (event, mass) {
        if ($rootScope.$state.current.name == 'search.index') {
          if (mass == 'search' && $scope.currentPage > 1) {
            mass = '';
            $rootScope.$state.go($rootScope.$state.current, {type: 'user', page: 1});
          } else {
            getData();
          }
        }
      });

      //destroy $on
      $scope.$on('$destroy', function () {
        unbindFilter();
        unbindViewMode();
      });

      /**
       * poll/banner
       */
      $scope.poll = '';
      $scope.banner = '';
      var isPoll = false;
      var isBanner = false;
      if (polls.polls.length > 0 && polls.banners.length > 0) {
        isPoll = Math.random() < .5;
        isBanner = !isPoll;
      } else if (polls.polls.length > 0) {
        isPoll = true;
      } else if (polls.banners.length > 0) {
        isBanner = true;
      }
      if (isPoll) {
        $scope.pageSize -= 1;
        $scope.poll = polls.polls[Math.floor(Math.random() * polls.polls.length)];
        $scope.slimScrollOptions = {
          height: 189,
          disableFadeOut: true
        };

        //select answer
        $scope.answerPoll = function (answer) {
          pollService.answer($scope.poll._id, answer);
        };
      }
      if (isBanner) {
        $scope.pageSize -= 1;
        $scope.banner = polls.banners[Math.floor(Math.random() * polls.banners.length)];
      }

      //default load data
      if ($rootScope.dataFilter) {
        getData();
      }
    }]);
  /**
   * Date mode controller
   */
  controllers.controller('SearchDatesCtrl', [
    '$scope', '$rootScope', 'Restangular', '$timeout', 'polls', 'pollService',
    'apBrowserHelper','modalPrompt',
    function ($scope, $rootScope, Restangular, $timeout, polls, pollService, 
    apBrowserHelper, modalPrompt) {
      
      //open modal prompt info page
      modalPrompt.info('PROMPT_VIEW_DATES_EVENTS');
      
      //detroy angular watch, $on & socket listener...
      var off = [];
      //destroy $on & socket listener
      $scope.$on('$destroy', function () {
        off.forEach(function (unbind) {
          unbind();
        });
        off = [];
      });

      //set default data
      $scope.curTime = moment();
      $scope.amountDate = 2;

      /**
       * set amount dates by width of browser
       */
      var w = $rootScope.detectedBrowser;
      if (w.width > 990) {
        $scope.amountDate = 6;
      } else if (w.width > 490) {
        $scope.amountDate = 3;
      }
      //check resize browser
      off.push($rootScope.$watchCollection('detectedBrowser', function () {
        var size = 2;
        if (w.width > 990) {
          size = 6;
        } else if (w.width > 490) {
          size = 3;
        }
        if (size != $scope.amountDate) {
          $scope.amountDate = size;
          getDates();
        }
      }));

      /**
       * get data for searching
       * show notify when data is empty
       */
      var getData = function () {
        var curTime = angular.copy($scope.curTime);
        var dataFilter = angular.copy($rootScope.dataFilter);
        dataFilter.startDate = curTime.format('YYYY-M-D');
        dataFilter.endDate = curTime.add($scope.amountDate, 'd').format('YYYY-M-D');
        Restangular.one('user').customPOST(dataFilter, 'search').then(function (res) {
          if (res.data.length > 0) {
            $scope.notify = '';
          } else {
            if (dataFilter.country && dataFilter.city && dataFilter.distance == 0) {
              if (window.CONFIG.distance == 'miles') {
                $scope.notify = 'DATE_EVENT_NOTIFY_MILIES';
              } else {
                $scope.notify = 'DATE_EVENT_NOTIFY_KM';
              }
            }
          }
        });
      };

      //search action submit data
      off.push($rootScope.$on('dataFilter-dates', function (event, mass) {
        getData();
      }));

      /**
       * get dates
       */
      var getDates = function () {
        $scope.dates = [];
        for (var i = 0; i < $scope.amountDate; i++) {
          var cur = angular.copy($scope.curTime);
          $scope.dates.push(cur.add(i, 'd'));
        }
      }
      getDates();

      //previous dates
      $scope.previous = function () {
        if ($scope.curTime > moment()) {
          $scope.curTime.add(-$scope.amountDate, 'd');
          getDates();
        }
      };

      //next dates
      $scope.next = function () {
        $scope.curTime.add($scope.amountDate, 'd');
        getDates();
      };

    }]);
  /**
   * Lounge mode controller
   */
  controllers.controller('SearchLoungeCtrl', [
    '$scope', '$rootScope', 'Restangular', '$timeout', 'socket', 'polls', 
    'pollService', '$state', 'apBrowserHelper', '$stateParams', '$growl', 
    '$window', 'modalPrompt',
    function ($scope, $rootScope, Restangular, $timeout, socket, polls, 
    pollService, $state, apBrowserHelper, $stateParams, $growl,
    $window, modalPrompt) {

      //open modal prompt info page
      modalPrompt.info('PROMPT_VIEW_LOUNGE_PAGE');
      
      //detroy angular watch, $on & socket listener...
      var off = [];
      //destroy $on & socket listener
      $scope.$on('$destroy', function () {
        off.forEach(function (unbind) {
          unbind();
        });
        off = [];
      });

      $scope.premium = window.ROLE.premium;
      //use browser helper of angular canvas detect mobile
      $scope.maxSize = apBrowserHelper.platform.mobile ? 5 : 12;
      $rootScope.pageLounge = $scope.currentPage = $stateParams.page || 1;// current page
      $scope.pageSize = 18;//number of user on each page
      $scope.totalSize = 0;//total items
      $scope.userList = [];
      $scope.viewMode = $rootScope.$stateParams.mode || 'small';
      $scope.videoSize = {height: '166', width: '125'};
      $scope.videoLargeSize = {height: '511', width: '381'};

      var filterBox = angular.element('.search-filter');
      off.push($rootScope.$watchCollection('detectedBrowser', function () {
        if (filterBox.width() <= 490 && $scope.videoLargeSize.width != filterBox.width() - 10) {
          $scope.videoLargeSize.width = filterBox.width() - 10;
          $scope.videoLargeSize.height = ((filterBox.width() - 10) * 4) / 3;
        } else {
          $scope.videoLargeSize = {height: '511', width: '381'};
        }
      }));

      off.push($rootScope.$on('viewMode', function (event, mass) {
        $scope.viewMode = mass;
        if ($scope.viewMode === 'small') {
          $scope.videoSize = {height: '166', width: '125'};
        } else {
          $timeout(function () {
            if (filterBox.width() > 0 && filterBox.width() <= 490) {
              $scope.videoLargeSize.width = filterBox.width() - 10;
              $scope.videoLargeSize.height = ((filterBox.width() - 10) * 4) / 3;
            }
          }, 300);
          $scope.videoSize = $scope.videoLargeSize;
        }
      }));

      //get searching information
      var getData = function () {
        if ($rootScope.dataFilter) {
          var dataFilter = angular.copy($rootScope.dataFilter);
          dataFilter.isPublicStreaming = true;
          dataFilter.streamingUsers = angular.copy($rootScope.publicStreamingUsers);
          dataFilter.pageSize = $scope.pageSize;
          dataFilter.currentPage = $scope.currentPage;
          Restangular.one('user').customPOST(dataFilter, 'search').then(function (res) {
            $scope.userList = res.data || [];
            $scope.totalSize = res.total;
            $rootScope.$emit('amountRecord', $scope.userList.length);
          });
        } else {
          $timeout(function () {
            getData();
          }, 1000);
        }
      };
      //search action submit data
      off.push($rootScope.$on('dataFilter-lounge', function (event, mass) {
        getData();
      }));

      /** 
       * when user logout or turn off "public sharing streaming". 
       * remove video is now loading
       */
      off.push($rootScope.$on('videoEndCall', function (event, userId) {
        angular.forEach($scope.userList, function (user, index) {
          if (user._id == userId) {
            $scope.userList.splice(index, 1);
            $scope.$apply();
          }
        });
      }));

      /**
       * poll/banner
       */
      $scope.poll = '';
      $scope.banner = '';
      var isPoll = false;
      var isBanner = false;
      if (polls.polls.length > 0 && polls.banners.length > 0) {
        isPoll = Math.random() < .5;
        isBanner = !isPoll;
      } else if (polls.polls.length > 0) {
        isPoll = true;
      } else if (polls.banners.length > 0) {
        isBanner = true;
      }
      if (isPoll) {
        $scope.pageSize -= 1;
        $scope.poll = polls.polls[Math.floor(Math.random() * polls.polls.length)];
        $scope.slimScrollOptions = {
          height: 189,
          disableFadeOut: true
        };

        //select answer
        $scope.answerPoll = function (answer) {
          pollService.answer($scope.poll._id, answer);
        };
      }
      if (isBanner) {
        $scope.pageSize -= 1;
        $scope.banner = polls.banners[Math.floor(Math.random() * polls.banners.length)];
      }

      //default load data
      if ($rootScope.dataFilter) {
        getData();
      }
    }]);
})();