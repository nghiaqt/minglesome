(function() {
  'use strict';
  angular.module('app.activity.states').config([
    'appStatesProvider',
    function(appStatesProvider) {
      appStatesProvider.push([
        {
          name: 'activity',
          url:'/activity',
          templateUrl: '/app/activity/views/index.html',
          controller: 'ActivityIndexCtrl'
        }
      ]);
    }
  ]);
})();