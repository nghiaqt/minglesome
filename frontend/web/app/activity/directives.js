angular.module('app.activity.directives')
        .directive('plActivity', function() {
  return {
    restrict: 'AE',
    transclude: true,
    controller: 'ActivityCtrl',
    templateUrl: '/app/activity/views/index.html'
  };
}).directive('plCamtab', function() {
  return {
    restrict: 'AE',
    transclude: true,
    controller: 'CamtabCtrl',
    templateUrl: '/app/activity/views/camtab.html'
  };
});