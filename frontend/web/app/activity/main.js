(function(){
  'use strict';

  angular.module('app.activity.states', ['app.states']);
  angular.module('app.activity.controllers', []);
  angular.module('app.activity.services', []);
  angular.module('app.activity.directives', []);

  angular.module('app.activity', [
    'app.activity.states',
    'app.activity.controllers',
    'app.activity.services',
    'app.activity.directives'
  ]);
})();
