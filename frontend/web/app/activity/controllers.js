(function () {
  'use strict';
  var controllers = angular.module('app.activity.controllers', []);
  controllers.controller('ActivityCtrl', [
    '$scope', '$timeout', '$rootScope', 'socket', 'authService', 'chatService', '$growl',
    'dateService', 'visitorService', 'profileService', '$filter', '$compile',
    function ($scope, $timeout, $rootScope, socket, authService, chatService, $growl,
      dateService, visitorService, profileService, $filter, $compile) {

      var off = [];
      //destroy $on & socket listener
      $scope.$on('$destroy', function () {
        off.forEach(function (unbind) {
          unbind();
        });
        off = [];
        socket.removeListener('connectServer');
        socket.removeListener('reconnect');
        socket.removeListener('getPublicStreamingUsers');
        socket.removeListener('receiveMessage');
        socket.removeListener('updateReadedConversation');
        socket.removeListener('errorSendMessage');
        socket.removeListener('onReplyEvent');
        socket.removeListener('filterMutualInterest');
        socket.removeListener('onVisitor');
      });
      /** connect to server */
      socket.emit('connectServer', $rootScope.authService.user);
      /** reconnect to server */
      socket.on('reconnect', function (a, b) {
        //rejoin server
        socket.emit('connectServer', $rootScope.authService.user);
        //reload list user public camera
        socket.emit('getPublicStreamingUsers');
      });

      /**
       * Activity responsive
       */
      $scope.isShowActivity = false;
      var activityDefault = {
        fullWidth: 400,
        width: {
          lagre: 58,
          small: 44
        }
      };
      var activity = {
        width: 58, fullWidth: 400
      };
      $scope.activityStyle = {
        width: activity.fullWidth + 'px',
        left: activity.width - activity.fullWidth + 'px'
      };

      // resize activity tab
      var activityResize = function (browser) {
        if (browser.width < activityDefault.fullWidth && browser.width >= 320) {
          activity.width = activityDefault.width.small;
          activity.fullWidth = browser.width;
          activityStyle();
        } else if (browser.width < 490 && browser.width >= 320) {
          if (activity.width !== activityDefault.width.small || activity.fullWidth !== activityDefault.fullWidth) {
            activity.width = activityDefault.width.small;
            activity.fullWidth = activityDefault.fullWidth;
            activityStyle();
          }
        } else if (activity.width !== activityDefault.width.lagre) {
          activity.width = activityDefault.width.lagre;
          activity.fullWidth = activityDefault.fullWidth;
          activityStyle();
        }
      };

      // add style activity tabs
      var activityStyle = function () {
        if ($scope.isShowActivity) {
          $scope.activityStyle = {
            width: activity.fullWidth + 'px',
            left: '0px'
          };
        } else {
          $scope.activityStyle = {
            width: activity.fullWidth + 'px',
            left: activity.width - activity.fullWidth + 'px'
          };
        }
      };

      //action show/hide activity
      $scope.openActivity = function () {
        $scope.isShowActivity = !$scope.isShowActivity;
        activityStyle();
      };

      //on resize
      off.push($rootScope.$watchCollection('detectedBrowser', function (browser, oldBrowser) {
        if (browser.width !== oldBrowser.width) {
          activityResize(browser);
        }
      }));

      // load default activity size
      activityResize($rootScope.detectedBrowser);
      /**
       * Activity tabs
       */
      $scope.slimScrollOptions = {
        position: 'left',
        height: 120,
        disableFadeOut: true
      };
      $scope.slimScrollResize = {pc: 358, mobile: 289};

      $scope.activity = 'message';
      //show activity user selected
      $scope.showActivity = function (activityTab) {
        $scope.activity = activityTab;
      };
      /**
       * conversation tab
       * load more conversation by last updated (sec)
       * realtime add new conversation data
       */
      $scope.qtyConversationUnread = 0;
      $scope.conversations = [];
      $scope.conversationLastUpdatedAt = '';
      $scope.conversationReadMore = false;
      //load data
      $scope.loadConversation = function () {
        chatService.conversations({date: $scope.conversationLastUpdatedAt}).then(function (res) {
          angular.forEach(res.data, function (value) {
            $scope.conversations.push(value);
            if (value.unread) {
              $scope.qtyConversationUnread++;
            }
          });
          $scope.conversationLastUpdatedAt = res.lateUpdatedAt;
          $scope.conversationReadMore = res.morePre;
        });
      };
      $scope.loadConversation();
      /**
       * new message tab
       * load more conversation by last updated (sec)
       * realtime add new conversation data
       */
      $scope.qtyMessageUnread = 0;
      $scope.newConversations = [];
      $scope.newConversationLastUpdatedAt = '';
      $scope.newConversationReadMore = false;
      //load data
      $scope.loadNewConversation = function () {
        chatService.conversations({type: 'new', date: $scope.newConversationLastUpdatedAt}).then(function (res) {
          angular.forEach(res.data, function (value) {
            $scope.newConversations.push(value);
            if (value.unread) {
              $scope.qtyMessageUnread++;
            }
          });
          $scope.newConversationLastUpdatedAt = res.lateUpdatedAt;
          $scope.newConversationReadMore = res.morePre;
        });
      };
      $scope.loadNewConversation();
      /**
       * update data for conversation, new message, visitor
       * @param {type} message
       * @returns {undefined}
       */
      var updateConversation = function (message, isConversation) {
        if (message) {
          //update data conversation or new message
          var updated = false;
          if (isConversation) {
            angular.forEach($scope.conversations, function (value) {
              if (value._id == message.conversationId) {
                value.lastMessage = message.text;
                value.updatedAt = message.createdAt;
                value.updatedBy = message.createdBy;
                if (message.createdBy != authService.user._id && value.unread == false) {
                  value.unread = true;
                  $scope.qtyConversationUnread++;
                }
                updated = true;
              }
            });
          } else {
            angular.forEach($scope.newConversations, function (value) {
              if (value._id == message.conversationId) {
                value.lastMessage = message.text;
                value.updatedAt = message.createdAt;
                value.updatedBy = message.createdBy;
                if (message.createdBy != authService.user._id && value.unread == false) {
                  value.unread = true;
                  $scope.qtyMessageUnread++;
                }
                updated = true;
              }
            });
          }

          if (updated == false) {
            //get new conversation add to conversation or message & visitor
            chatService.getConversation(message.conversationId)
              .then(function (res) {
                if (isConversation) {
                  //update conversation
                  $scope.conversations.unshift(res);
                  //update total unread
                  if (res.unread) {
                    $scope.qtyConversationUnread++;
                  }

                  //remove conversation in new conversation
                  angular.forEach($scope.newConversations, function (value, index) {
                    if (value._id == res._id) {
                      $scope.newConversations.splice(index, 1);
                      //update total new conversation unread
                      if (value.unread) {
                        $scope.qtyMessageUnread--;
                      }
                    }
                  });
                } else {
                  //add new conversation
                  $scope.newConversations.unshift(res);
                  if (res.unread) {
                    $scope.qtyMessageUnread++;
                  }
                }
              });
          }
          $scope.$apply();
        }
      };
      /** receive new message */
      socket.on('receiveMessage', function (message, isConversation, sender) {
        if (message) {
          //notify if new message not from current profile view
//          if ($rootScope.$state.current.name != 'profile.view' || (sender && $rootScope.$state.params.id != sender._id)) {
//            if (sender) {
//              var textMessage = message.text.length > 50 ? $filter('limitTo')(message.text, 50) + '...' : message.text;
//              $growl.box('', '<a href="#/view-profile/' + sender._id + '">' + sender.username + ': ' + textMessage + '</a>', {
//                class: 'success',
//                timeout: 10000
//              }).open();
//            }
//          }
          //update message to new message/conversation
          updateConversation(message, isConversation);
        }
      });

      /** update readed conversation when user click view profile user */
      off.push($rootScope.$on('updateReadedConversation', function (event, conversationId) {
        var isConversation = false;
        angular.forEach($scope.conversations, function (value) {
          if (value._id == conversationId) {
            isConversation = true;
            if (value.unread == true) {
              value.unread = false;
              $scope.qtyConversationUnread--;
            }
          }
        });
        if (isConversation == false) {
          angular.forEach($scope.newConversations, function (value) {
            if (value._id == conversationId && value.unread == true) {
              value.unread = false;
              $scope.qtyMessageUnread--;
            }
          });
        }
        chatService.read(conversationId);
      }));
      /** get error*/
      socket.on('errorSendMessage', function (err) {
        //show error message
        $growl.box('', err, {
          class: 'danger'
        }).open();
      });
      /**
       * Date event
       */
      // $scope.qtyDateUnread = 0;
      // $scope.dateEvents = [];
      // $scope.dateEventLastUpdatedAt = '';
      // $scope.dateEventReadMore = false;
      // $scope.loadDateEvent = function () {
      //   dateService.dateEvents({date: $scope.dateEventLastUpdatedAt}).then(function (res) {
      //     if (res) {
      //       angular.forEach(res.data, function (value) {
      //         $scope.dateEvents.push(value);
      //         if (value.unread) {
      //           $scope.qtyDateUnread++;
      //         }
      //       });
      //       $scope.dateEventLastUpdatedAt = res.lateUpdatedAt;
      //       $scope.dateEventReadMore = res.morePre;
      //     }
      //   });
      // };
      // $scope.loadDateEvent();
      // /** receive new reply event */
      // socket.on('onReplyEvent', function (reply) {
      //   dateService.getReply(reply._id).then(function (res) {
      //     $scope.dateEvents.push(res);
      //     if (res.unread) {
      //       $scope.qtyDateUnread++;
      //     }
      //   });
      // });
      // /** update read reply event */
      // $scope.readEvent = function (item) {
      //   if (item.unread) {
      //     item.unread = false;
      //     $scope.qtyDateUnread--;
      //     dateService.readReply(item._id);
      //   }
      // };
      /**
       * mutual interest
       */
      $scope.interests = [];
      $scope.qtyInterestUnread = 0;
      profileService.getInterests(authService.user._id).then(function (res) {
        angular.forEach(res, function (value) {
          $scope.interests.push(value);
          if (value.unread) {
            $scope.qtyInterestUnread++;
          }
        });
      });
      $scope.readInterest = function (item) {
        if (item.unread) {
          item.unread = false;
          $scope.qtyInterestUnread--;
          profileService.readInterest(item._id);
        }
      };
      socket.on('filterMutualInterest', function (interesting, isInterest) {
        /** get mutual interest when some one interest the current user and current user interest, too*/
        if (isInterest) {
          if (interesting.unread) {
            $scope.qtyInterestUnread++;
          }
          $scope.interests.push(interesting);
        } else {
          //remove the user from mutual interest when this user unlike the current user
          var mutualInterest = $scope.interests;
          for (var i in mutualInterest) {
            if (mutualInterest[i]._id === interesting._id) {
              if (mutualInterest[i].unread) {
                $scope.qtyInterestUnread--;
              }
              $scope.interests.splice(i, 1);
              break;
            }
          }
        }
        $scope.$apply();
      });
      /** get mutual interest when the current usert interest someone and someone interest, too*/
      off.push($rootScope.$on('mutualInterest', function (error, mass) {
        if (mass.isMutualInterest) {
          $scope.interests.push(mass.interest);
          if (mass.interest.unread) {
            $scope.qtyInterestUnread++;
          }
        } else {
          //remove the user from mutual interest when the current user unlike another
          var mutualInterest = $scope.interests;
          for (var i in mutualInterest) {
            if (mutualInterest[i]._id === mass.interest._id) {
              if (mutualInterest[i].unread) {
                $scope.qtyInterestUnread--;
              }
              $scope.interests.splice(i, 1);
              break;
            }
          }
        }
      }));
      /**
       * audio tab
       */
      $scope.stopAudio = function (userId) {
        $rootScope.sharePrivateAudio(userId, false);
      };
      /**
       * visistor tab
       */
      $scope.qtyVisitorUnread = 0;
      $scope.visitor = [];
      $scope.visitorLastUpdatedAt = '';
      $scope.visitorReadMore = false;
      //load data
      $scope.loadVisitor = function () {
        visitorService.get({date: $scope.visitorLastUpdatedAt}).then(function (res) {
          if (res) {
            angular.forEach(res.data, function (value) {
              $scope.visitor.push(value);
              if (value.unread) {
                $scope.qtyVisitorUnread++;
              }
            });
            $scope.visitorLastUpdatedAt = res.lateUpdatedAt;
            $scope.visitorReadMore = res.morePre;
          }
        });
      };
      $scope.loadVisitor();
      /** receive new visitor & update read visitor */
      socket.on('onVisitor', function (visitor) {
        var updated = false;
        angular.forEach($scope.visitor, function (value) {
          if (value.createdBy == visitor.createdBy) {
            value.updatedAt = visitor.updatedAt;
            value.lastMessage = visitor.lastMessage;
            if (value.unread != visitor.unread) {
              if (visitor.unread) {
                $scope.qtyVisitorUnread++;
              } else {
                $scope.qtyVisitorUnread--;
              }
              value.unread = visitor.unread;
            }
            updated = true;
          }
        });
        if (updated == false) {
          visitorService.view(visitor._id).then(function (res) {
            $scope.visitor.unshift(res);
            if (res.unread) {
              $scope.qtyVisitorUnread++;
            }
          });
        }
        $scope.$apply();
      });
      /**
       * Activity footer actions
       * Delete, block account, report, end call
       */
      $scope.checkbox = {conversations: [], messages: [], dateEvents: [], mutualInterest: [], visitor: [], audio: []};
      /**
       * delete conversation & message
       * @param {type} conversation
       * @returns {undefined}
       */
      var deleteConversation = function (conversation) {
        chatService.del({conversationId: conversation._id}).then(function (res) {
          if (conversation.isConversation) {
            if (conversation.unread) {
              $scope.qtyConversationUnread--;
            }
            var index = $scope.conversations.indexOf(conversation);
            if (index != -1) {
              $scope.conversations.splice(index, 1);
            }
          } else {
            if (conversation.unread) {
              $scope.qtyMessageUnread--;
            }
            var index = $scope.newConversations.indexOf(conversation);
            if (index != -1) {
              $scope.newConversations.splice(index, 1);
            }
          }
        });
      };
      /**
       * delete event reply
       * @param {type} reply
       * @returns {undefined}
       */
      var deleteEventReply = function (reply) {
        dateService.delReply(reply._id).then(function (res) {
          var index = $scope.dateEvents.indexOf(reply);
          if (index != -1) {
            $scope.dateEvents.splice(index, 1);
          }
        });
      };

      off.push($rootScope.$on('onDeleteDateEvent', function (errors, date) {
        _.remove($scope.dateEvents, function (item) {
          return item.dateEventId === date._id;
        });
      }));
      /**
       * action delete
       */
      $scope.deleteAction = function () {
        if ($scope.activity == 'conversation') {
          //delete item
          angular.forEach($scope.checkbox.conversations, function (value) {
            deleteConversation(value);
          });
          //reset checkbox
          $scope.checkbox.conversations = [];
        } else if ($scope.activity == 'message') {
          //delete item
          angular.forEach($scope.checkbox.messages, function (value) {
            deleteConversation(value);
          });
          //reset checkbox
          $scope.checkbox.messages = [];
        } else if ($scope.activity == 'date') {
          angular.forEach($scope.checkbox.dateEvents, function (value) {
            deleteEventReply(value);
          });
          $scope.checkbox.dateEvents = [];
        }
      };
      /**
       * block an user
       */
      $scope.blockAction = function () {
        if (authService.user.level < window.ROLE.premium) {
          //show error message
          $growl.box('', 'ERROR_402', {
            class: 'danger'
          }).open();
          return false;
        }
        var success = false;
        if ($scope.activity == 'conversation' && $scope.checkbox.conversations.length > 0) {
          //delete item
          angular.forEach($scope.checkbox.conversations, function (value) {
            profileService.blockUser(authService.user._id, value.author._id);
          });
          //reset checkbox
          $scope.checkbox.conversations = [];
          success = true;
        } else if ($scope.activity == 'message' && $scope.checkbox.messages.length > 0) {
          //delete item
          angular.forEach($scope.checkbox.messages, function (value) {
            profileService.blockUser(authService.user._id, value.author._id);
          });
          //reset checkbox
          $scope.checkbox.messages = [];
          success = true;
        } else if ($scope.activity == 'date' && $scope.checkbox.dateEvents.length > 0) {
          angular.forEach($scope.checkbox.dateEvents, function (value) {
            profileService.blockUser(authService.user._id, value.author._id);
          });
          $scope.checkbox.dateEvents = [];
          success = true;
        } else if ($scope.activity == 'mutualinterest' && $scope.checkbox.mutualInterest.length > 0) {
          angular.forEach($scope.checkbox.mutualInterest, function (value) {
            profileService.blockUser(authService.user._id, value);
          });
          $scope.checkbox.mutualInterest = [];
          success = true;
        } else if ($scope.activity == 'visitor' && $scope.checkbox.visitor.length > 0) {
          angular.forEach($scope.checkbox.visitor, function (value) {
            profileService.blockUser(authService.user._id, value);
          });
          $scope.checkbox.visitor = [];
          success = true;
        } else if ($scope.activity == 'audio' && $scope.checkbox.audio.length > 0) {
          angular.forEach($scope.checkbox.audio, function (value) {
            profileService.blockUser(authService.user._id, value);
          });
          $scope.checkbox.audio = [];
          success = true;
        }
        if (success) {
//          $growl.box('BLOCK', 'BLOCK_SUCCESS', {
//            class: 'success'
//          }).open();
        }
      };
      /**
       * send report user
       */
      $scope.reportAction = function () {
        if (authService.user.level < window.ROLE.premium) {
          //show error message
          $growl.box('', 'ERROR_402', {
            class: 'danger'
          }).open();
          return false;
        }
        var success = false;
        if ($scope.activity == 'conversation' && $scope.checkbox.conversations.length > 0) {
          //delete item
          angular.forEach($scope.checkbox.conversations, function (value) {
            profileService.reportUser({
              reportedUser: value.author._id,
              message: 'conversation message: ' + value.lastMessage
            });
          });
          //reset checkbox
          $scope.checkbox.conversations = [];
          success = true;
        } else if ($scope.activity == 'message' && $scope.checkbox.messages.length > 0) {
          //delete item
          angular.forEach($scope.checkbox.messages, function (value) {
            profileService.reportUser({
              reportedUser: value.author._id,
              message: 'new message: ' + value.lastMessage
            });
          });
          //reset checkbox
          $scope.checkbox.messages = [];
          success = true;
        } else if ($scope.activity == 'date' && $scope.checkbox.dateEvents.length > 0) {
          angular.forEach($scope.checkbox.dateEvents, function (value) {
            profileService.reportUser({
              reportedUser: value.author._id,
              message: 'date event reply: ' + value.text
            });
          });
          $scope.checkbox.dateEvents = [];
          success = true;
        } else if ($scope.activity == 'mutualinterest' && $scope.checkbox.mutualInterest.length > 0) {
          angular.forEach($scope.checkbox.mutualInterest, function (value) {
            profileService.reportUser({
              reportedUser: value,
              message: 'Mutual Friend'
            });
          });
          $scope.checkbox.mutualInterest = [];
          success = true;
        } else if ($scope.activity == 'visitor' && $scope.checkbox.visitor.length > 0) {
          angular.forEach($scope.checkbox.visitor, function (value) {
            profileService.reportUser({
              reportedUser: value,
              message: 'Visitor'
            });
          });
          $scope.checkbox.visitor = [];
          success = true;
        } else if ($scope.activity == 'audio' && $scope.checkbox.audio.length > 0) {
          angular.forEach($scope.checkbox.audio, function (value) {
            profileService.reportUser({
              reportedUser: value,
              message: 'Audio call'
            });
          });
          $scope.checkbox.audio = [];
          success = true;
        }
        if (success) {
//          $growl.box('REPORT', 'REPORT_SUCCESS', {
//            class: 'success'
//          }).open();
        }
      };
      /**
       * end call audio checked
       */
      $scope.endCallAction = function () {
        if ($scope.checkbox.audio.length > 0) {
          angular.forEach($scope.checkbox.audio, function (value) {
            $rootScope.sharePrivateAudio(value, false);
          });
        }
      };
    }]);
  /**
   * camtab
   * manager video & audio call
   */
  controllers.controller('CamtabCtrl', [
    '$scope', '$rootScope', 'socket', '$growl', 'authService', 'profileService', '$modal',
    '$window', '$http', 'WebrtcSocket', 'peerConnection',
    function ($scope, $rootScope, socket, $growl, authService, profileService, $modal,
      $window, $http, WebrtcSocket, peerConnection) {
      //detroy angular watch, $on,...
      var off = [];
      //destroy $on & socket listener
      $scope.$on('$destroy', function () {
        off.forEach(function (unbind) {
          unbind();
        });
        off = [];
        socket.removeListener('publicStreamingUser');
        socket.removeListener('receivePublicStreamingUsers');
        socket.removeListener('videoConnections');
        socket.removeListener('videoEndCall');
        socket.removeListener('onRemoveReceiverStream');
        socket.removeListener('audioEndCall');
        socket.removeListener('receiveAudioCall');
        socket.removeListener('failAcceptAudio');
        if ($scope.sock) {
          $scope.sock.disconnect();
        }
      });
      /** 
       * isVideoControl (on/off camera)
       * isPublicVideo (on/off public camera)
       * videoStream (url streaming video of current user)
       * streamingUsers (list user video [userId=>videostream])
       * audioStream (url streaming audio of current user)
       * audioStreamingUsers (list user connect audio [userId=>audioStream])
       * publicStreamingUsers (list user public streaming)
       * callers (peerconnection of callers)
       * callees (peerconnection of callees)
       * audioCalls (peerconnection of audio call)
       */
      $rootScope.isVideoControl = false;
      $rootScope.isPublicVideo = false;
      $rootScope.videoStream = '';
      $rootScope.streamingUsers = [];
      $scope.audioStream = '';
      $rootScope.audioStreamingUsers = [];
      $rootScope.publicStreamingUsers = [];
      $scope.callers = [];
      $scope.callees = [];
      $scope.audioCalls = [];
      /** get public streaming users */
      socket.emit('getPublicStreamingUsers');
      /** when user turn on "public sharing streaming" */
      socket.on('publicStreamingUser', function (userId, isPublic) {
        if (isPublic) {
          $rootScope.publicStreamingUsers.push(userId);
          $rootScope.$emit('publicStreamingUser', userId);
          $rootScope.$apply();
        } else {
          var index = $rootScope.publicStreamingUsers.indexOf(userId);
          if (index != -1) {
            $rootScope.publicStreamingUsers.splice(index, 1);
          }
          //remove streaming
          $rootScope.$emit('videoEndCall', userId);
        }
      });
      /** receive list user id public streaming users */
      socket.on('receivePublicStreamingUsers', function (listUser) {
        if (listUser) {
          $rootScope.publicStreamingUsers = listUser;
          $rootScope.$apply();
        }
      });
      /** add user to list video connections */
      socket.on('videoConnections', function (userId) {
        if (!$rootScope.streamingUsers[userId]) {
          $rootScope.streamingUsers[userId] = true;
          $rootScope.$emit('videoStartCall', userId);
          $rootScope.$apply();
        }
      });
      /** 
       * a user disconnect. remove audio & video of user 
       */
      socket.on('videoEndCall', function (userId) {
        //remove streaming
        if ($rootScope.streamingUsers[userId]) {
          //remove stream & peer connection
          delete $rootScope.streamingUsers[userId];
          //send action delete connect
          $rootScope.$emit('videoEndCall', userId);
          $rootScope.$apply();
        }
      });
      /**
       * remove peerconnection of caller & callee when remove stream
       */
      off.push($rootScope.$on('removeReceiverStream', function (event, userId) {
        if ($scope.callees[userId]) {
          $scope.callees[userId].peer.close();
          delete $scope.callees[userId];
        }
        socket.emit('removeReceiverStream', userId);
      }));
      socket.on('onRemoveReceiverStream', function (userId) {
        if ($scope.callers[userId]) {
          $scope.callers[userId].peer.close();
          delete $scope.callers[userId];
        }
      });
      /** 
       * a user disconnect. remove audio & video of user 
       */
      socket.on('audioEndCall', function (userId) {
        angular.forEach($rootScope.audioStreamingUsers, function (audio, index) {
          if (audio._id == userId) {
            $rootScope.audioStreamingUsers.splice(index, 1);
            $rootScope.$apply();
          }
        });
        $rootScope.$emit('audioEndCall', userId);
        if ($scope.audioCalls[userId] && $scope.audioCalls[userId].peer) {
          $scope.audioCalls[userId].peer.close();
          delete $scope.audioCalls[userId];
        }
      });
      /**
       * resize streaming video
       * video (large/small)
       */
      $scope.isLargeVideo = false;
      $scope.videoSize = {width: 58, height: 77};
      var smallVideoSize = {width: 58, height: 77};
      $scope.largeVideo = function () {
        $scope.isLargeVideo = !$scope.isLargeVideo;
        if ($scope.isLargeVideo) {
          if (smallVideoSize.width < 58) {
            $scope.videoSize = {width: 84, height: 112};
          } else {
            $scope.videoSize = {width: 120, height: 160};
          }
        } else {
          $scope.videoSize = {width: smallVideoSize.width, height: smallVideoSize.height};
        }
      };
      $scope.resizeVideo = function () {
        var video = angular.element('.activity-tab');
        if (video.width() > 0) {
          $scope.videoSize.width = smallVideoSize.width = video.width();
          $scope.videoSize.height = smallVideoSize.height = (video.width() * 4) / 3;
        }
        off.push($rootScope.$watchCollection('detectedBrowser', function () {
          if (smallVideoSize.width != video.width()) {
            if ($scope.isLargeVideo) {
              smallVideoSize.width = video.width();
              smallVideoSize.height = (video.width() * 4) / 3;
              if (smallVideoSize.width < 52) {
                $scope.videoSize = {width: 84, height: 112};
              } else {
                $scope.videoSize = {width: 120, height: 160};
              }
            } else {
              $scope.videoSize.width = smallVideoSize.width = video.width();
              $scope.videoSize.height = smallVideoSize.height = (video.width() * 4) / 3;
            }
          }
        }));
      };
      /** share camera an user */
      var privateUser = null;
      var isShareVideo = false;
      var isPublicVideo = false;
      /** send to server set public video (true/false) */
      var setPublicVideo = function (isPublic) {
        $rootScope.$emit('videoPublicly', isPublic);
        //turn off camera
        if (!isPublic) {
          $rootScope.streamingUsers = [];
          angular.forEach($scope.callers, function (caller) {
            caller.peer.close();
            caller = null;
          });
          angular.forEach($scope.callees, function (callee) {
            callee.peer.close();
            callee = null;
          });
//		  $rootScope.audioStreamingUsers = [];
//		  angular.forEach($scope.audioCalls, function (call) {
//			call.peer.close();
//			call = null;
//		  });
        }
        //turn public camera
        isPublicVideo = $rootScope.isPublicVideo = isPublic;
        socket.emit('setPublicVideo', isPublic);
        //turn off public camera
        if (!isPublic) {
          if ($rootScope.$state.current.name == 'search.index' && $rootScope.$stateParams.type == 'lounge') {
            $rootScope.$state.go('profile.edit');
          }
        }
      };
      /** action set public video (true/false) */
      $scope.publicCamera = function () {
        if (authService.user.level < window.ROLE.premium) {
          //show error message
          $growl.box('', 'ERROR_402', {
            class: 'danger'
          }).open();
          return false;
        }

        isPublicVideo = !isPublicVideo;
        if (isPublicVideo) {
          if ($rootScope.isVideoControl) {
            setPublicVideo(true);
          } else {
            $rootScope.isVideoControl = true;
          }
        } else {
          setPublicVideo(false);
        }
      };
      /** video control (on/off video)*/
      $scope.setVideoControl = function () {
        if (authService.user.level < window.ROLE.premium) {
          //show error message
          $growl.box('', 'ERROR_402', {
            class: 'danger'
          }).open();
          return false;
        }

        if ($rootScope.isVideoControl) {
          $rootScope.isVideoControl = false;
          setPublicVideo(false);
          $rootScope.videoStream.stop();
          $rootScope.videoStream = null;
          socket.emit('endStreaming');
          $scope.isLargeVideo = false;
        } else {
          $rootScope.isVideoControl = true;
          //start streaming time
          socket.emit('startStreaming');
        }
      };
      /** share video to an user */
      var sharePrivateVideo = function (userId, isShare) {
        socket.emit('sharePrivateVideo', userId, isShare);
      };
      /** share video to an user */
      $rootScope.sharePrivateVideo = function (userId, shareControl) {
        if ($rootScope.isVideoControl) {
          /** camera has exist. send video*/
          sharePrivateVideo(userId, shareControl);
        } else {
          /** camera does not exist. get camera before send*/
          $rootScope.isVideoControl = true;
          privateUser = userId;
          isShareVideo = shareControl;
          //start streaming time
          socket.emit('startStreaming');
        }
      };
      /**
       * Share audio to user
       * @param {type} userId
       * @param {type} isShare
       */
      var sharePrivateAudio = function (userId, isShare, isAccept) {
        if (isAccept) {
          socket.emit('acceptAudio', userId);
        } else if (isShare) {
          socket.emit('audioCall', userId);
        } else {
          socket.emit('declineAudio', userId);
          angular.forEach($rootScope.audioStreamingUsers, function (audio, index) {
            if (audio._id == userId) {
              $rootScope.audioStreamingUsers.splice(index, 1);
            }
          });
          $rootScope.$emit('audioEndCall', userId, true);
        }
      };
      //action call audio
      $rootScope.sharePrivateAudio = function (userId, shareControl, isAccept) {
        if (shareControl) {
          if ($scope.audioStream) {
            sharePrivateAudio(userId, shareControl, isAccept);
          } else {
            var stream = new Stream();
            stream.init({mediaConstraints: {audio: true, video: false}});
            // called when camera stream is loaded
            stream.onSuccessGetUserMediaHandler = function (streamObject) {
              $scope.audioStream = streamObject.stream;
              sharePrivateAudio(userId, shareControl, isAccept);
            };
            // called when any error happens
            stream.onErrorGetuserMediaHandler = function (err) {
              $growl.box('Audio', err.msg, {
                class: 'error'
              }).open();
            };
            stream.getUserMedia();
          }
        } else {
          sharePrivateAudio(userId, shareControl);
        }
      };
      //action receive audio
      socket.on('receiveAudioCall', function (userId) {
        $scope.modalInstance = $modal.open({
          keyboard: false,
          size: 'sm',
          backdrop: 'static',
          templateUrl: '/app/activity/views/audio-modal.html',
          controller: [
            '$scope', '$modalInstance', 'profileService',
            function ($scope, $modalInstance, profileService) {
              profileService.get(userId).then(function (res) {
                $scope.user = res;
              });
              $scope.audioAccept = function () {
                $rootScope.sharePrivateAudio(userId, true, true);
                $modalInstance.dismiss('cancel');
              };
              $scope.audioDecline = function () {
                $rootScope.sharePrivateAudio(userId, false);
                $modalInstance.dismiss('cancel');
              };
            }
          ]
        });
      });
      // fail to accept audio
      socket.on('failAcceptAudio', function (userId) {
        $growl.box('Audio', 'ERROR_AUDIO_ACCEPT', {
          class: 'error'
        }).open();
      });
      /** 
       * load webrtc on success
       * set public video
       * share private
       */
      $scope.onSuccess = function (stream, videoElem) {
        if (!$rootScope.videoStream) {
          $rootScope.videoStream = stream;
        }
        if (isPublicVideo !== $rootScope.isPublicVideo) {
          setPublicVideo(isPublicVideo);
          $rootScope.$apply();
        }
        if (privateUser) {
          sharePrivateVideo(privateUser, isShareVideo);
          privateUser = null;
        }
      };
      /** load webrtc on error */
      $scope.onError = function (err) {
        $rootScope.isVideoControl = false;
        isPublicVideo = false;
        privateUser = null;
        $growl.box('CAMERA', 'CAMERA_ERROR', {
          class: 'danger'
        }).open();
      };
      if (webrtcDetectedBrowser !== null) {
        /**
         * @type typeWebrtc
         */
        var pcConfig = {"iceServers": [
            {url: 'stun:stun01.sipphone.com'},
            {url: 'stun:stun.ekiga.net'},
            {url: 'stun:stun.fwdnet.net'},
            {url: 'stun:stun.ideasip.com'},
            {url: 'stun:stun.iptel.org'},
            {url: 'stun:stun.rixtelecom.se'},
            {url: 'stun:stun.schlund.de'},
            {url: 'stun:stun.l.google.com:19302'},
            {url: 'stun:stun1.l.google.com:19302'},
            {url: 'stun:stun2.l.google.com:19302'},
            {url: 'stun:stun3.l.google.com:19302'},
            {url: 'stun:stun4.l.google.com:19302'},
            {url: 'stun:stunserver.org'},
            {url: 'stun:stun.softjoys.com'},
            {url: 'stun:stun.voiparound.com'},
            {url: 'stun:stun.voipbuster.com'},
            {url: 'stun:stun.voipstunt.com'},
            {url: 'stun:stun.voxgratia.org'},
            {url: 'stun:stun.xten.com'}
          ]};
        profileService.turn()
          .then(function (res) {
            // this callback will be called asynchronously
            // when the response is available
            var iceServers = createIceServers(res.uris, res.username, res.password);
            //{"url": "turn:" + res.username + "@54.68.172.99:3478", "credential": res.credential};
            if (iceServers !== null) {
              pcConfig.iceServers = pcConfig.iceServers.concat(iceServers);
            }
            $scope.init();
          });
      }
      $scope.init = function () {
        var newPeer = function (callId, mandatory, type) {
          var pc = peerConnection({
            servers: pcConfig,
            sdp_opt: {
              'mandatory': {
                'OfferToReceiveAudio': mandatory.audio,
                'OfferToReceiveVideo': mandatory.video
              }
            },
            offerCallback: function (description) {
              $scope.sock.sendSDP(callId, description, mandatory, type);
            },
            answerCallback: function (description) {
              //              $scope.callees[callId].finishSignaling();
              $scope.sock.sendSDP(callId, description, mandatory, type);
            },
            onicecandidate: function (e) {
              if (!e.candidate)
                return;
              var description = {
                type: 'candidate',
                label: e.candidate.sdpMLineIndex,
                id: e.candidate.sdpMid,
                candidate: e.candidate.candidate,
                callType: type === 'caller' ? 'callee' : 'caller'
              };
              $scope.sock.sendSDP(callId, description, mandatory, type);
            },
            onaddstream: function (e) {
              if (e.stream) {
                if (mandatory.video) {
                  $rootScope.$emit('addVideoStreaming', callId, e.stream);
                } else if (mandatory.audio) {
                  profileService.get(callId).then(function (call) {
                    call.stream = e.stream;
                    call.callDate = new Date();
                    $rootScope.audioStreamingUsers.push(call);
                    $rootScope.$emit('addAudioStreaming', callId);
                  });
                }
              }
            },
            onIceConnectionStateChanged: function (e) {
              trace('ICE connection state changed to: ', e.currentTarget.iceConnectionState);
            }
          });
          pc.create();
          /** add stream */
          if (mandatory.video && $rootScope.videoStream) {
            if (type === 'caller') {
              pc.addStream($rootScope.videoStream);
            }
          }
          if (mandatory.audio && $scope.audioStream) {
            pc.addStream($scope.audioStream);
          }
          return pc;
        };
        //socket send description connect peer to peer
        $scope.sock = WebrtcSocket({
          receiveCallCallback: function (calleeId, mandatory) {
            if (mandatory.video === true) {
              if ($scope.callers[calleeId] && $scope.callers[calleeId].peer) {
                $scope.callers[calleeId].peer.close();
                delete $scope.callers[calleeId];
              }
              $scope.callers[calleeId] = newPeer(calleeId, mandatory, 'caller');
              $scope.callers[calleeId].offer();
            } else if (mandatory.audio === true) {
              if ($scope.audioCalls[calleeId] && $scope.audioCalls[calleeId].peer) {
                $scope.audioCalls[calleeId].peer.close();
                delete $scope.audioCalls[calleeId];
              }
              $scope.audioCalls[calleeId] = newPeer(calleeId, mandatory, 'caller');
              $scope.audioCalls[calleeId].offer();
            }
          },
          receiveSDPCallback: function (callerId, description, mandatory) {
            if (mandatory.video === true) {
              if ($scope.callees[callerId] && $scope.callees[callerId].peer) {
                $scope.callees[callerId].peer.close();
                delete $scope.callees[callerId];
              }
              $scope.callees[callerId] = newPeer(callerId, mandatory, 'callee');
              $scope.callees[callerId].setRemoteDescription(description);
              $scope.callees[callerId].answer(description);
            } else if (mandatory.audio === true) {
              if ($scope.audioCalls[callerId] && $scope.audioCalls[callerId].peer) {
                $scope.audioCalls[callerId].peer.close();
                delete $scope.audioCalls[callerId];
              }
              $scope.audioCalls[callerId] = newPeer(callerId, mandatory, 'callee');
              $scope.audioCalls[callerId].setRemoteDescription(description);
              $scope.audioCalls[callerId].answer(description);
            }
          }, receiveReturnSDPCallback: function (calleeId, description, mandatory) {
            if (mandatory.video === true) {
              $scope.callers[calleeId].setRemoteDescription(description);
              $scope.callers[calleeId].finishSignaling();
            } else if (mandatory.audio === true) {
              $scope.audioCalls[calleeId].setRemoteDescription(description);
              $scope.audioCalls[calleeId].finishSignaling();
            }
          },
          receiveCandidateallback: function (id, candidate, type, mandatory) {
            if (mandatory.video === true) {
              if (type === 'callee') {
                $scope.callees[id].addIceCandidate(candidate);
              } else {
                $scope.callers[id].addIceCandidate(candidate);
              }
            } else if (mandatory.audio === true) {
              $scope.audioCalls[id].addIceCandidate(candidate);
            }
          }
        });
        $scope.sock.connect();
      };
    }]);
})();