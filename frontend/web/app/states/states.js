// TODO: FIXME: tests
angular.module('app.states', []).provider('appStates',
  /**
   * @name appStatesProvider
   * @description Adds states to the application, allows to redefine a state: states added later will redefine states
   * added earlier. This provider is based on the Angular ui-router states though, so you'll need to understand how ui-router works.
   */
  function(){
    'use strict';
    // storage for all states, states added later shadow states added earlier
    this.states = {};

    this.push = function(states) {
      if(!angular.isArray(states)) {
        states = [states];
      }
      angular.forEach(states, function(state){
        if(!state.name){
          throw new Error('state name is required');
        }
        this.states[state.name] = state;
      }, this);
    };

    this.state = function(name, state){
      state.name = name;
      this.states[name] = state;
    };

    this.remove = function(name){
      delete this.states[name];
    };

    this.$get = function(){
      return this;
    };
  }
);
