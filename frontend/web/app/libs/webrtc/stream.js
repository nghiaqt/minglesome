function Stream() {
    this.onSuccessGetUserMediaHandler = null;
    this.onErrorGetuserMediaHandler = null;

    this.config = {
        mediaConstraints: {audio: true, video: true}
    };
}

Stream.prototype.init = function(params) {
    $.extend(this.config, params);
}

Stream.prototype.getUserMedia = function() {
    getUserMedia(this.config.mediaConstraints, $.proxy(this, 'onSuccessGetUserMedia'), $.proxy(this, 'onErrorGetUserMedia'));
}

//Event handler
Stream.prototype.onSuccessGetUserMedia = function(stream) {
    this.stream = stream;

    if (typeof (this.onSuccessGetUserMediaHandler) == 'function') {
        this.onSuccessGetUserMediaHandler({stream: stream});
    }
}

Stream.prototype.onErrorGetUserMedia = function(error) {
    if (typeof (this.onErrorGetuserMediaHandler) == 'function') {
        this.onErrorGetuserMediaHandler(error);
    }
}

Stream.prototype.pause = function() {
    $.each(this.stream.getVideoTracks(), function(key, index) {
        index.enabled = false;
    })
}

Stream.prototype.resume = function() {
    $.each(this.stream.getVideoTracks(), function(key, index) {
        index.enabled = true;
    })
}

Stream.prototype.mute = function() {
    $.each(this.stream.getAudioTracks(), function(key, index) {
        index.enabled = false;
    });
}

Stream.prototype.unMute = function() {
    $.each(this.stream.getAudioTracks(), function(key, index) {
        index.enabled = true;
    })
}

//Get
Stream.prototype.getStream = function() {
    return this.stream;
}