function Caller(stream, localInfo, remoteInfo) {
    this.pc = null;
    
    this.configuration = null;
    this.mediaConstraints = {audio: false, video: true};
    
    this.localInfo = {'id': null, 'name': null};
    this.remoteInfo = {'id': null, 'name': null};
    
    this.stream = stream;
    this.localInfo = $.extend(this.localInfo, localInfo);
    this.remoteInfo = $.extend(this.remoteInfo, remoteInfo);
    
    
    try {
        this.pc = new RTCPeerConnection(null);
    
        this.pc.onicecandidate = $.proxy(this, 'onicecandidate');
        
        this.pc.onaddstream = $.proxy(this, 'onaddstream');
        
        this.pc.addStream(this.stream);
        
    } catch(e) {
        console.log('Failed to create PeerConnection, exception: ' + e.message);
    }
}

Caller.prototype.offer = function() {
    this.pc.createOffer($.proxy(this, 'createOfferSuccess'), $.proxy(this, 'createOfferError'));
}

Caller.prototype.createOfferSuccess = function(sessionDescription) {
    this.pc.setLocalDescription(new RTCSessionDescription(sessionDescription));
    //send info to server
    socket.emit('solve_offer', {session: sessionDescription, callee_id: this.remoteInfo.id});
}

Caller.prototype.createOfferError = function(error) {
    console.log(error);
}

Caller.prototype.getType = function() {
    return 'caller';
}

Caller.prototype.setCandidate = function(candidate) {
//    console.log(new RTCIceCandidate(candidate));
    this.pc.addIceCandidate(new RTCIceCandidate(candidate));
}

Caller.prototype.receiveAnswer = function(sessionDescription) {
    this.pc.setRemoteDescription(new RTCSessionDescription(sessionDescription));
}

//event
Caller.prototype.onicecandidate = function(event) {
    if (event.candidate) {console.log('candidate==>');
        console.log('send candidate to: ' + this.remoteInfo.id);
//        console.log(event.candidate);
        socket.emit('candidate', {candidate: {sdpMLineIndex: event.candidate.sdpMLineIndex, sdpMid: event.candidate.sdpMid, candidate: event.candidate.candidate}, client_id: this.remoteInfo.id, type: this.getType()});
    }
}

Caller.prototype.onaddstream = function(event) {
    console.log('==============on add stream: caller: ==================' + this.remoteInfo.id);
    attachMediaStreamById(this.remoteInfo.id, event.stream);
//    attachMediaStream($('#video-holder li[h-id='+this.remoteInfo.id+'] video')[0], event.stream);
}








function Callee(stream, localInfo, remoteInfo) {
    this.pc = null;
    
    this.configuration = null;
    this.mediaConstraints = {audio: true, video: true};
    
    this.localInfo = {'id': null, 'name': null};
    this.remoteInfo = {'id': null, 'name': null};
    
    this.stream = stream;
    
    
    this.localInfo = $.extend(this.localInfo, localInfo);
    this.remoteInfo = $.extend(this.remoteInfo, remoteInfo);
    
    try {
        this.pc = new RTCPeerConnection(null);
    
        this.pc.onicecandidate = $.proxy(this, 'onicecandidate');
        
        this.pc.onaddstream = $.proxy(this, 'onaddstream');
        
        if(this.stream) {
            this.pc.addStream(this.stream);
        }
    } catch(e) {
        console.log('Failed to create PeerConnection, exception: ' + e.message);
    }
}
/**
 * Callee answer offer from caller.
 * 
 * @param {type} sessionDescription
 * @returns {undefined}
 */
Callee.prototype.answer = function(sessionDescription) {
    this.pc.setRemoteDescription(new RTCSessionDescription(sessionDescription), $.proxy(this, 'onSetRemoteDescriptionSuccess'), $.proxy(this, 'onSetRemoteDescriptionError'));
    
    this.pc.createAnswer($.proxy(this, 'onCreateAnswerSuccess'), $.proxy(this, 'onCreateAnswerError'));
}

Callee.prototype.onSetRemoteDescriptionSuccess = function() {
    
}

Callee.prototype.onSetRemoteDescriptionError = function(error) {
    console.log(error);
}


Callee.prototype.onCreateAnswerError = function(error) {
    
}

Callee.prototype.getType = function() {
    return 'callee';
}


/**
 * Set local stream.
 * 
 * @param {type} stream
 * @returns {undefined}
 */
Callee.prototype.setLocalStream = function(stream) {
    this.stream = stream;
    
    this.pc.addStream(this.stream);
}

Callee.prototype.onCreateAnswerSuccess = function(sessionDescription) {
    this.pc.setLocalDescription(sessionDescription);
    
    //send to server
    socket.emit('answer', {session: sessionDescription, client_id: this.remoteInfo.id})
}


Callee.prototype.setCandidate = function(candidate) {
//    console.log(new RTCIceCandidate(candidate));
    this.pc.addIceCandidate(new RTCIceCandidate(candidate));
}

Callee.prototype.onicecandidate = function(event) {
    if (event.candidate) {
        console.log('send candidate to: ' + this.remoteInfo.id);
//        console.log(event.candidate);
        socket.emit('candidate', {candidate: {sdpMLineIndex: event.candidate.sdpMLineIndex, sdpMid: event.candidate.sdpMid, candidate: event.candidate.candidate}, client_id: this.remoteInfo.id, type: this.getType()});
    }
}

Callee.prototype.onaddstream = function(event) {
    console.log('==========on add stream callee: ============' + this.remoteInfo.id);
    attachMediaStreamById(this.remoteInfo.id, event.stream);
    //attachMediaStream($('#video-holder li[h-id='+this.remoteInfo.id+'] video')[0], event.stream);
}