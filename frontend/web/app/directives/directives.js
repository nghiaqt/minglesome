angular.module('app.directives', [])
  /**
   * Load directive menu
   */
  .directive('plMenu', function () {
    return {
      restrict: 'AE',
      transclude: true,
      scope: {},
      controller: [
        '$scope', 'authService', '$state', '$rootScope', '$document',
        function ($scope, authService, $state, $rootScope, $document) {
          //show flash from yii
          if (window.isNotSupportBrowser) {
            $scope.isHideContact = true;
          }
          $scope.authService = authService;
          $scope.$state = $state;
          $scope.isOpen = false;
          $rootScope.$watch('pageSearch', function (value) {
            $scope.pageSearch = value;
          });
          $rootScope.$watch('pageLounge', function (value) {
            $scope.pageLounge = value;
          });
        }],
      templateUrl: 'app/partials/menu.tmpl.html',
      replace: true
    };
  })
  /**
   * Load directive based on controller
   */
  .directive('plAction', [
    '$compile', '$state', '$rootScope', 'authService',
    function ($compile, $state, $rootScope, authService) {
      return{
        restrict: 'AE',
        link: function (originalScope, element, attrs) {
          /** set default action */
          var directiveLoaded = '';//
          var scope = originalScope.$new();
          originalScope.$on('$destroy', function () {
            scope.$destroy();
          });
          //render directive when state change
          $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            if (toState.name != fromState.name) {
              var headerElem = angular.element('.header');
              if (toState.name === 'home') {
                headerElem.addClass('start-page');
              } else {
                headerElem.removeClass('start-page');
              }
              if (toState.name !== 'search.index' && toState.name !== 'search.dates' &&
                toState.name !== 'search.lounge') {
                if (authService.isAuthenticated()) {
                  /** set directive action for this page */
                  if (toState.name === 'profile.edit') {
                    directiveLoaded = '<div pl-action-profile-filter></div>';
                  } else if ($state.includes('account')) {
                    directiveLoaded = '<div pl-menu-account></div>';
                  } else if (toState.name === 'profile.view') {
                    directiveLoaded = '<div menu-profile-viewer></div>';
                  } else if (toState.name === 'magazine' || toState.name === 'article') {
                    directiveLoaded = '<div menu-magazine></div>';
                  } else if (toState.name === 'important') {
                    directiveLoaded = '<div pl-menu-important></div>';
                  } else {
                    directiveLoaded = '';
                  }
                } else {
                  /** default action login */
                  directiveLoaded = '<div login-form></div>';
                }
                /** load action to page */
                var popUpEl = angular.element(directiveLoaded);
                element.html($compile(popUpEl)(scope));
              } else if (fromState.name !== 'search.index' && fromState.name !== 'search.dates' &&
                fromState.name !== 'search.lounge') {
                directiveLoaded = '<div pl-action-search></div>';
                /** load action to page */
                var popUpEl = angular.element(directiveLoaded);
                element.html($compile(popUpEl)(scope));
              }
            }
          });
        }
      };
    }
  ])

  /**
   * Privacy filter
   */
  .directive('plActionProfileFilter', ['Restangular', function (Restangular) {
      return {
        restrict: 'AE',
        transclude: true,
        scope: {},
        link: function (scope, element, attrs) {
          //get all nesessary data for user
          Restangular.one('profile/get-dropdown-data').get().then(function (res) {
            if (res) {
              scope.dropdown = res;
            }
          });

          //get Privacy filter of user
          Restangular.one('privacy-filter/get-privacy-based-on-user-logged').get().then(function (res) {
            scope.privacyFilter = res;
          });
        },
        controller: [
          '$scope', '$growl', '$document', '$rootScope',
          function ($scope, $growl, $document, $rootScope) {
            //config slimscroll
            $scope.slimScrollResize = $rootScope.slimScrollActionResize;
            $scope.slimScrollOptions = {
              disableFadeOut: true
            };
            $scope.focusFilter = function (input) {
              if ($rootScope.detectedBrowser.platform.mobile) {
                $scope.slimScrollOptions.scrollTo = input;
              }
            };
            //save profile filter
            $scope.saveProfileFilter = function () {
              $scope.privacySuccess = '';
              $scope.privacyErrors = [];
              Restangular.one('privacy-filter').customPOST($scope.privacyFilter, 'save-privacy-based-on-logged-user').then(function (res) {
                //saved successfully
//              $growl.box('', res.replace(/"/g, ''), {
//                class: 'success'
//              }).open();
                $scope.isOpen = false;

              }, function error(res) { //error

                var errorHTML = '<ul>';
                $scope.privacyErrors = res;
                for (var i = 0; i < res.length; i++) {
                  errorHTML += '<li>' + res[i] + '</li>';
                }
                errorHTML += '</ul>';

                $growl.box('', errorHTML, {
                  class: 'danger'
                }).open();
              });
            };
          }],
        templateUrl: 'app/partials/profilefilter.tmpl.html'
      };
    }])
  /**
   * Load action menu for account page
   */
  .directive('plMenuAccount', [function () {
      return {
        restrict: 'AE',
        transclude: true,
        templateUrl: 'app/partials/accountmenu.tmpl.html'
      };
    }])
  .directive('plMenuImportant', [function () {
      return {
        restrict: 'AE',
        transclude: true,
        templateUrl: 'app/partials/importantmenu.tmpl.html'
      };
    }])
  /**
   * auto expand textare
   */
  .directive('plAutoExpand', function () {
    return {
      restrict: 'A',
      link: function ($scope, element, attrs) {
        var updateHeight = function () {
          setTimeout(function () {
            $(element).height(0);
            var height = $(element)[0].scrollHeight;
            if (height < 22) {
              height = 22;
            }
            $(element).height(height);
          }, 300);
        };

        element.bind('change', function () {
          if ($(element).val() === '') {
            updateHeight();
          }
        });

        // Expand the textarea as soon as keyup
        element.bind('keyup', function () {
          updateHeight();
        });
        
        $scope.$on('dateEvent:new',function(event,data) {
          updateHeight();;
        });

        // Expand the textarea as soon as it is added to the DOM
        updateHeight();
      }
    };
  })
  .directive('changeClassByState', ['$rootScope', function ($rootScope) {
      return {
        restrict: 'A',
        link: function ($scope, elem, attrs) {
          $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            if (toState.name === 'home') {
              elem.addClass('orange-yellow');
            } else {
              elem.removeClass('orange-yellow');
            }
          });
        }
      };
    }])
  .directive('placeholder', ['$timeout', function ($timeout) {
      return {
        restrict: 'A',
        link: function ($scope, elem, attrs) {
          elem.bind('focus', function () {
            elem[0].placeholder = '';
          });
          elem.bind('blur', function () {
            if ($(elem).val() === '') {
              elem[0].placeholder = attrs.placeholder;
            }
          });
          $scope.$watch(function () {
            return $(elem).val();
          }, function () {
            if ($(elem).val() === '') {
              elem[0].placeholder = attrs.placeholder;
            }
          });
        }
      };
    }])
  .directive('plPlaceholder', ['$timeout', function ($timeout) {
      return {
        restrict: 'AE',
        require: 'ngModel',
        replace: true,
        link: function ($scope, elem, attrs, ngModel) {
          var defaultText = '';
          var loadDefault = function () {
            $timeout(function () {
              if (ngModel.$modelValue != 'NaN') {
                defaultText = ngModel.$modelValue;
              } else {
                loadDefault();
              }
            }, 500);
          };
          loadDefault();
          elem.bind('focus', function () {
            if (ngModel.$viewValue == defaultText) {
              ngModel.$setViewValue('');
              ngModel.$render();
            }
          });
          elem.bind('blur', function () {
            if (ngModel.$viewValue == '') {
              ngModel.$setViewValue(defaultText);
              ngModel.$render();
            }
          });
        }
      };
    }])
  .directive('plClickOutside', ['$document', function ($document) {
      return {
        restrict: 'AE',
        replace: true,
        link: function ($scope, elem, attrs, ngModel) {
          elem.addClass('pl-outside');
          //hide menu when clicking to div
          $document.on('click', function (e) {
            if ($scope.isOpen) {
              var $target = $(e.target);
              if ($target.parents('.pl-outside').length === 0) {
                $scope.isOpen = false;
                $scope.$apply();
              } else if (e.target.tagName === 'A' && e.target.href !== '') {
                $scope.isOpen = false;
                $scope.$apply();
              }
            }
          });
        }
      };
    }])
  .directive('plUsername', ['socket', function (socket) {
      return {
        restrict: 'AE',
        replace: true,
        scope: {userId: '=', isLogged: '='},
        link: function ($scope, elem, attrs) {
          if ($scope.isLogged === false) {
            elem.addClass('color-red');
          }
          /** realtime user logged*/
          socket.on('userLogged', function (userId, isLogged) {
            if ($scope.userId === userId) {
              if (isLogged) {
                elem.removeClass('color-red');
              } else {
                elem.addClass('color-red');
              }
            }
          });
          $scope.$on('$destroy', function () {
            socket.removeListener('userLogged');
          });
        }
      };
    }])
  .directive('plAvatar', ['socket','$rootScope', function (socket,$rootScope) {
      return {
        restrict: 'AE',
        replace: true,
        scope: {
          user: '=plAvatar',
          height: '@plHeight',
          width: '@plWidth'
        },
        link: function ($scope, elem, attrs) {
          if ($rootScope.detectedBrowser.browser.msie && $rootScope.detectedBrowser.browser.version === 11 && $scope.user.isMatchPrivacyFilter) {
            var parent = $(elem).parent();
            console.log($scope.height)
            var append = '' +
            '<svg id="svg-image-blur">' +
              '<image id="svg-image" height="'+ $scope.height +'" width="'+ $scope.width +'" xlink:href="' + $scope.user.avatarUrl + '" style="filter:url(#blur-effect-1);" />' +
              '<filter id="blur-effect-1">' +
                '<feGaussianBlur stdDeviation="15" />' +
              '</filter>' +
            '</svg>'
            parent.append(append);
            attrs.$set('style','display:none');
          }
          if ($scope.user.isMatchPrivacyFilter) {
            elem.addClass('blurred');
          }
          /** realtime user logged*/
          socket.on('onUpdateAvatar', function (userId, avatarUrl) {
            if ($scope.user._id === userId && avatarUrl) {
              attrs.$set('src', avatarUrl);
            }
          });
          $scope.$on('$destroy', function () {
            socket.removeListener('onUpdateAvatar');
          });
        }
      };
    }])
    .directive('bindPrice', function() { 
        return {
            priority: 10, // adjust this value ;)
            scope: {
              price: '=bindPrice',
              html: '=ngBindHtml'
            },
            link: function($scope,element,attrs) {
                $scope.$watch(function() {
                  return $scope.html
                }, function(newvalue) {
                  var text = element.html();
                  var reg = new RegExp("\\[price\\]",'g');
                  text = text.replace(reg,$scope.price)
                  element.html(text);
                });           
            }
        };      
    });