angular.module('app.auth.states')
        .config([
  'appStatesProvider',
  function(appStatesProvider) {
    // default auth states
    appStatesProvider.push([
      {
        name: 'login',
        url: '/login',
        templateUrl: '/app/auth/views/login.html',
        controller: 'GenericLoginCtrl',
        loginRequired: false
      },
      {
        name: 'passwordRecovery',
        url: '/recover-password',
        templateUrl: '/app/auth/views/recovery.html',
        controller: 'GenericPasswordRecoveryCtrl',
        loginRequired: false
      },
      {
        name: 'passwordReset',
        url: '/reset-password?key',
        templateUrl: '/app/auth/views/reset.html',
        controller: 'GenericPasswordResetCtrl',
        loginRequired: false
      },
      {
        name: 'activeAccount',
        url: '/active-account/:activeKey',
        controller: 'ActiveAccount',
        loginRequired: false
      },
      {
        name: 'activeRestore',
        url: '/active-restore/:activeKey',
        controller: 'ActiveRestore',
        loginRequired: false
      },
      {
        name: 'logout',
        url: '/logout',
        controller: 'LogOut',
        loginRequired: false
      }
//        {
//          name: 'profile',
//          url: "/profile",
//          templateUrl: '/app/auth/views/profile.html',
//          controller: 'GenericProfileCtrl'
//        }
    ]);
  }
]);
