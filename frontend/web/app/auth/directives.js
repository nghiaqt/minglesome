angular.module('app.auth.directives')
  .directive('authStrategy', [
	'$window', 'sessionStorage', '$location',
	/**
	 * @description
	 * Directive for creating links to oauth/openid strategies. Accepts client-side redirect location for success and
	 * failure using `success-redirect` and `failure-redirect` attributes. Accepts strategy name using `auth-strategy`
	 * attribute. If no redirect endpoints specified `/` and current $location.path() will be used for success and
	 * failure respectively.
	 * @example
	 * ```
	 * <a auth-strategy="google" success-redirect="/profile" class="btn">Sign Up With Google</a>
	 * ```
	 */
	function ($window, sessionStorage, $location) {
	  'use strict';
	  return {
		restrict: 'A',
		link: function (scope, element, attrs) {
		  var strategyPath = '/api/auth/' + attrs.authStrategy;

		  // set a href for links just to make sure browser can show a link to the user
		  if (element.prop('tagName') === 'A') {
			element.attr('href', strategyPath);
		  }

		  element.bind('click', function (e) {
			var
			  successRedirect = $location.url(),
			  failureRedirect = $location.url();

			e.preventDefault();

			if (attrs.successRedirect) {
			  successRedirect = attrs.successRedirect;
			}
			if (attrs.failureRedirect) {
			  failureRedirect = attrs.failureRedirect;
			}

			sessionStorage.setItem('redirect', {
			  success: successRedirect,
			  failure: failureRedirect
			});

			$window.location.href = strategyPath;
		  });
		}
	  };
	}
  ])
  /**
   * @description
   * directive for showing form and making the registering function
   */
  .directive('registerForm', function () {
	return {
	  restrict: 'AE',
	  transclude: true,
	  scope: {},
	  templateUrl: '/app/auth/views/register.html',
	  controller: ['$scope', 'authService', '$rootScope', '$growl', '$timeout',
		function ($scope, authService, $rootScope, $growl, $timeout) {
		  $scope.language=window.CONFIG.language;
		  /**
		   * Slide toggle registration form
		   * 
		   * @param {obj} registerArrow is the wrapping Arrow image object 
		   * @param {type} registerImage is the arrow image object
		   */
		  function slideToggleForm(registerArrow, registerImage) {

			var registerWidth = registerImage.width();
			var newPosition = angular.element(window).scrollTop();
			//if registration box is opening
			if ($scope.isOpenRegisterForm) {
			  $scope.isOpenRegisterForm = false;
			  registerArrow.css('right', 'auto');
			  angular.element('.join-try').css({'position': 'fixed', 'top': '0px'});
			} else {
			  //if registration box is closing
			  $scope.isOpenRegisterForm = true;
			  angular.element('.join-try').css({'position': 'absolute', 'top': newPosition + 'px'});
			}
		  }

		  angular.element(window).on('scroll',function () {
			  if ($scope.isOpenRegisterForm) {
				var windowHeight = angular.element(window).height();
				var registerHeight = angular.element('.box-join').height();
				var scrollingLimit = 0;
				//if height of login is larger than height of window
				if (windowHeight > registerHeight) {
				  window.scrollTo(0, angular.element('.join-try').position().top);
				} else {
				  scrollingLimit = registerHeight - windowHeight;
				  var amountScrolled = angular.element(window).scrollTop() - angular.element('.join-try').position().top;
				  //scroll down over up height of register box
				  if (amountScrolled > scrollingLimit) {
					window.scrollTo(0, angular.element('.join-try').position().top + scrollingLimit);
				  } else {
					//scroll up over up height of register box
					if (amountScrolled <= 0) {
					  window.scrollTo(0, angular.element('.join-try').position().top);
					}
				  }
				}
			  }
		  });
		  $scope.isOpenRegisterForm = false;

		  /** add effect for arrow on register form */
		  var registerArrow = angular.element('.butjoin');
		  var registerImage = angular.element('.register-image');
		  $scope.toggleForm = function () {
			slideToggleForm(registerArrow, registerImage);
		  };
		  /** */

		  $scope.user = {
			email: '',
			username: '',
			password: '',
			birthdate: '',
			gender: ''
		  };

		  $scope.errors = '';
		  authService.getGenders().success(function (data, status, headers, config) {
			$scope.genders = data;
		  });
		  //press register
		  $scope.register = function () {
			//show flash from yii
			if (window.isNotSupportBrowser) {
			  angular.forEach(window.flash, function (value, key) {
				$growl.box('', value, {
				  class: key,
				  sticky: true
				}).open();
			  });
			  return;
			}
			authService.signUp($scope.user).success(function (data, status, headers, config) {
			  $scope.errors = '';
			  $scope.user.email = '';
			  $scope.user.username = '';
			  $scope.user.password = '';
			  $scope.user.birthdate = '';
			  $scope.user.gender = '';
			  $growl.box('', 'SUCCESS_REGISTER', {
				class: 'success'
			  }).open();
			}).error(function (data, status, headers, config) {
			  $growl.box('', data, {
				class: 'danger'
			  }).open();
			});
		  };
		  //show login form when clicking try, use
		  var unbindShowRegisterForm=$rootScope.$on('showRegisterForm', function (error, mass) {
			var registerArrow = angular.element('.butjoin');
			var registerImage = angular.element('.register-image');
			slideToggleForm(registerArrow, registerImage);

		  });
		  
		  $scope.$on('$destroy', function(){
			angular.element(window).off('scroll');
			unbindShowRegisterForm();
		  });
		}]
	};
  })
  /**
   * @description
   * directive for showing form and making the 'login' and 'forgot password' function
   */
  .directive('loginForm', function () {
	return {
	  restrict: 'AE',
	  transclude: true,
	  scope: {},
	  controller: [
		'$scope', 'authService', 'socket', '$growl', 'accountService',
		function ($scope, authService, socket, $growl, accountService) {
		  $scope.location = window.CONFIG.country;
		  $scope.email = '';
		  $scope.password = '';
		  $scope.remember = false;
		  $scope.loginErrors = '';
		  $scope.forgotPassErrors = '';
		  $scope.forgotPassSuccess = '';
		  $scope.showLogin = true;
		  $scope.showForgotPass = false;
		  $scope.isCancel = false;
		  $scope.isVerify = false;
		  $scope.restoreSuccess = false;

		  function getSubdomain() {
        var regexParse = new RegExp('[a-z\-0-9]{2,63}\.[a-z\.]{2,5}$');
        var urlParts = regexParse.exec(window.location.hostname);
        return window.location.hostname.replace(urlParts[0],'').slice(0, -1);
}

console.log(getSubdomain());

		  accountService.getCountry().then(function (res) {
			$scope.countries = res;
		  });
		  //login function
		  $scope.login = function () {
			//show flash from yii
			if (window.isNotSupportBrowser) {
			  angular.forEach(window.flash, function (value, key) {
				$growl.box('', value, {
				  class: key,
				  sticky: true
				}).open();
			  });
			  return;
			}
			$scope.loginErrors = '';
			authService.logIn($scope.email, $scope.password, $scope.location, $scope.remember)
			  .success(function (data) {
				socket.emit('isLoggedIn', {
				  'userId': data._id
				});
			  }).error(function (data, status, headers, config) {
			  if (status == 403) {
				if (data.isVerify) {
				  $scope.isVerify = true;
				} else {
				  $scope.isCancel = true;
				}
				$scope.token = data.token;
			  } else if (data) {
				$growl.box('', data, {
				  class: 'danger'
				}).open();
			  }
			});
		  };

		  //forgot password function
		  $scope.forgotPassword = function () {
			$scope.forgotPassErrors = '';
			authService.recover($scope.email).success(function (data, status, headers, config) {
			  $scope.forgotPassSuccess = data.replace(/"/g, '');
//                  $growl.box('FORGOT_PASSWORD', data.replace(/"/g, ''), {
//                    class: 'success'
//                  }).open();
			}).error(function (data, status, headers, config) {
			  if (data) {
				$scope.forgotPassErrors = data.replace(/"/g, '');
			  }
//                  $growl.box('FORGOT_PASSWORD', data.replace(/"/g, ''), {
//                    class: 'danger'
//                  }).open();
			});
		  };

		  $scope.restoreMembership = function () {
			authService.restoreMembership({key: $scope.token}).success(function (data) {
			  $scope.restoreSuccess = true;
			  $scope.isCancel = false;
			});
		  };
		  $scope.resendVerifyEmail = function () {
			authService.resendVerifyEmail($scope.token).success(function (data) {
			  $scope.verifySuccess = true;
			  $scope.isVerify = false;
			});
		  };

		}],
	  templateUrl: '/app/auth/views/login-form.html'
	};
  });
