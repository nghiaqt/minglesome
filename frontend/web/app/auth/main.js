angular.module('app.auth.services', ['app.config', 'sessionStorage', 'app.flash']);
angular.module('app.auth.states', ['app.states']);
angular.module('app.auth.controllers', []);
angular.module('app.auth.directives', ['sessionStorage']);

angular.module('app.auth', [
  'app.auth.services',
  'app.auth.states',
  'app.auth.controllers',
  'app.auth.directives'
]);