(function () {
  'use strict';

  /**
   * @name authService
   * @param {angular.$rootScope} $rootScope
   * @param {angular.$q} $q
   * @param {angular.$http} $http
   * @param {String} CONFIG Configuration object
   * @param {String} CONFIG.logoutUrl url for the logout
   * @constructor
   * @exports
   */
  var AuthService = function ($rootScope, $q, $http, CONFIG, socket, $location) {
    this.$rootScope = $rootScope;
    this.$q = $q;
    this.$http = $http;
    this.CONFIG = CONFIG;
    this.$location = $location;
    // user object
    this.user = null;

    // callback that is called on user authentication
    /** @type {function} */
    this.onauthenticated = null;
    this.socket = socket;
  };

  /**
   * Returns true if current user is authenticated
   * @returns {boolean}
   */
  AuthService.prototype.isAuthenticated = function () {
    return !!this.user;
  };

  /**
   * Authenticates user. If service has onauthenticated callback
   * it will be called passing given user object
   * @param {Object} user User object
   */
  AuthService.prototype.authenticate = function (user) {
    if (this.user) {
      this.user = null;
      this.$rootScope.$broadcast('authService.loggedOut');
    }
    this.user = user;

    this.$rootScope.$broadcast('authService.authenticated', user);
    if (!user.profileComplete && typeof this.onUncompletedProfile === 'function') {
      this.onUncompletedProfile(user);
    }
  };

  /**
   * Check remember login
   * Default load when start site
   * Set session user
   * redirect to profile edit if state current is home page
   * @param {type} user
   * @returns {undefined}
   */
  AuthService.prototype.cookieAuthenticate = function () {
    if (!this.user) {
      var _this = this;
      this.$http.get(this.CONFIG.apiUrl + 'auth/get-user').success(function (user) {
        _this.user = user;

        _this.$rootScope.$broadcast('authService.authenticated', user);
        if (!user.profileComplete && typeof _this.onUncompletedProfile === 'function') {
          _this.onUncompletedProfile(user);
        }
        //go to profile page
        if (_this.$rootScope.$state.current.name == 'home') {
          _this.$rootScope.$state.go('profile.edit');
        }
      });
    }
  };

  /**
   * Logs user out. If service has onloggedout callback it will be called
   * after successful logout http request
   */
  AuthService.prototype.logOut = function () {
    var _this = this;

    this.$http.post(this.CONFIG.apiUrl + 'auth/logout', '').success(function () {
      _this.user = null;
      _this.socket.emit('logout');
      _this.$rootScope.$broadcast('authService.loggedOut');

    });
  };

  AuthService.prototype.logIn = function (email, password, location, remember) {
    var _this = this;
    return this.$http.post(this.CONFIG.apiUrl + 'auth/login', {
      email: email,
      password: password,
      location: location,
      remember: remember
    })
      .success(function (res/*, status, headers, config*/) {
        if (res.redirectUrl) {
          window.location.href = res.redirectUrl;
        }
        _this.authenticate(res.data);

      });
  };

  /**
   * @name AuthService#signUp
   * @description
   * Creates a user using provided data
   * @param {{username: String, email:String, password:String, firstName:String, lastName:String}} data User data
   */
  AuthService.prototype.signUp = function (data) {
    var signUpURL = this.CONFIG.apiUrl + 'user/signup';
    return this.$http.post(signUpURL, data);
  };

  AuthService.prototype.recover = function (usernameOrEmail) {
    return this.$http.post(this.CONFIG.apiUrl + 'user/recovery-password', {
      email: usernameOrEmail
    });
  };

  /**
   * @description
   * Given a user object containing new fields, tries to save it using the API and if request was successful updates
   * current user object with new fields.
   * @param user User object
   */
  AuthService.prototype.save = function (user) {
    var _this = this, deferred;
    // If we detected password1 field we assume user entered new password and we should match it
    if (user.newPassword || user.confirmNewPassword) {
      if (user.newPassword !== user.confirmNewPassword) {
        deferred = this.$q.defer();
        deferred.reject({data: {newPassword: 'ERROR_NEW_PASSWORD'}});
        return deferred.promise;
      } else {
        user.isNewPassword = true;
      }
    }

    if (user.newEmail !== user.email) {
      user.isNewEmail = true;
    }

    // Profile edit endpoint should always return a user profile that we can save, so that we always have correct
    // user profile on the frontend
    return this.$http.put(this.CONFIG.apiUrl + 'user/update', user).success(function (data) {
      _this.user = data;
    });
  };

  /**
   * @description
   * Log activity of user
   * @param subscription subscription object
   */
  AuthService.prototype.logActivity = function () {
    var _this = this;
    // Profile edit endpoint should always return a user profile that we can save, so that we always have correct
    // user profile on the frontend
    return this.$http.put(this.CONFIG.apiUrl + 'user/log-activity').success(function (data) {
      _this.user.lastActivity = data.lastActivity;
    });
  };
  /**
   * @description
   * stop subscription
   * @param subscription subscription object
   */
  AuthService.prototype.stopSubscription = function (subscription) {
    var _this = this;
    // Profile edit endpoint should always return a user profile that we can save, so that we always have correct
    // user profile on the frontend
    return this.$http.put(this.CONFIG.apiUrl + 'user/stop-subscription', subscription).success(function (data) {
      _this.user.level = window.ROLE.free;
    });
  };

  /**
   * @description
   * stop subscription
   * @param subscription subscription object
   */
  AuthService.prototype.cancelMembership = function (membership) {
    var _this = this;
    // Profile edit endpoint should always return a user profile that we can save, so that we always have correct
    // user profile on the frontend
    return this.$http.put(this.CONFIG.apiUrl + 'user/cancel-membership', membership).success(function (data) {
      _this.logOut();
    });
  };

  AuthService.prototype.restoreMembership = function (membership) {
    // Profile edit endpoint should always return a user profile that we can save, so that we always have correct
    // user profile on the frontend
    return this.$http.post(this.CONFIG.apiUrl + 'user/restore-membership', membership).success(function (data) {
      return data;
    });
  };


  AuthService.prototype.activeRestore = function (activeKey) {
    return this.$http.post(this.CONFIG.apiUrl + 'user/active-restore', {activeKey: activeKey});
  };


  /**
   * @ngdoc function
   * @name AuthService#updateProfile
   * @description
   * Update is almost the save as AuthService#save, except it works only for profile and
   * takes field names in dot notation and can replace arrays
   * @param data - fields to update
   */
  AuthService.prototype.updateProfile = function (data) {
    var _this = this;
    return this.$http.put(this.CONFIG.apiUrl + 'profile/' + _this.user._id, data).success(function (data) {
      _this.user = data;
    });
  };

  /**
   * @ngdoc function
   * @name AuthService#can
   * @description
   * Helper function to resolve the permission for the current user on the specified action
   * @param {string} action the action string (create, update, etc.)
   * @param {string} groupOrId the group or the group id on which the action is performed
   * @param {string=} object the object name, which will be checked as 'action + object'
   * @return {boolean} the action is authorized for the user
   */
  AuthService.prototype.can = function (action, groupOrId, object) {
    if (this.groupList.length === 0) {
      return false;
    }

    if (!this.user) {
      return false;
    }

    var group = _.isObject(groupOrId) ? groupOrId : _.find(this.groupList, {_id: groupOrId});

    if (!group) {
      return false;
    }

    if (object) {
      action = action + object;
    }

    return !!group._permissions[action];
  };

  /**
   * @ngdoc function
   * @name AuthService#checkAvailability
   * @description
   * Checks availability for username and email
   * @param {Object} user - user object or a subset of the user object with just `email` and `username` fields
   */
  // TODO: test me
  AuthService.prototype.checkAvailability = function (user) {
    var _this = this, fields = {};
    angular.forEach(['username', 'email'], function (field) {
      if (user[field] !== _this.user[field]) {
        fields[field] = user[field];
      }
    });
    return this.$http.post(this.CONFIG.availabilityCheckURL, fields);
  };
  /**
   * get genders
   * 	 
   * @returns {array}genders 
   */
  AuthService.prototype.getGenders = function () {
    return this.$http.get(this.CONFIG.apiUrl + 'user/get-genders');
  };

  /**
   * get genders
   * 	 
   * @returns {array}genders 
   */
  AuthService.prototype.verifyEmail = function (activeKey) {
    return this.$http.post(this.CONFIG.apiUrl + 'user/active-account', {activeKey: activeKey});
  };
  /**
   * 
   * @param {type} activeKey
   * @returns {unresolved}
   */
  AuthService.prototype.resendVerifyEmail = function (activeKey) {
    return this.$http.post(this.CONFIG.apiUrl + 'user/active-account', {activeKey: activeKey, isResendVerifyEmail: true});
  };

  /**
   * Check message has readed
   * @param {type} message
   * @returns {Boolean}
   */
  AuthService.prototype.hasPrompted=function(message){
    if(!this.user){
      return true;
    }
    if(!this.user.prompts){
      return false;
    }
    return this.user.prompts.indexOf(message)!==-1;
  };
  
  AuthService.prototype.updatePrompt = function (message) {
    var _this = this;
    return this.$http.put(this.CONFIG.apiUrl + 'user/update-prompt', {prompt:message}).success(function () {
      _this.user.prompts.push(message);
    });
  };
  
  angular.module('app.auth.services').factory('authService', [
    '$rootScope', '$q', '$http', 'CONFIG', 'socket', '$location',
    function ($rootScope, $q, $http, CONFIG, socket, $location) {
      return new AuthService($rootScope, $q, $http, CONFIG, socket, $location);
    }
  ]);
})();
