(function () {
  'use strict';

  angular.module('app.auth.services').provider('guard',
      /**
       * @name guardProvider
       */
      function () {
        //TODO: test provider
        this.unauthorizedState = 'home';
        this.defaultState = 'profile.edit';
        this.onLoggedOutState = 'home';
        this.$get = [
          '$rootScope', '$window', '$timeout', '$location', '$state', 'authService', 'sessionStorage', 'flash',
          /**
           * @param {angular.$rootScope} $rootScope
           * @param {angular.$window} $window
           * @param {angular.$timeout} $timeout
           * @param {angular.$location} $location
           * @param {$state} $state
           * @param {authService} authService
           * @param {sessionStorage} sessionStorage
           * @param {flash} flash
           * @returns {{watch: Function}}
           */
          function ($rootScope, $window, $timeout, $location,  $state, authService, sessionStorage, flash) {
            var _this = this;
            return {
              /**
               * @name guard.watch
               * @description Listens for stage change events and prevents from navigating to private states
               */
              watch: function () {
                var
                  redirect = sessionStorage.getItem('redirect'),
                  offStateChangeSuccess;

                if(redirect) {
                  sessionStorage.removeItem('redirect');
                  if(flash.get('success')){
                    $location.path(redirect.success);
                  } else {
                    $location.path(redirect.failure);
                  }
                }

                // if we have flashed messaged we should empty them after loading the state, so that we don't  have
                // flashed messages for the next location
                /*
                if(flash.keys().length){
                  offStateChangeSuccess = $rootScope.$on('$stateChangeSuccess', function(){
                    flash.clear();
                    offStateChangeSuccess();
                  });
                }
                */

                //TODO: maybe we need a user service for that
                if ($window.USER && Object.keys($window.USER).length > 0) {
                  authService.authenticate($window.USER);
                }else{
                  //check cookie user from api server
                  authService.cookieAuthenticate();
                }

                $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                  if (toState.name === _this.unauthorizedState) {
                    return;
                  }
                  if (toState.loginRequired !== false && !authService.isAuthenticated()) {
                    // prevent transition to the current state
                    event.preventDefault();
                    $rootScope.$broadcast('$stateChangePrevented', toState, toParams, fromState, fromParams);
                    //TODO: configurable state
                    $state.go(_this.unauthorizedState);
                    return;
                  }
                });

                $rootScope.$on('authService.loggedOut', function(){
                  $state.go(_this.onLoggedOutState,{},{reload: true});									
                });

                $rootScope.$on('authService.authenticated', function(){																		
                  $state.go(_this.defaultState);
                });
              }
            };
          }
        ];
      }
    );
})();

