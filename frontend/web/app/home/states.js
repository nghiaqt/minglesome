(function() {
  'use strict';
  angular.module('app.home.states').config([
    'appStatesProvider',
    function(appStatesProvider) {
      appStatesProvider.push([
        {
          name: 'home',
          url: '/home',
          templateUrl: '/app/home/views/index.html',
          controller: 'HomeIndexCtrl',
          loginRequired: false,
          resolve:{
            home:['pageService',function(pageService){
                return pageService.home().then(function(res){
                  return res;
                });
            }]
          }
        },
        {
          name: 'faq',
          url: '/faq',
          templateUrl: '/app/home/views/faq.html',
          controller: 'FaqCtrl',
          loginRequired: false
        },
        {
          name: 'important',
          url: '/important/:type',
          templateUrl: '/app/home/views/important.html',
          controller: 'ImportantCtrl',
          loginRequired: false,
          resolve: {
            page:['pageService','$stateParams',function(pageService,$stateParams){
                var type='terms';
                if($stateParams.type){
                  type=$stateParams.type;
                }
                return pageService.get(type).then(function(res){
                  return res;
                });
            }]
          }
        },
        {
          name: 'contact',
          url: '/contact',
          templateUrl: '/app/home/views/contact.html',
          controller: 'ContactCtrl',
          loginRequired: false,
          resolve: {
            /**
             * get admin information
             */
            admin: ['contactService', '$stateParams', function(contactService, $stateParams) {
                return contactService.getAdmin().then(function(res) {
                  return res;
                });
              }],
            reasons:['contactService',function(contactService){
                return contactService.getReason().then(function(res){
                  return res;
                });
            }]
          }
        }
      ]);
    }
  ]);
})();