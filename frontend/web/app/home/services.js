(function() {
  var propertyServices = angular.module('app.home.services', []);
  propertyServices.factory('pageService', [
    'Restangular',
    function(Restangular) {
      var _rest = Restangular.all('pages');
      return {
        get: function(type) {
          return _rest.one(type).one('get').get();
        },
        home: function() {
          return _rest.get('home');
        }
      };
    }]);
  propertyServices.factory('contactService', [
    'Restangular',
    function(Restangular) {
      var _rest = Restangular.all('contacts');
      return {
        get: function() {
          return _rest.get();
        },
        create: function(contact) {
          return _rest.customPOST(contact);
        },
        getAdmin: function() {
          return _rest.get('get-admin');
        },
        getReason: function() {
          return _rest.get('get-reason').then(function(res){
            return res.data;
          });
        }
      };
    }]);
})();