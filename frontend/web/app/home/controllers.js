(function () {
  'use strict';
  var controllers = angular.module('app.home.controllers', []);
  controllers.controller('HomeIndexCtrl', [
    '$scope', 'Restangular', '$rootScope', 'authService', 'home', '$timeout',
    function ($scope, Restangular, $rootScope, authService, home, $timeout) {
      $scope.language = window.CONFIG.language;
      //load home page data
      $scope.social = home.social || [];
      $scope.membersSay = home.membersSay || '';
      $scope.lounge = [
        {name: 'Christina', age: 29, city: 'New York', url: 'images/lounge1.gif'},
        {name: 'Markus', age: 28, city: 'New York', url: 'images/lounge2.gif'},
        {name: 'Anne', age: 26, city: 'New York', url: 'images/lounge3.gif'},
        {name: 'Alex', age: 30, city: 'New York', url: 'images/lounge4.gif'},
        {name: 'Julia', age: 32, city: 'New York', url: 'images/lounge5.gif'},
        {name: 'Lee', age: 35, city: 'New York', url: 'images/lounge6.gif'}
      ];

      /**
       * Youtube video
       * config video
       * Set default video on ready
       *  + default mute
       *  + delay play video after 10s
       * Mute action
       */
      $scope.playerVars = {controls: 0, showinfo: 0, fs: 0, modestbranding: 1, rel: 0};
      $scope.videoHeight = parseInt(($rootScope.detectedBrowser.width * 9) / 16);
      $scope.player = {};
      $scope.mute = true;

      $scope.$on('youtube.player.ready', function ($event, player) {
        $scope.player = player;
        player.mute();
        var timer = $timeout(function () {
          if (player) {
            player.playVideo();
          }
        }, 10000);
        //destroy $on & socket listener
        $scope.$on('$destroy', function () {
          $timeout.cancel(timer);
        });
      });

      $scope.setMute = function () {
        $scope.mute = !$scope.mute;
        if ($scope.mute) {
          $scope.player.mute();
        } else {
          $scope.player.unMute();
        }
      };

      //load nearby date/event
      Restangular.one('date-event/nearly-event').get().then(function (res) {
        $scope.dateEvents = res.data;
      });

      $scope.showRegisterForm = function () {
        $rootScope.$broadcast('showRegisterForm', true);
      };
      $scope.isLoggedIn = authService.user;
    }]);
  controllers.controller('FaqCtrl', ['$scope', '$growl', '$location', function ($scope, $growl, $location) {

    }]);
  controllers.controller('ImportantCtrl', ['$scope', 'page', function ($scope, page) {
      $scope.page = page;
    }]);
  controllers.controller('ContactCtrl', [
    '$scope', 'admin', 'reasons', '$growl', 'authService', 'contactService', '$rootScope',
    function ($scope, admin, reasons, $growl, authService, contactService, $rootScope) {
      var isContact = true;
      $scope.admin = admin;
      $scope.reasons = reasons;
      $scope.subjectContact = '';
      $scope.contentContact = '';
      $scope.email = '';

      //send contact to admin
      $scope.sendContact = function () {
        if ($scope.subjectContact && $scope.contentContact) {
          if (!authService.isAuthenticated() && !$scope.email) {
            return;
          }
          var contact = {
            subject: $scope.subjectContact,
            content: $scope.contentContact,
            email: $scope.email
          };
          contactService.create(contact).then(function (res) {
            $scope.subjectContact = '';
            $scope.contentContact = '';
            $scope.email = '';
            $growl.box('', 'CONTACT_SUCCESS', {
              class: 'success'
            }).open();
          }, function (res) {
            /** show error message */
            $growl.box('MENU_CONTACT', res.data, {
              class: 'danger'
            }).open();
          });
        }
      };

      /** 
       * gallery slideshow
       * Load data Gallery slider 1 item & slider 9 item
       */
      $scope.slides = $scope.admin ? $scope.admin.gallery : [];
      $scope.listSlides = [];
      $scope.isShowList = false;
      $scope.showGallery = true;

      $scope.currentIndex = 0;
      $scope.direction = 'left';

      /** convert gallery only item to gallery list item */
      var converListGallery = function (galleries) {
        var imageArray = [];
        var tmp = [];
        var j = 0;
        for (var i = 0; i < galleries.length; i++) {
          if (galleries[i].url) {
            tmp.push(galleries[i]);
          }
          if (tmp.length !== Number(9)) {
            if (i == (galleries.length - 1)) {
              imageArray.push(tmp);
            }
          } else {
            imageArray.push(tmp);
            tmp = [];
            j = 0;
            if (i == (galleries.length - 1)) {
              imageArray.push(tmp);
            }
          }
        }
        return imageArray;
      };

      /** change list item or only item */
      $scope.showList = function () {
        $scope.isShowList = !$scope.isShowList;
        $scope.currentIndex = 0;
        if ($scope.isShowList) {
          $scope.listSlides = converListGallery($scope.slides);
        }
      };

      /** open image in large view */
      $scope.openImage = function (currentIndex) {
        $scope.isShowList = false;
        $scope.currentIndex = currentIndex;
      };

      $scope.setCurrentSlideIndex = function (index) {
        $scope.direction = (index > $scope.currentIndex) ? 'right' : 'left';
        $scope.currentIndex = index;
      };

      $scope.isCurrentSlideIndex = function (index) {
        return $scope.currentIndex === index;
      };

      $scope.prevSlide = function () {
        $scope.direction = 'right';
        if ($scope.isShowList) {
          $scope.currentIndex = ($scope.currentIndex > 0) ? --$scope.currentIndex : $scope.listSlides.length - 1;
        } else {
          $scope.currentIndex = ($scope.currentIndex > 0) ? --$scope.currentIndex : $scope.slides.length - 1;
        }
      };

      $scope.nextSlide = function () {
        $scope.direction = 'left';
        if ($scope.isShowList) {
          $scope.currentIndex = ($scope.currentIndex < $scope.listSlides.length - 1) ? ++$scope.currentIndex : 0;
        } else {
          $scope.currentIndex = ($scope.currentIndex < $scope.slides.length - 1) ? ++$scope.currentIndex : 0;
        }
      };

      //detroy angular watch, $on,...
      var off = [];
      //destroy $on & socket listener
      $scope.$on('$destroy', function () {
        off.forEach(function (unbind) {
          unbind();
        });
        off = [];
      });
      /**
       * resize gallery
       */
      $scope.imageSize = {width: 390, height: 520};
      $scope.resizeGallery = function () {
        var picture = angular.element('.picture');
        if (picture.width() > 0) {
          $scope.imageSize.width = picture.width();
          $scope.imageSize.height = ($scope.imageSize.width * 4) / 3;
        }
        off.push($rootScope.$watchCollection('detectedBrowser', function () {
          if ($scope.imageSize.width != picture.width()) {
            $scope.imageSize.width = picture.width();
            $scope.imageSize.height = (picture.width() * 4) / 3;
            $scope.$apply();
          }
        }));
      };
    }]);
})();
