(function() {
  'use strict';

  angular.module('app.home.states', ['app.states']);
  angular.module('app.home.controllers', []);
  angular.module('app.home.services', []);

  angular.module('app.home', [
    'app.home.states',
    'app.home.controllers',
    'app.auth.directives',
    'app.home.services'
  ]);
})();
