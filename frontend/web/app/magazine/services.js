(function() {
  var propertyService = angular.module('app.magazine.services', []);
  propertyService.factory('magazineService', [
    'Restangular', function(Restangular) {
      var _rest = Restangular.all('magazines');
      return {
        get: function(id) {
          return _rest.get(id);
        },
        getMagazines: function(month, year) {
          return _rest.get('', {month: month, year: year});
        },
        getMonthsHasMagazines: function(year) {
          return _rest.one('months-has-magazine', year).get();
        },
        getComments: function(articleId, params) {
          return _rest.one(articleId).one('comments').get(params);
        },
        addComment: function(articleId, comment) {
          return _rest.one(articleId).one('comment').customPOST(comment);
        },
        getComment: function(commentId) {
          return _rest.one(commentId).one('comment').get();
        },
      };
    }
  ]);
})();


