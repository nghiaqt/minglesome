(function(){
  'use strict';

  angular.module('app.magazine.states', ['app.states']);
  angular.module('app.magazine.controllers', []);
	angular.module('app.magazine.directives', []);
	angular.module('app.magazine.services', []);

  angular.module('app.magazine', [
    'app.magazine.states',
    'app.magazine.controllers',
		'app.magazine.directives',
		'app.magazine.services'
  ]);
})();
