(function() {
  'use strict';
  var controllers = angular.module('app.magazine.controllers', []);
  controllers.controller('MagazineIndexCtrl', ['$scope', 'magazines', '$sce', '$timeout', '$rootScope', 'magazineService',
    function($scope, magazines, $sce, $timeout, $rootScope, magazineService) {

      $scope.magazines = magazines.magazines;

      //send months have magazines to directive menu
      $timeout(function() {
        $rootScope.$emit('monthHasMagazine', {month: magazines.month, year: magazines.year});
      }, 2000);
      //get data filter from directive
      var unbindFilter=$rootScope.$on('dataFilterMagazine', function(error, mass) {
        if (mass && $rootScope.$state.current.name == "magazine") {
          //get magazines bu month, year
          magazineService.getMagazines(mass.month, mass.year).then(function(res) {
            $scope.magazines = res.data;
          });
        }
      });
      //destroy $on
      $scope.$on('$destroy', function() {
        unbindFilter();
      });

    }]);
  controllers.controller('MagazineViewCtrl', ['$rootScope', '$scope', 'article', 'magazineService', '$state',
    function($rootScope, $scope, article, magazineService, $state) {
      /** load article data */
      $scope.article = article;
      /** load comments data */
      $scope.comments = [];
      $scope.lastCommentDate = '';
      $scope.readMore = '';
      $scope.loadComments = function() {
        magazineService.getComments($scope.article._id, {date: $scope.lastCommentDate}).then(function(res) {
          angular.forEach(res.data, function(value) {
            $scope.comments.unshift(value);
          });
          $scope.lastCommentDate = res.lateCreatedAt;
          $scope.readMore = res.morePre;
        });
      };
      $scope.loadComments();

      /** add comment */
      $scope.textComment = '';
      $scope.addComment = function() {
        if ($scope.textComment) {
          magazineService.addComment($scope.article._id, {content: $scope.textComment}).then(function(res) {
            $scope.comments.push(res);
            $scope.textComment = '';
          });
        }
      };
      //get data filter from directive
      var unbindFilter=$rootScope.$on('dataFilterMagazine', function(error, mass) {
        if (mass && $rootScope.$state.current.name == "article") {
          $state.go('magazine', {year: mass.year, month: mass.month});
        }
      });
      //destroy $on
      $scope.$on('$destroy', function() {
        unbindFilter();
      });
    }]);
  controllers.controller('CommentCtrl', ['$scope', 'magazineService',
    function($scope, magazineService) {
      $scope.comment = {};

      magazineService.getComment($scope.commentId).then(function(res) {
        $scope.comment = res;
      });
    }]);
})();