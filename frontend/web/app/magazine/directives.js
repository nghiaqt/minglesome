angular.module('app.magazine.directives', [])
        .directive('menuMagazine', ['$rootScope', 'profileService', 'authService',
  function($rootScope, profileService, authService) {
    return {
      restrict: 'AE',
      scope: {abc: '=abc'},
      templateUrl: 'app/magazine/views/menu-magazine.html',
      controller: [
        '$scope', '$rootScope', 'magazineService',
        function($scope, $rootScope, magazineService) {
          var currTime = new Date();
          $scope.years = [];
          $scope.months = [];
          $scope.yearModel = 0;
          for (var i = 0; i < 10; i++) {
            $scope.years.push(currTime.getFullYear() - i);
          }
          $scope.searchMagazine = {
            "year": $scope.years[0],
            "month": 12
          };
          /** get months has magazine from controller*/
          var unbindFilter=$rootScope.$on('monthHasMagazine', function(error, mass) {
            $scope.searchMagazine.month = mass.month;
            $scope.yearSelected = $scope.searchMagazine.year = mass.year;
          });

          //destroy $on
          $scope.$on('$destroy', function() {
            unbindFilter();
          });

          /** show months have article in a year */
          $scope.showMonthHasArticle = function() {
            $scope.searchMagazine.year = currTime.getFullYear() - $scope.yearModel;
            magazineService.getMonthsHasMagazines($scope.searchMagazine.year).then(function(res) {
              $scope.months = res.data;
            });
          };
          $scope.showMonthHasArticle();

          /** get month user clicked*/
          $scope.getMagazinesByMonth = function(monthSelected) {
            $scope.searchMagazine.month = monthSelected;
            $scope.yearSelected = $scope.searchMagazine.year;
            //send data filter to controller
            $rootScope.$emit('dataFilterMagazine', $scope.searchMagazine);
            $scope.isOpen = false;
          };
        }]
    };
  }])

        /**
         * use videogular to load video
         * Note: because there is an error when reloading the videogular
         * So I set video playing automatically for 1s to prevent the losting Video Error 
         * 
         * @property {string} videoName is video's url
         */
        .directive('customVideo', [function() {
    return {
      restrict: "E",
      scope: {
        videoName: "="
      },
      controller: ['$scope', '$sce', '$timeout', function($scope, $sce, $timeout) {
          $scope.currentTime = 0;
          $scope.totalTime = 0;
          $scope.state = null;
          $scope.volume = 1;
          $scope.isCompleted = false;
          $scope.API = null;

          $scope.onPlayerReady = function(API) {
            $scope.API = API;
            $timeout(function() {
              $scope.API.pause();
            }, 1500);
          };


          $scope.onCompleteVideo = function() {
            $scope.isCompleted = true;
          };

          $scope.onUpdateState = function(state) {
            $scope.state = state;
          };

          $scope.onUpdateTime = function(currentTime, totalTime) {
            $scope.currentTime = currentTime;
            $scope.totalTime = totalTime;
          };

          $scope.onUpdateVolume = function(newVol) {
            $scope.volume = newVol;
          };

          $scope.onUpdateSize = function(width, height) {
            $scope.config.width = width;
            $scope.config.height = height;
          };
          $scope.config = {
            autoHide: false,
            autoHideTime: 3000,
            autoPlay: true,
            sources: [
              {src: $sce.trustAsResourceUrl($scope.videoName), type: "video/mp4"}
            ],
            tracks: [
              {
                src: "assets/subs/pale-blue-dot.vtt",
                kind: "subtitles",
                srclang: "en",
                label: "English",
                default: ""
              }
            ],
            loop: true,
            preload: "auto",
            transclude: true,
            controls: undefined,
            theme: {
              url: "styles/themes/default/videogular.css"
            },
            plugins: {
              poster: {
                //						url: "assets/images/videogular.png"
              },
              ads: {
                companion: "companionAd",
                companionSize: [728, 90],
                network: "6062",
                unitPath: "iab_vast_samples",
                adTagUrl: "http://pubads.g.doubleclick.net/gampad/ads?sz=400x300&iu=%2F6062%2Fiab_vast_samples&ciu_szs=300x250%2C728x90&gdfp_req=1&env=vp&output=xml_vast2&unviewed_position_start=1&url=[referrer_url]&correlator=[timestamp]&cust_params=iab_vast_samples%3Dlinear"
              }
            }
          };
        }],
      templateUrl: 'app/magazine/views/custom-video.html',
    };
  }])
        .directive('commentMagazine', [function() {
    return {
      restrict: 'AE',
      scope: {commentId: '='},
      templateUrl: 'app/magazine/views/comment.html',
      controller: 'CommentCtrl'
    };
  }]);