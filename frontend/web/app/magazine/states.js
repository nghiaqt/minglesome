(function() {
  'use strict';
  angular.module('app.magazine.states').config([
    'appStatesProvider',
    function(appStatesProvider) {
      appStatesProvider.push([
        {
          name: 'magazine',
          url: '/magazine?year&month',
          templateUrl: '/app/magazine/views/index.html',
          controller: 'MagazineIndexCtrl',
          resolve: {
            magazines: ['magazineService', '$stateParams', function(magazineService, $stateParams) {
                if ($stateParams.year && $stateParams.month) {
                  //get magazines by month and year
                  return magazineService.getMagazines($stateParams.month, $stateParams.year).then(function(res) {
                    return {
                      month: parseInt($stateParams.month),
                      year: parseInt($stateParams.year),
                      magazines: res.data
                    };
                  });
                } else {
                  var currTime = new Date();
                  //get magazines by month and year
                  return magazineService.getMagazines('', currTime.getFullYear()).then(function(res) {
                    if (res) {
                      return {
                        month: res.month,
                        year: currTime.getFullYear(),
                        magazines: res.data
                      };
                    } else {
                      return {
                        month: '',
                        year: currTime.getFullYear(),
                        magazines: []
                      };
                    }
                  });
                }
              }]
          },
          loginRequired: false
        },
        {
          name: 'article',
          url: '/article/:id',
          templateUrl: '/app/magazine/views/view.html',
          controller: 'MagazineViewCtrl',
          resolve: {
            article: [
              '$stateParams', 'magazineService', function($stateParams, magazineService) {
                return magazineService.get($stateParams.id).then(function(res) {
                  return res;
                });
              }
            ]
          },
          loginRequired: false
        }
      ]);
    }
  ]);
})();