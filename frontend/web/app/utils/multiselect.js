angular.module('pl.multiselect', [])

        //from bootstrap-ui typeahead parser
        .factory('optionParser', ['$parse', function($parse) {

    //                      00000111000000000000022200000000000000003333333333333330000000000044000
    var TYPEAHEAD_REGEXP = /^\s*(.*?)(?:\s+as\s+(.*?))?\s+for\s+(?:([\$\w][\$\w\d]*))\s+in\s+(.*)$/;

    return {
      parse: function(input) {

        var match = input.match(TYPEAHEAD_REGEXP), modelMapper, viewMapper, source;
        if (!match) {
          throw new Error(
                  "Expected typeahead specification in form of '_modelValue_ (as _label_)? for _item_ in _collection_'" +
                  " but got '" + input + "'.");
        }
        return {
          itemName: match[3],
          source: $parse(match[4]),
          viewMapper: $parse(match[2] || match[1]),
          modelMapper: $parse(match[1])
        };
      }
    };
  }])

        .directive('plMultiselect', ['$parse', '$document', '$compile', 'optionParser',
  function($parse, $document, $compile, optionParser) {
    return {
      restrict: 'EA',
      require: 'ngModel',
      link: function(originalScope, element, attrs, modelCtrl) {

        var exp = attrs.options,
                parsedResult = optionParser.parse(exp),
                isMultiple = attrs.multiple ? true : false,
                required = false,
                scope = originalScope.$new(),
                changeHandler = attrs.change || angular.noop,
                headerValue = attrs.showHeaderValue ? true : false;
        scope.profileTitle = attrs.profileTitle || '';
        scope.items = [];
        scope.header = '';
        scope.defaultHeader = attrs.defaultHeader || '';
        scope.headerItems = [];
        scope.multiple = isMultiple;
        scope.disabled = false;
        scope.itemClass = attrs.itemClass || '';

        originalScope.$on('$destroy', function() {
          scope.$destroy();
        });

        var popUpEl = angular.element('<multiselect-popup></multiselect-popup>');

        //required validator
        if (attrs.required || attrs.ngRequired) {
          required = true;
        }
        attrs.$observe('required', function(newVal) {
          required = newVal;
        });

        //watch disabled state
        scope.$watch(function() {
          return $parse(attrs.disabled)(originalScope);
        }, function(newVal) {
          scope.disabled = newVal;
        });

        //watch single/multiple state for dynamically change single to multiple
        scope.$watch(function() {
          return $parse(attrs.multiple)(originalScope);
        }, function(newVal) {
          isMultiple = newVal || false;
        });

        //watch option changes for options that are populated dynamically
        scope.$watch(function() {
          return parsedResult.source(originalScope);
        }, function(newVal) {
          if (angular.isDefined(newVal))
            parseModel();
        }, true);

        //watch model change
        scope.$watch(function() {
          return modelCtrl.$modelValue;
        }, function(newVal, oldVal) {
          //when directive initialize, newVal usually undefined. Also, if model value already set in the controller
          //for preselected list then we need to mark checked in our scope item. But we don't want to do this every time
          //model changes. We need to do this only if it is done outside directive scope, from controller, for example.
          if (angular.isDefined(newVal)) {
            markChecked(newVal);
            scope.$eval(changeHandler);
          }
          //empty header if no field select
          if ((angular.isArray(modelCtrl.$modelValue) && is_empty(modelCtrl.$modelValue)) || angular.isUndefined(modelCtrl.$modelValue))
            scope.header = '';
          modelCtrl.$setValidity('required', scope.valid());
        }, true);

        function parseModel() {
          scope.items.length = 0;
          var model = parsedResult.source(originalScope);
          if (!angular.isDefined(model))
            return;
          angular.forEach(model, function(value, key) {
            var local = {};
            local[parsedResult.itemName] = {id: key, label: value};
            scope.items.push({
              label: value,
              model: {id: key, label: value},
              checked: false
            });
          });
          if(modelCtrl.$modelValue){
            markChecked(modelCtrl.$modelValue);
          }
        }

        element.append($compile(popUpEl)(scope));

        function is_empty(obj) {
          if (!obj)
            return true;
          if (obj.length && obj.length > 0)
            return false;
          for (var prop in obj)
            if (obj[prop])
              return false;
          return true;
        }


        scope.valid = function validModel() {
          if (!required)
            return true;
          var value = modelCtrl.$modelValue;
          return (angular.isArray(value) && value.length > 0) || (!angular.isArray(value) && value != null);
        };

        function selectSingle(item) {
          if (item.checked) {
            scope.uncheckAll();
          } else {
            scope.uncheckAll();
            item.checked = !item.checked;
          }
          setModelValue(false);
        }

        function selectMultiple(item) {
          item.checked = !item.checked;
          setModelValue(true);
        }

        function setModelValue(isMultiple) {
          var value;

          if (isMultiple) {
            value = [];
            angular.forEach(scope.items, function(item) {
              if (item.checked)
                value.push(item.model.id);
            });
          } else {
            angular.forEach(scope.items, function(item) {
              if (item.checked) {
                value = item.model.id;
                return false;
              }
            });
          }
          modelCtrl.$setViewValue(value);
        }

        function markChecked(newVal) {
          if (!angular.isArray(newVal)) {
            angular.forEach(scope.items, function(item) {
              if (item.model.id == newVal) {
                item.checked = true;
                if (headerValue) {
                  scope.header = item.model.id;
                } else {
                  scope.header = item.label;
                }
//                return false;
              }
            });
          } else {
            scope.header = '';
            scope.headerItems = [];
            angular.forEach(newVal, function(i) {
              angular.forEach(scope.items, function(item) {
                if (item.model.id == i) {
                  item.checked = true;
                  if (headerValue) {
                    scope.headerItems.push(item.model.id);
                  } else {
                    scope.headerItems.push(item.label);
                  }
                }
              });
            });
          }
        }

        scope.checkAll = function() {
          if (!isMultiple)
            return;
          angular.forEach(scope.items, function(item) {
            item.checked = true;
          });
          setModelValue(true);
        };

        scope.uncheckAll = function() {
          angular.forEach(scope.items, function(item) {
            item.checked = false;
          });
          setModelValue(true);
        };

        scope.select = function(item) {
          if (isMultiple === false) {
            selectSingle(item);
            scope.toggleSelect();
          } else {
            selectMultiple(item);
          }
        };
      }
    };
  }])

        .directive('multiselectPopup', ['$document', function($document) {
    return {
      restrict: 'E',
      scope: false,
      replace: true,
      templateUrl: 'app/partials/multiselect.tmpl.html',
      link: function(scope, element, attrs) {

        scope.isVisible = false;

        scope.toggleSelect = function() {
          if (element.hasClass('open')) {
            element.removeClass('open');
            $document.unbind('click', clickHandler);
          } else {
            element.addClass('open');
            $document.bind('click', clickHandler);
            scope.focus();
          }
        };

        function clickHandler(event) {
          if (elementMatchesAnyInArray(event.target, element.find(event.target.tagName)))
            return;
          element.removeClass('open');
          $document.unbind('click', clickHandler);
          scope.$apply();
        }

        scope.focus = function focus() {
          var searchBox = element.find('input')[0];
//          searchBox.focus(); 
        };

        var elementMatchesAnyInArray = function(element, elementArray) {
          for (var i = 0; i < elementArray.length; i++)
            if (element == elementArray[i])
              return true;
          return false;
        };
      }
    };
  }]);