angular.module('app').directive('plCarousel', function () {

    var carouselInner, items, prev, next,
        direction, first, last,
        widthToMove, slider, clone,
        event;

    function carousel (scope, element) {

        carouselInner = element.find('.carousel-inner');
        items = carouselInner.find('.item');
        prev = element.find('.carousel-control.left');
        next = element.find('.carousel-control.right');

        assignControlClicks();
    }

    function assignControlClicks () {

        prev.on('click', function (e) {
            e.preventDefault();
            event = e;
            slide('right');
        });
        next.on('click', function (e) {
            e.preventDefault();
            event = e;
            slide('left');
        });
    }

    function slide (dir, event) {

        updateItems();

        direction = dir;
        first = $(items[0]);
        last = $(items[items.length - 1]);

        widthToMove = -first.width();
        clone = getClone();

        /*
         * If left, animate, remove first, append clone
         * If right, .before clone, animate, remove last
         */
        if (direction === 'left') {
            slideLeft();
        } else if (direction === 'right') {
            slideRight();
        }
    }

    function updateItems () {

        carouselInner = $(event.target).closest('.carousel').find('.carousel-inner');

        items = carouselInner.find('.item');
    }

    function getClone () {

        return (direction === 'left' ? first : last).closest('div.item').clone();
    }

    function slideLeft () {
        
        slider = first;
        animateSlide(widthToMove, finishLeftSlide);
    }

    function finishLeftSlide () {

        first.remove();
        carouselInner.append(clone);
    }

    function slideRight () {
        
        slider = clone;
        clone.css('margin-left', widthToMove + 'px');
        first.before(clone);
        animateSlide(0, finishRightSlide);
    }

    function finishRightSlide () {
        last.remove();
    }

    function animateSlide (slideTo, afterSlide) {
        slider.animate( { marginLeft: slideTo + 'px' }, 700, 'swing', afterSlide);
    }

    return {
        restrict: 'A',
        link: carousel
    };

});