(function() {
  'use strict';
  (function($) {
    /**
     * equalizes the heights of all elements in a jQuery collection
     * thanks to John Resig for optimizing this!
     * usage: $("#col1, #col2, #col3").equalizeCols();
     */

    $.fn.equalizeCols = function() {
      var height = 0, reset = $.support.boxModel < 7 ? "1%" : "auto";

      return this.css("height", reset).each(function() {
        height = Math.max(height, this.offsetHeight);
      }).css("height", height).each(function() {
        var h = this.offsetHeight;
        if (h > height) {
          $(this).css("height", height - (h - height));
        }
        ;
      });

    };

  })(jQuery);

  function getPageTitle($injector, state) {
    var title = state.data && state.data.title;
    if (!title) {
      return;
    }
    if (_.isString(title)) {
      return title;
    }
    return $injector.invoke(title, null, state.locals.globals);
  }

  function fakeNgModel(initValue) {
    return {
      $setViewValue: function(value) {
        this.$viewValue = value;
      },
      $viewValue: initValue
    };
  }

  angular.module('utils', ['ui.router', 'ngSanitize'])
  .constant('capitalize', function(str) {
    return str.charAt(0).toUpperCase() + str.substr(1);
  })
  .factory('userCan', function() {
    return function(action, context) {
      // FIXME
      return true;
    };
  })
  .run([
    '$rootScope', '$state', '$injector', 'userCan',
    function($rootScope, $state, $injector,userCan) {
      $rootScope.pageTitle = function() {
        return getPageTitle($injector, $state.$current);
      };

      $rootScope.parentPageTitle = function() {
        return getPageTitle($injector, $state.$current.parent);
      };

      // generates absolute state name from relative, used to provide absolute state names
      // for directives like ui-sref-active and ui-state-active
      $rootScope.relSref = function(state, root) {
        var current = $state.current.name.split('.');
        if (current[current.length - 1] !== root) {
          current.pop();
        }
        current = current.join('.');
        return current + '.' + state;
      };

      $rootScope.userCan = userCan;
    }
  ])
  // temp directive to remove the # action for any a links
  .directive('a', [function() {
    return {
      restrict: 'E',
      link: function(scope, element, attributes) {
        if (attributes.href === '#' || attributes.href === 'tel:') {
          element.on('click', function() {
            return false;
          });
        }
      }
    };
  }])
  /*.directive('transform', [function(){
      return {
        restrict: 'C',
        link: function(scope, element, attributes) {
          $(element).jqTransform();
        }
      };
  }])*/
  .filter('trimString', [function() {
    return function(input, length, ellip) {
      if (typeof input === 'undefined') {
        return '';
      }
      ellip = ellip || "&hellip;";
      if (input.length > length) {
        return input.substring(0, length) + ellip;
      } else {
        return input;
      }
    };
  }])
  .filter('removeSpaces', [function() {
    return function(string) {
      if (angular.isDefined(string)) {
        return string.replace(' ', '');
      } else {
        return '';
      }
    };
  }])
  .filter('elipsis', [function() {
    return function(string, count) {
      if (angular.isDefined(string)) {
        if (string.length > count)
          return string.substring(0, count) + "...";
        else
          return string;
      } else {
        return '';
      }
    };
  }])
  .filter('ucFirst', [function() {
    return function(string) {
      if (angular.isDefined(string)) {
        return string.charAt(0).toUpperCase() + string.slice(1);
      } else {
        return '';
      }
    };
  }])
  .filter('startFrom', [function() {
      return function(input, start) {
        if (input && input.length) {
          start = parseInt(start, 10);
          return input.slice(start);
        } else {
          return input;
        }
      };
    }])
  .filter('linkify', [function() {
      return function(text) {
        var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        return text.replace(exp, "<a href='$1' target=\"_blank\">$1</a>");
      };
    }])
  .filter('capitalize', ['capitalize', function(capitalize) {
    return  function(input) {
      return capitalize(input);
    };
  }])
  .filter('minutesAsTime', ['$filter', function($filter) {
    return function(input) {
      var date = new Date(0, 0, 0, Math.floor(input / 60), input % 60);
      return new $filter('date')(date, 'shortTime');
    };
  }])
  .filter('htmlToPlaintext', [function() {
    return function(text) {
      return String(text).replace(/<[^>]+>/gm, '');
    };
  }]);
})();
