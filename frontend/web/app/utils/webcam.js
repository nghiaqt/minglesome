'use strict';

angular.module('webcam', [])
        .directive('webcam', [
  '$rootScope', '$timeout',
  function($rootScope, $timeout) {
    return {
      template: '<div class="webcam" ng-transclude></div>',
      restrict: 'E',
      replace: true,
      transclude: true,
      scope:
              {
                onError: '&',
                onStream: '&',
                onStreaming: '&',
                placeholder: '=',
                maxHeight: '=height',
                maxWidth: '=width'
              },
      link: function($scope, element) {
        var videoElem, videoStream;
        var stream = new Stream();
        stream.init({mediaConstraints: {
            audio: false,
            video: {
              mandatory: {
                "minWidth": "320",
                "minHeight": "240",
                "maxWidth": "960",
                "maxHeight": "720",
				"maxFrameRate":30
              }
            }
          }});

        $scope.$on('$destroy', function() {
          if (!$rootScope.videoStream) {
            if (!!videoStream && typeof videoStream.stop === 'function') {
              videoStream.stop();
            }
            if (!!videoElem) {
              delete videoElem.src;
            }
          }
        });

        // Default variables
        var isStreaming = false;
        if ($scope.maxWidth > 0) {
          var width = $scope.maxWidth;
        } else {
          var width = 300;
        }
        if ($scope.maxHeight > 0) {
          var height = $scope.maxHeight;
        } else {
          var height = 400;
        }

        if ($scope.placeholder) {
          var placeholder = document.createElement('img');
          placeholder.setAttribute('class', 'webcam-loader');
          placeholder.src = $scope.placeholder;
          element.append(placeholder);
        }

        var removeLoader = function removeLoader() {
          if (placeholder) {
            angular.element(placeholder).remove();
          }
        };

        var onSuccess = function() {
          videoElem = document.createElement('video');
          videoElem.setAttribute('class', 'webcam-live');
          videoElem.setAttribute('autoplay', '');
          element.append(videoElem);
          // Firefox supports a src object
          attachMediaStream(videoElem, videoStream);
          /** resize width height of camera */
          var resizeVideo = function(videoWidth, videoHeight) {
            /** resize video by max height */
            var scale = height / videoHeight;

            width = videoWidth * scale;
            height = videoHeight * scale;
            videoElem.setAttribute('style', 'margin-left:' + (($scope.maxWidth - width) / 2) + 'px');

            videoElem.setAttribute('width', width);
            videoElem.setAttribute('height', height);
            removeLoader();

            /* Call custom callback */
            if ($scope.onStreaming) {
              $scope.onStreaming({video: videoElem});
            }
          };

          $scope.$watch('maxWidth', function() {
            if ($scope.maxWidth > 0) {
              width = $scope.maxWidth;
              height = width * 4 / 3;
              findVideoSize();
            }
          });

          var timeCount = 0;
          /** get video size */
          var findVideoSize = function() {
            if (videoElem.videoWidth > 0) {
              resizeVideo(videoElem.videoWidth, videoElem.videoHeight);
            } else {
              timeCount++;
              if (timeCount < 10) {
                $timeout(function() {
                  findVideoSize();
                }, 400);
              } else {
                resizeVideo($scope.maxWidth, $scope.maxHeight);
              }
            }
          };
          /**
           * Start streaming the webcam data when the video element can play
           * It will do it only once
           */
          videoElem.addEventListener('canplay', function() {
            if (!isStreaming) {
              findVideoSize();
              isStreaming = true;
              /* Call custom callback */
              if ($scope.onStream) {
                $scope.onStream({stream: videoStream, video: videoElem});
              }
            }
          }, false);
        };

        if ($rootScope.videoStream) {
          videoStream = $rootScope.videoStream;
          onSuccess();
        } else {
          // called when camera stream is loaded
          stream.onSuccessGetUserMediaHandler = function(streamObject) {
            videoStream = streamObject.stream;
            onSuccess();
          };

          // called when any error happens
          stream.onErrorGetuserMediaHandler = function(err) {
            removeLoader();
            /* Call custom callback */
            if ($scope.onError) {
              $scope.onError({err: err});
            }
            return;
          };

          stream.getUserMedia();
        }
      }
    };
  }])
        .directive('plVideo', [
  '$rootScope', '$timeout', 'socket',
  function($rootScope, $timeout, socket) {
    return {
      template: '<div class="video" ng-transclude></div>',
      restrict: 'E',
      replace: true,
      transclude: true,
      scope:
              {
                streamId: '=',
                maxHeight: '=',
                maxWidth: '='
              },
      link: function($scope, element) {
        var videoElem;
        var onSuccess = function(videoStream) {

          $scope.$on('$destroy', function() {
            videoStream = null;
            $rootScope.$emit('removeReceiverStream', $scope.streamId);
          });

          // Default variables
          var isStreaming = false;
          if ($scope.maxWidth > 0) {
            var width = $scope.maxWidth;
          } else {
            var width = 390;
          }
          if ($scope.maxHeight > 0) {
            var height = $scope.maxHeight;
          } else {
            var height = 0;
          }

          if ($scope.placeholder) {
            var placeholder = document.createElement('img');
            placeholder.setAttribute('class', 'webcam-loader');
            placeholder.src = $scope.placeholder;
            element.append(placeholder);
          }

          var removeLoader = function removeLoader() {
            if (placeholder) {
              angular.element(placeholder).remove();
            }
          };

          videoElem = document.createElement('video');
          videoElem.setAttribute('class', 'webcam-live');
          videoElem.setAttribute('autoplay', '');
          element.append(videoElem);

          /** set width height of camera */
          var resizeVideo = function() {
            /** resize video by max height */
            var scale = height / videoElem.videoHeight;
            width = videoElem.videoWidth * scale;
            height = videoElem.videoHeight * scale;

            videoElem.setAttribute('style', 'margin-left:' + (($scope.maxWidth - width) / 2) + 'px');
            videoElem.setAttribute('width', width);
            videoElem.setAttribute('height', height);
            // console.log('Started streaming');
            removeLoader();
          };

          $scope.$watch('maxWidth', function() {
            if ($scope.maxWidth > 0) {
              width = $scope.maxWidth;
              height = width * 4 / 3;
              findVideoSize();
            }
          });

          /** get video size */
          var findVideoSize = function() {
            if (videoElem.videoWidth > 0) {
              resizeVideo();
            } else {
              $timeout(function() {
                findVideoSize();
              }, 400);
            }
          };

          /**
           * Start streaming the webcam data when the video element can play
           * It will do it only once
           */
          videoElem.addEventListener('canplay', function() {
            if (!isStreaming) {
              findVideoSize();
              isStreaming = true;
            }
          }, false);
          // Firefox supports a src object
          attachMediaStream(videoElem, videoStream);
        };

        /**
         * load camera
         */
        socket.emit('call', $scope.streamId);
        var unbindAddVideoStreaming = $rootScope.$on('addVideoStreaming', function(event, userId, videoStream) {
          if ($scope.streamId == userId) {
            onSuccess(videoStream);
          }
        });
        $scope.$on('$destroy', function() {
          unbindAddVideoStreaming();
        });
      }
    };
  }])
        .directive('plAudio', [function() {
    return {
      template: '<div class="audio" ng-transclude></div>',
      restrict: 'AE',
      replace: true,
      transclude: true,
      scope:
              {
                stream: '='
              },
      link: function postLink($scope, element) {
        if ($scope.stream) {
          var audioElem = document.createElement('audio');
          audioElem.setAttribute('class', 'audio-live');
          audioElem.setAttribute('autoplay', '');
          audioElem.setAttribute('volume', '0.6');
          element.append(audioElem);
          attachMediaStream(audioElem, $scope.stream);
        }
      }
    };
  }]);
