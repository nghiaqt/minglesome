angular.module('ui.slimscroll', [])
  .directive('slimscroll', ['$rootScope', function ($rootScope) {
      'use strict';
      return {
        restrict: 'A',
        scope:{
          slimscroll:'=slimscroll',
          defaultHeight:'=defaultHeight',
          slimscrollWatch:'=slimscrollWatch'
        },
        link: function ($scope, $elem, $attr) {
          var off = [];
          var activationState = true, onload = true;
          //auto resize height. resize by default height for mobile & pc
          if ($attr.autoResize) {
            var windowHeight = $rootScope.detectedBrowser.height;
            var pageHeight = 0;
            var calHeight = function () {
              if ($scope.defaultHeight) {
                pageHeight = $rootScope.detectedBrowser.width > 490 ? $scope.defaultHeight.pc : $scope.defaultHeight.mobile;
              }
              var height = parseInt(windowHeight) - parseInt(pageHeight);
              if (height < 100) {
                height = 100;
              }
              height = height + 'px';
              if (height !== $scope.slimscroll.height) {
                $scope.slimscroll.height = height;
              }
            };

            calHeight();

            off.push($rootScope.$watchCollection('detectedBrowser', function () {
              windowHeight = $rootScope.detectedBrowser.height;
              calHeight();
            }));
          }

          var refresh = function () {
            if ($attr.slimscrollChat) {
              $($elem).slimScroll($scope.slimscroll).bind('slimscrolling', function (scroll) {
                onScroll();
              });
            } else {
              $($elem).slimScroll($scope.slimscroll);
            }
          };

          function shouldActivateAutoScroll() {
            // + 1 catches off by one errors in chrome
            return $elem[0].scrollTop + $elem[0].clientHeight + 30 >= $elem[0].scrollHeight;
          }
          function onScroll() {
            activationState = shouldActivateAutoScroll();
          }

          var init = function () {
            refresh();
            if ($scope.slimscroll && !$scope.slimscroll.noWatch) {
              off.push($scope.$watchCollection('slimscroll', refresh));
            }
            if ($attr.slimscrollChat && $scope.slimscrollWatch) {
              var scrollTimeOut=null;
              off.push($scope.$watchCollection('slimscrollWatch', function () {
                if (activationState || onload) {
                  clearTimeout(scrollTimeOut);
                  scrollTimeOut = setTimeout(function () {
                      $($elem).slimScroll({scrollTo: 'bottom'});
                  }, 300);
                  onload = false;
                }
              }));
            } else {
              off.push($scope.$watchCollection('slimscrollWatch', refresh));
            }
          };
          var destructor = function () {
            off.forEach(function (unbind) {
              unbind();
            });
            off = null;
          };
          off.push($scope.$on('$destroy', destructor));
          init();
        }
      };
    }]);
