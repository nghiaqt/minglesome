angular.module('app.slider', [])
        .directive('plSlide', ['$timeout', function($timeout) {
    return {
      restrict: 'A',
      replace: true,
      scope: {
        plSlide: '=',
        options: '=',
        params: '=',
        multipleItem: '@',
        callbackAfter: '&',
        callbackOnload: '&',
        callbackOpenImage: '&'
      },
      templateUrl: function(element, attr) {
        return attr.templateUrl || 'app/partials/bxslider.tmpl.html';
      },
      link: function(scope, elm, attrs) {
        /** convert gallery only item to gallery list item */
        var converListGallery = function(galleries) {
          var imageArray = [];
          var tmp = [];
          var j = 0;
          for (var i = 0; i < galleries.length; i++) {
            tmp.push(galleries[i]);
            if (++j !== Number(scope.multipleItem)) {
              if (i == (galleries.length - 1)) {
                imageArray.push(tmp);
              }
            } else {
              imageArray.push(tmp);
              tmp = [];
              j = 0;
              if (i == (galleries.length - 1)) {
                imageArray.push(tmp);
              }
            }
          }
          return imageArray;
        };

        /** conver slide only to list */
        if (scope.multipleItem) {
          scope.slides = converListGallery(scope.plSlide);
        }

        if (angular.isFunction(scope.callbackOpenImage)) {
          scope.openImage = function(currentIndex) {
            scope.callbackOpenImage({currentIndex: currentIndex});
          };
        }

        elm.ready(function() {
          /** Executes immediately after each slide transition. Function argument is the current slide element (when transition completes). */
          if (angular.isFunction(scope.callbackAfter)) {
            scope.options.onSlideAfter = function($slideElement, oldIndex, newIndex) {
              scope.callbackAfter({$slideElement: $slideElement, oldIndex: oldIndex, newIndex: newIndex});
            };
          }
          /** Executes immediately after the slider is fully loaded */
          if (angular.isFunction(scope.callbackOnload)) {
            scope.options.onSliderLoad = function(currentIndex) {
              scope.callbackOnload({currentIndex: currentIndex});
            };
          }

          elm.bxSlider(scope.options);
          var timeout = null;
          scope.$watch('plSlide', function(ov, nv) {
            if (scope.multipleItem) {
              scope.slides = converListGallery(scope.plSlide);
            }
            if (timeout) {
              $timeout.cancel(timeout);
            }
            timeout = $timeout(function() {
              elm.reloadSlider();
            }, 100);
          }, true);
        });
      }
    };
  }]);