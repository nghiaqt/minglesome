(function(){
  'use strict';

  angular.module('app.account.states', ['app.states']);
  angular.module('app.account.controllers', []);
  angular.module('app.account.services', []);

  angular.module('app.account', [
    'app.account.states',
    'app.account.controllers',
    'app.account.services'
  ]);
})();
