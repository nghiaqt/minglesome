(function() {
  'use strict';
  angular.module('app.account.states').config([
    'appStatesProvider',
    function(appStatesProvider) {
      appStatesProvider.push([
        {
          name: 'account',
          templateUrl: '/app/account/views/account.html',
          controller: [
            '$scope', '$state',
            function($scope, $state) {
//              $state.go('account.index',{},{reload: true});
            }
          ]
        },
        {
          name: 'account.index',
          url: '/edit-accout',
          templateUrl: '/app/account/views/index.html',
          controller: 'AccountIndexCtrl'
        },
        {
          name: 'account.purchase',
          url: '/purchase',
          templateUrl: '/app/account/views/purchase.html',
          controller: 'AccountPurchaseCtrl',
          resolve: {
            purchase: [
              'Restangular', function(Restangular) {
                return Restangular.one('purchase').get();
              }]
//            creditCard: [
//              'Restangular', function(Restangular) {
//                return Restangular.one('creditcard').get();
//              }]
          }
        },
        {
          name: 'account.test',
          url: '/test',
          templateUrl: '/app/account/views/test.html',
          controller: 'TestCtrl'
        },
        {
          name: 'account.sender',
          url: '/chat-sender',
          templateUrl: '/app/account/views/card.html',
          controller: 'ChatSender'
        },
        {
          name: 'account.receiver',
          url: '/chat-receiver',
          templateUrl: '/app/account/views/card1.html',
          controller: 'ChatReceiver'
        },
        {
          name: 'account.subscription',
          url: '/stop-subscription',
          templateUrl: '/app/account/views/subscription.html',
          controller: 'AccountStopSubscriptionCtrl',
          resolve:{
            reasons:[
              'accountService',function(accountService){
                return accountService.getReason();
              }
            ]
          }
        },
        {
          name: 'account.membership',
          url: '/cancel-membership',
          templateUrl: '/app/account/views/membership.html',
          controller: 'AccountCancelMembershipCtrl',
          resolve:{
            reasons:[
              'accountService',function(accountService){
                return accountService.getReason();
              }
            ]
          }
        }
      ]);
    }
  ]);
})();