(function () {
  'use strict';
  var controllers = angular.module('app.account.controllers', []);
  controllers.controller('AccountIndexCtrl', [
    '$scope', 'authService', '$growl',
    function ($scope, authService, $growl) {
      /** We don't want to keep changes in the model if user changed some fields in profile edit, but not yet saved it.*/
      $scope.user = angular.copy(authService.user);
      $scope.user.newEmail = $scope.user.email;

      /** edit account */
      $scope.editAccount = function () {
        authService.save($scope.user).then(function () {
          /** refesh data */
          $scope.user = angular.copy(authService.user);
          $scope.user.newEmail = $scope.user.email;
          $scope.errors = [];
          /** show success message */
          $growl.box('', 'EDIT_ACCOUNT_SUCCESS', {
            class: 'success'
          }).open();
        }, function (res) {
          /** show errors */
          $scope.errors = res.data;
        });
      };
    }]);
  controllers.controller('AccountPurchaseCtrl', [
    '$scope', '$growl', 'purchase', '$locale', 'accountService', '$window','$sce',
    function ($scope, $growl, purchase, $locale, accountService, $window,$sce) {
      //load data
      $scope.products = purchase.products;
      $scope.currency = purchase.currency;
      $scope.purchase = {};
      if ($scope.products.length > 0) {
        $scope.purchase = $scope.products[0];
      }
      $scope.$watch(function() {
        return $scope.purchase
      },function(value) {
        if (value) {
            $scope.price = $scope.purchase.amount + " " + $scope.currency;
        }
      })
      
      //check invite code & update discount
      $scope.loadDiscount = function () {
        accountService.inviteCode($scope.inviteCode).then(function (products) {
          $scope.products = products;
          if ($scope.products.length > 0) {
            $scope.purchase = $scope.products[0];
          }
        }, function (err) {
          if (err.data)
            $growl.box('', err.data, {
              class: 'danger'
            }).open();
        });
      };

      //add payment & redirect to paypal
      $scope.payment = function () {
        var data = {
          product: $scope.purchase._id,
          inviteCode: $scope.inviteCode
        };
        accountService.payment(data).then(function (res) {
          if (res.isFree) {
            /** show success message */
            $growl.box('', 'SUCCESS_PAYMENT', {
              class: 'success'
            }).open();
          } else {
            $window.location.href = res.url;
          }
        }, function (err) {
          if (err.data)
            $growl.box('', err.data, {
              class: 'danger'
            }).open();
        });
      };
    }]);
  controllers.controller('AccountStopSubscriptionCtrl', [
    '$scope', '$growl', 'authService', 'reasons',
    function ($scope, $growl, authService, reasons) {
      $scope.premium = window.ROLE.premium;
      $scope.reasons = reasons.data;
      $scope.subscription = {
        reasons: [],
        feedback: ''
      };
      $scope.stopSubscription = function () {
        if (authService.user.level == $scope.premium) {
          if ($scope.subscription.reasons.length < 1) {
            $growl.box('', 'ERROR_REQUIRED_REASONS', {
              class: 'danger'
            }).open();
            return;
          }
          authService.stopSubscription($scope.subscription).success(function () {
            $scope.subscription = {
              reasons: [],
              feedback: ''
            };
            /** show success message */
            $growl.box('', 'STOP_SUBSCRIPTION_SUCCESS', {
              class: 'success'
            }).open();
          }).error(function ($errors) {
            $growl.box('', $errors, {
              class: 'danger'
            }).open();
          });
        }
      };
    }]);
  controllers.controller('AccountCancelMembershipCtrl', [
    '$scope', '$growl', 'authService', 'reasons',
    function ($scope, $growl, authService, reasons) {
      $scope.reasons = reasons.data;
      $scope.membership = {
        reasons: [],
        feedback: ''
      };
      $scope.cancelMembership = function () {
        if ($scope.membership.reasons.length < 1) {
          $growl.box('', 'ERROR_REQUIRED_REASONS', {
            class: 'danger'
          }).open();
          return;
        }
        authService.cancelMembership($scope.membership).success(function () {
          $scope.membership = {
            reasons: [],
            feedback: ''
          };
          /** show success message */
          $growl.box('', 'CANCEL_MEMBERSHIP_SUCCESS', {
            class: 'success'
          }).open();
        }).error(function ($errors) {
          $growl.box('', $errors, {
            class: 'danger'
          }).open();
        });
      };
    }]);
})();