(function() {
  var propertyServices = angular.module('app.account.services', []);

  propertyServices.factory('accountService', [
    'Restangular',
    function(Restangular) {
      var _restPurchase = Restangular.all('purchase');
      var _restUtils = Restangular.all('utils');
      var _rest = Restangular.all('account');
      return {
        payment: function(payment) {
          return _restPurchase.customPOST(payment, '').then(function(res) {
            return res;
          });
        },
        getReason: function() {
          return _restUtils.get('reason').then(function(res) {
            return res;
          });
        },
        inviteCode: function(code) {
          return _restPurchase.get('invite-code', {code: code}).then(function(res) {
            return res;
          });
        },
        getCountry: function() {
          return _restUtils.get('country').then(function(res) {
            return res.data;
          });
        }
      };
    }
  ]);
})();