(function () {
  'use strict';
  // Declare app level module which depends on filters, and services
  var app = angular.module('app', [
    'ui.bootstrap',
    'ui.router',
    'restangular',
    'ngCookies',
    'ngSanitize',
    'angular-loading-bar',
    'ngAnimate',
    'ui.growl',
    'pascalprecht.translate',
    'app.services',
    'ap.canvas.ext',
    'checklist-model',
    'app.directives',
    'app.pagination',
    'utils',
    'webcam',
    'pl.multiselect',
    'ui.slimscroll',
    'timer',
    'duScroll',
    'checklist-model',
    'com.2fdevs.videogular',
    'com.2fdevs.videogular.plugins.controls',
    'com.2fdevs.videogular.plugins.overlayplay',
    'com.2fdevs.videogular.plugins.buffering',
    'com.2fdevs.videogular.plugins.poster',
    'youtube-embed',
    //app
    'app.chat',
    'app.auth',
    'app.auth.directives',
    'app.home',
    'app.profile',
    'app.account',
    'app.activity',
    'app.magazine',
    'app.search',
    'app.poll'
  ]);
  app.value('$anchorScroll', angular.noop);
  app.factory('socket', function ($rootScope) {
    if (window.isNotSupportBrowser) {
      var socket = {};
      socket.emit = function () {
      };
      socket.on = function () {
      };
      return socket;
    } else {
      var socket = io.connect(window.CONFIG.nodeurl);
      return socket;
    }
  });
  app.config([
    '$httpProvider', '$stateProvider', '$urlRouterProvider', 'RestangularProvider',
    'appStatesProvider', '$translateProvider',
    function ($httpProvider, $stateProvider, $urlRouterProvider, RestangularProvider,
      appStatesProvider, $translateProvider) {
      'use strict';
      // setup state provider
      $httpProvider.defaults.useXDomain = true;
      $httpProvider.defaults.withCredentials = true;
      delete $httpProvider.defaults.headers.common['X-Requested-With'];
      // add all states that were registered in other modules
      for (var s in appStatesProvider.states) {
        $stateProvider.state(appStatesProvider.states[s]);
      }
      $urlRouterProvider.when('', '/home');
      $urlRouterProvider.otherwise('profile/');

      // setup Restangular
      RestangularProvider.setBaseUrl(window.CONFIG.apiUrl);
      RestangularProvider.setRestangularFields({
        id: '_id'
      });
      //auth interceptor. check session login of user
      $httpProvider.interceptors.push('authInterceptor');
      // register translation table
      $translateProvider.useUrlLoader(window.CONFIG.apiUrl + 'translate');
      $translateProvider.preferredLanguage(window.CONFIG.language);

    }
  ])
    .factory('authInterceptor', function ($q, $location) {
      return {
        // Intercept 401s and redirect you to login
        responseError: function (response) {
          if (response.status === 401) {
            $location.path('logout');
            return $q.reject(response);
          }
          else {
            return $q.reject(response);
          }
        }
      };
    });
  app.run([
    'guard', 'authService', 'profileService', '$rootScope', '$state', '$stateParams', 'socket',
    '$growl', '$window', '$document', '$urlRouter', 'apBrowserHelper',
    function (guard, authService, profileService, $rootScope, $state, $stateParams, socket,
      $growl, $window, $document, $urlRouter, apBrowserHelper) {
      'use strict';
      //show flash from yii
      if (window.flash) {
        angular.forEach(window.flash, function (value, key) {
          $growl.box('', value, {
            class: key,
            sticky: true
          }).open();
        });
      }

      $rootScope.notCloseAllMenuTab = false;

      /**
       * set rootscope data
       */
      $rootScope.authService = authService;
      $rootScope.$state = $state;
      $rootScope.$stateParams = $stateParams;
      guard.watch();

      /**
       * auto scroll top when page load
       */
      $rootScope.$on('$viewContentLoaded', function () {
        var state = $state.$current;
        if (state.scrollTo == undefined)
          $window.scrollTo(0, 0);
        else {
          var to = 0;
          if (state.scrollTo.id != undefined)
            to = $(state.scrollTo.id).offset().top;
          if ($($window).scrollTop() == to)
            return;
          if (state.scrollTo.animated)
            $(document.body).animate({scrollTop: to});
          else
            $window.scrollTo(0, to);
        }
      });

      /**
       * check data when state start
       */
      $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        if (toState.name === 'profile.view') {
          if ($rootScope.stateChangeBypass) {
            $rootScope.stateChangeBypass = false;
            return;
          }
          event.preventDefault();
          profileService.checkPermissionView(toParams.id).then(function (res) {
            $rootScope.stateChangeBypass = true;
            $state.go(toState, toParams);
          }, function (error) {
            $growl.box('', error.data, {
              class: 'danger'
            }).open();
            if (fromState.name === '') {
              $rootScope.$state.transitionTo('profile.edit');
            }
          });
        } else if (toState.name === 'search.index' && toParams.type == 'lounge') {
          //check public camera
          if (!$rootScope.isPublicVideo) {
            event.preventDefault();
            // do stuff
            $growl.box('', 'LOUNGE_PAGE_NOTIFY', {
              class: 'danger',
              timeout: 10000
            }).open();
            $rootScope.$state.transitionTo('profile.edit');
          }
        }

        if (authService.isAuthenticated() && fromState.name !== '') {
          authService.logActivity();
        }
      });

      /**
       * detected browser client
       */
      var w = angular.element($window);
      $rootScope.detectedBrowser = {
        width: w.width(),
        height: w.height(),
        platform: apBrowserHelper.platform,
        browser: apBrowserHelper.browser
      };
      w.bind('resize', function () {
        $rootScope.detectedBrowser.width = w.width();
        $rootScope.detectedBrowser.height = w.height();
        $rootScope.$apply();
      });

      //define default height for action search, filter
      $rootScope.slimScrollActionResize = {pc: 70, mobile: 60};

      $rootScope.promptAccessProfile = function () {
        $growl.box('', 'NOTIFY_PRIVACY_FILTER_USER', {
          class: 'danger'
        }).open();
      };

      if (!webrtcDetectedBrowser) {
        $growl.box('', 'NOTIFY_BROWSER_DOES_NOT_SUPPORT_WEBRTC', {
          class: 'info',
          sticky: true
        }).open();
      }
    }
  ]);
})();