angular.module('app.profile.directives', [])
  .directive('menuProfileViewer', [
	'$rootScope', 'profileService', 'authService', '$stateParams',
	function ($rootScope, profileService, authService, $stateParams) {
	  return {
		restrict: 'AE',
		scope: {},
		templateUrl: 'app/profile/views/menu-profile-viewer.html',
		link: function (scope, element, attrs) {
		  //check user is a key
		  profileService.key($stateParams.id, 'isKey')
			.then(function (res) {
			  if (res==='true') {
				scope.keyName = 'REVOKE_KEY';
			  } else {
				scope.keyName = 'KEY';
			  }
			});
		  //check user is blocked
		  profileService.isBlocked(authService.user._id, $stateParams.id)
			.then(function (res) {
			  scope.isBlocked = res.data;
			});
		},
		controller: [
		  '$scope', '$growl', '$document', '$modal',
		  function ($scope, $growl, $document, $modal) {
			// key/revoke user
			$scope.keyUser = function () {
			  if ($scope.isKey) {
				action.keyUser();
			  } else {
				$scope.openModal('key');
			  }
			};

			//block user when user click on block
			$scope.blockUser = function () {
			  if (authService.user.level < window.ROLE.premium) {
				//show error message
				$growl.box('', 'ERROR_402', {
				  class: 'danger'
				}).open();
				return false;
			  }
			  if($scope.isBlocked){
				action.blockUser();
			  }else{
				$scope.openModal('block');
			  }
			};

			//report user
			$scope.reportUser = function () {
			  if (authService.user.level < window.ROLE.premium) {
				//show error message
				$growl.box('', 'ERROR_402', {
				  class: 'danger'
				}).open();
				return false;
			  }
			  $scope.openModal('report');
			};

			//action modal
			var action = {
			  keyUser: function () {
				var mode = 'key';
				if ($scope.isKey) {
				  mode = 'revoke';
				}
				profileService.key($stateParams.id, mode).then(function (res) {
				  if (res) {
					$scope.isKey = !$scope.isKey;
					if ($scope.isKey) {
					  $scope.keyName = 'REVOKE_KEY';
					} else {
					  $scope.keyName = 'KEY';
					}
				  }
				});
			  },
			  blockUser: function () {
				if (!$scope.isBlocked) {
				  profileService.blockUser(authService.user._id, $stateParams.id).then(function (res) {
					$scope.isBlocked = true;
				  });
				} else {
				  profileService.unblockUser(authService.user._id, $stateParams.id).then(function (res) {
					$scope.isBlocked = false;
				  });
				}
			  },
			  reportUser: function () {
				profileService.reportUser({
				  reportedUser: $stateParams.id,
				  message: 'View profile'
				});
			  }
			};

			//open model confirm action
			$scope.openModal = function (mode) {
			  $modal.open({
				keyboard: false,
				size: 'sm',
				backdrop: 'static',
				templateUrl: '/app/profile/views/viewer-action-modal.html',
				controller: [
				  '$scope', '$modalInstance',
				  function ($scope, $modalInstance) {
					$scope.mode = mode;
					$scope.accept = function () {
					  if (mode === 'key') {
						action.keyUser();
					  } else if (mode === 'block') {
						action.blockUser();
					  } else if (mode === 'report') {
						action.reportUser();
					  }
					  $modalInstance.dismiss('cancel');
					};
					$scope.cancel = function () {
					  $modalInstance.dismiss('cancel');
					};
				  }
				]
			  });
			}
		  }]
	  };
	}])
  .directive('dateComment', ['$rootScope', 'authService', 'dateService', '$growl', 'socket',
	function ($rootScope, authService, dateService, $growl, socket) {
	  return {
		restrict: 'A',
		scope: {
		  date: '=date'
		},
		controller: ['$scope', function ($scope) {
			$scope.user = authService.user;
			$scope.reply = {
			  createdAt: new Date(),
			  replierName: authService.user.username
			};
			$scope.replyContent = '';

			/** post a reply to date */
			$scope.replyDate = function () {
			  if ($scope.replyContent) {
				/** check permission before post reply */
				if (authService.user.level < window.ROLE.trial) {
				  //show error message
				  $growl.box('', 'ERROR_402', {
					class: 'danger'
				  }).open();
				} else {
				  socket.emit('replyEvent', $scope.date._id, $scope.replyContent);
				  $scope.date.replies.push({
					createdAt: new Date(),
					replierName: authService.user.username,
					text: $scope.replyContent
				  });
				  $scope.replyContent = '';
				}
			  }
			};

			/** receive new reply event */
			socket.on('onReplyEvent', function (reply, replierName) {
			  if (reply.dateEventId == $scope.date._id) {
				reply.replierName = replierName;
				$scope.date.replies.push(reply);
				$scope.$apply();
			  }
			});
			
			//destroy $on
			$scope.$on('$destroy', function () {
			  socket.removeListener('onReplyEvent');
			});
		  }],
		templateUrl: 'app/profile/views/date-comment.html'
	  };
	}]);