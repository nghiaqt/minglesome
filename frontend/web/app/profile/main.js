(function(){
  'use strict';

  angular.module('app.profile.states', ['app.states']);
  angular.module('app.profile.controllers', []);
  angular.module('app.profile.services', []);
	angular.module('app.profile.directives', []);

  angular.module('app.profile', [
    'app.profile.states',
    'app.profile.controllers',
    'app.profile.services',
		'app.profile.directives'
  ]);
})();
