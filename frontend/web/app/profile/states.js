(function() {
  'use strict';
  angular.module('app.profile.states').config([
    'appStatesProvider',
    function(appStatesProvider) {
      appStatesProvider.push([
        {
          name: 'profile',
          templateUrl: '/app/profile/views/index.html'
        },
        {
          name: 'profile.edit',
          url: '/profile/:eventId',
          templateUrl: '/app/profile/views/edit.html',
          controller: 'ProfileEditCtrl',
          resolve: {
            dropdown: [
              'Restangular', function(Restangular) {
                return Restangular.one('profile/get-dropdown-data').get();
              }],
            state:[
              'profileService', 'authService', function(profileService, authService) {
                return profileService.getStates({
                    countryCode: authService.user.country
                  }).then(function(res){
                    return res.data;
                  });
              }],
            city:[
              'profileService', 'authService', function(profileService, authService) {
                return profileService.getCities({
                    countryCode: authService.user.country,
                    stateId: authService.user.state
                  }).then(function(res){
                    return res.data;
                  });
              }],
            gallery: [
              'profileService', 'authService', function(profileService, authService) {
                return profileService.getGallery(authService.user._id);
              }],
            dates: [
              'dateService', 'authService', function(dateService, authService) {
                return dateService.get(authService.user._id);
              }]
          }
        },
        {
          name: 'profile.view',
          url: '/view-profile/:id',
          templateUrl: '/app/profile/views/view.html',
          controller: 'ProfileViewCtrl',
          resolve: {
            profile: [
              '$stateParams', 'profileService', function($stateParams, profileService) {
                return profileService.get($stateParams.id);
              }
            ],
            conversation: [
              '$stateParams', 'authService', 'chatService',
              function($stateParams, authService, chatService) {
                return chatService.private({recipients: [$stateParams.id, authService.user._id]});
              }
            ],
            gallery: [
              'profileService', 'authService', '$stateParams', function(profileService, authServicem, $stateParams) {
                return profileService.getGallery($stateParams.id);
              }],
            dates: [
              'dateService', 'authService', '$stateParams', function(dateService, authService, $stateParams) {
                return dateService.get($stateParams.id);
              }],
            /** check if user is interested */
            isInterested: [
              'profileService', '$stateParams', 'authService', function(profileService, $stateParams, authService) {
                return profileService.isInterested(authService.user._id, $stateParams.id).then(function(res) {
                  return res;
                });
              }
            ]
          }
        }
      ]);
    }
  ]);
})();