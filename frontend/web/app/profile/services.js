(function() {
  var propertyServices = angular.module('app.profile.services', []);
  propertyServices.factory('profileService', [
    'Restangular',
    function(Restangular) {
      var _restGallery = Restangular.all('galleries');
      var _rest = Restangular.all('profile');
      var _restUser = Restangular.all('users');
      var _restCountry = Restangular.all('country');
      return {
        /**
         * get user profile
         * @param {type} userId
         * @returns {object} Data profile
         */
        get: function(userId) {
          return _rest.get(userId).then(function(res) {
            return res;
          });
        },
        /**
         * get data turn server
         * @returns array
         */
        turn: function() {
          return _rest.one('turn').get();
        },
        /**
         * get all image in gallery of user
         * @param {type} userId
         * @returns {array} List image
         */
        getGallery: function(userId) {
          return _restGallery.get(userId).then(function(res) {
            return res;
          });
        },
        /**
         * add new picture to gallery of user
         * @param {type} picture
         * @returns {object} Picture data
         */
        savePicture: function(picture) {
          return _restGallery.customPOST(picture, '').then(function(res) {
            return res;
          });
        },
        /**
         * set image is avatar of user. update image to user profile
         * @param {type} picture
         * @returns {boolean} True if success, errors if not save
         */
        setAvatar: function(picture) {
          return _restGallery.customPUT(picture, '').then(function(res) {
            return res;
          });
        },
        /**
         * delete picture
         * @param {type} picture
         * @returns {boolean} True if success, errors if not save
         */
        deletePicture: function(picture) {
          return _restGallery.customDELETE(picture._id).then(function(res) {
            return res;
          });
        },
        /**
         * Check user is a key user
         * Set user is key user
         * Revoke key user
         * @param {string} userId
         * @param {string} mode Key mode (isKey, key, revoke)
         * @returns boolean
         */
        key: function(userId, mode) {
          return _restUser.one('key').customPOST({userId: userId, mode: mode});
        },
        /**
         * Block user
         * @param {string} blocker is user id
         * @param {string} blockedUser is user id
         * @returns {string}
         */
        blockUser: function(blocker, blockedUser) {
          return _restUser.one(blocker).one('block', blockedUser).customPOST({blocker: blocker, blockedUser: blockedUser}, '');
        },
        /**
         * Block user
         * @param {string} blocker is user id
         * @param {string} blockedUser is user id
         * @returns {string}
         */
        unblockUser: function(blocker, blockedUser) {
          return _restUser.one(blocker).one('unblock', blockedUser).customPOST({blocker: blocker, blockedUser: blockedUser}, '');
        },
        /**
         * check if user is blocked
         * @param {string} blocker is user id
         * @param {string} blockedUser is user id
         * @returns {string}
         */
        isBlocked: function(blocker, blockedUser) {
          return _restUser.one(blocker).one('isblocked', blockedUser).customPOST({blocker: blocker, blockedUser: blockedUser}, '');
        },
        /**
         * interest
         * @param {string} createdBy is user id
         * @param {string} interestedUser is user id
         */
        interest: function(createdBy, interestedUser) {
          return _restUser.one(createdBy).one('interest', interestedUser).customPOST({createdBy: createdBy, interestedUser: interestedUser}, '');
        },
        /**
         * uninterest
         * @param {string} createdBy is user id
         * @param {string} interestedUser is user id
         */
        uninterest: function(createdBy, interestedUser) {
          return _restUser.one(createdBy).one('uninterest', interestedUser).customPOST({createdBy: createdBy, interestedUser: interestedUser}, '');
        },
        /**
         * check if user is interested
         * @param {string} createdBy is user id
         * @param {string} interestedUser is user id
         */
        isInterested: function(createdBy, interestedUser) {
          return _restUser.one(createdBy).one('isinterested', interestedUser).customPOST({createdBy: createdBy, interestedUser: interestedUser}, '');
        },
        /**
         * get all mutualinterests
         * @param {string} userId
         */
        getInterests: function(userId) {
          return _restUser.one(userId).one('activity').get();
        },
        /**
         * set read interest
         * @param {type} id
         */
        readInterest: function(id) {
          return _restUser.one(id).one('read-activity').get();
        },
        /**
         * report user to admin
         * @param {type} report
         */
        reportUser: function(report) {
          return _restUser.one('report').customPOST(report);
        },
        /**
         * check permission of user when click view an profile
         * @param {type} userId
         * @returns object
         */
        checkPermissionView: function(userId) {
          return _restUser.one(userId).one('check-permission-view').get();
        },
        /**
         * Get all profile data options
         * @returns array
         */
        getDropdownData: function() {
          return _rest.one('get-dropdown-data').get();
        },
        getStates: function(params) {
          return _restCountry.one('get-states').customPOST(params);
        },
        getCities: function(params) {
          return _restCountry.one('get-cities').customPOST(params);
        }
      };
    }
  ]);
  propertyServices.factory('dateService', ['Restangular',
    function(Restangular) {
      var _rest = Restangular.all('dates');
      return {
        /**
         * Get dates
         * @param {type} userId
         * @returns {array}
         */
        get: function(userId) {
          return _rest.get(userId).then(function(res) {
            return res;
          });
        },
        create: function(date) {
          return _rest.customPOST(date, '').then(function(res) {
            return res;
          });
        },
		delete:function(date){
		  return _rest.one(date._id).customDELETE();
		},
        getReplies: function(dateId) {
          return _rest.one(dateId).one('date-replies').get();
        },
        getReply: function(replyId) {
          return _rest.one(replyId).one('date-reply').get();
        },
        createReply: function(createdBy, dateEventId, text) {
          return _rest.one(dateEventId).one('user', createdBy).one('reply').customPOST({createdBy: createdBy, dateEventId: dateEventId, text: text});
        },
        readReply: function(replyId) {
          return _rest.one(replyId).one('read-reply').get();
        },
        delReply: function(replyId) {
          return _rest.one(replyId).customDELETE('date-reply').then(function(res) {
            return res;
          });
        },
        dateEvents: function(params) {
          return _rest.get('activity', params).then(function(res) {
            return res;
          });
        }
      };
    }
  ]);
  propertyServices.factory('visitorService', ['Restangular',
    function(Restangular) {
      var _rest = Restangular.all('visitor');
      return {
        /**
         * Get dates
         * @param {type} userId
         * @returns {array}
         */
        get: function(params) {
          return _rest.get('activity', params).then(function(res) {
            return res;
          });
        },
        view: function(id) {
          return _rest.get(id).then(function(res) {
            return res;
          });
        }
      };
    }
  ]);
})();