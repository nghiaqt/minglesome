(function () {
  'use strict';
  var controllers = angular.module('app.profile.controllers', []);
  controllers.controller('ProfileEditCtrl', [
    '$rootScope', '$scope', 'authService', 'profileService', 'dateService', 'apImageHelper',
    '$growl', 'dropdown', 'state', 'city', 'dates', 'gallery', '$timeout', '$window',
    '$document', '$stateParams', 'modalPrompt','socket',
    function ($rootScope, $scope, authService, profileService, dateService, apImageHelper,
      $growl, dropdown, state, city, dates, gallery, $timeout, $window,
      $document, $stateParams, modalPrompt, socket) {
        
      //open modal prompt info page
      modalPrompt.info('PROMPT_PROFILE');
      
      //detroy angular watch, $on, socket...
      var off = [];
      $scope.$on('$destroy', function () {
        off.forEach(function (unbind) {
          unbind();
        });
        off = [];
        socket.removeListener('updateAvatar');
      });
      /** We don't want to keep changes in the model if user changed some fields in profile edit, but not yet saved it.*/
      $scope.user = angular.copy(authService.user);

      /** Change Format of height 
       * Restrict country: 'US' & 'CA'*/
      var currentCountry = window.CONFIG._country;
      var convertHeight = function() {
        if (currentCountry === 'us' || currentCountry === 'ca') {
          if ($scope.user.height) {
            var height = $scope.user.height.toString();
            height = (Math.floor(parseFloat(height)*10))/10;
            height = height.toString().replace(/\./g,"'") + "\"";
            $scope.user.height = height;
          }
        }
      };
      convertHeight();

      $scope.isToday = function (date) {
        var d1 = new Date();
        var d2 = new Date(date);
        return (d1.toDateString() === d2.toDateString());
      };
      /** load dropdown data for edit profile */
      $scope.dropdown = dropdown;
      $scope.dropdown.state = state;
      $scope.dropdown.city = city;
      $scope.loadState = function () {
        if ($scope.user.country) {
          $scope.dateEvent.country = $scope.user.country;
          profileService.getStates({
            countryCode: $scope.user.country
          }).then(function (res) {
            $scope.dropdown.state = res.data;
          });
        }
      };
      $scope.loadCity = function () {
        if ($scope.user.state) {
          profileService.getCities({
            countryCode: $scope.user.country,
            stateId: $scope.user.state
          }).then(function (res) {
            $scope.dropdown.city = res.data;
          });
        }
      };
      $scope.changeCity = function () {
        $scope.dateEvent.city = $scope.user.city;
      };

      /** 
       * get dates event
       * Delete date event
       */
      $scope.dates = dates || [];
      $scope.deleteDateEvent = function ($index) {
        if ($scope.dates[$index]) {
          dateService.delete($scope.dates[$index]).then(function (res) {
            $rootScope.$emit('onDeleteDateEvent', $scope.dates[$index]);
            $scope.dates.splice($index, 1);
          });
        }
      };

      /** 
       * gallery slideshow
       * Load data Gallery slider 1 item & slider 9 item
       */
      $scope.slides = gallery || [];
      $scope.slides.push({control: true});
      $scope.listSlides = [];
      $scope.isShowList = false;
      $scope.showGallery = true;

      $scope.currentIndex = 0;
      $scope.direction = 'left';

      /** convert gallery only item to gallery list item */
      var converListGallery = function (galleries) {
        var imageArray = [];
        var tmp = [];
        var j = 0;
        for (var i = 0; i < galleries.length; i++) {
          if (galleries[i].url) {
            tmp.push(galleries[i]);
          }
          if (tmp.length !== Number(9)) {
            if (i == (galleries.length - 1)) {
              imageArray.push(tmp);
            }
          } else {
            imageArray.push(tmp);
            tmp = [];
            j = 0;
            if (i == (galleries.length - 1)) {
              imageArray.push(tmp);
            }
          }
        }
        return imageArray;
      };

      /** change list item or only item */
      $scope.showList = function () {
        $scope.isShowList = !$scope.isShowList;
        $scope.currentIndex = 0;
        if ($scope.isShowList) {
          $scope.listSlides = converListGallery($scope.slides);
        }
      };

      /** open image in large view */
      $scope.openImage = function (currentIndex) {
        $scope.isShowList = false;
        $scope.currentIndex = currentIndex;
      };

      $scope.setCurrentSlideIndex = function (index) {
        $scope.direction = (index > $scope.currentIndex) ? 'right' : 'left';
        $scope.currentIndex = index;
      };

      $scope.isCurrentSlideIndex = function (index) {
        return $scope.currentIndex === index;
      };

      $scope.prevSlide = function () {
        $scope.direction = 'right';
        if ($scope.isShowList) {
          $scope.currentIndex = ($scope.currentIndex > 0) ? --$scope.currentIndex : $scope.listSlides.length - 1;
        } else {
          $scope.currentIndex = ($scope.currentIndex > 0) ? --$scope.currentIndex : $scope.slides.length - 1;
        }
      };

      $scope.nextSlide = function () {
        $scope.direction = 'left';
        if ($scope.isShowList) {
          $scope.currentIndex = ($scope.currentIndex < $scope.listSlides.length - 1) ? ++$scope.currentIndex : 0;
        } else {
          $scope.currentIndex = ($scope.currentIndex < $scope.slides.length - 1) ? ++$scope.currentIndex : 0;
        }
      };

      /**
       * set default value for add picture
       * resize gallery & camera
       */
      $scope.showWebcam = false;
      $scope.showImage = false;

      $scope.imageSize = {width: 390, height: 520};
      $scope.resizeGallery = function () {
        var picture = angular.element('.picture');
        if (picture.width() > 0) {
          $scope.imageSize.width = picture.width();
          $scope.imageSize.height = ($scope.imageSize.width * 4) / 3;
        }
        off.push($rootScope.$watchCollection('detectedBrowser', function () {
          if ($scope.imageSize.width != picture.width()) {
            $scope.imageSize.width = picture.width();
            $scope.imageSize.height = (picture.width() * 4) / 3;
          }
        }));
      };

      $scope.loadCamera = function () {
        $scope.showWebcam = true;
      };

      /**
       * camera snapshot
       */
      var _video = null;
      $scope.patOpts = {x: 0, y: 0, w: 25, h: 25};

      /** error load webrtc */
      $scope.onError = function (err) {
        $scope.showWebcam = false;
        $growl.box('', 'CAMERA_ERROR', {
          class: 'danger'
        }).open();
      };

      /** success load webrtc */
      $scope.onSuccess = function (videoElem) {
        $scope.showGallery = false;
        // The video element contains the captured camera data
        _video = videoElem;

        $scope.patOpts.w = videoElem.width;
        $scope.patOpts.h = videoElem.height;
        $scope.$apply();
      };

      $scope.canvas = {
        src: null,
        image: null,
        frame: null,
        scale: null,
        offset: null
      };

      /**
       * Make a snapshot of the camera data and show it in another canvas.
       */
      var makeSnapshot = function () {
        //hide webcam & show snapshot
        $scope.showWebcam = false;
        $scope.showImage = true;

        //get canvas
        var patCanvas = document.querySelector('#snapshot');
        if (!patCanvas)
          return;

        if ($scope.patOpts.w > patCanvas.width) {
          $scope.patOpts.x = ($scope.patOpts.w - patCanvas.width) / 2;
        }

        //add image to canvas
        var ctxPat = patCanvas.getContext('2d');
        var idata = getVideoData($scope.patOpts.x, $scope.patOpts.y, $scope.patOpts.w,
          $scope.patOpts.h);
        ctxPat.putImageData(idata, 0, 0);

        /** empty zoomed & image & set new image*/
        $scope.canvas.image = null;
        $scope.canvas.scale = null;
        $scope.canvas.src = patCanvas.toDataURL();
      };
      /** countdown 3s before snapshot */
      $scope.countdown = 3;
      $scope.showCountdown = false;
      $scope.makeSnapshot = function () {
        $scope.showCountdown = true;
        $timeout(function () {
          if ($scope.countdown == 1) {
            $scope.countdown = 3;
            $scope.showCountdown = false;
            makeSnapshot();
          } else {
            $scope.countdown--;
            $scope.makeSnapshot();
          }
        }, 1000);
      };

      /**
       * draw image snapshot
       */
      var getVideoData = function (x, y, w, h) {
        var hiddenCanvas = document.createElement('canvas');
        hiddenCanvas.width = w;
        hiddenCanvas.height = h;
        var ctx = hiddenCanvas.getContext('2d');
        ctx.drawImage(_video, 0, 0, w, h);
        return ctx.getImageData(x, y, w, h);
      };

      /**
       * Upload picture
       */
      $scope.uploadImage = function (element) {
        /** empty zoomed */
        $scope.canvas.scale = null;
        /** hidde gallery */
        $scope.showGallery = false;
        /** show image upload */
        $scope.showImage = true;
        /** load image to canvas */
        var file = element.files[0];
        if (file) {
          var updateImageSrc = function (src) {
            $scope.canvas.image = null;
            $scope.canvas.src = src;
            $scope.$apply();
          };
          apImageHelper.fileToImageDataURI(file, '', '', updateImageSrc);
        }
        $scope.$apply();
      };

      /**
       * canvas image. zoom in, zoom out
       */
      $scope.zoomIn = function () {
        $scope.canvas.scale *= 1.2;
      };

      $scope.zoomOut = function () {
        $scope.canvas.scale /= 1.2;
      };

      /**
       * add new image to gallery
       */
      $scope.saveImage = function () {
        var canvasData = apImageHelper.cropImage($scope.canvas.image, $scope.canvas.frame, {width: 1500, height: 1500}, 'image/png');
        profileService.savePicture({picture: canvasData.dataURI})
          .then(function (res) {
            /** show success message */
//          $growl.box('ADD_PICTURE', 'ADD_PICTURE_SUCCESS', {
//            class: 'success'
//          }).open();
            if(res.isAvatar){
              socket.emit('updateAvatar', res.url);
            }
            /** add item to gallery */
            $scope.slides.unshift(res);

            /** show gallery */
            $scope.showGallery = true;
            $scope.currentIndex = 0;

            /** remove picture data */
            $scope.showImage = false;
            $scope.canvas.image = null;
            $scope.canvas.src = null;
          }, function (res) {
            var err = res.data ? res.data : 'ERROR_SAVE_IMAGE';
            /** show error message */
            $growl.box('', err, {
              class: 'danger'
            }).open();
          });
      };

      /** cancel upload image. move to slide upload */
      $scope.cancelImage = function () {
        $scope.showImage = false;
        $scope.showGallery = true;
        $scope.currentIndex = $scope.slides.length - 1;
        $scope.canvas.image = null;
        $scope.canvas.src = null;
      };

      /**
       * delete image
       * @param {type} image
       */
      $scope.deletePicture = function () {
        var picture = $scope.slides[$scope.currentIndex];
        if (picture) {
          //TODO - remove real image
          profileService.deletePicture(picture).then(function (res) {
            /** show success message */
//            $growl.box('Delete Picture', 'DELETED_SUCCESSFULLY', {
//              class: 'success'
//            }).open();
            var gal = _.defaults($scope.slides);
            gal.splice($scope.currentIndex, 1);
            /** is currently selected */
            $scope.slides = gal;
            $scope.currentIndex = $scope.currentIndex - 1 > 0 ? $scope.currentIndex - 1 : 0;
          }, function (res) {
            /** show error message */
            $growl.box('', res.data, {
              class: 'danger'
            }).open();
          });
        }
      };


      /**
       * Change default avatar
       * @param {type} image
       */
      $scope.setProfileAvatar = function () {
        var picture = $scope.slides[$scope.currentIndex];
        if (picture) {
          profileService.setAvatar(picture).then(function () {
            /** show success message */
           $growl.box('SET_PICTURE', 'SET_PICTURE_SUCCESS', {
             class: 'success'
           }).open();
            socket.emit('updateAvatar', picture.url);
          }, function (res) {
            /** show errors message */
            $growl.box('', res.data, {
              class: 'danger'
            }).open();
          });
        }
      };

      /** Default data for add new event, set default text for activity */
      var formatDateEventForm = function () {
        $scope.dateEvent = {
          isGroup: false,
          description: '',
          onDate: new Date(),
          country: $scope.user.country,
          city: $scope.user.city
        };
      };
      formatDateEventForm();

      /** format datepicker */
      $scope.openDateEvent = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.formatDatePicker.opened = true;
      };

      $scope.formatDatePicker = {
        format: 'yyyy-MM-dd',
        minDate: new Date(),
        maxDate: moment().month(moment().month() + 1).format("YYYY-MM-DD"),
        opened: false
      };

      //add a keyword field
      $scope.addKeyword = function () {
        $scope.user.keywords.push('');
      };
      /**
       * update profile data
       * add date/event
       */
      $scope.save = function () {
        //save profile
        authService.updateProfile($scope.user)
          .then(function () {
            //show success message
            convertHeight()
          $growl.box('', 'PROFILE_SAVE_SUCCESS', {
            class: 'success'
          }).open();
          }, function (res) {
            //show error message
            $growl.box('', res.data, {
              class: 'danger'
            }).open();
          });

        //add date/event
        if ($scope.dateEvent.description.length > 0) {
          //remove text default
          if ($scope.dateEvent.activityEventId === 'activity') {
            $scope.dateEvent.activityEventId = '';
          }
          dateService.create($scope.dateEvent).then(function (res) {
            $scope.dates.push(res);
            formatDateEventForm();
            $scope.$broadcast('dateEvent:new',null);
            //show error message
//            $growl.box('DATE_EVENT', 'DATE_EVENT_SUCCESS', {
//              class: 'success'
//            }).open();
          }, function (res) {
            //show error message
            $growl.box('', res.data, {
              class: 'danger'
            }).open();
          });
        }
      };

      /**
       * scroll to event reply
       */
      var attemp = 0;
      $scope.eventScrollTo = function () {
        if ($stateParams.eventId) {
          var event = document.getElementById($stateParams.eventId);
          if (event) {
            $timeout(function () {
              //scroll to event
              $document.scrollToElement(event, 20, 2000);
            }, 1000);
          } else {
            if (attemp < 5) {
              $timeout(function () {
                $scope.eventScrollTo();
              }, 1000);
              attemp++;
            }
          }
        }
      };

    }]);

  controllers.controller('ProfileViewCtrl', [
    '$rootScope', '$scope', 'socket', 'authService', 'profileService', 'chatService',
    '$growl', 'profile', 'conversation', 'dates', 'gallery', '$stateParams', '$timeout',
    'isInterested', '$window', '$state', '$document', 'apBrowserHelper','modalPrompt',
    function ($rootScope, $scope, socket, authService, profileService, chatService,
      $growl, profile, conversation, dates, gallery, $stateParams, $timeout,
      isInterested, $window, $state, $document, apBrowserHelper, modalPrompt) {

      //open modal prompt info page
      modalPrompt.info('PROMPT_VIEW_PROFILE');
      
      //detroy angular watch, $on & socket listener...
      var off = [];
      //destroy $on & socket listener
      $scope.$on('$destroy', function () {
        off.forEach(function (unbind) {
          unbind();
        });
        off = [];
        socket.removeListener('receiveMessage');
        socket.removeListener('notifyTypingMessageCallback');
        socket.removeListener('onSharePrivate');
      });

      $scope.slimScrollOptions = {
        height: '470px',
        disableFadeOut: true
      };

      $scope.interesting = isInterested == 1 ? true : false;

      //load data of viewing user				
      $scope.profile = profile;

      /** load data of logged user */
      $scope.user = angular.copy(authService.user);

      /** 
       * gallery slideshow
       */
      $scope.slides = gallery || [];
      $scope.listSlides = [];
      $scope.isShowList = false;
      $scope.showGallery = true;

      $scope.currentIndex = 0;
      $scope.direction = 'left';

      /** convert gallery only item to gallery list item */
      var converListGallery = function (galleries) {
        var imageArray = [];
        var tmp = [];
        var j = 0;
        for (var i = 0; i < galleries.length; i++) {
          if (galleries[i].url) {
            tmp.push(galleries[i]);
          }
          if (tmp.length !== Number(9)) {
            if (i == (galleries.length - 1)) {
              imageArray.push(tmp);
            }
          } else {
            imageArray.push(tmp);
            tmp = [];
            j = 0;
            if (i == (galleries.length - 1)) {
              imageArray.push(tmp);
            }
          }
        }
        return imageArray;
      };

      /** change list item or only item */
      $scope.showList = function () {
        $scope.isShowList = !$scope.isShowList;
        $scope.currentIndex = 0;
        if ($scope.isShowList) {
          $scope.listSlides = converListGallery($scope.slides);
        }
      };

      /** open image in large view */
      $scope.openImage = function (currentIndex) {
        $scope.isShowList = false;
        $scope.currentIndex = currentIndex;
      };

      $scope.setCurrentSlideIndex = function (index) {
        $scope.direction = (index > $scope.currentIndex) ? 'right' : 'left';
        $scope.currentIndex = index;
      };

      $scope.isCurrentSlideIndex = function (index) {
        return $scope.currentIndex === index;
      };

      $scope.prevSlide = function () {
        $scope.direction = 'right';
        if ($scope.isShowList) {
          $scope.currentIndex = ($scope.currentIndex > 0) ? --$scope.currentIndex : $scope.listSlides.length - 1;
        } else {
          $scope.currentIndex = ($scope.currentIndex > 0) ? --$scope.currentIndex : $scope.slides.length - 1;
        }
      };

      $scope.nextSlide = function () {
        $scope.direction = 'left';
        if ($scope.isShowList) {
          $scope.currentIndex = ($scope.currentIndex < $scope.listSlides.length - 1) ? ++$scope.currentIndex : 0;
        } else {
          $scope.currentIndex = ($scope.currentIndex < $scope.slides.length - 1) ? ++$scope.currentIndex : 0;
        }
      };

      /**
       * scroll to gallery if in mobile
       */
      var attemp = 0;
      $scope.eventScrollTo = function () {
        var event = angular.element('.upload-profile');
        if (event) {
          $timeout(function () {
            //scroll to event
            $document.scrollToElement(event, 20, 1000);
          }, 1000);
        } else {
          if (attemp < 5) {
            $timeout(function () {
              $scope.eventScrollTo();
            }, 1000);
            attemp++;
          }
        }
      };

      /**
       * resize gallery
       */
      $scope.imageSize = {width: 390, height: 520};
      $scope.resizeGallery = function () {
        var picture = angular.element('.picture');
        if (picture.width() > 0) {
          //check if browser in mobile
          if ($scope.imageSize.width != picture.width()) {
            //resize gallery
            $scope.imageSize.width = picture.width();
            $scope.imageSize.height = ($scope.imageSize.width * 4) / 3;

            //resize height of box chat
            $scope.slimScrollOptions.height = $scope.imageSize.height - 50;
            //scroll to gallery
            $scope.eventScrollTo();
          }
        }
        //check resize browser
        off.push($rootScope.$watchCollection('detectedBrowser', function () {
          if ($scope.imageSize.width != picture.width()) {
            $scope.imageSize.width = picture.width();
            $scope.imageSize.height = ($scope.imageSize.width * 4) / 3;
            $scope.slimScrollOptions.height = $scope.imageSize.height - 50;
          }
        }));
      };

      /** interest user*/
      $scope.makeInterest = function () {
        if (!$scope.interesting) {
          //interested
          profileService.interest(authService.user._id, $stateParams.id).then(function (res) {
            //if the interested user has mutual interest with the current user
            //add him to list of mutual friend of current user												
            if (res.hasMutalInterest) {
              $rootScope.$broadcast('mutualInterest', {interest: res.interest, isMutualInterest: true});
              socket.emit('setInterest', $stateParams.id, res.interesting, true);
            }
          });
        } else {
          //uninterested
          profileService.uninterest(authService.user._id, $stateParams.id).then(function (res) {
            if (res.hasMutual) {
              $rootScope.$broadcast('mutualInterest', {interest: res.interest, isMutualInterest: false});
              socket.emit('setInterest', $stateParams.id, res.interesting, false);
            }
          });
        }
        //send to socket to remove this user from mutual interests
      };
      /** set default interesting */
      if (parseInt(isInterested) === 2) {
        $scope.makeInterest();
        $scope.interesting = true;
      }

      /**
       * update visitor
       */
      socket.emit('visitor', authService.user._id, $scope.profile._id);

      /** 
       * load chat 
       */
      $scope.conversation = conversation;
      if ($scope.conversation._id) {
        if ($scope.conversation.unread) {
          $rootScope.$emit('updateReadedConversation', $scope.conversation._id);
        }
        /** connect chat */
        socket.emit('joinChat', $scope.conversation._id);

        /** get chat data*/
        $scope.messages = [];
        $scope.loadMessage = function () {
          chatService.getMessage($scope.conversation._id, $scope.pagination).then(function (res) {
            angular.forEach(res.data, function (value) {
              $scope.messages.unshift(value);
            });
            $scope.pagination = res.pagination;
          });
        };

        $scope.pagination = {
          total: 0,
          offset: 0,
          morePre: false
        };

        $scope.loadMessage();

        /** chat form */
        $scope.message = {text: ''};
        /** send message */
        $scope.send = function () {
          if ($scope.message.text.length > 0) {
            /** check permission before post reply */
            if (authService.user.level < window.ROLE.premium) {
              /** trial limit send 10 messages */
              if (authService.user.level == window.ROLE.trial) {
                chatService.totalMessageToday().then(function (res) {
                  if (res < 10) {
                    socket.emit('sendMessage', $scope.conversation._id, $scope.message.text);
                    $scope.message.text = null;
                  } else {
                    //show error message
                    $growl.box('', 'ERROR_402', {
                      class: 'danger'
                    }).open();
                  }
                });
              } else {
                //show error message
                $growl.box('', 'ERROR_402', {
                  class: 'danger'
                }).open();
              }
            } else {
              socket.emit('sendMessage', $scope.conversation._id, $scope.message.text);
              $scope.message.text = null;
            }
          }
        };

        /** get new chat. remove notify typing message */
        socket.on('receiveMessage', function (message) {
          if ($scope.conversation._id == message.conversationId) {
            $rootScope.$emit('updateReadedConversation', $scope.conversation._id);
            $scope.messages.push(message);
            if (message.createdBy != $scope.user._id) {
              $scope.isTypingMessage = false;
            }
            $scope.$apply();
          }
        });

        /** 
         * add some kind of notification to the chat
         * so that user can see when the other member is typing a message 
         */
        $scope.sendNotify = false;
        $scope.isTypingMessage = false;
        var timeoutFunction = function () {
          $scope.sendNotify = false;
          socket.emit('notifyTypingMessage', $scope.profile._id, false);
        };
        var typingTimeOut;
        $scope.chatMessage = function ($event) {
          if ($event.keyCode == 13) {
            $scope.send();
          } else {
            if ($scope.message.text == '') {
              if ($scope.sendNotify == true) {
                $scope.sendNotify = false;
                socket.emit('notifyTypingMessage', $scope.profile._id, false);
              }
            } else if ($scope.sendNotify == false) {
              $scope.sendNotify = true;
              socket.emit('notifyTypingMessage', $scope.profile._id, true);
              typingTimeOut = setTimeout(timeoutFunction, 2000);
            } else {
              clearTimeout(typingTimeOut);
              typingTimeOut = setTimeout(timeoutFunction, 2000);
            }
          }
        };
        /** receive notify typing a message*/
        socket.on('notifyTypingMessageCallback', function (isTyping) {
          $scope.isTypingMessage = isTyping;
          $scope.$apply();
        });

        $scope.blurChatMessage = function () {
          if (apBrowserHelper.platform.mobile) {
            var chatBox = angular.element('.box-chat-message');
            if (chatBox) {
              //scroll to event
              $document.scrollToElement(chatBox, 20, 500);
            }
          }
        };
      }

      /**
       * video chat & cam chat
       * set default icon
       * load cam if 2 user pubic vieo
       * load cam if 1 user public & 1 shared video
       * load cam if 2 user shared video
       */
//      $scope.sharePrivateVideo = $rootScope.isPublicVideo;
      $scope.sharePrivateVideo = false;
      $scope.sharePrivateAudio = false;

      /** check connect to user */
      socket.emit('sharePrivate', $scope.profile._id);

      /** connect success & start get video */
      socket.on('onSharePrivate', function (data) {
        if ($rootScope.isPublicVideo) {
//          $scope.sharePrivateVideo = true;
        } else {
          $scope.sharePrivateVideo = data.video || false;
        }
        $scope.sharePrivateAudio = data.audio || false;
        $scope.$apply();
      });

      /** load video streaming */
      if ($rootScope.streamingUsers[$scope.profile._id]) {
        /** add media stream to gallery */
        $scope.slides.unshift({video: $scope.profile._id});
      } else if ($rootScope.isPublicVideo) {
        /** auto connect public user */
        $rootScope.sharePrivateVideo($scope.profile._id, true);
      }

      /**
       * share video of user for this user profile
       * user just can use action share video private when public is not true
       */
      $scope.shareVideo = function () {
        if (authService.user.level < window.ROLE.premium) {
          //show error message
          $growl.box('', 'ERROR_402', {
            class: 'danger'
          }).open();
          return false;
        }
        if (!$rootScope.isPublicVideo) {
          $scope.sharePrivateVideo = !$scope.sharePrivateVideo;
          $rootScope.sharePrivateVideo($scope.profile._id, $scope.sharePrivateVideo);
          //remove video of user if stop shared
          if (!$scope.sharePrivateVideo && $rootScope.streamingUsers[$scope.profile._id]) {
            if ($scope.slides[0].video) {
              $scope.slides.shift();
            }
            delete $rootScope.streamingUsers[$scope.profile._id];
          }
        }
      };

      $scope.shareAudio = function () {
        if (authService.user.level < window.ROLE.premium) {
          //show error message
          $growl.box('', 'ERROR_402', {
            class: 'danger'
          }).open();
          return false;
        }
        $scope.sharePrivateAudio = !$scope.sharePrivateAudio;
        $rootScope.sharePrivateAudio($scope.profile._id, $scope.sharePrivateAudio);

      };

      off.push($rootScope.$on('publicStreamingUser', function (event, userId) {
        if ($scope.profile._id === userId && $scope.sharePrivateVideo && !$rootScope.streamingUsers[$scope.profile._id]) {
          $rootScope.sharePrivateVideo($scope.profile._id, $scope.sharePrivateVideo);
        }
      }));
      /** connect video to this profile when user click public video */
      off.push($rootScope.$on('videoPublicly', function (event, data) {
        $scope.sharePrivateVideo = false;
        if (data) {
          //check video not connect
          if (!$rootScope.streamingUsers[$scope.profile._id]) {
            $rootScope.sharePrivateVideo($scope.profile._id, $scope.sharePrivateVideo);
          }
        } else {
          if ($scope.slides.length > 0 && $scope.slides[0].video) {
            $scope.slides.shift();
          }
//          $scope.sharePrivateAudio = false;
        }
      }));

      /** 
       * when user turn on "public sharing streaming". 
       * add video to slider 
       */
      off.push($rootScope.$on('videoStartCall', function (event, userId) {
        if ($scope.profile._id == userId) {
          $scope.slides.unshift({video: userId});
          $scope.$apply();
        }
      }));

      /** 
       * when user logout or turn off "public sharing streaming". 
       * remove video is now loading
       */
      off.push($rootScope.$on('videoEndCall', function (event, userId) {
        if ($scope.profile._id == userId) {
          $scope.sharePrivateVideo = false;
          if ($scope.slides.length > 0 && $scope.slides[0].video) {
            $scope.slides.shift();
          }
          $scope.$apply();
        }
      }));

      /** 
       * when user accept audio cal. 
       * enable active audio icon
       */
      off.push($rootScope.$on('addAudioStreaming', function (event, userId) {
        if ($scope.profile._id == userId) {
          $scope.sharePrivateAudio = true;
        }
      }));

      /** 
       * when user logout or decline audio call. 
       * remove active audio icon
       */
      off.push($rootScope.$on('audioEndCall', function (event, userId, notApply) {
        if ($scope.profile._id == userId) {
          $scope.sharePrivateAudio = false;
          if (!notApply) {
            $scope.$apply();
          }
        }
      }));

      /** Load date*/
      $scope.dates = dates;

    }
  ]);
  controllers.animation('.slide-horizontal', function () {
    return {
      beforeAddClass: function (element, className, done) {
        var scope = element.scope();

        if (className == 'ng-hide') {
          var finishPoint = element.parent().width();
          if (scope.direction !== 'right') {
            finishPoint = -finishPoint;
          }
          TweenMax.to(element, 0.5, {left: finishPoint, onComplete: done});
        }
        else {
          done();
        }
      },
      removeClass: function (element, className, done) {
        var scope = element.scope();

        if (className == 'ng-hide') {
          element.removeClass('ng-hide');

          var startPoint = element.parent().width();
          if (scope.direction === 'right') {
            startPoint = -startPoint;
          }

          TweenMax.fromTo(element, 0.5, {left: startPoint}, {left: 0, onComplete: done});
        }
        else {
          done();
        }
      }
    };
  });
})();