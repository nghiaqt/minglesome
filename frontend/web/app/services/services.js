(function () {
  'use strict';
  /**
   * @name @appService
   */
  angular.module('app.services', [])
    .factory('appService', function () {
      return {
        converDataMultiSelect: function (data, selectedData) {
          var list = [];
          angular.forEach(data, function (value, key) {
            var selected = false;
            if (angular.isArray(selectedData) && selectedData.indexOf(key) == -1) {
              selected = true;
            } else if (angular.isString(selectedData) && selectedData == key) {
              selected = true;
            }
            this.push({
              id: key,
              name: value,
              selected: selected
            });
          }, list);

          return list;
        }
      };
    })
    .factory('modalPrompt', ['$modal', 'authService', function ($modal, authService) {
        var isProrress=false;
        return {
          info: function (message) {
            if (!authService.hasPrompted(message) && !isProrress) {
              isProrress=true;
              $modal.open({
                templateUrl: '/app/partials/modalPrompt.html',
                backdrop: 'static',
                controller: [
                  '$scope', '$modalInstance',
                  function ($scope, $modalInstance) {
                    $scope.checkbox={remember:false};
                    $scope.message = message;
                    $scope.submit = function () {
                      isProrress=false;
                      if ($scope.checkbox.remember) {
                        authService.updatePrompt(message);
                      }
                      $modalInstance.dismiss('cancel');
                    };
                  }
                ]
              });
            }
          }
        };
      }])
    .factory('peerConnection', function () {
      var default_parameter = {
        onicecandidate: function (e) {
        },
        onIceConnectionStateChanged: function () {
        },
        onaddstream: function (e) {
        },
        onremovestream: function (e) {
        },
        ondatachannel: function (e) {
        },
        onconnection: function (e) {
        },
        offerCallback: function () {
        },
        offerErrorCallback: function () {
        },
        answerCallback: function (description) {
        },
        answerErrorCallback: function () {
        },
        servers: {
          iceServers: [{url: "stun:stun.l.google111.com:19302"}]
        },
        configuration: {
          optional: [{'googImprovedWifiBwe': true}]//{'DtlsSrtpKeyAgreement': true}, {'RtpDataChannels': true}
        },
        sdp_opt: {
          'mandatory': {
            'OfferToReceiveAudio': true,
            'OfferToReceiveVideo': true
          }
        }
      };

      return function (parameter) {
        parameter = angular.extend(angular.extend({}, default_parameter), parameter);

        return {
          peer: null,
          isFinishSignaling: 0, /**
           * @doc function
           * @name create
           * @description create
           * @methodOf peerConnection
           */
          create: function () {
            this.peer = new RTCPeerConnection(parameter.servers, parameter.configuration);

            this.peer.onaddstream = parameter.onaddstream;
            this.peer.onremovestream = parameter.onremovestream;
            this.peer.ondatachannel = parameter.ondatachannel;
            this.peer.onconnection = parameter.onconnection;
            this.peer.onicecandidate = parameter.onicecandidate;
            this.peer.oniceconnectionstatechange = parameter.onIceConnectionStateChanged;
          },
          /**
           * @doc function
           * @name offer
           * @description offer
           * @methodOf peerConnection
           */
          offer: function (callback) {
            var _this = this;
            this.peer.createOffer(function (description) {
              _this.setLocalDescription(description);
              parameter.offerCallback(description);
            }, parameter.offerErrorCallback, parameter.sdp_opt);
          },
          /**
           * @doc function
           * @name answer
           * @description answer
           * @methodOf peerConnection
           */
          answer: function () {
            var _this = this;
            this.peer.createAnswer(function (description) {
              _this.setLocalDescription(description);
              parameter.answerCallback(description);
            }, parameter.answerErrorCallback, parameter.sdp_opt);
          },
          /**
           * @doc function
           * @name setLocalDescription
           * @description setLocalDescription
           * @methodOf peerConnection
           */
          setLocalDescription: function (description) {
//          trace("setLocalDescription", description);
            this.peer.setLocalDescription(description);
          },
          /**
           * @doc function
           * @name setRemoteDescription
           * @description setRemoteDescription
           * @methodOf peerConnection
           */
          setRemoteDescription: function (description) {
//          trace("setRemoteDescription", description);
            this.peer.setRemoteDescription(description);
          },
          /**
           * @doc function
           * @name addIceCandidate
           * @description addIceCandidate
           * @methodOf peerConnection
           */
          addIceCandidate: function (candidate) {
//          trace("addIceCandidate", candidate);
            this.peer.addIceCandidate(candidate);
          },
          /**
           * @doc function
           * @name addStream
           * @description addStream
           * @methodOf peerConnection
           */
          addStream: function (stream) {
            this.peer.addStream(stream);
          },
          /**
           * @doc function
           * @name finishSignaling
           * @description finishSignaling
           * @methodOf peerConnection
           */
          finishSignaling: function () {
            this.isFinishSignaling = 1;
          }
        };
      };
    })
    .factory('WebrtcSocket', ['socket', function (socket) {
        var default_parameter = {
          onConnect: function () {
          },
          joinCallback: function () {
          },
          receiveSDPCallback: function (description) {
          },
          receiveReturnSDPCallback: function (description) {
          },
          receiveCandidateallback: function (candidate) {
          }
        };

        return function (parameter) {
          parameter = angular.extend(angular.extend({}, default_parameter), parameter);

          return {
            /**
             * @doc function
             * @name WebrtcSocket.connect
             * @methodOf WebrtcSocket
             * @description 소켓 연결하기
             */
            connect: function () {
              var that = this;

              socket.on('receiveCall', function (calleeId, mandatory) {//{audio, video}
                parameter.receiveCallCallback(calleeId, mandatory);
              });
              socket.on('onSdpMessage', function (id, description, mandatory) {
                if (!description) {
                  return;
                }
                if (typeof description == 'string') {
                  description = JSON.parse(description);
                }
                if (typeof mandatory.video == 'string') {
                  mandatory.video = mandatory.video === 'true' ? true : false;
                }
                if (typeof mandatory.audio == 'string') {
                  mandatory.audio = mandatory.audio === 'true' ? true : false;
                }
                if (description.type === 'offer') {
                  description = new RTCSessionDescription(description);
                  parameter.receiveSDPCallback(id, description, mandatory);
                } else if (description.type === 'answer') {
                  description = new RTCSessionDescription(description);
                  parameter.receiveReturnSDPCallback(id, description, mandatory);
                } else if (description.type === 'candidate') {
                  var candidate = new RTCIceCandidate({sdpMLineIndex: description.label, candidate: description.candidate});
                  parameter.receiveCandidateallback(id, candidate, description.callType, mandatory);
                } else if (description.type === 'bye') {
                  parameter.onRemoteHangup(id);
                }
              });
            },
            disconnect: function () {
              socket.removeListener('receiveCall');
              socket.removeListener('onSdpMessage');
            },
            /**
             * Send peerconnection to server
             * @param {string} calleeId
             * @param {object} description
             * @param {object} mandatory
             * @param {string} callType This value use for ios app
             * @returns {undefined}
             */
            sendSDP: function (calleeId, description, mandatory, callType) {
              description = JSON.parse(JSON.stringify(description));
              socket.emit("sdpMessage", calleeId, description, mandatory, callType);
            }
          };
        };
      }]);
})();