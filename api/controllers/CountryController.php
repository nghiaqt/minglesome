<?php

namespace api\controllers;

//load helper
use \Yii;
use yii\filters\AccessControl;
//load model
use common\models\Country;
use common\models\State;
use common\models\City;

/**
 * Country Controller
 *
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class CountryController extends ApiController {

  /**
   * Get all country
   * @return array
   */
  public function actionIndex() {
    return Country::getAllAsArray($this->language);
  }

  /**
   * Get all state
   * @return array
   */
  public function actionGetStates() {
    return [
      'data' => State::getAllAsArray($this->getRequestParam('countryCode'))
    ];
  }

  /**
   * Get all city by country & state
   * @return array
   */
  public function actionGetCities() {
    return [
      'data' => City::getAllAsArray($this->getRequestParam('countryCode'), $this->getRequestParam('stateId'))
    ];
  }

  public function actionView() {
    return [
      'language' => $this->language,
      'currency' => $this->currency,
      'country' => $this->country,
      'height' => $this->height,
      'distance' => $this->distance,
      'isIOS' => $this->isIOS
    ];
  }

}
