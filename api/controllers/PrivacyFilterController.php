<?php

namespace api\controllers;

//load helper
use \Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
//load model
use common\models\PrivacyFilter;
use common\models\User;
use common\models\ProfileContent;
use common\models\Country;
/**
 * Privacy Filter controller  
 */
class PrivacyFilterController extends ApiController {

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'access' => [
            'class' => AccessControl::className(),
            'only' => ['save-privacy-based-on-logged-user', 'get-privacy-based-on-user-logged'],
            'rules' => [
                [
                    'actions' => ['save-privacy-based-on-logged-user', 'get-privacy-based-on-user-logged'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
                [
                    'allow' => false, // Do not have access
                    'roles' => ['?']
                ]
            ]
        ]
    ];
  }

  /**
   * create/update Privacy Filter based on user logged ing
   */
  public function actionSavePrivacyBasedOnLoggedUser() {
    //load privacy filter
    $privacyFilter = PrivacyFilter::findOne([
                'createdBy' => $this->__user->_id
    ]);
    if(!$privacyFilter){
      return $this->send(404);
    }
    $privacyFilter->setAttributes($this->__request);
    if ($privacyFilter->save()) {
      return 'SAVED_PRIVACY_FILTER_SUCCESS';
    } else {
      return $this->send(400, $privacyFilter->getErrors(), true);
    }
  }

  /**
   * get privacy based on user logged in
   */
  public function actionGetPrivacyBasedOnUserLogged() {
    $privacyFilter = PrivacyFilter::findOne([
                'createdBy' => $this->__user->_id
            ]);

    if (!$privacyFilter) {
      $privacyFilter= new PrivacyFilter();
      $privacyFilter->createdBy=  $this->__user->_id;
      $privacyFilter->save();
    }
    return $privacyFilter;
  }

}
