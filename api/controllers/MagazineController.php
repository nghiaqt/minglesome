<?php

namespace api\controllers;

use common\models\User;
use common\models\Magazine;
use common\models\MagazineComment;
use common\models\MagazineReply;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MagazineController
 *
 * @author tuancasi
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class MagazineController extends ApiController {

  /**
   * View detail article
   * @param type $id
   * @return type
   */
  public function actionView($id) {
    $article = Magazine::find()->where(['_id' => new \MongoId($id)])->asArray()->one();
    if (!$article) {
      return $this->send(404);
    }
    if ($article['file']) {
      $article['file']['url'] = MAGAZINE_URL . $article['file']['name'];
    }
    if ($article['owner']) {
      $user = User::findOne($article['owner']);
      if ($user) {
        $article['author'] = $user->getPublicData($this->language, $this->__user);
      }
    }
    return $article;
  }

  /**
   * get magazine list
   */
  public function actionIndex($month, $year) {
    if (!$month) {
      $aggregateMonths = Magazine::getCollection()->aggregate([
          [
              '$project' => [
                  'month' => [
                      '$month' => '$updatedAt'
                  ],
                  'day' => [
                      '$dayOfMonth' => '$updatedAt'
                  ],
                  'year' => [
                      '$year' => '$updatedAt'
                  ]
              ]
          ],
          [
              '$match' => [
                  'year' => intval($year)
              ]
          ],
          [
              '$group' => [
                  '_id' => '$month', //group field				
              ]
          ],
          [
              '$sort' => [
                  '_id' => -1
              ]
          ]
      ]);
      if (isset($aggregateMonths[0]) && $aggregateMonths[0]['_id']) {
        $month = $aggregateMonths[0]['_id'];
      }
    }
    $magazines = Magazine::getCollection()->aggregate([
        [
            '$project' => [
                'month' => [
                    '$month' => '$updatedAt'
                ],
                'day' => [
                    '$dayOfMonth' => '$updatedAt'
                ],
                'year' => [
                    '$year' => '$updatedAt'
                ],
                'file' => '$file',
                'title' => '$title',
                'content' => '$content'
            ]
        ],
        [
            '$match' => [
                'month' => intval($month),
                'year' => intval($year)
            ]
        ]
    ]);

    foreach ($magazines as &$magazine) {
      if (isset($magazine['file']) && $magazine['file']) {
        $magazine['file']['url'] = MAGAZINE_URL . $magazine['file']['name'];
      }
    }

    return ['data' => $magazines, 'month' => $month];
  }

  /**
   * get months has magazine issues
   * 
   * @param int $year
   * @return array
   */
  public function actionMonthsHasMagazine($year) {
    $months = [];
    $monthLabels = ['JANUARY', 'FEBUARY', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY', 'AUGUST', 'SEPTEMBER', 'OCTOBER',
        'NOVEMBER', 'DECEMBER'];
    $aggregateMonths = Magazine::getCollection()->aggregate([
        [
            '$project' => [
                'month' => [
                    '$month' => '$updatedAt'
                ],
                'day' => [
                    '$dayOfMonth' => '$updatedAt'
                ],
                'year' => [
                    '$year' => '$updatedAt'
                ]
            ]
        ],
        [
            '$match' => [
                'year' => intval($year)
            ]
        ],
        [
            '$group' => [
                '_id' => '$month', //group field				
            ]
        ],
        [
            '$sort' => [
                '_id' => -1
            ]
        ]
    ]);
    foreach ($aggregateMonths as $month) {
      $months[] = ['label' => $monthLabels[$month['_id'] - 1], 'value' => $month['_id']];
    }
    return ['data' => $months];
  }

  /**
   * Get all comments by magazine id
   * @param type $id Magazine Pk
   * @return array
   */
  public function actionGetComments($id) {
    $limit = 10;
    $morePre = false;
    $date = '';

    // set default condition
    $where = ['magazineId' => new \MongoId($id)];

    //set condition load pre data
    if (isset($_GET['date']) && $_GET['date']) {
      $date = $_GET['date'];
      $where['createdAt'] = ['$lt' => new \MongoDate($date / 1000)];
    }

    $total = MagazineComment::find()->where($where)->count();
    $comments = MagazineComment::find()
            ->where($where)
            ->orderBy(['createdAt' => SORT_DESC])
            ->limit($limit)
            ->asArray()
            ->all();

    if ($total > count($comments)) {
      $morePre = true;
    }
    if ($comments) {
      $date = $comments[count($comments) - 1]['createdAt'];
    }

    return ['data' => $comments, 'morePre' => $morePre, 'lateCreatedAt' => $date];
  }

  /**
   * Get comment by comment id
   * @param type $id
   * @return type
   */
  public function actionViewComment($id) {
    $comment = MagazineComment::find()
            ->where(['_id' => new \MongoId($id)])
            ->asArray()
            ->one();
    /**
     * set data user add comment
     * set data user add reply comment
     */
    $comment['author'] = null;
    $user = User::findOne($comment['createdBy']);
    if ($user) {
      $comment['author'] = $user->getPublicData($this->language, $this->__user);
    }

    return $comment;
  }

  /**
   * Add comment magazine
   * @param type $id Magazine Pk
   */
  public function actionCreateComment($id) {
    $magazine = Magazine::findOne($id);
    if (!$magazine) {
      return $this->send(404);
    }
    $model = new MagazineComment();
    $model->magazineId = $magazine->_id;
    $model->createdBy = $this->__user->_id;
    $model->content = $this->getRequestParam('content');
    if ($model->save()) {
      $this->__user->comments+=1;
      $this->__user->save();
      return $model;
    } else {
      return $this->send(400, $model->getErrors(), true);
    }
  }

}
