<?php

namespace api\controllers;

//load helper
use yii\filters\AccessControl;
//load model
use common\models\User;
use common\models\Picture;
use common\models\Country;
use common\models\State;
use common\models\ProfileContent;
use common\models\ActivityEvent;
use common\models\Visitor;

/**
 * Profile Controller
 *
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class ProfileController extends ApiController {

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'access' => [
            'class' => AccessControl::className(),
            'only' => ['view', 'update', 'visitor', 'get-visitor','turn'],
            'rules' => [
                [
                    'actions' => ['view', 'update', 'visitor', 'get-visitor','turn'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
                [
                    'allow' => false, // Do not have access
                    'roles' => ['?']
                ]
            ]
        ]
    ];
  }

  /**
   * Get data dropdown for profile user
   * @return type
   */
  public function actionGetDropdownData() {
    $dropdown = [];
    $dropdown['country'] = Country::getAllAsArray($this->language);
    $dropdown['status'] = User::getStatusMarriedCode();
    $dropdown['orientation'] = ProfileContent::getAllAsArray('orientation', $this->language);
    $dropdown['style'] = ProfileContent::getAllAsArray('style', $this->language);
    $dropdown['figure'] = ProfileContent::getAllAsArray('figure', $this->language);
    $dropdown['ethnicity'] = ProfileContent::getAllAsArray('ethnicity', $this->language);
    $dropdown['other'] = ProfileContent::getAllAsArray('other', $this->language);
    $dropdown['religion'] = ProfileContent::getAllAsArray('religion', $this->language);
    $dropdown['upfor'] = ProfileContent::getAllAsArray('upfor', $this->language);
    $dropdown['speaking'] = ProfileContent::getAllAsArray('speaking', $this->language);
    $dropdown['question'] = ProfileContent::getAllAsArray('question', $this->language);
    $dropdown['gender'] = User::getGendersCode();
    $dropdown['activities'] = ActivityEvent::getAllAsArray($this->language);
    $dropdown['distances'] = \common\models\Setting::get('within', $this->distance);
    $dropdown['distanceType'] = $this->distance;
    $dropdown['heightType'] = $this->height;
    return $dropdown;
  }

  /**
   * view detail user
   * @param type $id
   * @return type
   */
  public function actionView($id) {
    $model = User::findIdentity($id);
    if (!$model) {
      return $this->send(404);
    }
    return $model->getFullPublicData($this->language, $this->height);
  }

  /**
   * Update profile data
   * @param type $id
   * @return array
   */
  public function actionUpdate($id) {
    $model = User::findIdentity($id);

    if (!$model) {
      return $this->send(404);
    }
    if ($model->city != $this->getRequestParam('city') ||
            $model->state != $this->getRequestParam('state') ||
            $model->country != $this->getRequestParam('country')) {
      $model->isNewAddress = true;
    }
    $model->answer = $this->getRequestParam('answer');
    $model->baggage = $this->getRequestParam('baggage');
    $model->city = $this->getRequestParam('city');
    $model->state = $this->getRequestParam('state');
    $model->country = $this->getRequestParam('country');
    if (($height = $this->getRequestParam('height'))) {
      if (is_numeric($height)) {
        if ($this->height == 'feet') {
          $model->height = [
              'feet' => $height,
              'cm' => round($height * 30.48)
          ];
        } else {
          $model->height = [
              'feet' => round($height / 30.48, 2),
              'cm' => $height
          ];
        }
      } else {
        $model->addError('height', 'Height is number');
      }
    }
    $model->inMyOwnWords = $this->getRequestParam('inMyOwnWords');
    $model->keywords = $this->getRequestParam('keywords');
    $model->notlikeThings = $this->getRequestParam('notlikeThings');
    $model->preferThings = $this->getRequestParam('preferThings');
    $model->statusComment = $this->getRequestParam('statusComment');
    $model->statusMarried = $this->getRequestParam('statusMarried');
    //object
    $model->ethnicity = $this->getRequestParam('ethnicity');
    $model->figure = $this->getRequestParam('figure');
    $model->orientation = $this->getRequestParam('orientation');
    $model->questionId = $this->getRequestParam('questionId');
    $model->religion = $this->getRequestParam('religion');
    //array object
    $model->others = $this->getRequestParam('others');
    $model->speakings = $this->getRequestParam('speakings');
    $model->styles = $this->getRequestParam('styles');
    $model->upfors = $this->getRequestParam('upfors');

    if ($model->save()) {
      return $model->getFullPublicData($this->language,  $this->height);
    } else {
      return $this->send(400, $model->getErrors(), true);
    }
  }

  /**
   * get visitor of user
   * @return type
   */
  public function actionVisitor() {
    $limit = 10;
    $morePre = false;
    $updatedAt = '';

    $where = ['viewed' => $this->__user->_id];
    //set condition load pre data
    if (isset($_GET['date']) && $_GET['date']) {
      $updatedAt = $_GET['date'];
      $where['updatedAt'] = ['$lt' => new \MongoDate($updatedAt / 1000)];
    }
    $total = Visitor::find()->andWhere($where)->count();

    $model = Visitor::find()
            ->andWhere($where)
            ->orderBy(['updatedAt' => SORT_DESC])
            ->limit($limit)
            ->asArray()
            ->all();

    if ($total > count($model)) {
      $morePre = true;
    }

    /**
     * get user visitor
     * check status unread message
     * get last message time
     */
    foreach ($model as $key => $value) {
      //set user visitor
      $author = null;

      $user = User::findOne($value['createdBy']);
      if ($user) {
        $author = $user->getPublicData($this->language, $this->__user);
      }

      $model[$key]['author'] = $author;

      //set last time
      $updatedAt = $value['updatedAt'];
    }

    return ['data' => $model, 'morePre' => $morePre, 'lateUpdatedAt' => $updatedAt];
  }

  /**
   * get visitor by pk
   * @param type $id
   * @return type
   */
  public function actionGetVisitor($id) {
    $model = Visitor::find()
            ->where(['_id' => $id])
            ->asArray()
            ->one();
    if (!$model) {
      return $this->send(404);
    }
    //set user visitor
    $author = null;

    $user = User::findOne($model['createdBy']);
    if ($user) {
      $author = $user->getPublicData($this->language, $this->__user);
    }

    $model['author'] = $author;

    return $model;
  }

  /**
   * get turn server
   * @return type
   */
  public function actionTurn() {
    $time = strtotime('+1 days');
    $username = (string) $this->__user->_id;
    $res = file_get_contents('http://184.72.71.110/turn.php?username=' . $username . '&password=' . $time);
    $res = trim($res);
    $res = json_decode($res);
    return [
        'username' => $res->user,
        'password' => $res->password,
        'uris' => [
            'turn:184.72.71.110:3478?transport=udp',
            'turn:184.72.71.110:3478?transport=tcp',
            'turn:184.72.71.110:3479?transport=udp',
            'turn:184.72.71.110:3479?transport=tcp',
        ],
        'ip' => $_SERVER['REMOTE_ADDR']];
  }

}
