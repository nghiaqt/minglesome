<?php

namespace api\controllers;

use \Yii;
use yii\filters\AccessControl;
use common\models\Contact;
use common\models\User;
use common\models\Picture;
use common\models\Reason;

/**
 * Contact controller
 * 
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class ContactController extends ApiController {

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'access' => [
            'class' => AccessControl::className(),
            'only' => ['index'],
            'rules' => [
                [
                    'actions' => ['index'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
                [
                    'allow' => false, // Do not have access
                    'roles' => ['?']
                ]
            ]
        ]
    ];
  }

  /**
   * Add contact user
   * @return boolean
   */
  public function actionCreate() {
    
    $model = new Contact();
    $model->country = $this->country;
    $model->subject = $this->getRequestParam('subject');
    $model->content = $this->getRequestParam('content');
    if ($this->__user) {
      $model->createdBy = $this->__user->_id;
      $model->email = $this->__user->email;
    } else {
      $model->email = $this->getRequestParam('email');
    }
    if ($model->save()) {
      return true;
    }else{
      return $this->send(400, $model->getErrors(), true);
    }
  }

  /**
   * 
   */
  public function actionGetAdmin() {
    $model = User::findOne(['level' => User::LEVEL_ADMIN]);
    if ($model) {
      $gallery = Picture::find()->where([
                          'createdBy' => $model->_id
                      ])
                      ->orderBy([
                          'isAvatar' => SORT_DESC,
                          'createdAt' => SORT_DESC
                      ])
                      ->asArray()->all();
      $pictures = [];
      foreach ($gallery as $item) {
        $pictures[] = ['_id' => $item['_id'], 'url' => PHOTO_UPLOAD_URL . $item['fileName']];
      }

      $data = $model->getPublicData($this->language);
      $data['gallery'] = $pictures;
      return $data;
    }
  }

  public function actionGetReason(){
    return ['data'=>Reason::getAllAsArray($this->language, 'contact')];
  }
}