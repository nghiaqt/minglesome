<?php

namespace api\controllers;

//load model
use \Yii;
use yii\filters\AccessControl;
//load model
use common\models\User;
use common\models\Package;
use common\models\Invoice;
use common\models\Setting;
use common\models\Invite;
use common\models\InviteHistory;
use common\helpers\Email;
use common\helpers\GeoLocation;
use common\models\RecurringPaymentsProfile;
use common\models\Country;

/**
 * Purchase Controller
 * Payment by paypal
 *
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class PurchaseController extends ApiController {

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['index', 'payment', 'invite-code', 'app'],
        'rules' => [
          [
            'actions' => ['index', 'payment', 'invite-code', 'app'],
            'allow' => true,
            'roles' => ['@'],
          ],
          [
            'allow' => false, // Do not have access
            'roles' => ['?']
          ]
        ]
      ]
    ];
  }

  /**
   * Get all products
   * Set data detail for purchase
   * @return array
   */
  public function actionIndex() {
    $currency = $this->currency;
    $_country = GeoLocation::getCountryByIp();
    if ($_country) {
      $country = Country::findOne(['countryCode' => $_country]);
      if ($country) {
        $currency = $country->currency;
      }
    }
    $products = Package::getAllAsArray($this->__user->isAffiliate, $this->country, $currency, $this->isIOS);
    return [
      'products' => $products,
      'currency' => $currency
    ];
  }

  /**
   * Payment upgrade account
   * create pay info & url to paypal
   * @return url Paypal url
   */
  public function actionPayment() {
    $recurring = RecurringPaymentsProfile::findOne([
        'profileStatus' => 'Active',
        'createdBy' => $this->__user->_id
    ]);
    if ($recurring) {
      return $this->send(400, ['error' => 'HAS_RECURRING_PAYMENT']);
    }
    //load product
    $package = Package::findOne($this->getRequestParam('product'));
    if (!$package) {
      $this->send(404);
    }

    //define currency
    $currency = $this->currency;
    if ($this->ipCountry) {
      $country = Country::findOne(['countryCode' => $this->ipCountry]);
      if ($country) {
        $currency = $country->currency;
      }
    }
    //load data package for country
    $package->loadDataForCountry($this->country, $currency);
    
    //set total amout
    $total = $package->amount;
    
    //check invite code
    $inviteId = '';
    $inviteCode = $this->getRequestParam('inviteCode');
    if ($inviteCode) {
      //check invite code is not use & invite code is not of user
      $history = InviteHistory::findOne(['inviteCode' => $inviteCode]);
      if (!$history) {
        $history = InviteHistory::findOne(['inviteCodeWithoutMemberId' => $inviteCode]);
      }
      if ($history && $this->__user->_id != $history->owner) {
        $inviteId = $history->inviteId;
      }
      //set total amount
      if ($inviteId) {
        $invite = Invite::findOne($inviteId);
        if ($invite) {
          $now = mktime(0, 0, 0);
          if ($invite->startDate->sec <= $now && $invite->endDate->sec >= $now) {
            $inviteCode='';
          }
        }
      }
    }
    
    //check invite by affiliate link
    if ($package->isAffiliate) {
      if (!$inviteId || !$invite || $invite->packageId != $package->_id) {
        $inviteCode = '';
        $inviteId = '';
        $invite = Invite::findOne([
            'packageId' => $package->_id,
            'endDate' => ['$gt' => new \MongoDate()]
        ]);
        if ($invite) {
          $inviteId = $invite->_id;
          //$inviteCode = $invite->code;
        }
      }
    }
    
    //add invoice info
    $invoice = new Invoice();
    $invoice->createdBy = $this->__user->_id;
    $invoice->email = $this->__user->email;
    $invoice->packageId = $package->_id;
    $invoice->packageName = $package->name;
    $invoice->packageMonth = $package->month;
    $invoice->amount = $package->amount;
    $invoice->currency = $currency;
    $invoice->inviteCode = $inviteCode;
    $invoice->inviteId = $inviteId;
    $invoice->total = $total;
    //payment by paypal
    if ($total > 0) {
      //set info payment
      $info = [
        'Order' => [
          'theTotal' => $total,
          'description' => $package->name,
          'quantity' => 1
        ]
      ];
      //create paypal request
      $paypal = Yii::$app->Paypal;
      $setting = Setting::get('paypal');
      if ($setting) {
        $paypal->apiLive = $setting['live'] ? true : false;
        $paypal->apiUsername = $setting['username'];
        $paypal->apiPassword = $setting['password'];
        $paypal->apiSignature = $setting['signature'];
      }
      $paypal->SetPaypalUrl();
      $paypal->isRecurring = $package->isRecurring;
      $paypal->currency = $currency;
      //set expresscheckout
      $result = $paypal->SetExpressCheckout($info);
      //Detect Errors
      if (!$paypal->isCallSucceeded($result)) {
        if ($paypal->apiLive == true) {
          //Live mode basic error message
          return $this->send(400, ['error' => 'ERROR_PAYMENT']);
        } else {
          //Sandbox output the actual error message to dive in.
          return $this->send(400, $result['L_LONGMESSAGE0']);
        }
      } else {
        $token = urldecode($result["TOKEN"]);
        $invoice->token = $token;
        $invoice->isRecurring = $package->isRecurring;
        if ($invoice->save()) {
          return ['url' => $paypal->paypalUrl . $token];
        } else {
          return $this->send(400, $invoice->getErrors(), true);
        }
      }
    } else {
      //free payment
      $invoice->isPayment = true;
      if ($invoice->save()) {
        //get user
        $user = $this->__user;

        if ($history) {
          //update Number of premium upgrades invited of invite member.
          $history->premiums+=1;
          $history->save();
          $ownerInvite = User::findOne($history->owner);
          if ($ownerInvite) {
            $ownerInvite->premiums+=1;
            $ownerInvite->save();
            $user->invitedBy = $ownerInvite->userId;
          }
        }

        //update premium for user payment
        if ($user->level < User::LEVEL_PREMIUM) {
          $user->level = User::LEVEL_PREMIUM;
        }
        if (is_object($user->expiredPremium) && $user->expiredPremium instanceof \MongoDate &&
          $user->expiredPremium->sec > time()) {
          $user->expiredPremium = new \MongoDate(strtotime('+' . $invoice->packageMonth . ' months', $user->expiredPremium->sec));
        } else {
          $user->expiredPremium = new \MongoDate(strtotime('+' . $invoice->packageMonth . ' months'));
        }
        if ($user->save()) {
          Email::sendConfirmFreeUpgrade($user, $invoice);
          return ['isFree' => true];
        } else {
          return $this->send(400, $user->getErrors(), true);
        }
      } else {
        return $this->send(400, $invoice->getErrors(), true);
      }
    }
  }

  /**
   * log purchase for ios app
   * update expired premium for user
   * @return type
   */
  public function actionApp() {
    if (!$this->isIOS) {
      return $this->send(400);
    }
    $user = $this->__user;
    //get package
    $package = Package::findOne($this->getRequestParam('product'));
    if (!$package) {
      $this->send(404);
    }
    $package->loadDataForCountry($this->country, $this->currency);
    
    //save invoice
    $invoice = new Invoice();
    $invoice->createdBy = $user->_id;
    $invoice->email = $user->email;
    $invoice->packageId = $package->_id;
    $invoice->packageName = $package->name;
    $invoice->packageMonth = $package->month;
    $invoice->total = $invoice->amount = $this->getRequestParam('amount');
    $invoice->currency = $this->currency;
    $invoice->isPayment = true;
    if ($invoice->save()) {
      //update premium for user payment
      if ($user->level < User::LEVEL_PREMIUM) {
        $user->level = User::LEVEL_PREMIUM;
      }
      if (is_object($user->expiredPremium) && $user->expiredPremium instanceof \MongoDate &&
        $user->expiredPremium->sec > time()) {
        $user->expiredPremium = new \MongoDate(strtotime('+' . $invoice->packageMonth . ' months', $user->expiredPremium->sec));
      } else {
        $user->expiredPremium = new \MongoDate(strtotime('+' . $invoice->packageMonth . ' months'));
      }
      if ($user->save()) {
        //send email confirm
        Email::sendConfirmUpgrade($user, $invoice, null);
      }
    }
  }

  /**
   * check invite code of user
   * @return object Invite data
   */
  public function actionInviteCode() {
    if (isset($_GET['code'])) {
      //check invite code
      $inviteCode = $_GET['code'];
      $history = InviteHistory::findOne(['inviteCode' => $inviteCode]);
      if (!$history) {
        $history = InviteHistory::findOne(['inviteCodeWithoutMemberId' => $inviteCode]);
      }
      if (!$history || $this->__user->_id == $history->owner) {
        return $this->send(402, 'ERROR_INVITE_CODE_NOT_FOUND');
      }

      //check invite expired
      $invite = Invite::findOne($history->inviteId);
      if (!$invite) {
        return $this->send(400, 'ERROR_INVITE_CODE_NOT_FOUND');
      }
      $now = mktime(0, 0, 0);
      if ($invite->startDate->sec <= $now && $invite->endDate->sec >= $now) {
        //define currency
        $currency = $this->currency;
        //$_country = $this->__user->country
        if ($this->ipCountry) {
          $country = Country::findOne(['countryCode' => $this->ipCountry]);
          if ($country) {
            $currency = $country->currency;
          }
        }
        //get products
        //$products = Package::getAllAsArray(true, $this->country, $currency);
        $products = Package::getById($invite->packageId, $this->country, $currency);
        return $products;
      } else {
        return $this->send(400, 'ERROR_INVITE_CODE_EXPIRED');
      }
    }
    return $this->send(400);
  }

}
