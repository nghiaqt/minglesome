<?php

namespace api\controllers;

use \Yii;
use common\models\Translation;
use common\models\TranslationForCountry;
/**
 * Tranlate controller
 *
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 * @version 0.1
 */
class TranslateController extends ApiController {

  /**
   * Get translate content
   * @return array
   */
  public function actionIndex() {
    //get language by country code
    //get translate content from database
    //return translate content		
    return array_merge(Translation::getAllAsArray($this->language), TranslationForCountry::getAllAsArray($this->country));
  }

}
