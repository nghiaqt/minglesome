<?php

namespace api\controllers;

use yii\filters\AccessControl;
/**
 * Credit Card Controller
 *
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class CreditCardController extends ApiController {

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'access' => [
            'class' => AccessControl::className(),
            'only' => ['index'],
            'rules' => [
                [
                    'actions' => ['index'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
                [
                    'allow' => false, // Do not have access
                    'roles' => ['?']
                ]
            ]
        ]
    ];
  }

  /**
   * get credit card of user
   * replace 10 char first of card number to *
   */
  public function actionIndex() {
    $user = $this->__user;
    $return = [];
    $return['cardHolder']=$user['cardHolder'];
    $return['cardType']=$user['cardType'];
    $return['cardNumber'] = $user['cardNumber'] ?
            substr_replace($user['cardNumber'], '**********', 0, 10) : '';
    $return['cardMonth']=$user['cardMonth'];
    $return['cardYear']=$user['cardYear'];
    $return['cardCvv']=$user['cardCvv'];
    return $return;
  }

}