<?php

namespace api\controllers;

//load helper
use yii\filters\AccessControl;
//load model
use common\models\Picture;
use common\helpers\File;
use common\models\User;

/**
 * Gallery Controller
 * Manager all photo of user
 *
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class GalleryController extends ApiController {

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'access' => [
            'class' => AccessControl::className(),
            'only' => ['index', 'create', 'update', 'delete'],
            'rules' => [
                [
                    'actions' => ['index', 'create', 'update', 'delete'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
                [
                    'allow' => false, // Do not have access
                    'roles' => ['?']
                ]
            ]
        ]
    ];
  }

  /**
   * Get all image in gallery of user
   * @param string $id
   * @return array Gallery of user
   */
  public function actionIndex($id) {
    $gallery = Picture::find()->where([
                        'createdBy' => new \MongoId($id)
                    ])
                    ->orderBy([
                        'isAvatar' => SORT_DESC,
                        'createdAt' => SORT_DESC
                    ])
                    ->asArray()->all();
    $return = [];
    foreach ($gallery as $item) {
      $return[] = ['_id' => $item['_id'], 'url' => PHOTO_UPLOAD_URL . $item['fileName']];
    }
    return $return;
  }

  /**
   * add new image to gallery of user
   * @return array Gallery of user
   */
  public function actionCreate() {
    $file = File::savePhotoFile($this->getRequestParam('picture'));
    if (!$file) {
      return $this->send(404);
    }
    $model = new Picture();
    $model->fileName = $file;
    $model->createdBy = $this->__user->_id;
    if ($model->save()) {
      //update avatar for user
      $user = $this->__user;
      if (!$user->avatar) {
        $user->avatar = $model->_id;
        $user->avatarUrl = $model->fileName;
        $picture['isAvatar']=true;
      }
      $user->photos += 1;
      $user->save();
      $picture['_id'] = $model->_id;
      $picture['url'] = PHOTO_UPLOAD_URL . $model->fileName;
      return $picture;
    } else {
      return $this->send(400, $model->getErrors(), true);
    }
  }

  /**
   * Update avatar for user
   * @return boolean
   */
  public function actionUpdate() {
    //remove flag avatar
    Picture::updateAll(['isAvatar' => false], ['createdBy' => $this->__user->_id]);
    //set new flag avatar
    $model = Picture::findOne($this->getRequestParam('_id'));
    if (!$model) {
      return $this->send(404);
    }
    $model->isAvatar = true;
    $model->save();

    //update avatar for user
    $user = $this->__user;
    $user->avatar = $model->_id;
    $user->avatarUrl = $model->fileName;

    if ($user->save()) {
      return true;
    } else {
      return $this->send(400, $user->getErrors(), true);
    }
  }

  /**
   * Delete picture of user
   * Check picture avatar. if it's avatar: change avatar
   * @param type $id
   */
  public function actionDelete($id) {
    $model = Picture::findOne($id);
    if (!$model) {
      return $this->send(404);
    }

    // if ((string) $model->_id === (string) $this->__user->avatar) {
    //   return $this->send(400, ['AVATAR_ERROR']);
    // }

    unlink(PHOTO_UPLOAD_PATH . $model->fileName);

    if ($model->delete()) {
      $this->__user->photos -= 1;
      $this->__user->save();
      return true;
    } else {
      return $this->send(400, $model->getErrors(), true);
    }
  }

}