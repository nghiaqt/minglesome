<?php

namespace api\controllers;

use common\models\Reason;
use common\models\Conversation;
use common\models\ChatConversation;

/**
 * Utils Controller
 *
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class UtilsController extends ApiController {

  /**
   * Get all reason
   * @return type
   */
  public function actionReason() {
    return ['data' => Reason::getAllAsArray($this->language)];
  }

  /**
   * contact to admin
   */
  public function actionContactAdmin() {

    $model = new Conversation();
    $model->subject = $this->__request['subject'];
    $model->lastMessage = $this->__request['content'];
    $model->isConversation = true;
    $model->isContact = true;
    if ($this->__user) {
      $model->recipients = [ new \MongoId(ADMIN_CONTACT), $this->__user->_id];
      $model->createdBy = $this->__user->_id;
      $model->updatedBy = $this->__user->_id;
    } else {
      $model->recipients = ['000000000000000000000000', '000000000000000000000000'];
      $model->email = $this->__request['email'];
      $model->createdBy = '000000000000000000000000';
    }

    if ($model->save()) {
      $modelChatConversation = new ChatConversation();
      $modelChatConversation->conversationId = $model->_id;
      $modelChatConversation->text = $this->__request['content'];
      $modelChatConversation->isContact = true;
      if ($this->__user) {
        $modelChatConversation->createdBy = $this->__user->_id;
      } else {
        $modelChatConversation->createdBy = '000000000000000000000000';
      }

      if ($modelChatConversation->save()) {
        return $modelChatConversation;
      } else {
        return $this->send(400, $modelChatConversation->getErrors(), true);
      }
    } else {
      return $this->send(400, $model->getErrors(), true);
    }
  }

  /**
   * get all country
   * @return type
   */
  public function actionCountry() {
    return ['data'=>\common\models\Country::getAllAsArray($this->language)];
  }

}