<?php

namespace api\controllers;

//load helper
use \Yii;
use yii\filters\AccessControl;
//load model
use common\models\User;
use common\models\Conversation;
use common\models\ChatConversation;
use common\models\ContactEmail;
use common\models\KeyUser;
use common\models\PrivacyFilter;

/**
 * Chat Controller
 *
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class ChatController extends ApiController {

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'access' => [
            'class' => AccessControl::className(),
            'only' => [
                'delete', 'message', 'private', 'activity', 'get-conversation', 'read',
                'total-message-today'
            ],
            'rules' => [
                [
                    'actions' => [
                        'delete', 'message', 'private', 'activity', 'get-conversation', 'read',
                        'total-message-today'
                    ],
                    'allow' => true,
                    'roles' => ['@'],
                ],
                [
                    'allow' => false, // Do not have access
                    'roles' => ['?']
                ]
            ]
        ]
    ];
  }

  /**
   * Delete conversation
   * update user into delete list of conversation
   * update user into delete list for all chat in conversation
   * @return boolean
   */
  public function actionDelete($id) {
    $model = Conversation::findOne($id);
    if (!$model) {
      return $this->send(404);
    }
    if (!in_array($this->__user->_id, $model->deletedUsers)) {
      $model->deletedUsers += [$this->__user->_id];
      if ($model->save()) {
        $chat = ChatConversation::findAll(['conversationId' => $model->_id]);
        foreach ($chat as $item) {
          if (empty($item->deletedUsers) || !in_array($this->__user->_id, $item->deletedUsers)) {
            $item->deletedUsers += [$this->__user->_id];
            $item->save();
          }
        }
        return true;
      } else {
        return $this->send(400, $model->getErrors(), true);
      }
    } else {
      return $this->send(404);
    }
  }

  /**
   * Get all chat conversation by conversation id
   * @param string $id This value is conversation id
   */
  public function actionMessage($id) {
    $limit = 10;
    $morePre = false;
    $where = [
        'conversationId' => new \MongoId($id),
        'deletedUsers' => ['$nin' => [$this->__user->_id]]
    ];

    $total = ChatConversation::find()->where($where)->count();

    if (isset($_GET['offset']) && $_GET['offset']) {
      $offset = $_GET['offset'];
    } else {
      $offset = 0;
    }

    if (isset($_GET['total']) && $_GET['total'] && $_GET['total'] < $total) {
      $offset = $offset + ($total - $_GET['total']);
    }

    $model = ChatConversation::find()->where($where)->orderBy(['createdAt' => SORT_DESC])->limit($limit)->offset($offset)->all();

    $offset+=$limit;
    if ($total > $offset) {
      $morePre = true;
    }

    return ['data' => $model, 'pagination' => ['total' => $total, 'offset' => $offset, 'morePre' => $morePre]];
  }

  /**
   * Get conversation data
   * Add new conversation if empty data
   * @return object Conversation
   */
  public function actionPrivate() {
    $recipients = $this->getRequestParam('recipients');
    if (!$recipients) {
      return $this->send(404);
    }
    $data = [];
    foreach ($recipients as $key => $value) {
      $data[] = new \MongoId($value);
    }
    //get conversation private
    $model = Conversation::find()
      ->where([
          'isPrivate' => true,
          'recipients' => [
              '$all' => $data
          ]
      ])
      ->asArray()
      ->one();

    if ($model) {
      if (isset($model['unreadUsers']) && in_array($this->__user->_id, $model['unreadUsers'])) {
        $model['unread'] = true;
      } else {
        $model['unread'] = false;
      }
      return $model;
    } else {
      $model = new Conversation();
      $model->recipients = $recipients;
      $model->isPrivate = true;
      $model->createdBy = $this->__user->_id;
      if ($model->save()) {
        return $model;
      } else {
        return $this->send(400, $model->getErrors(), true);
      }
    }
  }

  /**
   * create new conversation when user access contact page
   */
  public function actionContact() {
    /** if guest chat */
    if (isset($this->__request['isGuest'])) {
      $modelEmail = ContactEmail::findOne(['email' => $this->__request['email']]);

      //if this email was stored
      if ($modelEmail) {
        $model = Conversation::findOne([
              'isPrivate' => true,
              'recipients' => [
                  '$all' => [new \MongoId(ADMIN_CONTACT), $modelEmail->_id]
              ],
              'isUser' => false,
              'isForContact' => true
        ]);
        //if conversation existed
        if ($model) {
          return $model;
        } else {
          //create new conversation
          $model = new Conversation();
          $model->recipients = [ADMIN_CONTACT, (string) $modelEmail->_id];
          $model->isPrivate = true;
          $model->isForContact = true;
          $model->isUser = false;
          if ($model->save()) {
            return $model;
          } else {
            return $this->send(400, $model->getErrors(), true);
          }
        }
      } else {
        //if email doesn't exist
        //add this email to database
        $modelEmail = new ContactEmail();
        $modelEmail->email = $this->__request['email'];
        if ($modelEmail->save()) {
          //create new conversation
          $model = new Conversation();
          $model->recipients = [ADMIN_CONTACT, (string) $modelEmail->_id];
          $model->isPrivate = true;
          $model->isForContact = true;
          $model->isUser = false;
          if ($model->save()) {
            return $model;
          } else {
            return $this->send(400, $model->getErrors(), true);
          }
        } else {
          return $this->send(400, $modelEmail->getErrors(), true);
        }
      }
    } else {

      /** if this is member */
      $recipients = $this->getRequestParam('recipients');
      if (!$recipients) {
        return $this->send(404);
      }
      $data = [];
      foreach ($recipients as $key => $value) {
        $data[] = new \MongoId($value);
      }
      //get conversation private
      $model = Conversation::findOne([
            'isPrivate' => true,
            'recipients' => [
                '$all' => $data
            ],
            'isUser' => true,
            'isForContact' => true
      ]);
      if ($model) {
        return $model;
      } else {
        $model = new Conversation();
        $model->recipients = $recipients;
        $model->isPrivate = true;
        $model->isForContact = true;
        $model->isUser = true;
        if ($model->save()) {
          return $model;
        } else {
          return $this->send(400, $model->getErrors(), true);
        }
      }
    }
  }

  /**
   * Get conversations
   * limit 10
   * load pre conversation by updated date
   * @return array
   */
  public function actionActivity() {
    $limit = 10;
    $morePre = false;
    $updatedAt = '';

    if (isset($_GET['type']) && $_GET['type'] == 'new') {
      $where = [
          'isConversation' => false,
          'recipients' => ['$in' => [$this->__user->_id]],
          'lastMessage' => ['$exists' => true],
          'deletedUsers' => ['$nin' => [$this->__user->_id]]
      ];
    } elseif (isset($_GET['type']) && $_GET['type'] == 'visitor') {
      $where = [
          'recipients' => ['$in' => [$this->__user->_id]],
          'isPrivate' => true
      ];
    } else {
      $where = [
          'isConversation' => true,
          'recipients' => ['$in' => [$this->__user->_id]],
          'lastMessage' => ['$exists' => true],
          'deletedUsers' => ['$nin' => [$this->__user->_id]]
      ];
    }

    //set condition load pre data
    if (isset($_GET['date']) && $_GET['date']) {
      $updatedAt = $_GET['date'];
      $where['updatedAt'] = ['$lt' => new \MongoDate($updatedAt / 1000)];
    }

    $total = Conversation::find()->andWhere($where)->count();

    $models = Conversation::find()
      ->andWhere($where)
      ->orderBy(['updatedAt' => SORT_DESC])
      ->limit($limit)
      ->asArray()
      ->all();

    if ($total > count($models)) {
      $morePre = true;
    }

    /**
     * get user of conversation
     * check status unread message
     * get last message time
     */
    foreach ($models as &$model) {
      //set author conversation
      $author = null;
      if ($model['isPrivate']) {
        $user = User::findOne([
              '_id' => [
                  '$ne' => $this->__user->_id,
                  '$in' => $model['recipients']
              ]
        ]);
        if ($user) {
          $author = $user->getPublicData($this->language, $this->__user);
        }
      }
      $model['author'] = $author;

      //check status unread
      if (isset($model['unreadUsers']) && in_array($this->__user->_id, $model['unreadUsers'])) {
        $model['unread'] = true;
      } else {
        $model['unread'] = false;
      }

      //set last time
      $updatedAt = $model['updatedAt'];
    }

    return ['data' => $models, 'morePre' => $morePre, 'lateUpdatedAt' => $updatedAt];
  }

  /**
   * Get detail an conversation
   * @param ObjectId $id
   */
  public function actionGetConversation($id) {
    $conversation = Conversation::find()
      ->where(['_id' => $id])
      ->asArray()
      ->one();
    if (!$conversation) {
      $this->send(404);
    }

    $author = null;
    if ($conversation['isPrivate']) {
      $user = User::findOne([
            '_id' => [
                '$ne' => $this->__user->_id,
                '$in' => $conversation['recipients']
            ]
      ]);
      if ($user) {
        $author = $user->getPublicData($this->language, $this->__user);
      }
    }
    $conversation['author'] = $author;

    if (isset($conversation['unreadUsers']) && in_array($this->__user->_id, $conversation['unreadUsers'])) {
      $conversation['unread'] = true;
    } else {
      $conversation['unread'] = false;
    }

    return $conversation;
  }

  /**
   * update data for readed conversation
   * @return boolean
   */
  public function actionRead($id) {
    $model = Conversation::findOne($id);
    if (!$model) {
      $this->send(404);
    }
    $model->unreadUsers = array_diff($model->unreadUsers, [$this->__user->_id]);
    if ($model->save()) {
      return true;
    } else {
      return $this->send(400, $model->getErrors(), true);
    }
  }

  /**
   * get total message of user today
   */
  public function actionTotalMessageToday() {
    return ChatConversation::find()
        ->where([
            'createdBy' => $this->__user->_id,
            'createdAt' => ['$gt' => new \MongoDate(mktime(0, 0, 0))]
        ])
        ->count();
  }

}
