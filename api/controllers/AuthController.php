<?php

namespace api\controllers;

use \Yii;
use common\models\User;
use common\helpers\Email;
use common\helpers\GeoLocation;
use common\helpers\String;

/**
 * Description of AuthController
 *
 * @author Tuong Tran <tuong.tran@outlook.com>
 * @version 0.1
 */
class AuthController extends ApiController {

  /**
   * verify login data and return user object
   */
  public function actionLogin() {
    $email = $this->getRequestParam('email');
    $password = $this->getRequestParam('password');
    $remember = $this->getRequestParam('remember');
    $location = $this->getRequestParam('location');

    //if user don't enter required field
    if (!$email || !$password) {
      return $this->send(400, 'LOGIN_EMPTY_ERROR');
    } else {
      $user = User::findByEmail($email);
      //check if account exists
      if ($user && $user->validatePassword($password)) {
        //check if account is verified
        if (!$user->emailVerified) {
          $user->activeKey = String::generateRandomString(20);
          if ($user->save()) {
            return $this->send(403, ['token' => $user->activeKey, 'isVerify' => true]);
          } else {
            return $this->send(500);
          }
        } elseif ($user->status == User::STATUS_DELETED) {
          $user->activeKey = String::generateRandomString(20);
          if ($user->save()) {
            return $this->send(403, ['token' => $user->activeKey]);
          } else {
            return $this->send(500);
          }
        } elseif ($user->status == User::STATUS_BANNED) {
          return $this->send(400, 'ERROR_ACCOUNT_BANNED');
        } else {
          //update login data
          $user->coordinates = [ 'type' => 'Point', 'coordinates' => GeoLocation::getCoordiatesByIp()];
          $user->generateAuthKey();
          $user->lastSignedOn = new \MongoDate();

          if(Yii::$app->session['affiliate']){
            $user->isAffiliate=true;
          }
          $user->save();
          //set session logged user
          Yii::$app->user->login($user, $remember ? 3600 * 24 * 30 : 0);
          $redirectUrl = '';
          if ($location && $location !== $this->country) {
            $redirectUrl = 'http://' . $location . '.' . BASE_URL . '/#/profile/';
          }
          if ($location === 'us') {
            $redirectUrl = 'http://' . BASE_URL . '/#/profile';
          }
          //return user data
          return [
            'data' => $user->getFullPublicData($this->language, $this->height),
            'token' => $user->authKey,
            'redirectUrl' => $redirectUrl
          ];
        }
      } else {
        return $this->send(400, 'LOGIN_NOT_MATCH');
      }
    }
  }

  /**
   * logout api
   * @return boolean
   */
  public function actionLogout() {
    Yii::$app->user->logout();
    return true;
  }

  /**
   * get cookie user
   * @return array
   */
  public function actionGetUser() {
    if ($this->__user) {
      return $this->__user->getFullPublicData($this->language, $this->height);
    } else {
      return $this->send(400);
    }
  }

  /**
   * default response unauthorized
   * @return type
   */
  public function actionUnauthorized() {
    return $this->send(401);
  }

}
