<?php

namespace api\controllers;

use yii\rest\Controller;
use \Yii;
use common\models\Conversation;
use common\models\DateEvent;
use common\models\Interest;
use common\models\Visitor;

class DemoController extends Controller {

  public function actionIndex(){
    return \common\helpers\GeoLocation::getLocationByIp();
  }

  public function actionUpdate(){
    $models=  \common\models\User::findAll([]);
    foreach ($models as $model){
      $model->save();
    }
  }

  public function actionUpdateDate() {
    $model = \common\models\DateEvent::find()->all();
    foreach ($model as $item) {
      $time=  mktime(0, 0, 0, date('n'), date('j')+rand(1,14));
      $item->onDate=new \MongoDate($time);
      $item->save();
    }
  }
}
