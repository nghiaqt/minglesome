<?php

namespace api\controllers;

use common\models\Property;
use \Yii;
use common\models\User;
use common\models\Country;

/**
 * Main api controller
 * @author Tuong Tran <tuong.tran@outlook.com>
 * @version 0.1
 */
class ApiController extends \yii\rest\Controller {

  /**
   *
   * @var mixed request data
   */
  protected $__request = null;

  /**
   * user from access token or session
   * @var common\models\User
   */
  protected $__user = null;
  protected $data = [];
  public $language = 'en';
  public $currency = 'USD';
  public $country = 'us';
  public $height = 'feet';
  public $distance = 'miles';
  public $isIOS=false;
  public $ipCountry= 'us';

  public function __construct($id, $module, $config = array()) {
    parent::__construct($id, $module, $config);

    if (!$this->_checkAuthorized()) {
      $this->send(403);
      Yii::$app->end();
    }

    $this->_getRequest();

    if (Yii::$app->session['country']) {
      $this->country = Yii::$app->session['country'];
      $this->language = Yii::$app->session['language'] ? Yii::$app->session['language'] : $this->language;
      $this->currency = Yii::$app->session['currency'] ? Yii::$app->session['currency'] : $this->currency;
      $this->height = Yii::$app->session['height'] ? Yii::$app->session['height'] : $this->height;
      $this->distance = Yii::$app->session['distance'] ? Yii::$app->session['distance'] : $this->distance;
    } else if (isset($_GET['country']) && $_GET['country']) {
      $this->isIOS=true;
      $this->ipCountry = GeoLocation::getCountryByIp();
      $country = Country::findOne(['countryCode' => $_GET['country']]);
      if ($country) {
        $this->country = $country->countryCode ? $country->countryCode : $this->country;
        $this->language = $country->languageCode ? $country->languageCode : $this->language;
        $this->currency = $country->currency ? $country->currency : $this->currency;
        $this->height = $country->height ? $country->height : $this->height;
        $this->distance = $country->distance ? $country->distance : $this->distance;
      }
    }
  }

  public function init() {
    parent::init();
  }

  /**
   * send response to client
   * @return void
   * @param integer $status
   * @param type $data
   * @param boolean $isError If true. Conver data error of Yii to [key=>value]
   */
  protected function send($status = 200, $data = null, $isError = false) {
    //convert error of Yii ($model->getErrors())
    if ($isError) {
      $errors = [];
      foreach ($data as $itemErrors) {
        foreach ($itemErrors as $item) {
          $errors[] = $item;
        }
      }
      $data = $errors;
    }
    //return error code for translate errors
    if ($status != 200) {
      if ($data) {
        $data = is_string($data) ? [$data] : $data;
      } else {
        $data = ['ERROR_' . $status];
      }
    }
    Yii::$app->response->setStatusCode($status);
    return $data;
  }

  /**
   * get request data from __request var
   * @param string $name
   * @return mixed
   */
  protected function getRequestParam($name) {
    return isset($this->__request[$name]) ? $this->__request[$name] : null;
  }

  /**
   * set http status code
   * @param type $status
   */
  protected function setStatus($status = 200) {
    \Yii::$app->response->setStatusCode($status);
  }

  /**
   * check user has been login or not
   * we share session between domain and subdomain, this is helpful
   * for external call, we will need check the access token
   *
   * @return void
   */
  private function _checkAuthorized() {
    //TODO - check access token
    //if (\Yii::$app->user->isGuest) {
    //  return $this->json(403);
    //}
    if (isset($_GET['token']) && $_GET['token']) {
      $this->__user = User::findOne(['authKey' => $_GET['token']]);
      if ($this->__user) {
        Yii::$app->user->login($this->__user);
      }
    } else {
      $this->__user = Yii::$app->user->identity;
    }
    $r = Yii::$app->getRequest();

    if ($r->isPut || $r->isDelete) {
      if (!$this->__user) {
        return false;
      }
    }
    return true;
  }

  /**
   * get raw body request form application/json request
   * @return void
   */
  private function _getRequest() {
    $request = Yii::$app->getRequest()->getRawBody();
    if (!empty($request)) {
      $this->__request = json_decode($request, true);
    }
  }

  /**
   * get status message form status code
   * @param int $code
   * @return string
   */
  private function _getStatusMessage($code) {
    $status = [
      100 => 'Continue',
      101 => 'Switching Protocols',
      200 => 'OK',
      201 => 'Created',
      202 => 'Accepted',
      203 => 'Non-Authoritative Information',
      204 => 'No Content',
      205 => 'Reset Content',
      206 => 'Partial Content',
      300 => 'Multiple Choices',
      301 => 'Moved Permanently',
      302 => 'Found',
      303 => 'See Other',
      304 => 'Not Modified',
      305 => 'Use Proxy',
      306 => '(Unused)',
      307 => 'Temporary Redirect',
      400 => 'Bad Request',
      401 => 'Unauthorized',
      402 => 'Payment Required',
      403 => 'Forbidden',
      404 => 'Not Found',
      405 => 'Method Not Allowed',
      406 => 'Not Acceptable',
      407 => 'Proxy Authentication Required',
      408 => 'Request Timeout',
      409 => 'Conflict',
      410 => 'Gone',
      411 => 'Length Required',
      412 => 'Precondition Failed',
      413 => 'Request Entity Too Large',
      414 => 'Request-URI Too Long',
      415 => 'Unsupported Media Type',
      416 => 'Requested Range Not Satisfiable',
      417 => 'Expectation Failed',
      500 => 'Internal Server Error',
      501 => 'Not Implemented',
      502 => 'Bad Gateway',
      503 => 'Service Unavailable',
      504 => 'Gateway Timeout',
      505 => 'HTTP Version Not Supported'
    ];
    return ($status[$code]) ? $status[$code] : $status[500];
  }

  /**
   * allow options request from origin
   * @return boolean
   */
  public function actionOptions() {
    return true;
  }

  /**
   * handle error
   * we don't need to show system error to client, simply return 'error' string and html status code
   * @return string
   */
  public function actionError() {
    $exception = \Yii::$app->errorHandler->exception;
    if ($exception !== null) {
      return $exception;
    }

    return null;
  }

  /**
   * the acl for user role
   *
   * @param string $event 'update', 'delete', 'create', 'read'
   * @param mixed $object activerecord object
   */
  protected function userCan($event, $object) {
    //TODO - do it
    if (!in_array($event, ['read', 'create', 'update', 'delete'])) {
      return false;
    }
    //if property is available, check role of object depens on property roles
    if (isset($this->data['property']) && $this->data['property']) {
      if (in_array($event, ['create', 'update', 'delete'])) {
        if (!$this->__user) {
          return false;
        }
        //get current role of user in the property
        foreach ($this->data['property']->roles as $role) {
          if ($role['userId'] == $this->__user->_id) {
            //TODO - get instance of object and test
            //
            if ($role['role'] == 'admin') {
              return true;
            }
          }
        }

        return false;
      }
    }

    return true;
  }

}
