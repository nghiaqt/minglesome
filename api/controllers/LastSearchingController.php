<?php

namespace api\controllers;

//load helper
use yii\filters\AccessControl;
//load model
use common\models\LastSearching;
use common\models\User;
use common\models\ProfileContent;
use \yii\helpers\ArrayHelper;

/**
 * Execute last searching data from user 
 */
class LastSearchingController extends ApiController {

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'access' => [
            'class' => AccessControl::className(),
            'only' => ['index', 'create'],
            'rules' => [
                [
                    'actions' => ['index', 'create'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
                [
                    'allow' => false, // Do not have access
                    'roles' => ['?']
                ]
            ]
        ]
    ];
  }

  /**
   * get last searching
   * @return array
   */
  public function actionIndex() {
    $lastSearching = LastSearching::findOne([
                'createdBy' => $this->__user->_id
            ]);
    if (!$lastSearching) {
      $distance = \common\models\Setting::get('within', $this->distance);
      $lastSearching = new LastSearching;
      //set default distance is index of last distance
      if ($distance) {
        $lastSearching->distance = count($distance) - 1;
      }
      $lastSearching->createdBy = $this->__user->_id;
      $lastSearching->save();
    }
    return $lastSearching->loadDataAsArray($this->height, $this->distance);
  }

  /**
   * save last searching
   * @return type
   */
  public function actionCreate() {
    $lastSearching = LastSearching::findOne([
                'createdBy' => $this->__user->_id
    ]);
    if (!$lastSearching) {
      $lastSearching = new LastSearching;
      $lastSearching->createdBy = $this->__user->_id;
    }
    $heightFrom=$lastSearching->heightFrom;
    $heightFrom[$this->height]=$this->getRequestParam('heightFrom');
    $heightTo=$lastSearching->heightTo;
    $heightTo[$this->height]=  $this->getRequestParam('heightTo');
    unset($this->__request['heightFrom']);
    unset($this->__request['heightTo']);
    $lastSearching->setAttributes($this->__request);
    $lastSearching->heightFrom=$heightFrom;
    $lastSearching->heightTo=$heightTo;

    if ($lastSearching->save()) {
      return $this->send(200, 'success');
    } else {
      return $this->send(400, $lastSearching->getErrors(), true);
    }
  }

}

