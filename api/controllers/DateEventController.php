<?php

namespace api\controllers;

//load helper
use \Yii;
use yii\filters\AccessControl;
//load model
use common\models\DateEvent;
use common\models\DateEventReply;
use common\helpers\GeoLocation;
use common\models\User;
use common\models\ActivityEvent;
use common\models\PrivacyFilter;
use common\models\KeyUser;

/**
 * Date event controller
 */
class DateEventController extends ApiController {

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => [
          'index', 'create', 'search', 'reply', 'get-date-replies', 'activity',
          'get-reply', 'read-reply', 'delete-reply'
        ],
        'rules' => [
          [
            'actions' => [
              'index', 'create', 'search', 'reply', 'get-date-replies', 'activity',
              'get-reply', 'read-reply', 'delete-reply'
            ],
            'allow' => true,
            'roles' => ['@'],
          ],
          [
            'allow' => false, // Do not have access
            'roles' => ['?']
          ]
        ]
      ]
    ];
  }

  /**
   * Get all date event by user id
   * @param string $id This value is user id
   * @return array 
   */
  public function actionIndex($id) {
    return DateEvent::getAllAsArray($id, $this->__user->_id, $this->language);
  }

  /**
   * add new date event
   * if free user. only post 1 event per day
   * @return array
   */
  public function actionCreate() {
    //check premium of user
    if ($this->__user->level < User::LEVEL_PREMIUM) {
      if ($this->__user->level == User::LEVEL_TRIAL) {
        //check event today
        $event = DateEventReply::find()
            ->where([
              'createdBy' => $this->__user->_id,
              'createdAt' => ['$gt' => new \MongoDate(mktime(0, 0, 0))]
            ])
            ->count();
        if ($event > 0) {
          //return error premium
          return $this->send(400,'ERROR_PREMIUM_DATE_EVENT');
        }
      } else {
        //return error premium
        return $this->send(400,'ERROR_PREMIUM_DATE_EVENT');
      }
    }
    // add new date event
    $model = new DateEvent();
    $model->isGroup = $this->getRequestParam('isGroup');
    $model->description = $this->getRequestParam('description');
    $model->onDate = $this->getRequestParam('onDate');
    $model->country = $this->getRequestParam('country');
    $model->city = $this->getRequestParam('city');
    $model->createdBy = $this->__user->_id;
    $model->activityEventId = $this->getRequestParam('activityEventId');

    if ($model->save()) {
      $model->loadDataForCountry($this->language);
      $this->__user->events+=1;
      $this->__user->save();
      return $model;
    } else {
      return $this->send(400, $model->getErrors(), true);
    }
  }

  public function actionDelete($id) {
    $date = DateEvent::findOne(['_id' => $id, 'createdBy' => $this->__user->_id]);
    if (!$date) {
      return $this->send(400);
    }
    if ($date->delete()) {
      DateEventReply::deleteAll(['dateEventId' => $date->_id]);
      return true;
    }
  }

  /**
   * get random event 
   * @return string
   */
  public function actionNearlyEvent() {

    $return = ['data' => [], 'count' => []];
    $addedUser = [$this->__user->_id];

    //get nearby dates/events 		
    if (!Yii::$app->session->get('coordinates')) {
      Yii::$app->session->set('coordinates', GeoLocation::getCoordiatesByIp());
    }
    $coordinates = Yii::$app->session->get('coordinates');
    $startDate = strtotime('-3 days');
    $endDate = strtotime('now');
    $events = DateEvent::find()->where([
      'onDate' => [
        '$gt' => new \MongoDate($startDate),
        '$lt' => new \MongoDate($endDate)
      ]
    ])->all();
    foreach ($events as $event) {
      if (!in_array($event->createdBy,$addedUser) && count($return['data']) <= 6) {
        $where = [
          '_id' => $event->createdBy,
          'coordinates' => [
            '$near' => [
              '$geometry' => [
                'type' => 'Point',
                'coordinates' => $coordinates
              ],
              '$maxDistance' => 100
            ]
          ],
          'status' => ['$lt' => User::STATUS_BANNED]
        ];

        //if user logged in, getting nearby user except current user
        if ($this->__user) {
          $where = array_merge($where, [
            'level' => ['$ne' => User::LEVEL_ADMIN]
          ]);
        }
        //get users in distance 10km
        $user = User::find()->where($where)->one();
        if ($user) {
           $data = $user->getPublicData($this->language, $this->__user);
          array_push($addedUser, $event->createdBy);
          $return['data'][] = $data;
        }
       
      } 
    }
    

    //get random dates/events of nearby users
    // foreach ($users as $user) {
    //   //load public data
    //   $
    //   $return['count'][] = $event;
    // }
    return $return;
  }

  /**
   * reply date event
   */
  public function actionReply() {
    //if this is free account, only allowing to reply one time
    if ($this->__user->level < User::LEVEL_PREMIUM) {
      if ($this->__user->level == User::LEVEL_TRIAL) {
        $reply = DateEventReply::find()
            ->where([
              'createdBy' => $this->__user->_id,
              'createdAt' => ['$gt' => new \MongoDate(mktime(0, 0, 0))]
            ])
            ->count();
        if ($reply > 9) {
          return $this->send(402);
        }
      } else {
        return $this->send(402);
      }
    }
    $dateReply = new DateEventReply();
    $dateReply->createdBy = $this->__user->_id;
    $dateReply->dateEventId = $this->__request['dateEventId'];
    $dateReply->text = $this->__request['text'];
    if ($dateReply->save()) {
      return $dateReply;
    } else {
      return $this->send(400, $dateReply->getErrors(), true);
    }
  }

  /**
   * get replies date
   * @param int $id
   */
  public function actionGetDateReplies($id) {
    $dateReplies = DateEventReply::find()->andWhere([
          'dateEventId' => new \MongoId($id)
        ])->asArray()->all();
    if ($dateReplies) {
      foreach ($dateReplies as &$dateReply) {
        $user = User::findOne([
              '_id' => $dateReply['createdBy']
        ]);
        $dateReply['replierName'] = $user->username;
      }
    }
    return $dateReplies;
  }

  /**
   * get reply date event for activity
   */
  public function actionActivity() {
    $limit = 10;
    $morePre = false;
    $createdAt = '';

    $where = [
      'authorEvent' => $this->__user->_id,
      'onDate' => ['$gt' => new \MongoDate(mktime(0, 0, 0))]
    ];

    //set reply load pre data
    if (isset($_GET['date']) && $_GET['date']) {
      $createdAt = $_GET['date'];
      $where['createdAt'] = ['$lt' => new \MongoDate($createdAt / 1000)];
    }

    $total = DateEventReply::find()->andWhere($where)->count();

    $model = DateEventReply::find()
        ->andWhere($where)
        ->orderBy(['createdAt' => SORT_DESC])
        ->limit($limit)
        ->asArray()
        ->all();

    if ($total > count($model)) {
      $morePre = true;
    }

    /**
     * get user of reply
     * get last message time
     */
    foreach ($model as $key => $value) {
      //set author reply
      $author = null;
      $user = User::findOne($value['createdBy']);

      if ($user) {
        $author = $user->getPublicData($this->language, $this->__user);
      }

      $model[$key]['author'] = $author;

      //set last time
      $createdAt = $value['createdAt'];
    }

    return ['data' => $model, 'morePre' => $morePre, 'lateUpdatedAt' => $createdAt];
  }

  /**
   * Get reply data
   * @param type $id
   * @return type
   */
  public function actionGetReply($id) {
    $model = DateEventReply::find()
        ->andWhere(['_id' => $id])
        ->asArray()
        ->one();
    if (!$model) {
      return $this->send(404);
    }

    //set author reply
    $author = null;
    $user = User::findOne($model['createdBy']);
    if ($user) {
      $author = $user->getPublicData($this->language, $this->__user);
    }
    $model['author'] = $author;

    return $model;
  }

  public function actionReadReply($id) {
    return DateEventReply::updateAll(['unread' => false], ['_id' => new \MongoId($id), 'unread' => true]);
  }

  /**
   * delete an reply
   * @param type $id
   * @return boolean
   */
  public function actionDeleteReply($id) {
    $model = DateEventReply::findOne($id);
    if (!$model) {
      return $this->send(404);
    }
    if ($model->delete()) {
      return true;
    }
  }

}
