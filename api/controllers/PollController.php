<?php

namespace api\controllers;

use \Yii;
use yii\filters\AccessControl;
use common\models\Polls;
use common\models\PollAnswer;
use common\models\Banner;

/**
 * Poll Controller
 *
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class PollController extends ApiController {

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'access' => [
            'class' => AccessControl::className(),
            'only' => [
            ],
            'rules' => [
                [
                    'actions' => [
                    ],
                    'allow' => true,
                    'roles' => ['@'],
                ],
                [
                    'allow' => false, // Do not have access
                    'roles' => ['?']
                ]
            ]
        ]
    ];
  }

  /**
   * get all polls
   */
  public function actionIndex() {
    $answers = PollAnswer::findAll(['createdBy' => $this->__user->_id]);
    $polls = Polls::findAll([
                'status' => STATUS_CONFIRMED,
                '_id' => [
                    '$nin' => \yii\helpers\ArrayHelper::getColumn($answers, 'pollId')
                ],
                'country' => $this->country
    ]);

    $now = new \MongoDate;
    $banners = Banner::find()
            ->where([
                'startDate' => ['$lt' => $now],
                'endDate' => ['$gt' => $now],
                'country' => $this->country
            ])
            ->asArray()
            ->all();
    foreach ($banners as $key => $banner) {
      $banners[$key]['file']['url']=BANNER_URL.$banner['file']['name'];
    }
    return ['banners' => $banners, 'polls' => $polls];
  }

  /**
   * View detail poll
   * load qty select answer
   * @param type $id
   * @return type
   */
  public function actionView($id) {
    //get poll
    $poll = Polls::find()->where(['_id' => new \MongoId($id)])->asArray()->one();
    if (!$poll) {
      return $this->send(404);
    }
    //get total user answer poll
    $poll['totalAnswer'] = PollAnswer::find()->where(['pollId' => $poll['_id']])->count();
    //get total user select answer
    foreach ($poll['answers'] as $key => $value) {
      $total = PollAnswer::find()->where(['pollId' => $poll['_id'], 'answer' => $value])->count();
      $poll['answers'][$key] = ['answer' => $value, 'total' => $total];
    }
    //check answer of this user
    $userAnswer = PollAnswer::findOne(['pollId' => $poll['_id'], 'createdBy' => $this->__user->_id]);
    $poll['userAnswer'] = $userAnswer ? $userAnswer->answer : '';
    return $poll;
  }

  /**
   * user answer poll question
   * @param string $id This value is poll id
   * @return boolean
   */
  public function actionAnswer($id) {
    $poll = Polls::findOne($id);
    if (!$poll || $this->getRequestParam('answer') === '') {
      return $this->send(404);
    }
    $model = PollAnswer::findOne(['pollId' => $poll->_id, 'createdBy' => $this->__user->_id]);
    if (!$model) {
      $model = new PollAnswer();
      $model->createdBy = $this->__user->_id;
      $model->pollId = $poll->_id;
    }
    $model->answer = (string) $this->getRequestParam('answer');
    if ($model->save()) {
      return true;
    } else {
      return $this->send(400, $model->getErrors(), true);
    }
  }

}