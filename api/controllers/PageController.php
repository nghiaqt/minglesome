<?php

namespace api\controllers;

use \Yii;
use common\models\Page;
use common\models\Setting;

/**
 * Page Controller
 *
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class PageController extends ApiController {
  
  /**
   * Get page content
   * @param type $type
   * @return type
   */
  public function actionGet($type){
    $model=  Page::getPageByType($type, $this->language);
    if(!$model){
      return $this->send(404);
    }
    return $model;
  }
  
  /**
   * get content on home page
   * @return array
   */
  public function actionHome(){
    $return =[];
    $return['social']=  Setting::get('social');
    $return['membersSay']=  Setting::get('member-say', $this->language);
    return $return;
  }
}