<?php

namespace api\controllers;

//load helper
use \Yii;
use yii\rest\Controller;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use common\helpers\Email;
use common\helpers\GeoLocation;
use common\helpers\String;
//load model
use common\models\User;
use common\models\Reason;
use common\models\UserFeedback;
use common\models\PrivacyFilter;
use common\models\BlockUser;
use common\models\Interest;
use common\models\Picture;
use common\models\ReportUser;
use common\models\KeyUser;
use common\models\DateEvent;
use common\models\RecurringPaymentsProfile;
use common\models\Setting;

/**
 * User controller
 * @author Tuong Tran <tuong.tran@outlook.com>
 * @version 0.1
 */
class UserController extends ApiController {

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
        'access' => [
            'class' => AccessControl::className(),
            'only' => [
                'index', 'update', 'reason', 'stop-subscription', 'cancel-membership', 'search',
                'report-user', 'block-user', 'unblock-user', 'is-blocked', 'interest', 'uninterest',
                'log-activity', 'is-interested', 'key', 'activity', 'check-permission-view',
                'update-prompt'
            ],
            'rules' => [
                [
                    'actions' => [
                        'index', 'update', 'reason', 'stop-subscription', 'cancel-membership', 'search',
                        'report-user', 'block-user', 'unblock-user', 'is-blocked', 'interest', 'uninterest',
                        'log-activity', 'is-interested', 'key', 'activity', 'check-permission-view',
                        'update-prompt'
                    ],
                    'allow' => true,
                    'roles' => ['@'],
                ],
                [
                    'allow' => false, // Do not have access
                    'roles' => ['?']
                ]
            ]
        ]
    ];
  }

  /**
   * Edit account
   * Send email notify to user if change password
   * Send email notify to old email if change email
   * @param string $id
   * @return array
   */
  public function actionUpdate() {
    $model = $this->__user;

    //get data update
    $model->username = $this->getRequestParam('username');

    //update new password
    $isNewPassword = $this->getRequestParam('isNewPassword') ? true : false;
    if ($isNewPassword) {
      if ($model->validatePassword($this->getRequestParam('password'))) {
        $model->setPassword($this->getRequestParam('newPassword'));
      } else {
        return $this->send(400, ['newPassword' => 'ERROR_PASSWORD']);
      }
    }
    //update new email
    $isNewEmail = $this->getRequestParam('isNewEmail') ? true : false;
    if ($isNewEmail) {
      $oldEmail = $model->email;
      $model->email = $this->getRequestParam('newEmail');
    }
    //save
    if ($model->save()) {
      //check user change email
      if ($isNewEmail) {
        //send email notify change email to both new & pre email
        Email::sendNotifyChangeEmail($model, $oldEmail);
      }
      //check user change password
      if ($isNewPassword) {
        //check user change email
        if ($isNewEmail) {
          //send email notify change password to both new & pre email
          Email::sendNotifyChangeNewPassword($model, $this->getRequestParam('newPassword'), $oldEmail);
        } else {
          //send email notify change password to new email
          Email::sendNotifyChangeNewPassword($model, $this->getRequestParam('newPassword'));
        }
      }
      return $model->getFullPublicData($this->language, $this->height);
    } else {
      return $this->send(400, $model->getErrors(), true);
    }
  }

  /**
   * add prompt is not show again
   */
  public function actionUpdatePrompt() {
    $user = $this->__user;
    $prompts=$user->prompts;
    $prompts[] = $this->getRequestParam('prompt');
    $user->prompts=$prompts;
    if ($user->save()) {
      return true;
    } else {
      return $this->send(400);
    }
  }

  /**
   * Update lastActivity of user
   * @return type
   */
  public function actionLogActivity() {
    $model = $this->__user;
    if ($model->save()) {
      return ['lastActivity' => $model->lastActivity];
    }
  }

  /**
   * Stop premium account of user
   * @return boolean
   */
  public function actionStopSubscription() {
    //check user id
    $model = User::findIdentity($this->__user->_id);
    if (!$model) {
      return $this->send(404);
    }
    if ($model->level != User::LEVEL_PREMIUM) {
      return $this->send(400);
    }
    //add feedback reason
    if ($this->getRequestParam('reasons')) {
      $feedback = new UserFeedback();
      $feedback->email = $model->email;
      $feedback->createdBy = $model->_id;
      $feedback->type = UserFeedback::TYPE_SUB;
      $feedback->feedback = $this->getRequestParam('feedback');
      $feedback->reasons = $this->getRequestParam('reasons');
      $feedback->save();
    }
    //set free account
    $model->level = User::LEVEL_FREE;
    if ($model->save()) {
      //cancel recurring payment on paypal
      $recuring = RecurringPaymentsProfile::findOne([
                  'profileStatus' => 'Active',
                  'createdBy' => $this->__user->_id
      ]);
      if ($recuring) {
        $paypal = Yii::$app->Paypal;
        $setting = Setting::get('paypal');
        if ($setting) {
          $paypal->apiLive = $setting['live'] ? true : false;
          $paypal->apiUsername = $setting['username'];
          $paypal->apiPassword = $setting['password'];
          $paypal->apiSignature = $setting['signature'];
        }
        $paypal->SetPaypalUrl();
        $paypal->isRecurring = true;
        $result = $paypal->ManageRecurringPaymentsProfileStatus($recuring->profileId);
        if (!$paypal->isCallSucceeded($result)) {
          return $this->send(400, ['FAIL_TO_CANCEL_RECURRING_PAYMENT']);
        }
        $recuring->cancelParams = $result;
        $recuring->profileStatus = 'Cancelled';
        $recuring->save();
      }
      return true;
    } else {
      return $this->send(400, ['STOP_SUBSCRIPTION_ERROR']);
    }
  }

  /**
   * Delete account
   * update status deleted for user
   * account & all info will be delete at 30day later
   * @return boolean
   */
  public function actionCancelMembership() {
    //check user id
    $model = $this->__user;
    if (!$model) {
      return $this->send(404);
    }
    //add feedback reason
    if ($this->getRequestParam('reasons')) {
      $feedback = new UserFeedback();
      $feedback->email = $model->email;
      $feedback->createdBy = $model->_id;
      $feedback->type = UserFeedback::TYPE_MEM;
      $feedback->feedback = $this->getRequestParam('feedback');
      $feedback->reasons = $this->getRequestParam('reasons');
      $feedback->save();
    }
    //set deleted status
    $model->status = User::STATUS_DELETED;
    $model->expiredDelayCancel = new \MongoDate(time('+10 days'));
    if ($model->save()) {
      Email::sendNotifyCancelMembership($model);
      return true;
    } else {
      return $this->send(400, ['CANCEL_MEMBERSHIP_ERROR']);
    }
  }

  /**
   * Undelete account
   * Send email confirm open account to user
   * @return boolean
   */
  public function actionRestoreMembership() {
    if (!$this->getRequestParam('key')) {
      return $this->send(404);
    }
    $model = User::findOne(array('activeKey' => $this->getRequestParam('key')));
    if (!$model) {
      return $this->send(404);
    }

    $model->activeKey = String::generateRandomString(20);
    $model->save();
    Email::sendEmailActiveRestoreMembership($model);
    return true;
  }

  /**
   * Undelete user
   * update status active for user
   * @return boolean
   */
  public function actionActiveRestore() {
    if (!$this->getRequestParam('activeKey')) {
      return $this->send(404);
    }
    $model = User::findOne(array('activeKey' => $this->getRequestParam('activeKey')));
    if (!$model) {
      return $this->send(404);
    }
    $model->status = User::STATUS_NEW;
    $model->activeKey = '';
    if ($model->save()) {
      return true;
    } else {
      return $this->send(400);
    }
  }

  /**
   * get all genders
   * @return json
   */
  public function actionGetGenders() {
    return User::getGenders();
  }

  /**
   * Create new account
   * send email active account to user
   * @return User
   */
  public function actionSignup() {
    //add signup data
    $request = Yii::$app->getRequest()->getRawBody();
    $data = json_decode($request, true);
    $user = new User();
    $user->username = $data['username'];
    $user->email = $data['email'];
    $user->password = $data['password'];
    $user->birthdate = $data['birthdate'];
    $user->gender = $data['gender'];

    //validate data
    if ($user->validate()) {
      //update location of user by ip
      $user->coordinates = [ 'type' => 'Point', 'coordinates' => GeoLocation::getCoordiatesByIp()];

      //update user age by birthdate
      $curYear = date('Y', time());
      $curYearAge = date('Y', strtotime($user->birthdate));
      $user->age = $curYear - $curYearAge;

      //hash pasword
      $user->setPassword($user->password);
      $user->generateAuthKey();

      //create active url for email
      $activeKey = String::generateRandomString(20);
      $user->activeKey = $activeKey;

      //update invite id if user signup from affiliate url
      if (Yii::$app->session['invite']) {
        $user->inviteId = Yii::$app->session['invite'];
      }

      //save data
      if ($user->save()) {
        Email::sendActiveKey($user,$this->country, $this->isIOS);
        return $user;
      }
    }
    return $this->send(400, $user->getErrors(), true);
  }

  /**
   * verify email account
   *
   * @return string
   */
  public function actionActiveAccount() {
    $activeKey = $this->getRequestParam('activeKey');
    if (!$activeKey) {
      return $this->send(404);
    }

    $user = User::findOne(['activeKey' => $activeKey]);
    if ($user) {
      if ($this->getRequestParam('isResendVerifyEmail')) {
        $user->activeKey = String::generateRandomString(20);
        if ($user->save()) {
          Email::sendActiveKey($user, $this->isIOS);
          return true;
        }
      } else {
        $user->emailVerified = true;
        $user->activeKey = '';
        $user->save();
        return 'ACTIVE_SUCCESS';
      }
    } else {
      return $this->send(400);
    }
  }

  /**
   * @return string
   */
  public function actionRecoveryPassword() {
    $request = Yii::$app->getRequest()->getRawBody();
    $data = json_decode($request, true);
    if ($data['email']) {
      if (filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
        $user = User::findByEmail($data['email']);
        if ($user) {
          $newPass = String::generateRandomString(20);
          $user->setPassword($newPass);
          $user->save();
          Email::sendNewPassword($user->username, $newPass, $user->email);
          return 'RECOVERY_PASS_SUCCESS';
        } else {
          Yii::$app->response->setStatusCode(400);
          return 'EMAIL_NOT_EXIST';
        }
      } else {
        Yii::$app->response->setStatusCode(400);
        return 'INVALID_EMAIL';
      }
    } else {
      Yii::$app->response->setStatusCode(400);
      return 'INVALID_EMAIL';
    }
  }

  /**
   * search user based on username, genders, ...
   * search lounge
   * search dates/events
   * check total dates/events from start date to end date
   * @return Array List public data of user
   */
  public function actionSearch() {
    //default data return
    $return = ['data' => [],
        'total' => 0,
        'isMorePage' => false,
        'offset' => 0
    ];

    //default conditions
    $where = [
        'status' => ['$lt' => User::STATUS_BANNED]
    ];
    //search lounge page: search users who have the public streaming
    if ($this->getRequestParam('isPublicStreaming')) {
      //empty user public camera
      if (!$this->getRequestParam('streamingUsers')) {
        return $return;
      }

      $streamingIds = [];
      foreach ($this->getRequestParam('streamingUsers') as $streamingUser) {
        if ($streamingUser !== (string) $this->__user->_id) {
          $streamingIds[] = new \MongoId($streamingUser);
        }
      }
      $where['_id'] = ['$in' => $streamingIds];
    }

    //search dates/event page: search users by dates based on day of month and month
    if ($this->getRequestParam('dayOfMonth') || $this->getRequestParam('month') ||
            $this->getRequestParam('year')) {
      $dates = [];
      if (!$this->getRequestParam('dates') && !$this->getRequestParam('events')) {
        $dates = DateEvent::getCollection()->aggregate([
            [
                '$project' => [
                    'month' => [
                        '$month' => '$onDate'
                    ],
                    'day' => [
                        '$dayOfMonth' => '$onDate'
                    ],
                    'year' => [
                        '$year' => '$onDate'
                    ],
                    'createdBy' => '$createdBy',
                    'name' => '$name'
                ]
            ],
            [
                '$match' =>
                [
                    'day' => intval($this->getRequestParam('dayOfMonth')),
                    'month' => intval($this->getRequestParam('month')),
                    'year' => intval($this->getRequestParam('year')),
                    'createdBy' => [
                        '$nin' => [$this->__user->_id]
                    ]
                ]
            ],
            [
                '$group' =>
                [
                    '_id' => '$createdBy',
                    'name' => [
                        '$first' => '$name'
                    ],
                ]
            ]
        ]);
      } elseif ($this->getRequestParam('dates') || $this->getRequestParam('events')) {
        $dates = DateEvent::getCollection()->aggregate([
            [
                '$project' => [
                    'month' => [
                        '$month' => '$onDate'
                    ],
                    'day' => [
                        '$dayOfMonth' => '$onDate'
                    ],
                    'year' => [
                        '$year' => '$onDate'
                    ],
                    'createdBy' => '$createdBy',
                    'name' => '$name',
                    'isGroup' => '$isGroup'
                ]
            ],
            [
                '$match' =>
                [
                    'day' => intval($this->getRequestParam('dayOfMonth')),
                    'month' => intval($this->getRequestParam('month')),
                    'year' => intval($this->getRequestParam('year')),
                    'createdBy' => [
                        '$nin' => [$this->__user->_id]
                    ],
                    'isGroup' => !$this->getRequestParam('events')
                ]
            ],
            [
                '$group' =>
                [
                    '_id' => '$createdBy',
                    'name' => [
                        '$first' => '$name'
                    ],
                ]
            ]
        ]);
      }
      //check empty dates/events
      if (empty($dates)) {
        return $return;
      }
      //load list user
      foreach ($dates as $date) {
        $userIds[] = $date['_id'];
      }
      $where['_id'] = ['$in' => $userIds];
    }

    if ($this->getRequestParam('startDate') && $this->getRequestParam('endDate')) {
      $startDate = strtotime($this->getRequestParam('startDate'));
      $endDate = strtotime($this->getRequestParam('endDate')) + 3600 * 24;
      $query = DateEvent::find()
              ->andWhere([
          'onDate' => [
              '$lt' => new \MongoDate($endDate),
              '$gt' => new \MongoDate($startDate)
          ],
          'createdBy' => [
              '$nin' => [$this->__user->_id]
          ]
      ]);
      if ($this->getRequestParam('dates') || $this->getRequestParam('events')) {
        $query->andWhere(['isGroup' => !$this->getRequestParam('events')]);
      }
      $dates = $query->all();
      //check empty dates/events
      if (empty($dates)) {
        return $return;
      }
      //load list user
      foreach ($dates as $date) {
        $userIds[] = $date['createdBy'];
      }
      $where['_id'] = ['$in' => $userIds];
    }
    //get request data
    $request = [
        'username' => $this->getRequestParam('username'),
        'genders' => $this->getRequestParam('genders'),
        'statuses' => $this->getRequestParam('statuses'),
        'styles' => $this->getRequestParam('styles'),
        'upfors' => $this->getRequestParam('upfors'),
        'others' => $this->getRequestParam('others'),
        'ethnicities' => $this->getRequestParam('ethnicities'),
        'religions' => $this->getRequestParam('religions'),
        'figures' => $this->getRequestParam('figures'),
        'speakings' => $this->getRequestParam('speakings'),
        'orientations' => $this->getRequestParam('orientations'),
        'city' => $this->getRequestParam('city'),
        'state' => $this->getRequestParam('state'),
        'country' => $this->getRequestParam('country'),
        'heightFrom' => $this->getRequestParam('heightFrom'),
        'heightTo' => $this->getRequestParam('heightTo'),
        'ageFrom' => $this->getRequestParam('ageFrom'),
        'ageTo' => $this->getRequestParam('ageTo'),
        'distance' => $this->getRequestParam('distance'),
        'sortMode' => $this->getRequestParam('sortMode'),
        'keywords' => $this->getRequestParam('keywords'),
        'offline' => $this->getRequestParam('offline') === 'true' || $this->getRequestParam('offline') === true ? true : false,
        'premium' => $this->getRequestParam('premium') === 'true' || $this->getRequestParam('premium') === true ? true : false,
        'picture' => $this->getRequestParam('picture') === 'true' || $this->getRequestParam('picture') === true ? true : false,
        'interested' => $this->getRequestParam('interested') === 'true' || $this->getRequestParam('interested') === true ? true : false,
        'interesting' => $this->getRequestParam('interesting') === 'true' || $this->getRequestParam('interesting') === true ? true : false,
        'filter' => $this->getRequestParam('filter') === 'true' || $this->getRequestParam('filter') === true ? true : false,
        'pageSize' => $this->getRequestParam('pageSize'),
        'currentPage' => $this->getRequestParam('currentPage')
    ];

    //get list ids not in filter
    $notInIds = [$this->__user->_id];

    //get user non premium
    if (!$this->getRequestParam('isPublicStreaming') && $request['offline']) {
      $where['isLogged'] = true;
    }
    //get user non premium
    if (!$this->getRequestParam('isPublicStreaming') && $request['premium']) {
      $where['level'] = ['$ne' => User::LEVEL_PREMIUM];
    }
    //get user not picture
    if (!$this->getRequestParam('isPublicStreaming') && $request['picture']) {
      $where['photos'] = 0;
    }
    //will not return members in results that are NOT interested in this user.
    if ($request['interested']) {
      $interested = Interest::findAll(['interestedUser' => $this->__user->_id, 'isInterest' => false]);
      if ($interested) {
        $notInIds = array_merge($notInIds, ArrayHelper::getColumn($interested, 'createdBy'));
      }
    }
    //will not return members in results that user is NOT interesting in.
    if ($request['interesting']) {
      $interesting = Interest::findAll(['createdBy' => $this->__user->_id, 'isInterest' => false]);
      if ($interesting) {
        $notInIds = array_merge($notInIds, ArrayHelper::getColumn($interesting, 'interestedUser'));
      }
    }

    //get user filter mismatch
    if ($request['filter']) {
      $privacyFilters = PrivacyFilter::findAll(['createdBy' => ['$ne' => $this->__user->_id]]);
      foreach ($privacyFilters as $item) {
        if ($item->isMatchPrivacyFilter($this->__user)) {
          $notInIds[] = $item->createdBy;
        }
      }
    }
    $where['_id']['$nin'] = $notInIds;

    //search by username
    if ($request['username']) {
      $where['username'] = ['$regex' => $request['username'], '$options' => 'i'];
    }
    //search by gender
    if ($request['genders']) {
      $where['gender'] = ['$nin' => $request['genders']];
    }
    //search by status married
    if ($request['statuses']) {
      $where['statusMarried'] = ['$nin' => $request['statuses']];
    }
    //search by orientation
    if ($request['orientations']) {
      $orientations = [];
      foreach ($request['orientations'] as $orientation) {
        $orientations[] = new \MongoId($orientation);
      }
      $where['orientation'] = ['$nin' => $orientations];
    }
    //search by age
    if ($request['ageFrom'] || $request['ageTo']) {
      $queryAge = [];
      if ($request['ageFrom']) {
        $queryAge['$gt'] = (int) $request['ageFrom'];
      }
      if ($request['ageTo']) {
        $queryAge['$lt'] = (int) $request['ageTo'];
      }
      $where['age'] = $queryAge;
    }
    //search by upfor
    if ($request['upfors']) {
      $upfors = [];
      foreach ($request['upfors'] as $upfor) {
        $upfors[] = new \MongoId($upfor);
      }
      $where['upfors'] = ['$nin' => $upfors];
    }
    //search by others
    if ($request['others']) {
      $others = [];
      foreach ($request['others'] as $other) {
        $others[] = new \MongoId($other);
      }
      $where['others'] = ['$nin' => $others];
    }
    //search by ethnicity
    if ($request['ethnicities']) {
      $ethnicities = [];
      foreach ($request['ethnicities'] as $ethnicity) {
        $ethnicities[] = new \MongoId($ethnicity);
      }
      $where['ethnicity'] = ['$nin' => $ethnicities];
    }
    //search by religion
    if ($request['religions']) {
      $religions = [];
      foreach ($request['religions'] as $religion) {
        $religions[] = new \MongoId($religion);
      }
      $where['religion'] = ['$nin' => $religions];
    }
    //search by figure
    if ($request['figures']) {
      $figures = [];
      foreach ($request['figures'] as $figure) {
        $figures[] = new \MongoId($figure);
      }
      $where['figure'] = ['$nin' => $figures];
    }
    //search by height
    if ($request['heightFrom'] || $request['heightTo']) {
      $queryHeight = [];
      if ($request['heightFrom']) {
        $queryHeight['$gt'] = (float) $request['heightFrom'];
      }
      if ($request['heightTo']) {
        $queryHeight['$lt'] = (float) $request['heightTo'];
      }
      if ($this->height) {
        $where['height.' . $this->height] = $queryHeight;
      } else {
        $where['height.cm'] = $queryHeight;
      }
    }
    //search by speaking
    if ($request['speakings']) {
      $speakings = [];
      foreach ($request['speakings'] as $speaking) {
        $speakings[] = new \MongoId($speaking);
      }
      $where['speakings'] = ['$nin' => $speakings];
    }
    //search by style
    if ($request['styles']) {
      $styles = [];
      foreach ($request['styles'] as $style) {
        $styles[] = new \MongoId($style);
      }
      $where['styles'] = ['$nin' => $styles];
    }
    //search by keywords
    if ($request['keywords']) {
      $keywords = explode(',', trim($request['keywords']));
      $where['keywords'] = ['$in' => $keywords];
    }

    //search by geocode city, state, country
    if ($request['country']) {
      $where['country'] = $request['country'];
      if ($request['city']) {
        $stateName = '';
        $state = \common\models\State::findOne($request['state']);
        if ($state) {
          $stateName = $state->name;
        }
        $distance = \common\models\Setting::get('within', 'km');
        if ($distance && isset($distance[$request['distance']])) {
          $request['distance'] = $distance[$request['distance']];
        } else {
          $request['distance'] = $distance[4];
        }
        $coordinates = GeoLocation::getCoordiatesByAddress($request['city'] . ' ' . $stateName . ' ' . $request['country']);
        if ($coordinates) {
          $distance = (int) $request['distance'] * 1000;
          $where['coordinatesByAddress'] = [
              '$near' => [
                  '$geometry' => [
                      'type' => 'Point',
                      'coordinates' => $coordinates
                  ],
                  '$maxDistance' => $distance
          ]];
        }
      } elseif ($request['state']) {
        $where['state'] = $request['state'];
      }
    } else {
      //sort distance
      if ($request['sortMode'] === 'distance') {
        //get nearby dates/events
        if (!Yii::$app->session->get('coordinates')) {
          Yii::$app->session->set('coordinates', GeoLocation::getCoordiatesByIp());
        }
        $coordinates = Yii::$app->session->get('coordinates');
        //get users in distance 10000000000000000000000km
        $where['coordinates'] = [
            '$near' => [
                '$geometry' => [
                    'type' => 'Point',
                    'coordinates' => $coordinates
                ],
                '$maxDistance' => 10000000000000000000000000000000000
        ]];
      }
    }

    $query = User::find()->where($where);
    //get user by conditions
    if ($request['sortMode'] && $request['sortMode'] !== 'distance') {
      //sort
      switch ($request['sortMode']) {
        case 'newest': $orderby = ['createdAt' => SORT_DESC];
          break;
        case 'lastLoggedIn': $orderby = ['lastSignedOn' => SORT_DESC];
          break;
        case 'alphabet': $orderby = ['username' => SORT_ASC];
          break;
        default :
          $orderby = ['createdAt' => SORT_DESC];
          break;
      }
      $query->addOrderBy($orderby);
    }

    if ((int) $request['pageSize'] > 0) {
      $total = User::find()->where($where)->count();
      $query->limit($request['pageSize']);
      $offset = 0;
      if ((int) $request['currentPage'] > 1) {
        $offset = (int) $request['pageSize'] * ((int) $request['currentPage'] - 1);
      }
      $query->offset($offset);
      $return['offset'] = $offset;
      $return['total'] = $total;
      $return['isMorePage'] = ($total > $offset + $request['pageSize']);
    } else {
      $query->limit(1000);
    }

    $users = $query->all();

    //set return data
    if ($users) {
      foreach ($users as $user) {
        //load public data
        $data = $user->getPublicData($this->language, $this->__user);
        $return['data'][] = $data;
      }
    }
    return $return;
  }

  /**
   * send report user to admin
   * @return boolean
   */
  public function actionReportUser() {
    $user = User::findOne($this->getRequestParam('reportedUser'));
    if (!$user || $user->_id == $this->__user->_id) {
      return $this->send(404);
    }
    $model = new ReportUser();
    $model->createdBy = $this->__user->_id;
    $model->reportedUser = $this->getRequestParam('reportedUser');
    $model->message = $this->getRequestParam('message');
    if ($model->save()) {
      $user->reported+=1;
      $user->save();
      return true;
    } else {
      return $this->send(400, $model->getErrors(), true);
    }
  }

  /**
   * Block user
   * @return string
   */
  public function actionBlockUser() {
    $blockedUser = $this->getRequestParam('blockedUser');
    $model = BlockUser::findOne([
                'blocker' => $this->__user->_id,
                'blockedUser' => new \MongoId($blockedUser)
    ]);
    if (!$model) {
      $user = User::findOne($blockedUser);
      if (!$user || $user->_id == $this->__user->_id) {
        return $this->send(404);
      }
      $blockUser = new BlockUser();
      $blockUser->blocker = new \MongoId($this->__request['blocker']);
      $blockUser->blockedUser = new \MongoId($this->__request['blockedUser']);
      if ($blockUser->save()) {
        $user->blocked+=1;
        $user->save();
        return $this->send(200, 'block success');
      } else {
        return $this->send(400, 'block is not successful');
      }
    }
  }

  /**
   * unblock user
   */
  public function actionUnblockUser() {
    $blockUser = BlockUser::findOne([
                'blocker' => $this->__user->_id,
                'blockedUser' => new \MongoId($this->__request['blockedUser'])
    ]);
    if ($blockUser) {
      $blockUser->delete();
      $user = User::findOne($this->__request['blockedUser']);
      if ($user) {
        $user->blocked-=1;
        $user->save();
      }
    }
  }

  /**
   * check if user is blocked
   */
  public function actionIsBlocked() {
    $blockUser = BlockUser::findOne([
                'blocker' => new \MongoId($this->__request['blocker']),
                'blockedUser' => new \MongoId($this->__request['blockedUser'])
    ]);
    if ($blockUser) {
      return ['data' => true];
    }
    return ['data' => false];
  }

  /**
   * set interest for user
   */
  public function actionInterest() {
    if (!$this->getRequestParam('interestedUser')) {
      return $this->send(404);
    }
    $interestInfo = [
        'interest' => [],
        'interesting' => [],
        'hasMutalInterest' => false
    ];
    $interest = Interest::findOne([
                'createdBy' => $this->__user->_id,
                'interestedUser' => new \MongoId($this->getRequestParam('interestedUser'))
    ]);
    if (!$interest) {
      $interest = new Interest();
      $interest->interestedUser = new \MongoId($this->getRequestParam('interestedUser'));
      $interest->createdBy = $this->__user->_id;
      $interest->unread = false;
    }
    $interest->isInterest = true;
    if ($interest->save()) {
      //check if this user has mutual friend
      $mutualInterest = Interest::find()->where([
                  'createdBy' => new \MongoId($this->__request['interestedUser']),
                  'interestedUser' => $this->__user->_id,
                  'isInterest' => true
              ])->asArray()->one();
      if ($mutualInterest) {
        $user = User::findOne($this->__request['interestedUser']);
        if ($user) {
          $interestInfo['hasMutalInterest'] = true;

          $interestInfo['interest'] = $interest->toArray();
          $interestInfo['interest']['author'] = $user->getPublicData($this->language, $this->__user);

          $interestInfo['interesting'] = $mutualInterest;
          $interestInfo['interesting']['author'] = $this->__user->getPublicData($this->language, $this->__user);
        }
      }
      return $this->send(200, $interestInfo);
    } else {
      return $this->send(400, 'interest is not successful');
    }
  }

  /**
   * uniterested user
   */
  public function actionUninterest() {
    $interestInfo = [
        'interest' => [],
        'interesting' => [],
        'hasMutual' => false
    ];
    $interest = Interest::findOne([
                'createdBy' => $this->__user->_id,
                'interestedUser' => new \MongoId($this->__request['interestedUser'])
    ]);
    if ($interest) {
      $mutualInterest = Interest::findOne([
                  'createdBy' => new \MongoId($this->__request['interestedUser']),
                  'interestedUser' => $this->__user->_id,
                  'isInterest' => true
      ]);
      if ($mutualInterest) {
        $interestInfo['hasMutual'] = true;
        $interestInfo['interest'] = $interest;
        $interestInfo['interesting'] = $mutualInterest;
      }
      $interest->isInterest = false;
      $interest->save();
      return $interestInfo;
    } else {
      return $this->send(404);
    }
  }

  /**
   * check if user is interested
   */
  public function actionIsInterested() {
    if (!$this->getRequestParam('interestedUser')) {
      return $this->send(404);
    }
    if ($this->__user->_id == new \MongoId($this->getRequestParam('interestedUser'))) {
      return $this->send(400);
    }
    $interest = Interest::findOne([
                'createdBy' => $this->__user->_id,
                'interestedUser' => new \MongoId($this->getRequestParam('interestedUser'))
    ]);
    if ($interest) {
      return $interest->isInterest ? 1 : 0;
    } else {
      return 2;
    }
  }

  /**
   * Check user is key user
   * add key user
   * revoke key user
   */
  public function actionKey() {
    $mode = $this->getRequestParam('mode');
    $userId = $this->getRequestParam('userId');
    if ($userId) {
      $userId = new \MongoId($userId);
      $keyUser = KeyUser::findOne(['keyUser' => $userId, 'createdBy' => $this->__user->_id]);
      if ($mode == 'isKey') {
        return (bool)$keyUser;
      } elseif ($mode == 'key' && !$keyUser) {
        $keyUser = new KeyUser();
        $keyUser->keyUser = $userId;
        $keyUser->createdBy = $this->__user->_id;
        return (bool)$keyUser->save();
      } elseif ($mode == 'revoke' && $keyUser) {
        return (bool)$keyUser->delete();
      }
    }
  }

  /**
   * get all activities of user
   * @param int $id is id of user
   */
  public function actionActivity($id) {
    $activities = [];
    //get persons whom this user like
    $interests = Interest::find()
            ->andWhere(['createdBy' => $this->__user->_id, 'isInterest' => true])
            ->orderBy(['createdAt' => SORT_DESC])
            ->all();
    //check if these persons like the current user
    foreach ($interests as $interest) {
      $isMutualInterest = Interest::findOne([
                  'createdBy' => $interest->interestedUser,
                  'interestedUser' => $this->__user->_id,
                  'isInterest' => true
      ]);
      if ($isMutualInterest) {//if this person has mutual interest with current user
        $user = User::findOne($interest->interestedUser);
        if ($user) {
          $data = $interest->toArray();
          $data['author'] = $user->getPublicData($this->language, $this->__user);
          $activities[] = $data;
        }
      }
    }
    return $activities;
  }

  /**
   *
   * @param type $id
   * @return type
   */
  public function actionReadActivity($id) {
    return Interest::updateAll(['unread' => false], ['_id' => new \MongoId($id)]);
  }

  /**
   * get information of active admin
   * @param string $id is user id
   * @param string $type determine type of user
   */
  public function actionGetActiveAdminInfo($type, $id) {
    $model = User::findOne(['_id' => new \MongoId($id)]);
    if (!$model) {
      return $this->send(404);
    }
    $user = $model->getFullPublicData($this->language, $this->height);
    $galleries = Picture::find()->where([
                        'createdBy' => $model->_id
                    ])
                    ->orderBy([
                        'isAvatar' => SORT_DESC,
                        'createdAt' => SORT_DESC
                    ])
                    ->asArray()->all();
    $user['gallery'] = [];
    foreach ($galleries as $item) {
      $user['gallery'][] = ['_id' => $item['_id'], 'url' => PHOTO_UPLOAD_URL . $item['fileName']];
    }

    return $user;
  }

  /**
   * check permission of user when click view an profile
   * @param type $id
   */
  public function actionCheckPermissionView($id) {
    if (!$id) {
      return $this->send(404);
    }
    $id = new \MongoId($id);
    $block = BlockUser::findOne(['blocker' => $id, 'blockedUser' => $this->__user->_id]);
    if ($block) {
      return $this->send(400, 'NOTIFY_BLOCKED_USER');
    }
    $key = KeyUser::findOne(['keyUser' => $this->__user->_id, 'createdBy' => $id]);
    if (!$key) {
      $privacyFilter = PrivacyFilter::findOne(['createdBy' => $id]);
      if ($privacyFilter && $privacyFilter->isMatchPrivacyFilter($this->__user)) {
        return $this->send(400, 'NOTIFY_PRIVACY_FILTER_USER');
      }
    }
    return true;
  }

}
