<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'appAPI',
    'language' => 'en-US',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'api\controllers',
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'loginUrl'=>'auth/unauthorized'
        ],
//        'i18n' => [
//            'class' => api\modules\i18n\components\I18N::className(),
//            'languages' => ['ru-RU', 'de-DE', 'it-IT'],
//            'translations' => [
//                'yii' => [
//                    'class' => yii\i18n\DbMessageSource::className()
//                ]
//            ]
//        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'api/error',
        ],
        'response' => [
            //set default format is json
            'on beforeSend' => function ($event) {
              $response = $event->sender;
              $response->format = yii\web\Response::FORMAT_JSON;
              api\helpers\Response::populateMongoOject($response->data);
            }
        ],
        'urlManager' => [
            //http://www.yiiframework.com/doc-2.0/guide-rest.html
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
            'rules' => require (__DIR__ . '/urlRules.php')
        ],
        'Paypal' => $params['Paypal']
    ],
    'params' => $params,
];
