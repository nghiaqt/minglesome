<?php

return [

    //custom routes,
    //purchase
    'GET purchase' => 'purchase/index',
    'POST purchase' => 'purchase/payment',
    'POST purchase/app' => 'purchase/app',
    //profile
    'GET profile/turn' => 'profile/turn',
    'PUT profile/<id:\w+>' => 'profile/update',
    'GET profile/<id:\w+>' => 'profile/view',
    //visitor
    'GET visitor/activity' => 'profile/visitor',
    'GET visitor/<id:\w+>' => 'profile/get-visitor',
    //chat
    'GET chat/conversations' => 'chat/conversations',
    'GET chat/total-message-today' => 'chat/total-message-today',
    'GET chat/<id:\w+>/message' => 'chat/message',
    'GET chat/<id:\w+>/get' => 'chat/get-conversation',
    'GET chat/<id:\w+>/read' => 'chat/read',
    'POST chat/private' => 'chat/private',
    'DELETE chat/<id:\w+>' => 'chat/delete',
    //auth
    'POST auth/login' => 'auth/login',
    'POST auth/logout' => 'auth/logout',
    //gallery
    'POST galleries' => 'gallery/create',
    'PUT galleries' => 'gallery/update',
    'GET galleries/<id:\w+>' => 'gallery/index',
    'DELETE galleries/<id:\w+>' => 'gallery/delete',
    //date event
    'GET dates/activity' => 'date-event/activity',
    'GET dates/<id:\w+>' => 'date-event/index',
    'DELETE dates/<id:\w+>' => 'date-event/delete',
    'GET dates/<id:\w+>/date-replies' => 'date-event/get-date-replies',
    'GET dates/<id:\w+>/date-reply' => 'date-event/get-reply',
    'GET dates/<id:\w+>/read-reply' => 'date-event/read-reply',
    'POST dates' => 'date-event/create',
    'POST dates/<id:\w+>/user/<createdBy:\w+>/reply' => 'date-event/create-reply',
    'DELETE dates/<id:\w+>/date-reply' => 'date-event/delete-reply',
    //creditcard
    'GET creditcard' => 'credit-card/index',
    'GET pages/home' => 'page/home',
    'GET pages/<type:\w+>/get' => 'page/get',
    //User
    'POST users/key' => 'user/key',
    'POST users/report' => 'user/report-user',
    'POST users/<blocker:\w+>/block/<blockedUser:\w+>' => 'user/block-user',
    'POST users/<blocker:\w+>/unblock/<blockedUser:\w+>' => 'user/unblock-user',
    'POST users/<blocker:\w+>/isblocked/<blockedUser:\w+>' => 'user/is-blocked',
    'POST users/<createdBy:\w+>/interest/<interestedUser:\w+>' => 'user/interest',
    'POST users/<createdBy:\w+>/uninterest/<interestedUser:\w+>' => 'user/uninterest',
    'POST users/<createdBy:\w+>/isinterested/<interestedUser:\w+>' => 'user/is-interested',
    'PUT users/update-prompt' => 'user/update-prompt',
    'GET users/info' => 'user/get-active-admin-info',
    'GET users/<id:\w+>/activity' => 'user/activity',
    'GET users/<id:\w+>/read-activity' => 'user/read-activity',
    'GET users/<id:\w+>/check-permission-view' => 'user/check-permission-view',
    //magazine
    'GET magazines' => 'magazine/index',
    'GET magazines/<id:\w+>/comments' => 'magazine/get-comments',
    'GET magazines/<id:\w+>/comment' => 'magazine/view-comment',
    'GET magazines/<id:\w+>' => 'magazine/view',
    'POST magazines/<id:\w+>/comment' => 'magazine/create-comment',
    'GET magazines/months-has-magazine/<year:\w+>' => 'magazine/months-has-magazine',
    //polls
    'GET polls'=>'poll/index',
    'GET polls/<id:\w+>'=>'poll/view',
    'POST polls/<id:\w+>'=>'poll/answer',
    //contact
    'GET contacts/get-admin'=>'contact/get-admin',
    'GET contacts/get-reason'=>'contact/get-reason',
    'POST contacts'=>'contact/create'
];