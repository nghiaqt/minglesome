var User = require('../models/users');

module.exports = function (app, io, Client) {
  io.on('connection', function (socket) {

    socket.on('startStreaming', function () {
      var client = Client.get(socket.id);
      if (client) {
//        console.log('start streaming');
        client.streaming = new Date();
      }
    });

    socket.on('endStreaming', function () {
      var client = Client.get(socket.id);
      if (client && client.streaming) {
        var time = new Date() - client.streaming;
        User.update({_id: client._id}, {$inc: {streaming: +time}}, {}, function (err) {
          if (err) {
            console.log('update messages: ' + client._id + ' - ' + err);
          }
        });
        client.streaming = '';
      }
    });

    socket.on('disconnect', function () {
      var client = Client.get(socket.id);
      if (client && client.streaming) {
        var time = new Date() - client.streaming;
        User.update({_id: client._id}, {$inc: {streaming: +time}}, {}, function (err) {
          if (err) {
            console.log('update messages: ' + client._id + ' - ' + err);
          }
        });
      }
    });
  });
};