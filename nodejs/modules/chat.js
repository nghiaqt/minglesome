var ChatConversation = require('../models/chat');
var Conversation = require('../models/conversation');
var Visitor = require('../models/visitor');
var User = require('../models/users');

module.exports = function (app, io, Client) {
  io.on('connection', function (socket) {

    /** 
     * client disconnect server
     * remove client data
     * remove connecting video & audio of this user with diffrent clients
     */
    socket.on('disconnect', function () {
      var user = Client.get(socket.id);
      if (user) {
        //send notify remove camera to directive cam tab
        for (var i in user.streamingUsers) {
          /** send action remove video to callee */
          socket.broadcast.to(i).emit('videoEndCall', user._id);

          if (user.streamingUsers[i].audio === true) {
            /** send action remove audio to callee */
            socket.broadcast.to(i).emit('audioEndCall', user._id);
          }
        }
        //send notify turn off public camera
        socket.broadcast.emit('publicStreamingUser', user._id, false);
        Client.delete(socket.id);
      }
    });

    /** 
     * client logout server
     * remove client data
     * remove connecting video & audio of this user with diffrent clients
     */
    socket.on('logout', function () {
      var user = Client.get(socket.id);
      if (user) {
        //send notify remove camera to directive cam tab
        for (var i in user.streamingUsers) {
          /** send action remove video to callee */
          socket.broadcast.to(i).emit('videoEndCall', user._id);

          if (user.streamingUsers[i].audio === true) {
            /** send action remove audio to callee */
            socket.broadcast.to(i).emit('audioEndCall', user._id);
          }
        }
        if (user.isPublicStreaming) {
          //send notify turn off public camera
          socket.broadcast.emit('publicStreamingUser', user._id, false);
        }
        Client.delete(socket.id);
      }
    });

    /**
     * Client set public video
     * Share video all user has shared video to this user
     * @param {boolean} isPublic
     */
    socket.on('setPublicVideo', function (isPublic) {
      isPublic = isPublic === true || isPublic === 'true' ? true : false;
      var user = Client.get(socket.id);
      if (user) {
        user.isPublicStreaming = isPublic;
        socket.broadcast.emit('publicStreamingUser', user._id, isPublic);

        if (isPublic === false) {
          //send notify remove camera
          for (var i in user.streamingUsers) {
            /** send action remove video to callee */
            socket.broadcast.to(i).emit('videoEndCall', user._id);

//            if (user.streamingUsers[i].audio === true) {
//              /** send action remove audio to callee */
//              socket.broadcast.to(i).emit('audioEndCall', user._id);
//            }
          }
          user.streamingUsers = [];
        }
      }
    });

    /** check video shared */
    socket.on('sharePrivate', function (userId) {
      var caller = Client.get(socket.id);
      if (caller) {
        var mandatory = {video: false, audio: false};
        if (caller && caller.streamingUsers[userId]) {
          mandatory = caller.streamingUsers[userId];
        }
        socket.emit('onSharePrivate', mandatory);
      }
    });

    /**
     * get list ids user public video
     */
    socket.on('getPublicStreamingUsers', function () {
      var listUser = [];
      for (var i in Client.clients) {
        if (i !== socket.id && Client.clients[i].isPublicStreaming) {
          listUser.push(Client.clients[i]._id);
        }
      }
      socket.emit('receivePublicStreamingUsers', listUser);
    });

    /**
     * share private video to an user
     * when both user shared or 1 share + 1 public => create connect of 2 user
     * @param {string} userId
     * @param {boolean} isShare (on/off share video)
     */
    socket.on('sharePrivateVideo', function (userId, isShare) {
      isShare = isShare === true || isShare === 'true' ? true : false;
      var caller = Client.get(socket.id);
      if (caller) {
        if (isShare) {
          if (caller.streamingUsers[userId]) {
            caller.streamingUsers[userId].video = true;
          } else {
            /** on share private video */
            Client.addStreaming(socket.id, userId, {video: true, audio: false});
          }
        } else {
          /** remove streaming of caller */
          if (caller.streamingUsers[userId]) {
            caller.streamingUsers[userId].video = false;
          }
        }

        var callee = Client.getClientByUserId(userId);
        /** check connect of 2 user */
        if (callee) {
          if (isShare) {
            if (callee.isPublicStreaming) {
              if (callee.streamingUsers[caller._id]) {
                callee.streamingUsers[caller._id].video = true;
              } else {
                callee.streamingUsers[caller._id] = {video: true, audio: false};
              }
              socket.emit('videoConnections', callee._id);
              socket.broadcast.to(callee._id).emit('videoConnections', caller._id);
            } else if (callee.streamingUsers[caller._id] && callee.streamingUsers[caller._id].video === true) {
              socket.emit('videoConnections', callee._id);
              socket.broadcast.to(callee._id).emit('videoConnections', caller._id);
            }
          } else {
            /** send action remove video to callee */
            if (callee.streamingUsers[caller._id]) {
              callee.streamingUsers[caller._id].video = false;
            }
            socket.broadcast.to(callee._id).emit('videoEndCall', caller._id);
            /** send action remove audio to callee */
//            socket.broadcast.to(callee._id).emit('audioEndCall', caller._id);
          }
        }
      }
    });

    socket.on('removeReceiverStream', function (userId) {
      var client = Client.get(socket.id);
      if (client) {
        socket.broadcast.to(userId).emit('onRemoveReceiverStream', client._id);
      }
    });

    /**
     * share private audio to user
     */
    socket.on('audioCall', function (userId) {
      var caller = Client.get(socket.id);
      if (caller) {
        var calleeClentId = Client.getClientIdByUserId(userId);
        var callee = Client.get(calleeClentId);
        if (callee) {
          if (caller.streamingUsers[callee._id]) {
            caller.streamingUsers[callee._id].audio = true;
          } else {
            /** on share private audio */
            Client.addStreaming(socket.id, userId, {video: false, audio: true});
          }
          socket.broadcast.to(callee._id).emit('receiveAudioCall', caller._id);
        }
      }
    });

    socket.on('acceptAudio', function (userId) {
      var callee = Client.get(socket.id);
      if (callee) {
        var calleeClentId = Client.getClientIdByUserId(userId);
        var caller = Client.get(calleeClentId);
        if (caller) {
          if (callee.streamingUsers[userId] && callee.streamingUsers[userId].audio === true) {
            socket.emit('failAcceptAudio', caller._id);
          } else {
            if (callee.streamingUsers[userId]) {
              callee.streamingUsers[userId].audio = true;
            } else {
              Client.addStreaming(socket.id, userId, {video: false, audio: true});
            }
            socket.emit('receiveCall', caller._id, {audio: true, video: false});
          }
        }
      }
    });

    socket.on('declineAudio', function (userId) {
      var caller = Client.get(socket.id);
      if (caller) {
        var calleeClentId = Client.getClientIdByUserId(userId);
        var callee = Client.get(calleeClentId);
        if (callee) {
          if (caller.streamingUsers[callee._id]) {
            caller.streamingUsers[callee._id].audio = false;
          }
          if (callee.streamingUsers[caller._id]) {
            callee.streamingUsers[caller._id].audio = false;
          }
          /** send action remove audio to callee */
          socket.broadcast.to(callee._id).emit('audioEndCall', caller._id);
        }
      }
    });

    /** callee call to caller */
    socket.on('call', function (callerId) {
      var client = Client.get(socket.id);
      if (client) {
//        console.log('caller ' + client._id + ' callee ' + callerId);
        socket.broadcast.to(callerId).emit('receiveCall', client._id, {video: true, audio: false});
      }
    });

    /** 
     * caller send offer to callee 
     */
    socket.on('sdpMessage', function (id, description, mandatory, callType) {
      var client = Client.get(socket.id);
      if (client) {
        if (typeof description === 'string') {
          description = JSON.parse(description);
        }
        if (typeof mandatory === 'string') {
          mandatory = JSON.parse(mandatory);
        }
        socket.broadcast.to(id).emit('onSdpMessage', client._id, description, mandatory, callType);
      }
    });

    /** user join chat room */
    socket.on('joinChat', function (id) {
      socket.join(id);
    });

    /** 
     * save conversation & chat conversation
     * send chat message to user
     * @param {string} id is conversation id
     * @param {string} msg is the chat message
     * @param {boolean} isContact to determine if this is a contact message
     */
    socket.on('sendMessage', function (id, msg, isContact) {
      var client = Client.get(socket.id);
      isContact = typeof isContact !== 'undefined' ? isContact : false;
      if (client) {
        Conversation.findOne({_id: id}, function (err, doc) {
          if (doc) {
            //set data & set unread user
            doc.lastMessage = msg;
            doc.updatedBy = client._id;
            doc.unreadUsers = [];
            doc.deletedUsers = [];
            doc.updatedAt = new Date();
            if (doc.recipients instanceof Array) {
              for (var i = 0; i < doc.recipients.length; i++) {
                if (doc.recipients[i] !== client._id) {
                  doc.unreadUsers=[doc.recipients[i]];
                }
              }
            }

            //for contact page
            //when member send message to admin
            //set status is new for this message
            if (isContact) {
              doc.isReplied = false;
            }

            //check status conversation
            if (doc.isConversation) {
              saveConversation();
            } else {
              //check update status conversation
              ChatConversation.find({conversationId: doc._id, createdBy: {'$ne': client._id}}, function (err, data) {
                if (data instanceof Array && data.length > 0) {
                  doc.isConversation = true;
                }
                saveConversation();
              });
            }
            //callback after save
            function saveConversation() {
              doc.save(function (err) {
                if (err) {
                  socket.emit('errorSendMessage', err);
                } else {
                  var message = new ChatConversation({
                    conversationId: doc._id,
                    text: msg,
                    createdBy: client._id,
                    createdAt: doc.updatedAt,
                    isContact: isContact
                  });

                  message.save(function (err) {
                    if (err) {
                      socket.emit('errorSendMessage', err);
                    } else {
                      //send message to sender
                      socket.emit('receiveMessage', message, doc.isConversation);

                      //send message to all user in group
                      for (var i = 0; i < doc.recipients.length; i++) {
                        if (doc.recipients[i] !== client._id) {
                          if (doc.isPrivate) {
                            //update visitor
                            Visitor.findOne({createdBy: client._id, viewed: doc.recipients[i]},
                            function (err, visitor) {
                              if (visitor) {
                                visitor.lastMessage = msg;
                                visitor.save(function (err) {
                                  if (!err) {
                                    socket.broadcast.to(visitor.viewed).emit('onVisitor', visitor);
                                  }
                                });
                              }
                            });
                          }
                          socket.broadcast.to(doc.recipients[i]).emit('receiveMessage', message, doc.isConversation, client);
                        }
                      }
                      //update Qty of messages sent by this member
                      User.update({_id: client._id}, {$inc: {messages: +1}}, {}, function (err) {
                        if (err) {
                          console.log('update messages: ', err);
                        }
                      });
                    }
                  });
                }
              });
            }
          }
        });
      }
    });

    /** receive notify typing a message*/
    socket.on('notifyTypingMessage', function (id, isTyping) {
      isTyping = isTyping === true || isTyping === 'true' ? true : false;
      socket.broadcast.to(id).emit('notifyTypingMessageCallback', isTyping);
    });
  });
};