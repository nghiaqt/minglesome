var DateEvent = require('../models/dateEvent');
var DateEventReply = require('../models/dateEventReply');
module.exports = function(app, io, Client) {
  io.on('connection', function(socket) {
    /**
     * add new reply to date event
     * notify to activity of author event
     */
    socket.on('replyEvent', function(dateEventId, text) {
      var client = Client.get(socket.id);
      if (client) {
        DateEvent.findOne({_id: dateEventId}, function(err, doc) {
          if (doc) {
            var reply = new DateEventReply({
              dateEventId: doc._id,
              authorEvent: doc.createdBy,
              onDate: doc.onDate,
              createdBy: client._id,
              text: text
            });
            reply.save(function(err) {
              if (!err) {
                socket.broadcast.to(doc.createdBy).emit('onReplyEvent', reply, client.username);
              }
            });
          }
        });
      }
    });
  });
};