var Visitor = require('../models/visitor');
/* 
 * manage all socket for activity tabs
 */
module.exports = function (app, io) {
  io.on('connection', function (socket) {
    /**
     * send to client what person user interest/doesn't interest
     * @param {int} createdBy is user interest
     * @param {int} interestedUser is interested user
     * @param {boolean} isInterest determine user interest/doesn't interest
     */
    socket.on('setInterest', function (interestedUser, interesting, isInterest) {
      isInterest = isInterest === true || isInterest === 'true' ? true : false;
//      console.log(interestedUser + ' ' + interesting + ' ' + isInterest)
      socket.broadcast.to(interestedUser).emit('filterMutualInterest', interesting, isInterest);
    });

    socket.on('visitor', function (createdBy, viewed) {
      if (createdBy != viewed) {
        //add new visitor
        Visitor.findOne({createdBy: createdBy, viewed: viewed}, function (err, doc) {
          if (doc) {
            doc.updatedAt = new Date();
            doc.save(function (err) {
              if (!err) {
                success(doc);
              }
            });
          } else {
            var visitor = new Visitor({
              createdBy: createdBy,
              viewed: viewed
            });
            visitor.save(function (err) {
              if (!err) {
                success(visitor);
              }
            });
          }
        });

        function success(visitor) {
          socket.broadcast.to(viewed).emit('onVisitor', visitor);
        }

        //update visitor
        Visitor.findOne({createdBy: viewed, viewed: createdBy}, function (err, doc) {
          if (doc && doc.unread) {
            doc.unread = false;
            doc.save(function (err) {
              if (!err) {
                socket.emit('onVisitor', doc);
              }
            });
          }
        });
      }
    });
  });
};