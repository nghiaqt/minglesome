var User = require('../models/users');

module.exports = function (app, io, Client) {
//connect to socket
  io.on('connection', function (socket) {

    //check user onl
    socket.on('checkUserOnline', function (userId) {
      socket.emit('isOnline', Client.getClientIdByUserId(userId) ? true : false);
    });

    /** 
     * set default data client connect to server 
     * @param {object} user User info
     */
    socket.on('connectServer', function (user) {
      if (user) {
//      console.log('connect server '+user.username+' socket id:',socket.id);
        //add data client
        var data = {
          _id: user._id,
          username: user.username,
          email: user.email,
          level: user.level,
          isPublicStreaming: false,
          streamingUsers: []
        };
        Client.add(socket.id, data);
        socket.join(user._id);

        //update status user is logged
        User.update({_id: user._id}, {isLogged: true}, {}, function (err) {
          if (err) {
            console.log('update user login: ' + user._id + ' - ' + err);
          } else {
            socket.broadcast.emit('userLogged', user._id, true);
          }
        });
      }
    });

    /** 
     * client disconnect server
     * update status user is logout
     */
    socket.on('disconnect', function () {
      var client = Client.get(socket.id);
      if (client) {
        //update Qty of messages sent by this member
        User.update({_id: client._id}, {isLogged: false}, {}, function (err) {
          if (err) {
            console.log('update user login: ' + client._id + ' - ' + err);
          } else {
            //send notify log off
            socket.broadcast.emit('userLogged', client._id, false);
          }
        });
      }
    });

    /** 
     * client logout server
     * update status user is logout
     */
    socket.on('logout', function () {
      var client = Client.get(socket.id);
      if (client) {
        //update Qty of messages sent by this member
        User.update({_id: client._id}, {isLogged: false}, {}, function (err) {
          if (err) {
            console.log('update user login: ' + client._id + ' - ' + err);
          } else {
            //send notify log off
            socket.broadcast.emit('userLogged', client._id, false);
          }
        });
      }
    });

    /** 
     * client logout server
     * update status user is logout
     */
    socket.on('updateAvatar', function (avatarUrl) {
      var client = Client.get(socket.id);
      if (client) {
        //send notify log off
        socket.broadcast.emit('onUpdateAvatar', client._id, avatarUrl);
      }
    });
  });
};