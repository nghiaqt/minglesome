var mongoose = require('mongoose');
var User = require('./users');
var schema = new mongoose.Schema({
  unreadUsers: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
  deletedUsers: Array,
  recipients: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
  isConversation: Boolean,
  lastMessage: String,
  updatedBy: mongoose.Schema.Types.ObjectId,
  updatedAt: {type: Date, default: Date.now},
  isReplied: Boolean,
  isPrivate: Boolean
}, {collection: 'conversation'});

schema.methods.toJSON = function () {
  return {
    _id: this._id,
    unreadUsers: this.unreadUsers,
    deletedUsers: this.deletedUsers,
    recipients: this.recipients,
    isConversation: this.isConversation,
    lastMessage: this.lastMessage,
    updatedBy: this.updatedBy,
    updatedAt: this.updatedAt.getTime(),
    isReplied: this.isReplied,
    isPrivate: this.isPrivate
  };
};

module.exports = mongoose.model('Conversation', schema);