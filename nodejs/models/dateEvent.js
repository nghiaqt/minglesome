var mongoose = require('mongoose');
var schema = new mongoose.Schema({
  onDate: {type: Date, default: Date.now},
  createdBy: mongoose.Schema.Types.ObjectId,
  updatedAt: {type: Date, default: Date.now}
}, {collection: 'dateEvents'});

schema.methods.toJSON = function () {
  return {
    _id: this._id,
    onDate: this.onDate.getTime(),
    createdBy: this.createdBy,
    updatedAt: this.updatedAt.getTime()
  };
};

module.exports = mongoose.model('DateEvents', schema);