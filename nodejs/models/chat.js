var mongoose = require('mongoose');
var User = require('./users');
var schema = new mongoose.Schema({
  conversationId: mongoose.Schema.Types.ObjectId,
  deletedUsers: Array,
  text: String,
  createdBy: mongoose.Schema.Types.ObjectId,
  createdAt: {type: Date, default: Date.now},
	isContact: Boolean
}, {collection: 'chatConversation'});

schema.methods.toJSON = function () {
  return {
    _id: this._id,
    conversationId: this.conversationId,
    deletedUsers: this.deletedUsers,
    text: this.text,
    createdBy: this.createdBy,
    createdAt: this.createdAt.getTime(),
    isContact: this.isContact
  };
};

module.exports = mongoose.model('ChatConversation', schema);
