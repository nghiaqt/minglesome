var mongoose = require('mongoose');
var schema = new mongoose.Schema({
  dateEventId: mongoose.Schema.Types.ObjectId,
  authorEvent: mongoose.Schema.Types.ObjectId,
  createdBy: mongoose.Schema.Types.ObjectId,
  text: String,
  createdAt: {type: Date, default: Date.now},
  onDate: {type: Date, default: Date.now},
  unread: {type: Boolean, default: true}
}, {collection: 'dateEventReplies'});

schema.methods.toJSON = function () {
  return {
    _id: this._id,
    dateEventId: this.dateEventId,
    authorEvent: this.authorEvent,
    createdBy: this.createdBy,
    text: this.text,
    createdAt: this.createdAt.getTime(),
    onDate: this.onDate.getTime(),
    unread: this.unread
  };
};

module.exports = mongoose.model('DateEventReplies', schema);