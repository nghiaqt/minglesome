var mongoose = require('mongoose');
var schema = new mongoose.Schema({
  viewed: mongoose.Schema.Types.ObjectId,
  createdBy: mongoose.Schema.Types.ObjectId,
  lastMessage: String,
  updatedAt: {type: Date, default: Date.now},
  unread: {type: Boolean, default: true}
}, {collection: 'visitor'});

schema.methods.toJSON = function () {
  return {
    _id: this._id,
    viewed: this.viewed,
    createdBy: this.createdBy,
    lastMessage: this.lastMessage,
    updatedAt: this.updatedAt.getTime(),
    unread: this.unread
  };
};
module.exports = mongoose.model('Visitor', schema);