var mongoose = require('mongoose');
var userSchema = new mongoose.Schema({
  isLogged: {type: Boolean, default: false},
  lastSignedOn: {type: Date, default: Date.now},
  messages: Number,
  streaming:Number
}, {collection: 'users'});

userSchema.methods.toJSON = function () {
  return {
    _id: this._id,
    isLogged: this.isLogged,
    lastSignedOn: this.lastSignedOn.getTime(),
    messages: this.messages,
    streaming: this.streaming
  };
};

module.exports = mongoose.model('User', userSchema);
