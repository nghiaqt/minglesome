var express = require('express');
var app = express();
var mongoose = require('mongoose');
mongoose.connect('localhost:27017/bunch-of-lovely-people');
var server = app.listen(4000);
var io = require('socket.io').listen(server);

var Client = {
  clients: {},
  add: function(index, value) {
    this.clients[index] = value;
  },
  delete: function(index) {
    delete this.clients[index];
  },
  get: function(index) {
    return this.clients[index];
  },
  getClientByUserId: function(userId) {
    for (var i in this.clients) {
      if (this.clients[i]['_id'] == userId) {
        return this.clients[i];
      }
    }
    return false;
  },
  getClientIdByUserId: function(userId) {
    for (var i in this.clients) {
      if (this.clients[i]['_id'] == userId) {
        return i;
      }
    }
    return false;
  },
  addStreaming: function(index, userId, value) {
    if (this.clients[index]) {
      if (!this.clients[index].streamingUsers) {
        this.clients[index].streamingUsers=[];
      }
      this.clients[index].streamingUsers[userId] = value;
    }
  },
  deleteStreaming: function(index, userId) {
    if (this.clients[index] && this.clients[index].streamingUsers) {
      delete this.clients[index].streamingUsers[userId];
    }
  },
  getStreaming: function(index, userId) {
    return this.clients[index].streamingUsers[userId];
  }
};

require('./modules/client')(app, io, Client);
require('./modules/chat')(app, io, Client);
require('./modules/activity')(app, io);
require('./modules/dateEvent')(app, io, Client);
require('./modules/streaming')(app, io, Client);