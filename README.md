Dating application
===================================

Project is developed base on Yii 2 framework, MongoDB

The project includes three tiers: front end, back end, and console, each of which
is a separate Yii application.

DIRECTORY STRUCTURE
-------------------

```
common
    	config/				contains shared configurations
	mail/				contains view files for e-mails
	models/				contains model classes used in both backend and frontend
	tests/				contains various tests for objects that are common among applications
console
	config/				contains console configurations
	controllers/		contains console controllers (commands)
	migrations/			contains database migrations
	models/				contains console-specific model classes
	runtime/			contains files generated during runtime
	tests/				contains various tests for the console application
backend
	assets/				contains application assets such as JavaScript and CSS
	config/				contains backend configurations
	controllers/		contains Web controller classes
	models/				contains backend-specific model classes
	runtime/			contains files generated during runtime
	tests/				contains various tests for the backend application
	views/				contains view files for the Web application
	web/				contains the entry script and Web resources
frontend
	assets/				contains application assets such as JavaScript and CSS
	config/				contains frontend configurations
	controllers/		contains Web controller classes
	models/				contains frontend-specific model classes
	runtime/			contains files generated during runtime
	tests/				contains various tests for the frontend application
	views/				contains view files for the Web application
	web/				contains the entry script and Web resources
api
	config/				contains api configurations
	controllers/		contains Web controller classes
	models/				contains api-specific model classes
	runtime/			contains files generated during runtime
	tests/				contains various tests for the api application
	web/				contains the entry script and Web resources
vendor/					contains dependent 3rd-party packages
environments/			contains environment-based overrides
```


REQUIREMENTS
------------

The minimum requirement by this application template that your Web server supports PHP 5.4.0.


INSTALLATION
------------

### Install 3rd package via Composer

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

You can then install the application using the following command:

~~~
php composer.phar install
~~~


GETTING STARTED
---------------

After you install the application, you have to conduct the following steps to initialize
the installed application. You only need to do these once for all.

1. install bower components
	* cd frontend
	* npm bower install (or "npm bower update" to update)
2. Set document roots of your Web server:
	- for frontend `/path/to/pms/web/` and using the URL `http://pms/`
	- for backend `/path/to/backend/web/` and using the URL `http://backend/`
3. Set up migration
    - run command: yii migration/up
4. Install (combine) css
    - we are using [less](http://lesscss.org/)
    - to combine css go to frontend/css then run this command: find ./ -name '*.less' -exec lessc -x {} \; > combined.css

To login into the application, you need to first sign up, with any of your email address, username and password.
Then, you can login into the application with same email address and password at any time.

Coding standard
-----
To ensure consistency throughout the source code, keep these rules in mind as you are working:
All features or bug fixes must be tested by one or more specs.
All public API methods must be documented with phpdpcs/jsdoc
Variable/Properties (of mongoDB, class) must follow camelCase rule

With the exceptions listed below, we follow the rules contained in Idiomatic JavaScript:
Wrap all code at 100 characters.
We're not using comma first convention.
We use whitespace rule in section 2.D instead of 2.A, which means we don't put extra spaces inside of parentheses.
Instead of complex inheritance hierarchies, we prefer simple objects. We use prototypical inheritance only when absolutely necessary.

We love functions and closures and, whenever possible, prefer them over objects.
To write concise code that can be better minified, internally we use aliases that map to the external API.
We don't go crazy with type annotations for private internal APIs unless it's an internal API that is used throughout the project. The best guidance is to do what makes the most sense.

TESTING
-------
We use [codeception](http://codeception.com/) for testing

//.....