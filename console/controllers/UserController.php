<?php

namespace console\controllers;

use yii\console\Controller;
use common\models\User;
use common\helpers\Email;

/**
 * User Controller
 * 1- send birthday email
 * 2- update age of user
 * 3- update permission
 * 4- delete cancelled membership
 * 
 * @author Hoai Tran <tranthanhhoai1990@gmail.com>
 */
class UserController extends Controller {

  /**
   * send birthday email to user
   * to run each everyday at 24h
   */
  public function actionBirthdayEmail() {
    $users = User::getCollection()->aggregate([
      [
        '$project' => [
          'month' => [
            '$month' => '$birthdate'
          ],
          'day' => [
            '$dayOfMonth' => '$birthdate'
          ],
          'level' => '$level',
          '_id' => '$_id',
          'username' => '$username',
          'email' => '$email',
          'country' => '$country',
          'age' => '$age'
        ]
      ],
      [
        '$match' =>
        [
          'day' => intval(date('d')),
          'month' => intval(date('m')),
          'level' => [
            '$lt' => User::LEVEL_ADMIN
          ]
        ]
      ]
    ]);
    if ($users) {
      foreach ($users as $user) {
        Email::sendBirthdayEmail($user);
      }
    }
  }

  /**
   * update age all users
   * to run each year
   */
  public function actionUpdateAge() {
    $users = User::find()->all();
    foreach ($users as $user) {
      //before save will check user age
      $user->save();
    }
  }

  /**
   * Check update permission of user 
   * premium to free by expired premium
   * trial to free by date register
   * to run each everyday at 24h
   */
  public function actionUpdatePermission() {
    $users = User::findAll(['$or' => [
            [
              'level' => User::LEVEL_PREMIUM,
              'expiredPremium' => ['$lt' => new \MongoDate()]
            ],
            [
              'level' => User::LEVEL_TRIAL,
              'createdAt' => ['$lt' => new \MongoDate(time('-3 days'))]
            ]
          ]
    ]);
    foreach ($users as $user) {
      $level = $user->level;
      $user->level = User::LEVEL_FREE;
      if ($user->save()) {
        if ($level == User::LEVEL_TRIAL) {
          Email::sendNotifyEndTrial($user);
        }
      }
    }
  }

  /**
   * Check delete canceled user by expired delay cancel
   * to run each everyday at 24h
   */
  public function actionDeleteCanceled() {
    $users = User::findAll([
          'status' => User::STATUS_DELETED,
          'expiredDelayCancel' => ['$lt' => new \MongoDate()]
    ]);
    foreach ($users as $user) {
      $user->delete();
    }
  }

}
