<?php

namespace console\controllers;

use yii\console\Controller;
use common\models\User;
use common\models\ProfileContent;
use common\models\ActivityEvent;
use common\models\DateEvent;
use common\helpers\GeoLocation;
use common\models\Country;
use common\models\Package;
use common\models\Reason;
use common\models\Page;

class MigrationController extends Controller {

  public function actionUp() {
//    $this->setupOrientation();
//    $this->setupUpfor();
//    $this->setupOther();
//    $this->setupReligion();
//    $this->setupFigure();
//    $this->setupSpeaking();
//    $this->setupStyle();
//    $this->setupQuestion();
//    $this->setupEthnicity();
    $this->setUpCountry();
//    $this->setupActivityEvent();
//    $this->setupUser();
//    $this->setupDateEvent();
//    $this->setupPackage();
//    $this->setupReason();
    $this->setupPage();
  }

  /**
   * migrate data for user
   */
  public function actionDataForUser() {
    $this->setupOrientation();
    $this->setupUpfor();
    $this->setupOther();
    $this->setupReligion();
    $this->setupFigure();
    $this->setupSpeaking();
    $this->setupStyle();
    $this->setupQuestion();
    $this->setupEthnicity();
//    $this->setUpCountry();
  }

  /**
   * migrate user
   */
  public function actionRandomUser() {
    return $this->setupUser();
  }

  /**
   * migrate date event
   */
  public function actionDateEvent() {
    $this->setupActivityEvent();
    $this->setupDateEvent();
  }

  public function actionPackage() {
    $this->setupPackage();
  }

  public function actionReason() {
    $this->setupReason();
  }

  public function actionPage() {
    $this->setupPage();
  }

  public function setupPage() {
    $data = [
        ['type' => 'terms', 'title' => 'TERMS', 'content' => 'text'],
        ['type' => 'privacy', 'title' => 'PRIVACY POLICY', 'content' => 'text'],
        ['type' => 'security', 'title' => 'SECURITY TIPS', 'content' => 'text'],
    ];
    foreach ($data as $value) {
      $page = new Page();
      $page->type = $value['type'];
      $page->title = ['en' => $value['title']];
      $page->content = ['en' => $value['content']];
      $page->status = true;
      $page->save();
    }
  }

  public function setupDateEvent() {
    $users = User::find()->asArray()->all();
    $activities = ActivityEvent::find()->asArray()->all();
    $activityIndex = array_rand($activities);
    $isGroup = false;

    for ($i = 0; $i < 20; $i++) {
      $userIndex = array_rand($users);
      if ($isGroup)
        $isGroup = false;
      else
        $isGroup = true;

      $dateEvent = new DateEvent();
      $dateEvent->onDate = new \MongoDate(time());
      $dateEvent->name = 'Event' . $i;
      $dateEvent->description = 'Description' . $i;
      $dateEvent->country = 'VN';
      $dateEvent->city = 'HCM';
      $dateEvent->createdBy = $users[$userIndex]['_id'];
      $dateEvent->activityEventId = $activities[$activityIndex]['_id'];
      $dateEvent->isGroup = $isGroup;
      $dateEvent->save();
    }
  }

  public function setupEthnicity() {
    $ethnicityDatas = ['caucasian', 'asian', 'black', 'arab', 'hispanic', 'indian', 'mixed'];
    $ethnicities = [];
    foreach ($ethnicityDatas as $ethnicityData) {
      $ethnicity = new ProfileContent();
      $ethnicity->type='ethnicity';
      $ethnicity->name = ['en' => $ethnicityData];
      $ethnicity->createdBy = '';
      $ethnicity->save();
      $ethnicities[] = $ethnicity;
    }
    return $ethnicities;
  }

  public function setupOrientation() {
    $orientationDatas = ['hetero', 'bi', 'home', 'mixed'];
    $orientations = [];
    foreach ($orientationDatas as $orientationData) {
      $orientation = new ProfileContent();
      $orientation->name = ['en' => $orientationData];
      $orientation->type='orientation';
      $orientation->createdBy = '';
      $orientation->save();
      $orientations[] = $orientation;
    }
    return $orientations;
  }

  public function setupUpfor() {
    $upforDatas = ['friendship', 'socializing', 'flirting', 'romence', 'we\'ll see'];
    $upfors = [];
    foreach ($upforDatas as $upforData) {
      $upfor = new ProfileContent();
      $upfor->type='upfor';
      $upfor->name = ['en' => $upforData];
      $upfor->createdBy = '';
      $upfor->save();
      $upfors[] = $upfor;
    }
    return $upfors;
  }

  public function setupOther() {
    $otherDatas = ['tobacco', 'alcohol', 'ofline', 'non-prenium', 'no picture', 'filter mismatch', 'not interested',
        'not interesting'];
    $others = [];
    foreach ($otherDatas as $otherData) {
      $other = new ProfileContent();
      $other->type='other';
      $other->name = ['en' => $otherData];
      $other->createdBy = '';
      $other->save();
      $others[] = $other;
    }
    return $others;
  }

  public function setupReligion() {
    $religionDatas = ['not religion', 'christian', 'muslim', 'hindu', 'jewish', 'other'];
    $religions = [];
    foreach ($religionDatas as $religionData) {
      $religion = new ProfileContent();
      $religion->type='religion';
      $religion->name = ['en' => $religionData];
      $religion->createdBy = '';
      $religion->save();
      $religions[] = $religion;
    }
    return $religions;
  }

  public function setupFigure() {
    $figureDatas = ['slender', 'normal', 'fit', 'muscular', 'curvy', 'chubby', 'lot to love'];
    $figures = [];
    foreach ($figureDatas as $figureData) {
      $figure = new ProfileContent();
      $figure->type='figure';
      $figure->name = ['en' => $figureData];
      $figure->createdBy = '';
      $figure->save();
      $figures[] = $figure;
    }
    return $figures;
  }

  public function setupSpeaking() {
    $speakingDatas = ['english', 'spanish', 'german', 'portugues', 'french', 'italian', 'swedish', 'danish', 'norwegian',
        'dutch', 'polish', 'hungarian', 'mandarin', 'turkish', 'arabic', 'bugarian', 'romanian', 'ukranian'];
    $speakings = [];
    foreach ($speakingDatas as $speakingData) {
      $speaking = new ProfileContent();
      $speaking->type='speaking';
      $speaking->name = ['en' => $speakingData];
      $speaking->createdBy = '';
      $speaking->save();
      $speakings[] = $speaking;
    }
    return $speakings;
  }

  public function setupStyle() {
    $styleDatas = ['who canes', 'formal', 'business', 'ohic', 'merdy', 'skater', 'rocker', 'alternative', 'punk', 'trendy',
        'sporty', 'outdoors', 'urban', 'leisure', 'grunge', 'preppy', 'dark', 'normal', 'high end', 'western', 'rocka billy'];
    $styles = [];
    foreach ($styleDatas as $styleData) {
      $style = new ProfileContent();
      $style->type='style';
      $style->name = ['en' => $styleData];
      $style->createdBy = '';
      $style->save();
      $styles[] = $style;
    }
    return $styles;
  }

  public function setupQuestion() {
    $questionDatas = ['question1', 'question2', 'question3'];
    $questions = [];
    foreach ($questionDatas as $questionData) {
      $question = new ProfileContent();
      $question->type='question';
      $question->name = ['en' => $questionData];
      $question->createdBy = '';
      $question->save();
      $questions[] = $question;
    }
    return $questions;
  }

  public function setupActivityEvent() {
    $activities = [];
    for ($i = 0; $i < 5; $i++) {
      $activity = new ActivityEvent();
      $activity->name = ['en' => 'activity' . $i];
      $activity->createdBy = '';
      $activity->save();
      $activities[] = $activity;
    }
    return $activities;
  }

  public function actionCountry() {
    $this->setUpCountry();
  }

  private function setUpCountry() {
    $countryDatas = [
        [
            'code' => 'us',
            'name' => 'USA',
            'currency' => 'USD',
            'language' => 'en'
        ],
        [
            'code' => 'ca',
            'name' => 'Canada',
            'currency' => 'CAD',
            'language' => 'en'
        ],
        [
            'code' => 'uk',
            'name' => 'UK',
            'currency' => 'GBP',
            'language' => 'en'
        ],
        [
            'code' => 'ie',
            'name' => 'Ireland',
            'currency' => 'EUR',
            'language' => 'en'
        ],
        [
            'code' => 'au',
            'name' => 'Australia',
            'currency' => 'AUD',
            'language' => 'en'
        ],
        [
            'code' => 'es',
            'name' => 'España',
            'currency' => 'EUR',
            'language' => 'es'
        ],
        [
            'code' => 'mx',
            'name' => 'Mexico',
            'currency' => 'MXN',
            'language' => 'es'
        ],
        [
            'code' => 'ar',
            'name' => 'Argentina',
            'currency' => 'ARS',
            'language' => 'es'
        ],
        [
            'code' => 'pt',
            'name' => 'Portugal',
            'currency' => 'EUR',
            'language' => 'pt'
        ],
        [
            'code' => 'br',
            'name' => 'Brazil',
            'currency' => 'BRL',
            'language' => 'pt'
        ],
        [
            'code' => 'de',
            'name' => 'Deutschland',
            'currency' => 'EUR',
            'language' => 'de'
        ],
        [
            'code' => 'at',
            'name' => 'Österreich',
            'currency' => 'EUR',
            'language' => 'de'
        ],
        [
            'code' => 'ch',
            'name' => 'Schweiz',
            'currency' => 'CHF',
            'language' => 'de'
        ],
        [
            'code' => 'fr',
            'name' => 'France',
            'currency' => 'EUR',
            'language' => 'fr'
        ],
        [
            'code' => 'it',
            'name' => 'Italia',
            'currency' => 'EUR',
            'language' => 'it'
        ],
        [
            'code' => 'nl',
            'name' => 'Nederland',
            'currency' => 'EUR',
            'language' => 'nl'
        ],
        [
            'code' => 'nl',
            'name' => 'België',
            'currency' => 'EUR',
            'language' => 'nl'
        ],
        [
            'code' => 'dk',
            'name' => 'Danmark',
            'currency' => 'DKK',
            'language' => 'da'
        ],
        [
            'code' => 'no',
            'name' => 'Norge',
            'currency' => 'NOK',
            'language' => 'nb'
        ],
        [
            'code' => 'se',
            'name' => 'Sverige',
            'currency' => 'SEK',
            'language' => 'sv'
        ],
    ];
    $countries = [];
    foreach ($countryDatas as $ct) {
      $country = new Country();
      $country->countryCode = $ct['code'];
      $country->countryName = ['en' => $ct['name']];
      $country->currency = $ct['currency'];
      $country->languageCode = $ct['language'];
      $country->save();
      $countries[] = $country;
    }
    return $countries;
  }

  function setupPackage() {
    $data = [
        [
            'name' => '3 months premium',
            'description' => '<p>
            Premium includes all features within our site, except 
            special products/services, which you will be able to 
            buy separately from the PRODUCT drop down above. 
            </p><h1>PRICE: USD 39 / 3 MONTHS</h1><p>
              This is a subscription, $39 will be automatically drawn from your
              card every third month. You can stop the subscription at any time. 
              Your premium membership will then be deactivated and turn into a 
              free membership at the end of the paid period.
            </p>',
            'month' => 3,
            'amount' => 39
        ],
        [
            'name' => '6 months premium',
            'description' => '<p>
            Premium includes all features within our site, except 
            special products/services, which you will be able to 
            buy separately from the PRODUCT drop down above. 
            </p><h1>PRICE: USD 70 / 6 MONTHS</h1><p>
              This is a subscription, $70 will be automatically drawn from your
              card every third month. You can stop the subscription at any time. 
              Your premium membership will then be deactivated and turn into a 
              free membership at the end of the paid period.
            </p>',
            'month' => 6,
            'amount' => 70
        ],
        [
            'name' => '9 months premium',
            'description' => '<p>
            Premium includes all features within our site, except 
            special products/services, which you will be able to 
            buy separately from the PRODUCT drop down above. 
            </p><h1>PRICE: USD 100 / 9 MONTHS</h1><p>
              This is a subscription, $100 will be automatically drawn from your
              card every third month. You can stop the subscription at any time. 
              Your premium membership will then be deactivated and turn into a 
              free membership at the end of the paid period.
            </p>',
            'month' => 9,
            'amount' => 100
        ]
    ];
    foreach ($data as $item) {
      $package = new Package();
      $package->name = ['en' => $item['name']];
      $package->description = ['en' => $item['description']];
      $package->month = $item['month'];
      $package->amount = ['USD' => $item['amount']];
      $package->save();
    }
  }

  function setupReason() {
    $data = ['found partner on this site', 'found partner else where', 'tired of online dating',
        'not enough members that suit me', 'unhappy with the service or support',
        'unsatisfied with the features', 'tired of this site', 'found a better site',
        'prices are too high', 'negative experiances with other members'];
    foreach ($data as $item) {
      $reason = new Reason();
      $reason->name = ['en' => $item];
      $reason->save();
    }
  }

  /**
   * create user
   * @return \common\models\User
   */
  private function setupUser() {

    $randomUsers = $this->getFakeUsers(20);
    $statusMarrieds = User::getStatusMarried();
    $genders = User::getGenders();
    $orientations = Orientation::find()->asArray()->all();
    $upfors = Upfor::find()->asArray()->all();
    $others = Other::find()->asArray()->all();
    $religions = Religion::find()->asArray()->all();
    $figures = Figure::find()->asArray()->all();
    $speakings = SpeakingLanguage::find()->asArray()->all();
    $styles = Style::find()->asArray()->all();
    $questions = ProfileQuestion::find()->asArray()->all();
    $countries = Country::find()->asArray()->all();
    $ethnicities = Ethnicity::find()->asArray()->all();

    $users = [];
    $curYear = date('Y', time());

    $coordinates = GeoLocation::getCoordiatesByIp();

    for ($i = 0; $i < count($randomUsers); $i++) {
//		for ($i = 0; $i < 15; $i++) {
      $genderId = array_rand($genders);
      $curYearAge = date('Y', $randomUsers[$i]['results'][0]['user']['dob']);
      $age = $curYear - $curYearAge;
//			$age = 12;

      $statusMarriedId = array_rand($statusMarrieds);

      $randomKeyOrientation = array_rand($orientations);
      $orientationId = $orientations[$randomKeyOrientation]['_id'];

      $randomKeyUpfor = array_rand($upfors, 2);
      $upforIds = [];
      foreach ($randomKeyUpfor as $upfor) {
        $upforIds[] = $upfors[$upfor]['_id'];
      }

      $randomKeyOther = array_rand($others, 2);
      $otherIds = [];
      foreach ($randomKeyOther as $other) {
        $otherIds[] = $others[$other]['_id'];
      }

      $randomKeyReligion = array_rand($religions);
      $religionId = $religions[$randomKeyReligion]['_id'];

      $randomKeyFigure = array_rand($figures);
      $figureId = $figures[$randomKeyFigure]['_id'];

      $randomKeySpeaking = array_rand($speakings, 2);
      $speakingIds = [];
      foreach ($randomKeySpeaking as $speaking) {
        $speakingIds[] = $speakings[$speaking]['_id'];
      }

      $randomKeyStyle = array_rand($styles, 2);
      $styleIds = [];
      foreach ($randomKeyStyle as $style) {
        $styleIds[] = $styles[$style]['_id'];
      }

      $randomKeyQuestion = array_rand($questions);
      $questionId = $questions[$randomKeyQuestion]['_id'];

      $randomCountryId = array_rand($countries);

      $randomEthnicityId = array_rand($ethnicities);
      $ethnicityId = $ethnicities[$randomEthnicityId]['_id'];

      $attributes = [
          'username' => 'dev' . $i,
          'password' => 'dev' . $i,
//        'email' => $randomUsers[$i]['results'][0]['user']['email'],
          'email' => 'dev' . $i . '@gmail.com',
          'emailVerified' => true,
//        'firstName' => $randomUsers[$i]['results'][0]['user']['name']['first'],
//        'lastName' => $randomUsers[$i]['results'][0]['user']['name']['last'],
          'firstName' => 'dev' . $i,
          'lastName' => 'dev' . $i,
          'coordinates' => [ 'type' => "Point", 'coordinates' => $coordinates],
          'preferThings' => 'prefer things',
          'notlikeThings' => 'not like things',
          'statusComment' => 'status comment',
          'gender' => $genders[$genderId],
          'statusMarried' => $statusMarrieds[$statusMarriedId],
          'orientation' => $orientationId,
          'upfors' => $upforIds,
          'others' => $otherIds,
          'religion' => $religionId,
          'figure' => $figureId,
          'speakings' => $speakingIds,
          'styles' => $styleIds,
          'baggage' => 'baggage',
          'height' => 1,
          'country' => $countries[$randomCountryId]['countryCode'],
          'countryName' => $countries[$randomCountryId]['countryName'],
          'age' => $age,
          'state' => 'state',
          'city' => 'city',
          'keywords' => ['optimist', 'creative'],
          'inMyOwnWords' => 'in my own words',
          'avatar' => '',
          'emailVerified' => true,
          'avatarUrl' => $randomUsers[$i]['results'][0]['user']['picture'],
//					'avatarUrl' => '',
          'birthdate' => new \MongoDate($randomUsers[$i]['results'][0]['user']['dob']),
//					'birthdate' => new \MongoDate(strtotime('-20 years')),
          'questionId' => $questionId,
          'answer' => 'answer 1',
          'ethnicity' => $ethnicityId,
          'isLogged' => FALSE
      ];
      if (($model = User::create($attributes))) {
        $users[] = $model;
      }
    }
    return $users;
  }

  /**
   * Gets fake user data from randomuser.me
   * 
   * @param  integer $number The number of users to return
   * @param  integer $unique The unique users. Default equals to number.
   * @return array           Array of users
   */
  function getFakeUsers($number = 5, $unique = 0) {

    if (!$unique)
      $unique = $number;

    $users = array();
    $requests = array();
    $request_url = 'http://api.randomuser.me';

    for ($i = 0; $i < $unique; $i++) {
      $requests[$i] = $request_url . '?seed=' . $i;
      $users[] = json_decode(file_get_contents($requests[$i]), true);
    }

    if ($number > $unique) {
      $surplus = $number - $unique;
      for ($i = 0; $i < $surplus; $i++) {
        $rand = array_rand($requests);
        $users[] = json_decode(file_get_contents($requests[$rand]), true);
      }
    }

    shuffle($users);
    return $users;
  }

}