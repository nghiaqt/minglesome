# Localhost test:
php/php.exe path/console.php Action

# Config server cronjob

crontab -e
# Birthday Email: send birthday email to user. to run each everyday at 24h
1 0 * * * php /var/www/minglesome.com/yii user/birthday-email

# Update Age all users. to run each year
1 0 1 1 * php /var/www/minglesome.com/yii user/update-age

# Update Permission: Check update permission of user . to run each everyday at 24h
1 0 * * * php /var/www/minglesome.com/yii user/update-permission

# Delete Canceled: delete canceled user by expired delay cancel. to run each everyday at 24h
1 0 * * * php /var/www/minglesome.com/yii user/delete-canceled
